FROM node:9

WORKDIR /var/www/html

COPY package.json ./

RUN cd /var/www/html && \
    npm install gulpjs/gulp.git#4.0 gulp-sass gulp-compass browserify gulp-concat gulp-uglify gulp-plumber gulp-rename gulp-uglify-es gulp-webserver gulp-sourcemaps --save-dev && \
    npm install gulp-cli --global

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim"]

RUN apt-get install ruby -y ruby-dev gcc automake libtool rubygems build-essential

RUN ["gem", "install", "sass", "-v", "3.4.23"]
RUN ["gem", "install", "compass"]

EXPOSE 8080

ENTRYPOINT npm run gulp
