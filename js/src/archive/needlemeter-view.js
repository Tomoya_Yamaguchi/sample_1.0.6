/**
 * Needle meter View class
 */
var needlemeter_view = _.extend(window.evbd_ctx||{}, {

	/**
	 * 扇型のゲージ View クラス
	 * @param model
	 * 	.kind	閾値種別 - 'plan':計画比, 'ly':前年比、other:標準閾値定義を適用。
	 * 	.ratio	割合値
	 */
	NeedleMeterView: Backbone.View.extend({
		className: 'chart-wrap',
		template: _.template(''
			+ '<div class="chart-figure--gauge" style="display: inline-block;">'
			+	'<img src="<%= clcom.config.urlRoot %>/images/needlemeter/gray.svg" class="chart-figure--gauge--base">'
			+	'<img src="<%= clcom.config.urlRoot %>/images/needlemeter/needle.svg" height="200%" class="cl_meter-animation-gaugeRotationUp">'
			+ '</div>'
		),
		initialize: function(opt){
			if(this.model == null){
				this.model = {
					ratio: 0
				};
			}
		},
		render: function(){
			this.$el.html(this.template({}));

			// 針の角度
			this.setRatio();

			return this;
		},

                /**
                 * 割合値を設定する。
                 * @param ratio 割合値
                 */
                setRatio: function(ratio){
                        this.model.ratio = ratio || this.model.ratio;

                        // 扇の色: 閾値別背景イメージに差替えるための class 属性名を引く。
                        var gaugeColorCSS;
                        switch(this.model.kind){
                        case 'plan':    // 計画比
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--warning', 'gauge--danger') + ' cl_gauge--plan';
                                break;
                        case 'ly':      // 前年比
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--warning', 'gauge--danger') + ' cl_gauge--ly';
                                break;
                        case 'step3':   // 3階調
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--warning', 'gauge--danger') + ' cl_gauge--step3';
                                break;
                        case 'step3rev':// 3階調(逆順)
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--warning', 'gauge--danger') + ' cl_gauge--step3-rev';
                                break;
                        case 'step4':   // 4階調
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--info', 'gauge--warning', 'gauge--danger') + ' cl_gauge--step4';
                                break;
                        case 'step4rev':// 4階調(逆順)
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--info', 'gauge--warning', 'gauge--danger') + ' cl_gauge--step4-rev';
                                break;
                        default:        // その他
                                gaugeColorCSS = needlemeter_util.improvedProgressStyle(this.model, 'gauge--success', 'gauge--warning', 'gauge--danger');
                                break;
                        }
                        this.$('.chart-figure--gauge').removeClass('cl_gauge--ly cl_gauge--plan gauge--success gauge--info gauge--warning gauge--danger').addClass(gaugeColorCSS);

                        // 角度
                        var angle = needlemeter_util.rotateProgressStyle( this.model.ratio );
                        this.$('.cl_meter-animation-gaugeRotationUp').css({
                                'transform': 'rotate(' + angle + 'deg)',
                                'transition': 'transform .5s',
                                '-webkit-transform': 'rotate(' + angle + 'deg)',
                                '-webkit-transition': 'transform .5s'
                        });
                }
        }),

	/**
	 * 扇メーターを初期化する。
	 * @param $c 扇メーターの要素
	 * @param model
	 * 	.kind	閾値種別 - 'plan':計画比, 'ly':前年比、other:標準閾値定義を適用。
	 * 	.ratio	割合値
	 * @return NeedleMeterView インスタンス
	 */
	initNeedleMeterView: function($c, model){
		var v = $c.data('cl_NeedleMeterView');
		if(v == null){
			var args = { model: model };
			if($c != null){
				args.el = $c;
			}
			v = new needlemeter_view.NeedleMeterView(args).render();
			v.$el.addClass('cl_NeedleMeterView').data('cl_NeedleMeterView', v);
		}else{
			v.model = model;
			v.setRatio();
		}
		return v;
	}
});