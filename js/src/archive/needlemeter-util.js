/**
 * Needle meter Utility
 */
var _needlemeter_util = function(opt){
    // 閾値の定義
    this.options = _.defaults(opt||{}, {
        // 金額ベース値 - 100 倍値ベースの場合は、100 を設定する。
        amBase: 1,
        // 0割値、Inifnity, NaN 値の代替表現
        badFiniteNum: 0,
        // 閾値定義集
        thr: {
            WARN: 95,
            GOOD: 100
        },
        // 計画比の定義
        // ・赤 90%未満, 黄 90%以上98%未満, 青 98%以上
        thr_plan: {
            WARN: 90,
            GOOD: 98
        },
        // 前年比の定義
        // ・赤 95%未満, 黄 95%以上100%未満, 青 100%以上
        thr_ly: {
            WARN: 95,
            GOOD: 100
        },
        // 3階調の定義
        // ・赤 90%未満, 黄 90%以上100%未満, 青 100%以上
        thr_step3: {
            WARN: 90,
            GOOD: 100
        },
        // 3階調(逆順)の定義
        // ・青 100%未満, 黄 100%以上110%未満, 赤 110%以上
        thr_step3rev: {
            WARN: 100,
            DANG: 110
        },
        // 4階調の定義
        // ・赤 90%未満, 黄 90%以上100%未満, 緑 100%以上110%未満, 青 110%以上
        thr_step4: {
            WARN: 90,
            INFO: 100,
            GOOD: 110
        },
        // 4階調(逆順)の定義
        // ・青 90%未満, 緑 90%以上100%未満, 黄 100%以上110%未満, 赤 110%以上
        thr_step4rev: {
            INFO: 90,
            WARN: 100,
            DANG: 110
        }
    });
};

_.extend(_needlemeter_util.prototype, {

	/**
	 * src/dust した割合 ( % ) を求める ( 四捨五入 )
	 * @param src
	 * @param dust
	 * @param keta  少数桁
	 * @returns
	 */
	percentage: function( src, dust, keta ) {

		if( !src || !dust ) return 0;

		// 四捨五入するときは「true」、切り捨てのときは「false」を指定する
		if(true){
			// 少数桁（keta+1）を四捨五入
			var _pow = Math.pow( 10 , keta ) ;

			var val = Math.round( (src/dust * 100) * _pow ) / _pow ;
			if(_.isFinite(val) === false){
				// 0割り対処
				return this.options.badFiniteNum;
			}

			//  整数の場合は少数桁 (.0) 付ける
			if ( this.isInteger( val ) ) {

				var strVal = parseFloat( val ).toFixed(keta);

				//  文字列型
				return strVal;
			}
			return val;

		}else{
			// 切り捨て
			var _pow = Math.pow( 10 , keta );

			// 正×負でパーセンテージを求めようとすると
			// 少数桁で四捨五入されてしまうため、
			// パーセンテージを求める際は、一旦正の数どおしで計算させるよう
			// 以下、対応を行っている

			// src, dust のそれぞれの正負を求める
			// -> 正だったら１、負だったらー１
			var srcFlg  = ( src  < 0 ? -1 : 1);
			var dustFlg = ( dust < 0 ? -1 : 1);

			// 求めたフラグをかけ、それぞれ、正の値にする
			var wSrc  = src  * srcFlg;
			var wDust = dust * dustFlg;

			// 正の数どおしでパーセンテージを求める
			var val = Math.floor( ( wSrc / wDust * 100 ) * _pow ) / _pow;
			if(_.isFinite(val) === false){
				// 0割り対処
				return this.options.badFiniteNum;
			}

			// 最後に、元々計算すると正になるものは正に、負になるものは負に戻す
			val = val * srcFlg * dustFlg;

			//  整数の場合は少数桁 (.0) 付ける
			if ( this.isInteger( val ) ) {

				var strVal = parseFloat( val ).toFixed(keta);

				//  文字列型
				return strVal;
			}

			return val;
		}
	},

	/**
	 * src/dust した値を求める ( 四捨五入 )
	 * @param src
	 * @param dust
	 * @param keta  少数桁
	 * @returns
	 */
	percentval: function( src, dust, keta ) {

		if( !src || !dust ) return 0;

		// 四捨五入するときは「true」、切り捨てのときは「false」を指定する
		// 少数桁（keta+1）を四捨五入
		var _pow = Math.pow( 10 , keta ) ;

		var val = Math.round( (src/dust) * _pow ) / _pow ;

		//  整数の場合は少数桁 (.0) 付ける
		if ( this.isInteger( val ) ) {

			var strVal = parseFloat( val ).toFixed(keta);

			//  文字列型
			return strVal;
		}
		return val;
	},

        /**
         * 整数か少数かを判定
         * @param aValue
         * @returns
         */
        isInteger: function( aValue ) {
                aValue = Number(aValue);
                return aValue >= 0 && Math.floor(aValue) === aValue;
        },

        /**
         * メータ オブジェクト ( 改良型 )
         * 計画比と基準進捗と比較して色を返却する
         * @param model - { kind: 種別, ratio: 割合 }
         * @param aStyleNameA - 属性名、省略時は 'text-success'
         * @param aStyleNameB - 属性名、省略時は 'text-warning'
         * @param aStyleNameC - 属性名、省略時は 'text-danger'
         * @returns 'クラス名 1', 'クラス名 2', or 'クラス名 3'
         * @auther I.Ooyama.
         */
        improvedProgressStyle: function( model, aStyleNameA, aStyleNameB, aStyleNameC, aStyleNameD ) {
            var thr;
            switch(model.kind){
            default:
                thr = this.options.thr;
                break;
            case 'plan':
                thr = this.options.thr_plan;
                break;
            case 'ly':
                thr = this.options.thr_ly;
                break;
            case 'step3':
                thr = this.options.thr_step3;
                break;
            case 'step3rev':
                thr = this.options.thr_step3rev;
                break;
            case 'step4':
                thr = this.options.thr_step4;
                break;
            case 'step4rev':
                thr = this.options.thr_step4rev;
                break;
            }
            var css = { aClass: aStyleNameA, bClass: aStyleNameB, cClass: aStyleNameC, dClass: aStyleNameD };
            return this.improvedColorStyle(model, thr, css);
        },

        /**
         * @param model - 評価値(%) または { value: 実績値, cmpval: 比較値 }
         * @param thr - 閾値定義: .WARN 警告境界値、.GOOD 優良境界値、needlemeter_util.options.thr* 参照。
         * @param css - 色定義(クラス名): .aClass 優良、.bClass 良、.cClass 可
         * @auther I.Ooyama.
         */
        improvedColorStyle: function(model, thr, css){
            var fixValue = null;
            if(_.isFinite(model.ratio)){
                fixValue = parseFloat(model.ratio);
            }else if(_.isObject(model.ratio) && model.ratio.value != null && model.ratio.cmpval != null && model.ratio.cmpval !== 0){
                fixValue = (model.ratio.value * 100) / model.ratio.cmpval;      // percent 扱いで評価
            }
            if(fixValue == null){
                return '';              // 値評価不能
            }
            if(thr == null) thr = this.options.thr;

            switch(model.kind){
            default:
            case 'plan':
            case 'ly':
            case 'step3':
                css = _.defaults(css||{}, {
                    aClass: 'text-success',
                    bClass: 'text-warning',
                    cClass: 'text-danger'
                });
                if(fixValue >= thr.GOOD){
                    return css.aClass;
                }else if(fixValue >= thr.WARN){
                    return css.bClass;
                }else{
                    return css.cClass;
                }
                break;
            case 'step3rev':
                css = _.defaults(css||{}, {
                    aClass: 'text-success',
                    bClass: 'text-warning',
                    cClass: 'text-danger'
                });
                if(fixValue >= thr.DANG){
                    return css.cClass;
                }else if(fixValue >= thr.WARN){
                    return css.bClass;
                }else{
                    return css.aClass;
                }
                break;
            case 'step4':
                css = _.defaults(css||{}, {
                    aClass: 'text-success',
                    bClass: 'text-info',
                    cClass: 'text-warning',
                    dClass: 'text-danger'
                });
                if(fixValue >= thr.GOOD){
                    return css.aClass;
                }else if(fixValue >= thr.INFO){
                    return css.bClass;
                }else if(fixValue >= thr.WARN){
                    return css.cClass;
                }else{
                    return css.dClass;
                }
                break;
            case 'step4rev':
                css = _.defaults(css||{}, {
                    aClass: 'text-success',
                    bClass: 'text-info',
                    cClass: 'text-warning',
                    dClass: 'text-danger'
                });
                if(fixValue >= thr.DANG){
                    return css.dClass;
                }else if(fixValue >= thr.WARN){
                    return css.cClass;
                }else if(fixValue >= thr.INFO){
                    return css.bClass;
                }else{
                    return css.aClass;
                }
                break;
            }
        },

	/**
	 * メータ 針
	 * 角度 (deg) を返却する
	 * @param aPerPlan    - 計画比 (%)
	 * @param _aInProgress - 基準進捗 (%)　TODO:今は算出できないので100として扱う
	 */
	rotateProgressStyle: function( aPerPlan, _aInProgress ) {
/*
		var val = Number(aPerPlan);
		var percentage = 0;
		// 例外
		if( val <= 0 ) {
			percentage =  -64;
		} else if( val == 50 ) {
			percentage =  0;
		} else if(val > 0 && val < 50) {
			percentage =  ((100-val)/100)*-64;
		} else if(val <= 100) {		//100以下>
			percentage =  (val/100)*64;
		} else {
			percentage =  64;
		}
*/
		var aInProgress = 100;	//TODO:今は算出できないので100として扱う
		var val = Number(aPerPlan);
		if( !aInProgress )  return 0;		// 基準進捗が0%のときは針は真ん中
		if( val <= 0 ) return -64;
		//  メータの許容値を超えた時
		if ( aInProgress + 20 < val ) return  64;
		if ( aInProgress - 20 > val ) return -64;

		// +-上限角度 ( 現在 +-20% で設定 )
		// +-上限角度を設定した際の、計画比から算出した比率角度
		var percentage = ( ( val - aInProgress ) / 20 ) * 64

		return percentage;
	},

	/**
	 * CSS で定義しているカラー属性クラスを付加する。
	 * @param $elem 適用先 jq
	 * @param color カラー定義属性のクラス名: text-muted text-inverse text-primary text-warning text-danger text-success text-info
	 * @param onoff true:指定カラー定義属性のクラスを設定する、false:指カラー定義属性のクラスを削除する、省略時は true(設定する)。
	 */
	cssTextColor: function($elem, colorClass, onoff){
		var cssColorAll = 'text-muted text-inverse text-primary text-warning text-danger text-success text-info';

		var $x  = ($elem instanceof $) ? $elem : $($elem);
		if(colorClass != null){
			if(onoff === true || onoff == null/*引数なしは on 扱い*/){
				$x.removeClass(cssColorAll).addClass(colorClass);
			}else{
				$x.removeClass(colorClass);
			}
		}
		return $x;
	}

});

var needlemeter_util = new _needlemeter_util();