/**
 * このファイルは自動生成されたものです。これを直接編集せずに build/src/js/clcom/*.js を編集してください。
 */

/**
 * IE で使えない関数を補完する。
 */
//IEでは、[console]が使えないので、回避するためのおまじない
if (typeof window.console != 'object'){
    window.console = {log:function(){},debug:function(){},info:function(){},warn:function(){},error:function(){},assert:function(){},dir:function(){},dirxml:function(){},trace:function(){},group:function(){},groupEnd:function(){},time:function(){},timeEnd:function(){},profile:function(){},profileEnd:function(){},count:function(){}};
} else if (typeof window.console.debug != 'object') {
    window.console.debug = function(){};
}

//IE8では、[indexOf]が使えないので、回避するためのおまじない
if (!Array.prototype.indexOf)
{
    Array.prototype.indexOf = function(elt /*, from*/)
    {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++){
            if (from in this && this[from] === elt)
                return from;
        }
        return -1;
    };
}

// IE11 String.startsWith() 関数補完
if(!_.isFunction(String.prototype.startsWith)){
    console.info('function String.startsWith not found, add this method.');
    String.prototype.startsWith = function(prefix){
        return this.lastIndexOf(prefix, 0) === 0;
    }
}
/**
 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.3.1
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,e,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);
/*
 * Purl (A JavaScript URL parser) v2.3.1
 * Developed and maintanined by Mark Perkins, mark@allmarkedup.com
 * Source repository: https://github.com/allmarkedup/jQuery-URL-Parser
 * Licensed under an MIT-style license. See https://github.com/allmarkedup/jQuery-URL-Parser/blob/master/LICENSE for details.
 */

;(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else {
        window.purl = factory();
    }
})(function() {

    var tag2attr = {
            a       : 'href',
            img     : 'src',
            form    : 'action',
            base    : 'href',
            script  : 'src',
            iframe  : 'src',
            link    : 'href',
            embed   : 'src',
            object  : 'data'
        },

        key = ['source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'fragment'], // keys available to query

        aliases = { 'anchor' : 'fragment' }, // aliases for backwards compatability

        parser = {
            strict : /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,  //less intuitive, more accurate to the specs
            loose :  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // more intuitive, fails on relative paths and deviates from specs
        },

        isint = /^[0-9]+$/;

    function parseUri( url, strictMode ) {
        var str = decodeURI( url ),
        res   = parser[ strictMode || false ? 'strict' : 'loose' ].exec( str ),
        uri = { attr : {}, param : {}, seg : {} },
        i   = 14;

        while ( i-- ) {
            uri.attr[ key[i] ] = res[i] || '';
        }

        // build query and fragment parameters
        uri.param['query'] = parseString(uri.attr['query']);
        uri.param['fragment'] = parseString(uri.attr['fragment']);

        // split path and fragement into segments
        uri.seg['path'] = uri.attr.path.replace(/^\/+|\/+$/g,'').split('/');
        uri.seg['fragment'] = uri.attr.fragment.replace(/^\/+|\/+$/g,'').split('/');

        // compile a 'base' domain attribute
        uri.attr['base'] = uri.attr.host ? (uri.attr.protocol ?  uri.attr.protocol+'://'+uri.attr.host : uri.attr.host) + (uri.attr.port ? ':'+uri.attr.port : '') : '';

        return uri;
    }

    function getAttrName( elm ) {
        var tn = elm.tagName;
        if ( typeof tn !== 'undefined' ) return tag2attr[tn.toLowerCase()];
        return tn;
    }

    function promote(parent, key) {
        if (parent[key].length === 0) return parent[key] = {};
        var t = {};
        for (var i in parent[key]) t[i] = parent[key][i];
        parent[key] = t;
        return t;
    }

    function parse(parts, parent, key, val) {
        var part = parts.shift();
        if (!part) {
            if (isArray(parent[key])) {
                parent[key].push(val);
            } else if ('object' == typeof parent[key]) {
                parent[key] = val;
            } else if ('undefined' == typeof parent[key]) {
                parent[key] = val;
            } else {
                parent[key] = [parent[key], val];
            }
        } else {
            var obj = parent[key] = parent[key] || [];
            if (']' == part) {
                if (isArray(obj)) {
                    if ('' !== val) obj.push(val);
                } else if ('object' == typeof obj) {
                    obj[keys(obj).length] = val;
                } else {
                    obj = parent[key] = [parent[key], val];
                }
            } else if (~part.indexOf(']')) {
                part = part.substr(0, part.length - 1);
                if (!isint.test(part) && isArray(obj)) obj = promote(parent, key);
                parse(parts, obj, part, val);
                // key
            } else {
                if (!isint.test(part) && isArray(obj)) obj = promote(parent, key);
                parse(parts, obj, part, val);
            }
        }
    }

    function merge(parent, key, val) {
        if (~key.indexOf(']')) {
            var parts = key.split('[');
            parse(parts, parent, 'base', val);
        } else {
            if (!isint.test(key) && isArray(parent.base)) {
                var t = {};
                for (var k in parent.base) t[k] = parent.base[k];
                parent.base = t;
            }
            if (key !== '') {
                set(parent.base, key, val);
            }
        }
        return parent;
    }

    function parseString(str) {
        return reduce(String(str).split(/&|;/), function(ret, pair) {
            try {
                pair = decodeURIComponent(pair.replace(/\+/g, ' '));
            } catch(e) {
                // ignore
            }
            var eql = pair.indexOf('='),
                brace = lastBraceInKey(pair),
                key = pair.substr(0, brace || eql),
                val = pair.substr(brace || eql, pair.length);

            val = val.substr(val.indexOf('=') + 1, val.length);

            if (key === '') {
                key = pair;
                val = '';
            }

            return merge(ret, key, val);
        }, { base: {} }).base;
    }

    function set(obj, key, val) {
        var v = obj[key];
        if (typeof v === 'undefined') {
            obj[key] = val;
        } else if (isArray(v)) {
            v.push(val);
        } else {
            obj[key] = [v, val];
        }
    }

    function lastBraceInKey(str) {
        var len = str.length,
            brace,
            c;
        for (var i = 0; i < len; ++i) {
            c = str[i];
            if (']' == c) brace = false;
            if ('[' == c) brace = true;
            if ('=' == c && !brace) return i;
        }
    }

    function reduce(obj, accumulator){
        var i = 0,
            l = obj.length >> 0,
            curr = arguments[2];
        while (i < l) {
            if (i in obj) curr = accumulator.call(undefined, curr, obj[i], i, obj);
            ++i;
        }
        return curr;
    }

    function isArray(vArg) {
        return Object.prototype.toString.call(vArg) === "[object Array]";
    }

    function keys(obj) {
        var key_array = [];
        for ( var prop in obj ) {
            if ( obj.hasOwnProperty(prop) ) key_array.push(prop);
        }
        return key_array;
    }

    function purl( url, strictMode ) {
        if ( arguments.length === 1 && url === true ) {
            strictMode = true;
            url = undefined;
        }
        strictMode = strictMode || false;
        url = url || window.location.toString();

        return {

            data : parseUri(url, strictMode),

            // get various attributes from the URI
            attr : function( attr ) {
                attr = aliases[attr] || attr;
                return typeof attr !== 'undefined' ? this.data.attr[attr] : this.data.attr;
            },

            // return query string parameters
            param : function( param ) {
                return typeof param !== 'undefined' ? this.data.param.query[param] : this.data.param.query;
            },

            // return fragment parameters
            fparam : function( param ) {
                return typeof param !== 'undefined' ? this.data.param.fragment[param] : this.data.param.fragment;
            },

            // return path segments
            segment : function( seg ) {
                if ( typeof seg === 'undefined' ) {
                    return this.data.seg.path;
                } else {
                    seg = seg < 0 ? this.data.seg.path.length + seg : seg - 1; // negative segments count from the end
                    return this.data.seg.path[seg];
                }
            },

            // return fragment segments
            fsegment : function( seg ) {
                if ( typeof seg === 'undefined' ) {
                    return this.data.seg.fragment;
                } else {
                    seg = seg < 0 ? this.data.seg.fragment.length + seg : seg - 1; // negative segments count from the end
                    return this.data.seg.fragment[seg];
                }
            }

        };

    }

    purl.jQuery = function($){
        if ($ != null) {
            $.fn.url = function( strictMode ) {
                var url = '';
                if ( this.length ) {
                    url = $(this).attr( getAttrName(this[0]) ) || '';
                }
                return purl( url, strictMode );
            };

            $.url = purl;
        }
    };

    purl.jQuery(window.jQuery);

    return purl;

});
/*
--------------------------------------------------------
dateformat.js - Simple date formatter
Version 1.1 (Update 2008/04/02)

Copyright (c) 2007-2008 onozaty (http://www.enjoyxstudy.com)

Released under an MIT-style license.

For details, see the web site:
 http://www.enjoyxstudy.com/javascript/dateformat/

--------------------------------------------------------
patterns
y : Year         ex. "yyyy" -> "2007", "yy" -> "07"
M : Month        ex. "MM" -> "05" "12", "M" -> "5" "12"
d : Day          ex. "dd" -> "09" "30", "d" -> "9" "30"
H : Hour (0-23)  ex. "HH" -> "00" "23", "H" -> "0" "23"
m : Minute       ex. "mm" -> "01" "59", "m" -> "1" "59"
s : Second       ex. "ss" -> "00" "59", "H" -> "0" "59"
S : Millisecond  ex. "SSS" -> "000" "012" "999",
                     "SS" -> "00" "12" "999", "S" -> "0" "12" "999"

Text can be quoted using single quotes (') to avoid interpretation.
"''" represents a single quote.


Useing..

 var fmt = new DateFormat("yyyy/MM/dd HH:mm:ss SSS");

 var str = fmt.format(new Date()); // "2007/05/10 12:21:19 002"
 var date = fmt.parse("2007/05/10 12:21:19 002"); // return Date object

--------------------------------------------------------
*/

var DateFormat = function(pattern) {
  this._init(pattern);
};

DateFormat.prototype = {
  _init: function(pattern) {

    this.pattern = pattern;
    this._patterns = [];

    for (var i = 0; i < pattern.length; i++) {
      var ch = pattern.charAt(i);
      if (this._patterns.length == 0) {
        this._patterns[0] = ch;
      } else {
        var index = this._patterns.length - 1;
        if (this._patterns[index].charAt(0) == "'") {
          if (this._patterns[index].length == 1
             || this._patterns[index].charAt(this._patterns[index].length - 1) != "'") {
            this._patterns[index] += ch;
          } else {
            this._patterns[index + 1] = ch;
          }
        } else if (this._patterns[index].charAt(0) == ch) {
          this._patterns[index] += ch;
        } else {
          this._patterns[index + 1] = ch;
        }
      }
    }
  },

  format: function(date) {

    var result = [];
    for (var i = 0; i < this._patterns.length; i++) {
      result[i] = this._formatWord(date, this._patterns[i]);
    }
    return result.join('');
  },
  _formatWord: function(date, pattern) {

    var formatter = this._formatter[pattern.charAt(0)];
    if (formatter) {
      return formatter.apply(this, [date, pattern]);
    } else {
      return pattern;
    }
  },
  _formatter: {
    "y": function(date, pattern) {
      // Year
      var year = String(date.getFullYear());
      if (pattern.length <= 2) {
        year = year.substring(2, 4);
      } else {
        year = this._zeroPadding(year, pattern.length);
      }
      return year;
    },
    "M": function(date, pattern) {
      // Month in year
      return this._zeroPadding(String(date.getMonth() + 1), pattern.length);
    },
    "d": function(date, pattern) {
      // Day in month
      return this._zeroPadding(String(date.getDate()), pattern.length);
    },
    "H": function(date, pattern) {
      // Hour in day (0-23)
      return this._zeroPadding(String(date.getHours()), pattern.length);
    },
    "m": function(date, pattern) {
      // Minute in hour
      return this._zeroPadding(String(date.getMinutes()), pattern.length);
    },
    "s": function(date, pattern) {
      // Second in minute
      return this._zeroPadding(String(date.getSeconds()), pattern.length);
    },
    "S": function(date, pattern) {
      // Millisecond
      return this._zeroPadding(String(date.getMilliseconds()), pattern.length);
    },
    "E": function(date, pattern) {
      // Day in week
      var wday = this._dayOfWeekMap[date.getDay()];
      if(wday == null){
          return;
      }
      var len = pattern.length;
      switch(len){
      case 2:
          return wday.name2;
      case 3:
          return wday.name3;
      default:
          return wday.name;
      }
    },
    "'": function(date, pattern) {
      // escape
      if (pattern == "''") {
        return "'";
      } else {
        return pattern.replace(/'/g, '');
      }
    }
  },

  _zeroPadding: function(str, length) {
    if (str.length >= length) {
      return str;
    }

    return new Array(length - str.length + 1).join("0") + str;
  },


  /// Parser ///
  parse: function(text) {

    if (typeof text != 'string' || text == '') return null;

    var result = {year: 1970, month: 1, day: 1, hour: 0, min: 0, sec: 0, msec: 0};

    for (var i = 0; i < this._patterns.length; i++) {
       if (text == '') return null; // parse error!!
       text = this._parseWord(text, this._patterns[i], result);
       if (text === null) return null; // parse error!!
    }
    if (text != '') return null; // parse error!!

    return new Date(
                result.year,
                result.month - 1,
                result.day,
                result.hour,
                result.min,
                result.sec,
                result.msec);
  },
  _parseWord: function(text, pattern, result) {

    var parser = this._parser[pattern.charAt(0)];
    if (parser) {
      return parser.apply(this, [text, pattern, result]);
    } else {
      if (text.indexOf(pattern) != 0) {
        return null;
      } else {
        return text.substring(pattern.length);
      }
    }
  },
  _parser: {
    "y": function(text, pattern, result) {
      // Year
      var year;
      if (pattern.length <= 2) {
        year = text.substring(0, 2);
        year = year < 70 ? '20' + year : '19' + year;
        text = text.substring(2);
      } else {
        var length = (pattern.length == 3) ? 4 : pattern.length;
        year = text.substring(0, length);
        text = text.substring(length);
      }
      if (!this._isNumber(year)) return null; // error
      result.year = parseInt(year, 10);
      return text;
    },
    "M": function(text, pattern, result) {
      // Month in year
      var month;
      if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/1[0-2]/) != null) {
        month = text.substring(0, 2);
        text  = text.substring(2);
      } else {
        month = text.substring(0, pattern.length);
        text  = text.substring(pattern.length);
      }
      if (!this._isNumber(month)) return null; // error
      result.month = parseInt(month, 10);
      return text;
    },
    "d": function(text, pattern, result) {
      // Day in month
      var day;
      if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/1[0-9]|2[0-9]|3[0-1]/) != null) {
        day  = text.substring(0, 2);
        text = text.substring(2);
      } else {
        day  = text.substring(0, pattern.length);
        text = text.substring(pattern.length);
      }
      if (!this._isNumber(day)) return null; // error
      result.day = parseInt(day, 10);
      return text;
    },
    "H": function(text, pattern, result) {
      // Hour in day (0-23)
      var hour;
      if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/1[0-9]|2[0-3]/) != null) {
        hour = text.substring(0, 2);
        text = text.substring(2);
      } else {
        hour = text.substring(0, pattern.length);
        text = text.substring(pattern.length);
      }
      if (!this._isNumber(hour)) return null; // error
      result.hour = parseInt(hour, 10);
      return text;
    },
    "m": function(text, pattern, result) {
      // Minute in hour
      var min;
      if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/[1-5][0-9]/) != null) {
        min  = text.substring(0, 2);
        text = text.substring(2);
      } else {
        min  = text.substring(0, pattern.length);
        text = text.substring(pattern.length);
      }
      if (!this._isNumber(min)) return null; // error
      result.min = parseInt(min, 10);
      return text;
    },
    "s": function(text, pattern, result) {
      // Second in minute
      var sec;
      if (pattern.length == 1 && text.length > 1
          && text.substring(0, 2).match(/[1-5][0-9]/) != null) {
        sec  = text.substring(0, 2);
        text = text.substring(2);
      } else {
        sec  = text.substring(0, pattern.length);
        text = text.substring(pattern.length);
      }
      if (!this._isNumber(sec)) return null; // error
      result.sec = parseInt(sec, 10);
      return text;
    },
    "S": function(text, pattern, result) {
      // Millimsecond
      var msec;
      if (pattern.length == 1 || pattern.length == 2) {
        if (text.length > 2 && text.substring(0, 3).match(/[1-9][0-9][0-9]/) != null) {
          msec = text.substring(0, 3);
          text = text.substring(3);
        } else if (text.length > 1 && text.substring(0, 2).match(/[1-9][0-9]/) != null) {
          msec = text.substring(0, 2);
          text = text.substring(2);
        } else {
          msec = text.substring(0, pattern.length);
          text = text.substring(pattern.length);
        }
      } else {
        msec = text.substring(0, pattern.length);
        text = text.substring(pattern.length);
      }
      if (!this._isNumber(msec)) return null; // error
      result.msec = parseInt(msec, 10);
      return text;
    },
    "'": function(text, pattern, result) {
      // escape
      if (pattern == "''") {
        pattern = "'";
      } else {
        pattern = pattern.replace(/'/g, '');
      }
      if (text.indexOf(pattern) != 0) {
        return null; // error
      } else {
        return text.substring(pattern.length);
      }
    }
  },
  _dayOfWeekMap: [
    { name:'日', name3:'Sun', name2:'Sunday' },
    { name:'月', name3:'Mon', name2:'Monday' },
    { name:'火', name3:'Tue', name2:'Tuesday' },
    { name:'水', name3:'Wed', name2:'Wednesday' },
    { name:'木', name3:'Thu', name2:'Thursday' },
    { name:'金', name3:'Fri', name2:'Friday' },
    { name:'土', name3:'Sat', name2:'Saturday' }
  ],

  _isNumber: function(str) {
    return /^[0-9]*$/.test(str);
  }
};
//////////////////////////////////////////////////////////////////
// Datepicker.js (jQuery UI Datepicker 1.10.4) 独自メソッド追加
(function(){
    var dpPROP_NAME = 'Datepicker';

    if(!$.datepicker){
        console.warn('Datepicker not found.');
        return;
    }

    // jQuery UI Datepicker 既存メソッド魔改造

    // トリガーボタンの描画
    var fnAttachments = $.datepicker._attachments;
    $.datepicker._attachments = function($input, inst){
        var argsArray = _.toArray(arguments);
        var result = fnAttachments.apply(this, argsArray);

        // trigger ボタンなら、tabIndex=-1 をセットする。フォーカスルートから外すため。
        var $trigBtn = $input.next();
        if($trigBtn.is('button.ui-datepicker-trigger')){
            $trigBtn.attr('tabIndex', -1);
        }
        return result;
    };

    // parseDate
    var fnParseDate = $.datepicker.parseDate;
    /* Parse a string value into a date object.
     * See formatDate below for the possible formats.
     *
     * @param  format string - the expected format of the date
     * @param  value string - the date in the above format
     * @param  settings Object - attributes include:
     *                  shortYearCutoff  number - the cutoff year for determining the century (optional)
     *                  dayNamesShort   string[7] - abbreviated names of the days from Sunday (optional)
     *                  dayNames        string[7] - names of the days from Sunday (optional)
     *                  monthNamesShort string[12] - abbreviated names of the months (optional)
     *                  monthNames      string[12] - names of the months (optional)
     * @return  Date - the extracted date value or null if value is blank
     */
    $.datepicker.parseDate = function(format, value, settings){
        // string -> Date
        // Datepicker の dateFormat パーサーに依らず、Date 化できる場合は変換可能と見做す。
        value = value || '';
        var dt = clutil.date.toDate(value);
        if(clutil.date.isValidDate(dt)){
            return dt;
        }

        var argsArray = _.toArray(arguments);
        var result = fnParseDate.apply(this, argsArray);
        return result;
    };

// ＜$input.datepicker(関数名, val1, val2, ...) 拡張する実装例＞
//    /**
//     * Datepicker に８ケタ日付を設定する。yyyyMMdd 引数を省略した場合はクリアに相当。
//     * 使い方: $elem.datepicker('setIymd', yyyyMMdd)
//     */
//    $.datepicker['_setIymd' + dpPROP_NAME] = function(target, iymd){
//        console.log('_setIymd' + dpPROP_NAME);
//
//        var $dp = $(target).val('');
//        var $dpWrap = $dp.closest('.datepicker_wrap');
//
//        // クリア
//        $dpWrap.removeClass("dayOfWeek0 dayOfWeek1 dayOfWeek2 dayOfWeek3 dayOfWeek4 dayOfWeek5 dayOfWeek6");
//
//        if(iymd == null || iymd == undefined || iymd <= 0){
//            // テキストを空欄にする。
//            $dp.datepicker('setDate', null);
//            return;
//        }
//        var fixDate = clutil.ymd2date(iymd);
//        if(!_.isDate(fixDate) || isNaN(fixDate)){
//            console.warn('datepicker.setIymd: bad iymd[' + iymd + ']');
//            return;
//        }
//
//        $dp.datepicker('setDate', fixDate);
//        $dpWrap.addClass('dayOfWeek' + fixDate.getDay());
//    };
//
//    /**
//     * Datepicker 表示をリフレッシュする。
//     * 使い方: $elem.datepicker('refresh')
//     */
//    $.datepicker['_refresh' + dpPROP_NAME] = function(target){
//        var $target = $(target);
//        var iymd = -1;
//        var val = $target.val();
//        if(!_.isEmpty(val)){
//            var date = new Date(val);
//            if(_.isDate(date) || !isNaN(date.getMilliseconds())){
//                // valid date format
//                var iymd = date.getFullYear() * 10000 + (date.getMonth()+1) * 100 + date.getDate();
//            }
//        }
//
//        console.log('_refresh' + dpPROP_NAME + ': iymd[' + iymd + '], reflesh');
//
//        $target.datepicker('setIymd', iymd);
//    };
}());
var clcom_defs = {
	COMMON_RSP_STATUS_OK : 0,
	COMMON_RSP_STATUS_NG : 1,
	role_id : {
		TRAINER		: 2,		// トレーナー
		COUNSELOR	: 3,		// カウンセラ－
		MANAGER		: 4,		// 店舗責任者
		FRONT		: 5,		// フロント
		CALLCENTER	: 6,		// コールセンター
		ADMIN		: 7,		// システム管理者
		OFFICESTAFF	: 8,		// 本社スタッフ
	}
};
var sort_req = {
	SORT_ORDER_ASCENDING : 1,
	SORT_ORDER_DESCENDING : -1
};

// 本ファイルは直接編集せず、元本データを編集して自動生成ツールで生成すること
/******************************************************************************
 *
 * 説明  区分定義
 *
 *          Copyright (C) SURIGIKEN Co.,Ltd.  2017. All rights reserved.
 ******************************************************************************/
var tmcm_type = {

	BUSI_SEL: 1,		// [区分種別] 業態区分選択用
	BUSI_SEL_ROOM: 1,		// 個別教室
	BUSI_SEL_HOME: 2,		// 家庭教師
	BUSI_SEL_SCHOOL: 3,		// 高等学院
	BUSI_SEL_ADULT: 4,		// 大人・プロ合宿
	BUSI_SEL_GOV: 5,		// 行政
	BUSI_SEL_MYGYM: 6,		// MyGym
	BUSI_SEL_NOTYET: 9,		// 未定

	SCHOOL_GRADE_SEL: 2,		// [区分種別] 学年選択用
	SCHOOL_GRADE_SEL_E1: 1,		// 小学
	SCHOOL_GRADE_SEL_E2: 2,		// 小学
	SCHOOL_GRADE_SEL_E3: 3,		// 小学
	SCHOOL_GRADE_SEL_E4: 4,		// 小学
	SCHOOL_GRADE_SEL_E5: 5,		// 小学
	SCHOOL_GRADE_SEL_E6: 6,		// 小学
	SCHOOL_GRADE_SEL_J1: 11,		// 中学
	SCHOOL_GRADE_SEL_J2: 12,		// 中学
	SCHOOL_GRADE_SEL_J3: 13,		// 中学
	SCHOOL_GRADE_SEL_H1: 21,		// 高校
	SCHOOL_GRADE_SEL_H2: 22,		// 高校
	SCHOOL_GRADE_SEL_H3: 23,		// 高校
	SCHOOL_GRADE_SEL_OTHER: 90,		// その他

	INQ_TYPE: 3,		// [区分種別] 問合せ種別
	INQ_TYPE_NEW: 1,		// 新規
	INQ_TYPE_SIBLING: 2,		// 兄弟
	INQ_TYPE_REVIVAL: 3,		// 復活
	INQ_TYPE_REINQUIRY: 4,		// 再お問合せ
	INQ_TYPE_OTHER: 90,		// その他

	INQ_ROUTE: 4,		// [区分種別] 問合せ経路選択用
	INQ_ROUTE_TEL: 1,		// TEL
	INQ_ROUTE_HP_HOME: 11,		// HP（家庭教師）
	INQ_ROUTE_HP_ROOM: 12,		// HP（個別教室）
	INQ_ROUTE_HP_SCHOOL: 13,		// HP（高等学院）
	INQ_ROUTE_HP_ADULT: 14,		// HP（大人）
	INQ_ROUTE_HP_PROSPORTS: 15,		// HP（スポーツ）
	INQ_ROUTE_HP_MEDICAL: 16,		// HP（医学部）
	INQ_ROUTE_HP_INTEGRA: 17,		// HP（インテグラ）
	INQ_ROUTE_HP_CAMP: 18,		// HP（合宿）
	INQ_ROUTE_HP_GOV: 19,		// HP（行政）
	INQ_ROUTE_SITE_JIRITSU: 51,		// 比較サイト（自立学習）
	INQ_ROUTE_SITE_JNAVI: 52,		// 比較サイト（塾ナビ）
	INQ_ROUTE_SITE_KNAVI: 53,		// 比較サイト（かてきょナビ）
	INQ_ROUTE_SITE_JHIROBA: 54,		// 比較サイト（塾ひろば）
	INQ_ROUTE_SITE_HIKAKU: 55,		// 比較サイト（比較ネット）
	INQ_ROUTE_SITE_WELCOME: 56,		// 比較サイト（ウェルカム）
	INQ_ROUTE_VISIT: 90,		// 来校

	GENDER: 5,		// [区分種別] 性別
	GENDER_MALE: 1,		// 男性
	GENDER_FEMALE: 2,		// 女性
	GENDER_UNKNOWN: 9,		// 不明

	FAMILY_REL: 6,		// [区分種別] 続柄
	FAMILY_REL_FATHER: 1,		// 父
	FAMILY_REL_MOTHER: 2,		// 母
	FAMILY_REL_GRANDFATHER: 3,		// 祖父
	FAMILY_REL_GRANDMOTHER: 4,		// 祖母
	FAMILY_REL_SELF: 5,		// 本人
	FAMILY_REL_OTHER: 9,		// その他

	INQ_STAT: 7,		// [区分種別] 問い合わせステータス
	INQ_STAT_NOTCONN: 1,		// 未コネ
	INQ_STAT_INQUIRY: 2,		// 問合せ
	INQ_STAT_CANCEL: 3,		// 前キャン
	INQ_STAT_PAMPH: 4,		// パンフ
	INQ_STAT_TEL: 5,		// tel決め
	INQ_STAT_PENDING: 6,		// 保留

	TRIG_MEDIA: 8,		// [区分種別] きっかけの媒体
	TRIG_MEDIA_TVCM: 1,		// TVCM
	TRIG_MEDIA_OTHER_SITE: 2,		// 比較サイト
	TRIG_MEDIA_OWN_SITE: 3,		// 自社サイト
	TRIG_MEDIA_FRIEND: 4,		// 友達の紹介
	TRIG_MEDIA_FAMILY: 5,		// 兄弟

	CONTACT: 9,		// [区分種別] コンタクト種別
	CONTACT_CLAIM: 1,		// 相談クレーム
	CONTACT_CALLBACK: 2,		// 要折り返し
	CONTACT_CONFIRM: 3,		// 要確認
	CONTACT_FIN: 4,		// 完結

	TELKIND: 10,		// [区分種別] 電話番号種別
	TELKIND_FATHER_MO: 1,		// 父携帯
	TELKIND_MOTHER_MO: 2,		// 母携帯
	TELKIND_SELF_MO: 3,		// 本人携帯
	TELKIND_OTHER: 90,		// その他

	REQ_GENDER: 11,		// [区分種別] 希望性別
	REQ_GENDER_MALE_ONLY: 1,		// 男性のみ
	REQ_GENDER_MALE: 2,		// 男性希望だが女性も可
	REQ_GENDER_FREE: 3,		// どちらでもよい
	REQ_GENDER_FEMALE: 4,		// 女性希望だが男性も可
	REQ_GENDER_FEMALE_ONLY: 5,		// 女性のみ

	BANK: 12,		// [区分種別] 金融機関区分
	BANK_BANK: 1,		// 金融機関
	BANK_JPBANK: 2,		// ゆうちょ銀行

	PAYMETHOD: 13,		// [区分種別] 支払方法
	PAYMETHOD_WITHDRAWAL: 1,		// 口座引落し
	PAYMETHOD_TRANSFER: 2,		// 口座振込

	BKACCOUNT: 14,		// [区分種別] 口座種別
	BKACCOUNT_SAVING: 1,		// 総合/普通
	BKACCOUNT_CHECKING: 2,		// 当座

	TASK: 15,		// [区分種別] タスク種別
	TASK_TRIAL: 1,		// 体験授業
	TASK_ARRANGE: 2,		// 講師代行手配
	TASK_STAFF1: 3,		// スタッフ業務1

	NG_REASON: 16,		// [区分種別] 教師NG理由
	NG_REASON_ADOPTION: 0,		// 採用
	NG_REASON_CONDITION: 1,		// 条件が合わない
	NG_REASON_CHEMISTRY: 2,		// 相性が合わない
	NG_REASON_NG: 3,		// 講師NG

	CONTRACT_STAT: 17,		// [区分種別] 契約ステータス
	CONTRACT_STAT_RUN: 1,		// 契約中
	CONTRACT_STAT_SUSPEND: 2,		// 休止
	CONTRACT_STAT_FIN: 3,		// 終了

	JOB: 19,		// [区分種別] 職業
	JOB_STUDENT: 1,		// 学生
	JOB_WORKER: 2,		// 社会人

	SKILL_SUB: 20,		// [区分種別] 得意科目
	SKILL_SUB_E_LANG: 101,		// 小学
	SKILL_SUB_E_MATH: 102,		// 小学
	SKILL_SUB_E_ENGL: 103,		// 小学
	SKILL_SUB_E_SCIE: 104,		// 小学
	SKILL_SUB_E_SOCI: 105,		// 小学
	SKILL_SUB_J_LANG: 201,		// 中学
	SKILL_SUB_J_MATH: 202,		// 中学
	SKILL_SUB_J_ENGL: 203,		// 中学
	SKILL_SUB_J_SCIE: 204,		// 中学
	SKILL_SUB_J_SOCI: 205,		// 中学
	SKILL_SUB_H_LANG: 301,		// 高校
	SKILL_SUB_H_ANTLANG: 302,		// 高校
	SKILL_SUB_H_MATH: 303,		// 高校
	SKILL_SUB_H_ENGL: 304,		// 高校
	SKILL_SUB_H_PHYS: 305,		// 高校
	SKILL_SUB_H_CHEM: 306,		// 高校
	SKILL_SUB_H_BIOL: 307,		// 高校
	SKILL_SUB_H_EARTH: 308,		// 高校
	SKILL_SUB_H_WHST: 309,		// 高校
	SKILL_SUB_H_JHST: 310,		// 高校

	SKILL_UP: 23,		// [区分種別] 強化したい科目
	SKILL_UP_ENGL: 1,		// 英語
	SKILL_UP_MATH: 2,		// 数学
	SKILL_UP_LANG: 3,		// 国語
	SKILL_UP_SCIE: 4,		// 理科
	SKILL_UP_SOCI: 5,		// 社会
	SKILL_UP_H_ENG_GRM: 11,		// 高校英語文法
	SKILL_UP_H_ENG_RDR: 12,		// 高校英語構文
	SKILL_UP_H_MTH_I: 13,		// 高校数学I
	SKILL_UP_H_MTH_II: 14,		// 高校数学II
	SKILL_UP_H_MTH_III: 15,		// 高校数学III
	SKILL_UP_H_MTH_A: 16,		// 高校数学A
	SKILL_UP_H_MTH_B: 17,		// 高校数学B
	SKILL_UP_H_PHY_BS: 18,		// 高校物理基礎
	SKILL_UP_H_BIO_BS: 19,		// 高校生物基礎
	SKILL_UP_H_CHM_BS: 20,		// 高校化学基礎
	SKILL_UP_H_PHY: 21,		// 高校物理
	SKILL_UP_H_BIO: 22,		// 高校生物
	SKILL_UP_H_CHM: 23,		// 高校化学
	SKILL_UP_H_JHST_B: 24,		// 高校日本史B
	SKILL_UP_H_WHST_B: 25,		// 高校世界史B
	SKILL_UP_H_EART_B: 26,		// 高校地理B
	SKILL_UP_OTHR: 99,		// その他

	HITO_TYPE: 28,		// [区分種別] ヒト種別区分
	HITO_TYPE_STUDENT: 1,		// 生徒
	HITO_TYPE_TEACHER: 2,		// 教師
	HITO_TYPE_GUARDIAN: 3,		// 保護者
	HITO_TYPE_STAFF: 4,		// スタッフ
	HITO_TYPE_EMPLOYEE: 5,		// 従業員

	STAFF_SHIFT_TIME: 29,		// [区分種別] スタッフシフト時間
	STAFF_SHIFT_TIME_ABSENCE: 1,		// 休み
	STAFF_SHIFT_TIME_AM: 2,		// 10-13
	STAFF_SHIFT_TIME_PM: 3,		// 13-18
	STAFF_SHIFT_TIME_NIGHT: 4,		// 18-22
	STAFF_SHIFT_TIME_FREE: 5,		// 自由入力

	SCHEDULE_TYPE: 30,		// [区分種別] 予定種別
	SCHEDULE_TYPE_ORIEN: 1,		// 予定
	SCHEDULE_TYPE_CST_DISCUS: 2,		// 予定
	SCHEDULE_TYPE_VISIT_CSTY_DISCUS: 3,		// 予定
	SCHEDULE_TYPE_CNTRCT_DISCUS: 4,		// 予定
	SCHEDULE_TYPE_VISIT_CNTRCT_DISCUS: 5,		// 予定
	SCHEDULE_TYPE_TEACHER_DISCUS: 6,		// 予定
	SCHEDULE_TYPE_K_TRIAL_LECTURE: 7,		// 仮予定
	SCHEDULE_TYPE_K_TUTOR: 8,		// 仮予定
	SCHEDULE_TYPE_K_MOVE: 9,		// 仮予定
	SCHEDULE_TYPE_K_LECTURE: 10,		// 仮予定
	SCHEDULE_TYPE_K_TEACHER_LESSON: 11,		// 仮予定
	SCHEDULE_TYPE_K_STAFF_WORK: 12,		// 仮予定
	SCHEDULE_TYPE_K_MEETING: 13,		// 仮予定
	SCHEDULE_TYPE_K_OTHER: 14,		// 仮予定

	CANCEL_TYPE: 31,		// [区分種別] 解約種別
	CANCEL_TYPE_FORCE: 0,		// 即時解約
	CANCEL_TYPE_INTEND: 1,		// 指導消化後解約
	CANCEL_TYPE_GRAD: 2,		// 合格解約

	HAS_CONTRACT: 32,		// [区分種別] 契約有無
	HAS_CONTRACT_EFFECT: 0,		// 無効
	HAS_CONTRACT_NO_EFFECT: 1,		// 有効

	RECORD_STATE: 33,		// [区分種別] 指導記録状態
	RECORD_STATE_NONE: 0,		// 未提出
	RECORD_STATE_INTEND: 1,		// 提出予定
	RECORD_STATE_COMPLETE: 2,		// 提出済

	BASIC_GRADE: 34,		// [区分種別] ベーシックコース学年種別
	BASIC_GRADE_PRIMARY: 1,		// 小学生
	BASIC_GRADE_MIDDLE: 2,		// 中学１・２年生
	BASIC_GRADE_MIDDLE_EXMEE: 3,		// 中学３年生
	BASIC_GRADE_HIGH: 4,		// 高校１・２年生
	BASIC_GRADE_HIGH_EXMEE: 5,		// 高校３年生

	COVER_PERSONS: 35,		// [区分種別] 指導形態区分
	COVER_PERSONS_PRIVATE: 1,		// 個別
	COVER_PERSONS_FEW: 2,		// １対２
	COVER_PERSONS_GROUP: 3,		// グループ

	CALENDAR_TIME: 36,		// [区分種別] カレンダー時間指定
	CALENDAR_TIME_SEVEN_ZERO: 1,		// 7:00
	CALENDAR_TIME_SEVEN_FIFTEEN: 2,		// 7:15
	CALENDAR_TIME_SEVEN_THIRTY: 3,		// 7:30
	CALENDAR_TIME_SEVEN_FORTYFIVE: 4,		// 7:45
	CALENDAR_TIME_EIGHT_ZERO: 5,		// 8:00
	CALENDAR_TIME_EIGHT_FIFTEEN: 6,		// 8:15
	CALENDAR_TIME_EIGHT_THIRTY: 7,		// 8:30
	CALENDAR_TIME_EIGHT_FORTYFIVE: 8,		// 8:45
	CALENDAR_TIME_NINE_ZERO: 9,		// 9:00
	CALENDAR_TIME_NINE_FIFTEEN: 10,		// 9:15
	CALENDAR_TIME_NINE_THIRTY: 11,		// 9:30
	CALENDAR_TIME_NINE_FORTYFIVE: 12,		// 9:45
	CALENDAR_TIME_TEN_ZERO: 13,		// 10:00
	CALENDAR_TIME_TEN_FIFTEEN: 14,		// 10:15
	CALENDAR_TIME_TEN_THIRTY: 15,		// 10:30
	CALENDAR_TIME_TEN_FORTYFIVE: 16,		// 10:45
	CALENDAR_TIME_ELEVEN_ZERO: 17,		// 11:00
	CALENDAR_TIME_ELEVEN_FIFTEEN: 18,		// 11:15
	CALENDAR_TIME_ELEVEN_THIRTY: 19,		// 11:30
	CALENDAR_TIME_ELEVEN_FORTYFIVE: 20,		// 11:45
	CALENDAR_TIME_TWELVE_ZERO: 21,		// 12:00
	CALENDAR_TIME_TWELVE_FIFTEEN: 22,		// 12:15
	CALENDAR_TIME_TWELVE_THIRTY: 23,		// 12:30
	CALENDAR_TIME_TWELVE_FORTYFIVE: 24,		// 12:45
	CALENDAR_TIME_THIRTEEN_ZERO: 25,		// 13:00
	CALENDAR_TIME_THIRTEEN_FIFTEEN: 26,		// 13:15
	CALENDAR_TIME_THIRTEEN_THIRTY: 27,		// 13:30
	CALENDAR_TIME_THIRTEEN_FORTYFIVE: 28,		// 13:45
	CALENDAR_TIME_FOURTEEN_ZERO: 29,		// 14:00
	CALENDAR_TIME_FOURTEEN_FIFTEEN: 30,		// 14:15
	CALENDAR_TIME_FOURTEEN_THIRTY: 31,		// 14:30
	CALENDAR_TIME_FOURTEEN_FORTYFIVE: 32,		// 14:45
	CALENDAR_TIME_FIFTEEN_ZERO: 33,		// 15:00
	CALENDAR_TIME_FIFTEEN_FIFTEEN: 34,		// 15:15
	CALENDAR_TIME_FIFTEEN_THIRTY: 35,		// 15:30
	CALENDAR_TIME_FIFTEEN_FORTYFIVE: 36,		// 15:45
	CALENDAR_TIME_SIXTEEN_ZERO: 37,		// 16:00
	CALENDAR_TIME_SIXTEEN_FIFTEEN: 38,		// 16:15
	CALENDAR_TIME_SIXTEEN_THIRTY: 39,		// 16:30
	CALENDAR_TIME_SIXTEEN_FORTYFIVE: 40,		// 16:45
	CALENDAR_TIME_SEVENTEEN_ZERO: 41,		// 17:00
	CALENDAR_TIME_SEVENTEEN_FIFTEEN: 42,		// 17:15
	CALENDAR_TIME_SEVENTEEN_THIRTY: 43,		// 17:30
	CALENDAR_TIME_SEVENTEEN_FORTYFIVE: 44,		// 17:45
	CALENDAR_TIME_EIGHTEEN_ZERO: 45,		// 18:00
	CALENDAR_TIME_EIGHTEEN_FIFTEEN: 46,		// 18:15
	CALENDAR_TIME_EIGHTEEN_THIRTY: 47,		// 18:30
	CALENDAR_TIME_EIGHTEEN_FORTYFIVE: 48,		// 18:45
	CALENDAR_TIME_NINETEEN_ZERO: 49,		// 19:00
	CALENDAR_TIME_NINETEEN_FIFTEEN: 50,		// 19:15
	CALENDAR_TIME_NINETEEN_THIRTY: 51,		// 19:30
	CALENDAR_TIME_NINETEEN_FORTYFIVE: 52,		// 19:45
	CALENDAR_TIME_TWENTY_ZERO: 53,		// 20:00
	CALENDAR_TIME_TWENTY_FIFTEEN: 54,		// 20:15
	CALENDAR_TIME_TWENTY_THIRTY: 55,		// 20:30
	CALENDAR_TIME_TWENTY_FORTYFIVE: 56,		// 20:45
	CALENDAR_TIME_TWENTYONE_ZERO: 57,		// 21:00
	CALENDAR_TIME_TWENTYONE_FIFTEEN: 58,		// 21:15
	CALENDAR_TIME_TWENTYONE_THIRTY: 59,		// 21:30
	CALENDAR_TIME_TWENTYONE_FORTYFIVE: 60,		// 21:45
	CALENDAR_TIME_TWENTYTWO_ZERO: 61,		// 22:00
	CALENDAR_TIME_TWENTYTWO_FIFTEEN: 62,		// 22:15
	CALENDAR_TIME_TWENTYTWO_THIRTY: 63,		// 22:30
	CALENDAR_TIME_TWENTYTWO_FORTYFIVE: 64,		// 22:45
	CALENDAR_TIME_TWENTYTHREE_ZERO: 65,		// 23:00

	STAFF_WORK: 37,		// [区分種別] スタッフ業務区分
	STAFF_WORK_BRANCH: 1,		// 支店でのスタッフ業務
	STAFF_WORK_TRY_SCHOOL: 2,		// 教室でのスタッフ業務
	STAFF_WORK_HIGH_SCHOOL: 3,		// 高等学院キャンパスでのスタッフ業務
	STAFF_WORK_HEAD: 4,		// 本部でのスタッフ業務
	STAFF_WORK_EXPERIENCE_GUIDANCE: 5,		// 体験指導
	STAFF_WORK_SUPPORTRE: 6,		// サポーター業務
	STAFF_WORK_OTHER: 7,		// その他

	CALENDAR_SCHEDULE_TYPE: 38,		// [区分種別] カレンダー予約種別
	CALENDAR_SCHEDULE_TYPE_RESERVATION: 1,		// 予約可
	CALENDAR_SCHEDULE_TYPE_PROVISIONAL_RESERVATION: 2,		// 仮予約可

	_eof: 'end of tmcm_type//'
};
/* Copyright (c) 2010-2016 Marcus Westin */
(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.store = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
"use strict";module.exports=function(){function e(){try{return o in n&&n[o]}catch(e){return!1}}var t,r={},n="undefined"!=typeof window?window:global,i=n.document,o="localStorage",a="script";if(r.disabled=!1,r.version="1.3.20",r.set=function(e,t){},r.get=function(e,t){},r.has=function(e){return void 0!==r.get(e)},r.remove=function(e){},r.clear=function(){},r.transact=function(e,t,n){null==n&&(n=t,t=null),null==t&&(t={});var i=r.get(e,t);n(i),r.set(e,i)},r.getAll=function(){},r.forEach=function(){},r.serialize=function(e){return JSON.stringify(e)},r.deserialize=function(e){if("string"==typeof e)try{return JSON.parse(e)}catch(t){return e||void 0}},e())t=n[o],r.set=function(e,n){return void 0===n?r.remove(e):(t.setItem(e,r.serialize(n)),n)},r.get=function(e,n){var i=r.deserialize(t.getItem(e));return void 0===i?n:i},r.remove=function(e){t.removeItem(e)},r.clear=function(){t.clear()},r.getAll=function(){var e={};return r.forEach(function(t,r){e[t]=r}),e},r.forEach=function(e){for(var n=0;n<t.length;n++){var i=t.key(n);e(i,r.get(i))}};else if(i&&i.documentElement.addBehavior){var c,u;try{u=new ActiveXObject("htmlfile"),u.open(),u.write("<"+a+">document.w=window</"+a+'><iframe src="/favicon.ico"></iframe>'),u.close(),c=u.w.frames[0].document,t=c.createElement("div")}catch(l){t=i.createElement("div"),c=i.body}var f=function(e){return function(){var n=Array.prototype.slice.call(arguments,0);n.unshift(t),c.appendChild(t),t.addBehavior("#default#userData"),t.load(o);var i=e.apply(r,n);return c.removeChild(t),i}},d=new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]","g"),s=function(e){return e.replace(/^d/,"___$&").replace(d,"___")};r.set=f(function(e,t,n){return t=s(t),void 0===n?r.remove(t):(e.setAttribute(t,r.serialize(n)),e.save(o),n)}),r.get=f(function(e,t,n){t=s(t);var i=r.deserialize(e.getAttribute(t));return void 0===i?n:i}),r.remove=f(function(e,t){t=s(t),e.removeAttribute(t),e.save(o)}),r.clear=f(function(e){var t=e.XMLDocument.documentElement.attributes;e.load(o);for(var r=t.length-1;r>=0;r--)e.removeAttribute(t[r].name);e.save(o)}),r.getAll=function(e){var t={};return r.forEach(function(e,r){t[e]=r}),t},r.forEach=f(function(e,t){for(var n,i=e.XMLDocument.documentElement.attributes,o=0;n=i[o];++o)t(n.name,r.deserialize(e.getAttribute(n.name)))})}try{var v="__storejs__";r.set(v,v),r.get(v)!=v&&(r.disabled=!0),r.remove(v)}catch(l){r.disabled=!0}return r.enabled=!r.disabled,r}();
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1])(1)
});
//////////////////////////////////////////////////////////////////
// store.js プラグイン拡張 - セットしているキーがどんなかを管理
(function(store){
    if(!store){
        throw 'store.js がロードされておりません。';
    }

    var origRemoveFunc = store.remove;

    /**
     * 削除対象 key を配列で与えられるよう拡張
     */
    store.remove = function(k){
        var kk = _.isArray(k) ? k : [ k ];
        for(var i = 0; i < kk.length; i++){
            var k = kk[i];
            origRemoveFunc(k);
        }
    };

}(store));
(function ($) {
    var vent = _.extend({}, Backbone.Events);
    function getRange(el) {
        var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange, $el;

        if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
            start = el.selectionStart;
            end = el.selectionEnd;
        } else if (document.selection) {
            $el = $(el);
            if ($el.is(':visible:not(:disabled)')) {

                range = document.selection.createRange();

                if (range && range.parentElement() == el) {
                    len = el.value.length;
                    normalizedValue = el.value.replace(/\r\n/g, "\n");

                    // Create a working TextRange that lives only in the input
                    textInputRange = el.createTextRange();
                    textInputRange.moveToBookmark(range.getBookmark());

                    // Check if the start and end of the selection are at the very end
                    // of the input, since moveStart/moveEnd doesn't return what we want
                    // in those cases
                    endRange = el.createTextRange();
                    endRange.collapse(false);

                    if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                        start = end = len;
                    } else {
                        start = -textInputRange.moveStart("character", -len);
                        start += normalizedValue.slice(0, start).split("\n").length - 1;

                        if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                            end = len;
                        } else {
                            end = -textInputRange.moveEnd("character", -len);
                            end += normalizedValue.slice(0, end).split("\n").length - 1;
                        }
                    }
                }
            }
        }

        return {
            start: start,
            end: end
        };
    }

    function getCursorPosition(input) {
        if (!input) return; // No (input) element found
        if ('selectionStart' in input) {
            // Standard-compliant browsers
            return input.selectionStart;
        } else if (document.selection) {
            // IE
            var $input = $(input);
            if ($input.is(':visible:not(:disabled)')) {
                input.focus();
                var sel = document.selection.createRange();
                var selLen = document.selection.createRange().text.length;
                sel.moveStart('character', -input.value.length);
                return sel.text.length - selLen;
            } else {
                return $input.val().length;
            }
        }
        return 0;
    }

    function setRange($input, start, end) {
        var input = $input.get(0);
        if (input.setSelectionRange) {
            if ($input.is(':visible:not(:disabled)')) {
                input.focus();
                input.setSelectionRange(start, end);
            }
        } else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    }

    function setCursorPosition($input, pos) {
        setRange($input, pos, pos);
    }


    var compiledLimiters = {},
    compiledFilters = {};

    // デバッグ用
    var eventLogHandler = function(e){
        // var str = (e.type + '																				').slice(0,20);
        // if (e.keyCode) str += '\tkeyCode:' + e.keyCode;
        // if (e.data) str += '\tdata:' + e.data;
        // console.log("inputlimiter:", str);
    };

    var Limiters = (function () {
        var i,

        regexLimiter = function (reg) {
            if (!reg instanceof RegExp)
                reg = new RegExp(reg);
            return function (value) {
                return (value.match(reg) || [])[0] || "";
            };
        },

        len = function (length) {
            return function (value) {
                return value.substring(0, parseInt(length, 10));
            };
        },

        time = function () {
            var regs = [
                regexLimiter(/[0-1](?:[0-9](?:[0-5][0-9]?)?)?/),
                regexLimiter(/2(?:[0-9](?:[0-5][0-9]?)?)?/),
                regexLimiter(/3(?:[0-5](?:[0-5][0-9]?)?)?/),
                regexLimiter(/3(6(0(0?)?)?)?/),
                regexLimiter(/[0-9](?:[0-5][0-9]?)?/)
                ];

            return function (value) {
                return _.reduce(_.map(regs, function (reg) {return reg(value)}), function (memo, v) {
                    if (memo.length > v.length)
                        return memo;
                    else
                        return v;
                }, '');
            };
        },

        alpha = function () {
            return regexLimiter(/[A-Za-z]+/);
        },

        alnum = function () {
            return regexLimiter(/[A-Za-z0-9]+/);
        },

        reg = function (r) {
            var compiled = r;
            if (!r instanceof RegExp)
                compiled = new RegExp(reg);
            return function (value) {
                var match = value.match(reg);
                return match ? match[0] : '';
            };
        },

        code = function (len) {
            var reg = /([0-9]+)/;
            return function (value) {
                var match = value.match(reg);
                if (!match)
                    return '';
                return match[1].substring(0, len || undefined);
            };
        },

        code2 = function (len) {
            var reg = /([-0-9]+)/;
            return function (value) {
                var match = value.match(reg);
                if (!match)
                    return '';
                return match[1].substring(0, len || undefined);
            };
        },

        // :5
        integer2 = function (len) {
            var reg = /(0|[1-9][0-9]*)/;
            return function (value) {
                var match = value.match(reg);
                if (!match)
                    return '';
                return match[1].substring(0, len || undefined);
            };
        },

        // integer:5
        integer = function (len) {
            var reg = /(-?)(0|[1-9][0-9]*)?/;
            return function (value) {
                var match = value.match(reg);
                if (!match)
                    return '';
                return match[1] + (match[2]||'').substring(0, len || undefined);
            };
        },

        // number:10,3
        number = function (len1, len2, allowNegative) {
            var reg = /([-]?)(?:(0|[1-9][0-9]*))?(?:(\.)([0-9]*))?/;
            len1 = parseInt(len1, 10);
            len2 = parseInt(len2, 10);
            allowNegative = allowNegative === '-';
            return function (value) {
                var match = value.match(reg);
                if (!match)
                    return '';
                return (allowNegative ? (match[1] || '') : '') +
                (match[2] || '').substring(0, len1 || undefined) +
                (match[3] || '') +
                (match[4] || '').substring(0, len2 || undefined);
            };
        },

        yyyymm = function () {
            return function (value) {
                if (/[0-9/]{0,7}/.test(value))
                    return value;
                return '';
            };
        },

        ym = function (len) {
            return function (value) {
                if (!value) return '';
                if (!/^[0-9/]+$/.test(value)) return '';
                return value.substring(0, len);
            };
        },

        truncateByCharMap = function (charMap) {
            return function (value) {
                var length, valueLength = value.length;
                for (length = 0; length < valueLength; length++) {
                    if (!charMap(value.charCodeAt(length)))
                        break;
                }
                return value.substring(0, length);
            };
        },

        isHalf = function (c) {
            return (c >= 0x0 && c < 0x81) || (c == 0xf8f0) ||
            (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4);
        },

        hankaku = function () {
            return truncateByCharMap(isHalf);
        },

        zenkaku = function () {
            return truncateByCharMap(function (c) { return !isHalf(c);});
        };

        return {
            'regex': regexLimiter,
            len: len,
            'int': integer,
            'int2': integer2,
            time: time,
            code2: code2,
            alpha: alpha,
            alnum: alnum,
            // digit: digit,
            digit: code,
            number: number,
            hankaku: hankaku,
            zenkaku: zenkaku,
            yyyymm: yyyymm,
            ym: ym
        };
    }());

    var Filters = (function () {
        var i,

        fixed = function (num) {
            var f = function (value) {
                var n = Number(value);
                if (isFinite(n)) {
                    var d = Math.pow(10, num);
                    n = Math.round(n * d) / d;
                    return n.toFixed(num);
                } else {
                    return value;
                }
            };
            return {
                set: f
            };
        },

        /**
         * @param {Number} digits 0から20までの整数
         */
        floor = function (digits) {
            var f = function (value) {
                var x = value.split('.'),
                dpart = (x[0] || '0'),
                ipart = ((x[1] || '') + '00000000000000000000').substring(0, digits);
                return digits > 0 ? dpart + '.' + ipart : dpart;
            };
            return {
                set: f
            };
        },

        comma = function () {
            var format = /([\d]+?)(?=(?:\d{3})+$)/g;

            return {
                mask: function (value) {
                    var xs = value.split('.');
                    value = xs[0].replace(format, function(t){
                        return t + ',';
                    });
                    xs[0] = value;
                    return xs.join('.');
                },

                unmask: function (value) {
                    return value.split(',').join('');
                }
            };
        },

        time = function () {
            var format = /(\d+?)(?=(?:\d{2})+$)/g;

            return {
                mask: function (value) {
                    return value.replace(format, function (t) {return t + ':'});
                },

                unmask: function (value) {
                    return value.split(':').join('');
                }
            };
        };

        return {
            comma: comma,
            time: time,
            fixed: fixed,
            floor: floor
        };

    }());

    var compiledConverter = {},

    buildConverter = function (limiters) {
        var i,

        filter = function (method, value) {
            if (!limiters.filters)
                return value;
            value = _.reduce(limiters.filters, function (memo, filter) {
                var args = filter.args.slice();
                args.unshift(memo);
                if (filter.filter[method])
                    memo = filter.filter[method].apply(null, args);
                return memo;
            }, value);
            return value;
        },

        limitValue = function (value) {
            if (!limiters.limiters)
                return value;
            value = _.reduce(limiters.limiters, function (memo, limiter) {
                var args = limiter.args.slice();
                args.unshift(memo);
                return limiter.limiter.apply(null, args);
            }, value);
            return value;
        },

        filterValue = function (value) {
            value = _.compose(
                    _.bind(filter, null, 'mask'),
                    limitValue,
                    _.bind(filter, null, 'set'),
                    _.bind(filter, null, 'unmask')
            )(value);
            return value;
        },

        value = function (value) {
            value = _.compose(
                    _.bind(filter, null, 'unmask'),
                    filterValue
            )(value);
            return value;
        };

        return {
            filter: filter,
            limitValue: limitValue,
            filterValue: filterValue,
            value: value
        };

    },

    splitter = /\s+/,

    buildLimiter = function (dataLimit, dataFilter) {
        var limiters,
        filters;

        if (dataLimit) {
            limiters = compiledLimiters[dataLimit];
            if (!limiters) {
                limiters = _.map(dataLimit.split(splitter), function (s) {
                    var args = s.split(':'),
                    limiter = args.shift();
                    args = (args[0] || "").split(',');
                    if (!Limiters[limiter]) {
                        throw new Error('Invalid limiter:' + limiter);
                    }

                    return {
                        limiter: Limiters[limiter].apply(this, args),
                        args: args
                    };
                });
                compiledLimiters[dataLimit] = limiters;
            }
        }

        if (dataFilter) {
            filters = compiledFilters[dataFilter];
            if (!filters) {
                filters = _.map(dataFilter.split(splitter), function (s) {
                    var args = s.split(':'),
                    filter = args.shift();
                    args = (args[0] || '').split(',');
                    if (!Filters[filter]) {
                        throw new Error('Invalid Filter:' + filter);
                    }

                    return {
                        filter: Filters[filter].apply(this, args),
                        args: args
                    };
                });
                compiledFilters[dataFilter] = filters;
            }
        }

        return {
            limiters: limiters,
            filters: filters
        };
    },

    getConverter = function (limitExpr, filterExpr) {
        if (!compiledConverter[limitExpr]) {
            compiledConverter[limitExpr] = {};
        }
        var converter = compiledConverter[limitExpr][filterExpr];

        if (!converter) {
            converter = buildConverter(buildLimiter(limitExpr, filterExpr));
            compiledConverter[limitExpr][filterExpr] = converter;
        }

        return converter;
    },

    getConverterByElement = function ($input) {
        return getConverter(
                $input.attr('data-limit') || '',
                $input.attr('data-filter') || ''
        );
    },

    createHandler = function (_converter) {
        var focused = false,

        splice = function (s, index, howMany/*[,s1][, ..., sN]*/) {
            var args = Array.prototype.slice.call(arguments, 1),
            array = s.split('');
            Array.prototype.splice.apply(array, args);
            return array.join('');
        },

        onKeyup = function (e) {
            var isEnter = e.which === 13;
            if (!isEnter) {
                return;
            }

            var input = e.currentTarget,
            $input = $(input);

            if (!$input.is('input,textarea'))
                return;

            // IMEでENTERキーで確定時の処理
            var converter = _converter || getConverterByElement($input),
            pos,
            curVal = converter.filter('unmask', $(e.currentTarget).val()),
            newVal = converter.limitValue(curVal),
            modifierKeyPressed = e.ctrlKey || e.altKey || e.metaKey;

            if (curVal !== newVal) {
                pos = getCursorPosition(e.currentTarget);
                $input.val(newVal);
                setCursorPosition($input, pos);
            }
        },

        onCompositionStart = function(e) {
            eventLogHandler(e);
        },

        onCompositionEnd = function(e) {
            eventLogHandler(e);
            $(e.currentTarget).data("compositioningend", true);
        },

        // IMEで確定時用の処理
        onInput = function(e){
            eventLogHandler(e);
            var input = e.currentTarget, $input = $(input);

            if ($input.data("compositioningend")){
                $input.data("compositioningend", false);
            } else {
                return;
            }

            // IMEでENTERキーで確定時の処理
            var converter = _converter || getConverterByElement($input),
            pos,
            curVal = converter.filter('unmask', $(e.currentTarget).val()),
            newVal = converter.limitValue(curVal);

            if (curVal !== newVal) {
                pos = getCursorPosition(e.currentTarget);
                $input.val(newVal);
                setCursorPosition($input, pos);
            }
        },

        onKeypress = function (e) {
            // // カーソルキー XXXX
            // if (e.which >= 33 && e.which <= 40) {
            //   return;
            // }

            var modifierKeyPressed = e.ctrlKey || e.altKey || e.metaKey;

            var input = e.currentTarget,
            $input = $(input);

            if (!$input.is('input,textarea'))
                return;

            if (!modifierKeyPressed && (e.which >= 32 && e.which <= 122)) {
                var selection = getRange(input),
                converter = _converter || getConverterByElement($input),
                ch = String.fromCharCode(e.which),
                prevVal = $(e.currentTarget).val(),
                curVal = converter.filter('unmask', splice(prevVal, selection.start, selection.end - selection.start, ch)),
                newVal = converter.limitValue(curVal);

                // console.log('###(press)', 'type:', e.type, 'prv:', prevVal, 'new:', newVal,
                //             'code:', e.which, 'ch:', ch, 'pos:', pos, 'p:', curVal);

                if (curVal !== newVal) {
                    return false;
                }
            }
        },

        onceMouseUp = function (e) {
            var $input = $(e.currentTarget);
            if ($input.is(':focusable')) {
                $input.select();
                e.preventDefault();
            }
        },

        onMouseUp = function (e) {
            if (focused) {
                $(e.currentTarget).select();
                e.preventDefault();
                focused = false;
            }
        },

        /**
         * Paste時のハンドラ。
         */
        onPaste = function (e) {
            var input = e.currentTarget,
            $input = $(input);

            if (!$input.is('input,textarea'))
                return;

            var converter = _converter || getConverterByElement($input);
            // 遅延するのはpasteイベントがChromeではペーストす
            // る直前のタイミングで行われるため。
            _.defer(function(){
                var value = $input.val(),
                filtered = converter.filterValue(value),
                masked = converter.filter('unmask', filtered);

                $input.data('inputfilter.previousValue', masked);
                console.log('onPaste', input.id, input.className, value, filtered, masked);
                if (!$input.is(".bootstrap-select-searchbox >")) {
                    // TODO bootstrap-select liveSearch
                    $input.val(masked);
                    vent.trigger('inputlimiter:change');
                }
            });
        },

        onFocus = function (e) {
            var input = e.currentTarget,
            $input = $(input);

            if (!$input.is('input,textarea'))
                return;

//          $input.trigger('focus.placeholder');

            var converter = _converter || getConverterByElement($input),
            value = $input.val();
            value = converter.filter('unmask', value);
            $input.val(value).select();
            focused = true;
            $input.select();
        },

        onBlur = function (e) {
            var input = e.currentTarget,
            $input = $(input);

            if (!$input.is('input,textarea'))
                return;

//          $input.trigger('blur.placeholder');

            var converter = _converter || getConverterByElement($input),
            value = $input.val();
            value = converter.filterValue(value);
            $input.val(value);
        };


        return {
            onBlur: onBlur,
            onFocus: onFocus,
            onKeypress: onKeypress,
            onKeyup: onKeyup,
            onMouseUp: onMouseUp,
            onPaste: onPaste,
            onInput: onInput,
            onCompositionStart: onCompositionStart,
            onCompositionEnd: onCompositionEnd
        };
    },

    start = function () {
        var handler = createHandler(),
        selector = '[data-limit]:not(.bindInputlimiter),[data-filter]:not(.bindInputlimiter)';

        $(document).off('.inputlimiter')
        .on('compositionstart.inputlimiter', selector, handler.onCompositionStart)
        .on('compositionend.inputlimiter', selector, handler.onCompositionEnd)
        .on('input.inputlimiter', selector, handler.onInput)
        .on('keypress.inputlimiter', selector, handler.onKeypress)
        .on('keyup.inputlimiter', selector, handler.onKeyup)
        .on('focusin.inputlimiter', selector, handler.onFocus)
        .on('mouseup.inputlimiter', selector, handler.onMouseUp)
        .on('paste.inputlimiter', selector, handler.onPaste)
        .on('focusout.inputlimiter', selector, handler.onBlur);
        this._started = true;
    },

    stop = function () {
        $(document).off('.inputlimiter');
        this._started = false;
    },

    bindElement = function ($el, converter) {
        if (!$el.is('input,textarea')) {
            return;
        }

        var handler = createHandler(converter);
        $el.unbind('.inputlimiter')
        .bind('keypress.inputlimiter', handler.onKeypress)
        .bind('keyup.inputlimiter', handler.onKeyup)
        .bind('mouseup.inputlimiter', handler.onMouseUp)
        .bind('focusin.inputlimiter', handler.onFocus)
        .bind('focusout.inputlimiter', handler.onBlur)
        .addClass('bindInputlimiter');
    };

    $.fn.inputlimiter = function (method, options) {
        if (typeof method === 'object') {
            method = undefined;
            options = method;
        }

        if (typeof method === 'undefined') {
            method = 'new';
        }

        var opts = $.extend({}, $.fn.inputlimiter.defaults, options),

        $elements = $(this);

        function val($element, value) {
            if (arguments.length === 1) {
                if ($element.is('input,textarea')) {
                    return $element.val();
                } else {
                    return $element.text();
                }
            } else {
                if ($element.is('input,textarea')) {
                    return $element.val(value);
                } else {
                    return $element.text(value);
                }
            }
        }

        var returnValue;
        _.some($(this), function (element) {
            var $element = $(element),
            converter = getConverterByElement($element);

            if (method === 'update') {
                val($element, converter.filterValue(val($element)));
            } else if (method === 'get') {
                returnValue = converter.value(val($element));
                return true;
            } else if (method === 'set') {
                val($element, converter.filterValue(options));
            } else if (method === 'destroy') {
                $element.unbind('.inputlimiter').removeClass('bindInputlimiter');
            } else if (method === 'new') {
                bindElement($element, converter);
            }
        });
        return returnValue;
    };

    $.fn.inputlimiter.defaults = {
    };

    $.inputlimiter = {
        Filters: Filters,
        mask: function (value, options) {
            value = (value == null) ? '' : String(value);
            var converter = getConverter(options.limit, options.filter);
            return converter.filterValue(value);
        },
        unmask: function (value, options) {
            value = (value == null) ? '' : String(value);
            var converter = getConverter(options.limit, options.filter);
            return converter.value(value);
        },
        start: start,
        stop: stop
    };
})(jQuery);
/**
 * ORIGINAL messages_ja.js
 */
var clmsg = {

    cl_echoback: '入力項目が間違っています。',
    cl_no_data: '検索結果は0件です。',

    // http エラー
    cl_http_status_xxx: '障害が発生しました。ご迷惑をお掛けしています。システム管理者へご連絡下さい。',
    cl_http_status_0: 'サーバーに接続できませんでした。',
    cl_http_status_unauthorized: 'ログイン情報が確認できませんでした。しばらくお待ち下さい。',

    // サーバ処理系のエラーメッセージ
    cl_sys_error: '障害が発生しました。ご迷惑をお掛けしています。システム管理者へご連絡下さい。',
    cl_sys_epageargs: 'パラメータ不正です。',

    cl_sys_db_nomem: 'メモリ確保に失敗しました。',
    cl_sys_db_error: 'システムエラー：データベースアクセスに失敗しました。',
    cl_sys_db_access: 'システムエラー：データベースアクセスに失敗しました。',
    cl_sys_db_other: '別のユーザによってDBが更新されました。',

    cl_sys_fread: 'ファイルの読み込みに失敗しました。',
    cl_sys_fwrite: 'ファイルの書き込みに失敗しました。',

    // 処理区分系
    cl_rtype_r_upd: '登録できないデータが含まれています。',
    cl_rtype_r_del: '削除できないデータが含まれています。',

    cl_rtype_upd: '登録できないデータです。',
    cl_rtype_del: '削除できないデータです。',

    cl_rtype_add_confirm: '新規登録しました。',
    cl_rtype_edit_confirm: '変更を保存しました。',
    cl_rtype_upd_confirm: '登録しました。',
    cl_rtype_del_confirm: '削除しました。',
    cl_rtype_delcancel_confirm: '削除取消しました。',
    cl_rtype_send_confirm: '送信しました。',

    cl_rtype_add_confirm_chk: '新規登録を行います。よろしいですか。',
    cl_rtype_upd_confirm_chk: '登録を行います。よろしいですか。',
    cl_rtype_del_confirm_chk: '削除してよろしいですか？',
    cl_rtype_delcancel_confirm_chk: '削除取消を行います。よろしいですか。',

    // その他頻出メッセージ
    cl_date_error: '開始期間と終了期間が反転しています。',
    cl_month_error: '開始年月と終了年月が反転しています。',
    cl_time_error: '開始時刻と終了時刻が反転しています。'
};

/**
 * Legacy compatible
 * @Deplicated
 */
_.extend(clmsg, {
    cl_email: 'メールアドレス形式をご確認ください。',
    cl_cng_IE: '印刷対応ブラウザではありません。ブラウザをInternetExplorerに変更してください。'
});
/**
 * ORIGINAL validator2.messages_ja.js
 */
if(window.clmsg == null){
    throw 'messages_ja.js required!!!';
}

/**
 * clutil.validator2.js 使用メッセージ定義
 */
_.extend(clmsg, {

    // 必須チェック
    cl_required                     : '入力してください。',
    cl_select_required              : '選択してください。',
    cl_its_required                 : '{0}を入力してください。',
    cl_its_select_required          : '{0}を選択してください。',

    // 文字列長チェック
    cl_length_noteq                 : '{0}文字で入力してください。',
    cl_length_short1                : '{0}文字以上で入力してください。',
    cl_length_short2                : '{0}～{1}文字で入力してください。',
    cl_length_long1                 : '{0}文字以下で入力してください。',
    cl_length_long2                 : '{0}～{1}文字で入力してください。',
    cl_its_length_short0            : '{0}が短すぎます。{1}文字で入力してください。',
    cl_its_length_short1            : '{0}が短すぎます。{1}文字以上で入力してください。',
    cl_its_length_short2            : '{0}が短すぎます。{1}～{2}文字で入力してください。',
    cl_its_length_long0             : '{0}が長すぎます。{1}文字で入力してください。',
    cl_its_length_long1             : '{0}が長すぎます。{1}文字以下で入力してください。',
    cl_its_length_long2             : '{0}が長すぎます。{1}～{2}文字で入力してください。',

    // 数値入力評価
    cl_num_inval                    : '数値で入力してください。',
    cl_num_declen_overflow          : '小数{0}桁以下で入力してください。',
    cl_num_nodecpt                  : '小数点以下の入力はできません。',
    cl_num_outrange                 : '{0}～{1}の範囲で入力してください。',
    cl_num_less                     : '{0}以上の値を入力してください。',
    cl_num_more                     : '{0}以下の値を入力してください。',
    cl_its_num_inval                : '{0}は数値で入力してください。',
    cl_its_num_declen_overflow      : '{0}は小数{1}桁以下で入力してください。',
    cl_its_num_nodecpt              : '{0}は小数点以下の入力はできません。',
    cl_its_num_outrange             : '{0}は{1}～{2}の範囲で入力してください。',
    cl_its_num_less                 : '{0}は{1}以上の値を入力してください。',
    cl_its_num_more                 : '{0}は{1}以下の値を入力してください。',

    // 日付評価
    cl_date_inval                   : '日付形式が誤っています。',
    cl_month_inval                  : '日付形式が誤っています。',
    cl_date_max                     : '日付が範囲外です。{0}前の日付を入力してください。',
    cl_date_min                     : '日付が範囲外です。{0}後の日付を入力してください。',
    cl_date_range                   : '日付が範囲外です。{0}～{1}の日付を入力してください。',
    cl_its_date_inval               : '{0}の日付形式が誤っています。',
    cl_its_month_inval              : '{0}の日付形式が誤っています。',
    cl_its_date_max                 : '{0}の日付が範囲外です。{1}前の日付を入力してください。',
    cl_its_date_min                 : '{0}の日付が範囲外です。{1}後の日付を入力してください。',
    cl_its_date_range               : '{0}の日付が範囲外です。{1}～{2}の日付を入力してください。',

    // 時刻評価
    cl_hhmm_inval                   : '時刻形式が誤っています。',
    cl_hhmm_max                     : '時刻が範囲外です。{0}前の時刻を入力してください。',
    cl_hhmm_min                     : '時刻が範囲外です。{0}後の時刻を入力してください。',
    cl_hhmm_range                   : '時刻が範囲外です。{0}～{1}の時刻を入力してください。',
    cl_its_hhmm_inval               : '{0}の時刻形式が誤っています。',
    cl_its_hhmm_max                 : '{0}の時刻が範囲外です。{0}前の時刻を入力してください。',
    cl_its_hhmm_min                 : '{0}の時刻が範囲外です。{0}後の時刻を入力してください。',
    cl_its_hhmm_range               : '{0}の時刻が範囲外です。{0}～{1}の時刻を入力してください。',

    // 選択評価
    cl_select_less                  : '{0}個以上選んでください。',
    cl_sleect_more                  : '{0}個以下選んでください。',
    cl_select_range                 : '{0}～{1}個まで選んでください。',
    cl_its_select_less              : '{0}は{1}個以上選んでください。',
    cl_its_sleect_more              : '{0}は{1}個以下選んでください。',
    cl_its_select_range             : '{0}は{1}～{2}個まで選んでください。',

    // フォーマット評価
    cl_invalid_format               : '形式が誤っています。',
    cl_its_invalid_format           : '{0}の形式が誤っています。入力をご確認ください。',
    cl_kana_k                       : '全角カタカナで入力してください。',
    cl_kana_hk                      : '半角ｶﾀｶﾅで入力してください。',
    cl_kana_h                       : 'ひらがなで入力してください。',
    cl_zen                          : '全角文字列で入力してください。',
    cl_han                          : '半角文字列で入力してください。',
    cl_ascii                        : '英数文字列で入力してください。',
    cl_alnum                        : '英数文字列で入力してください。',
    cl_tel                          : '電話番号形式をご確認ください。',
    cl_tel_hyphen                   : '電話番号形式をご確認ください。ハイフン区切りを付けて入力してください。',
    cl_tel_nohyphen                 : '電話番号形式をご確認ください。ハイフン区切りは不要です。',
    cl_postal                       : '郵便番号形式をご確認ください。',
    cl_postal_hyphen                : '郵便番号形式をご確認ください。ハイフン区切りを付けて入力してください。',
    cl_postal_nohyphen              : '郵便番号形式をご確認ください。ハイフン区切りは不要です。',
    cl_url                          : 'URL形式をご確認ください。',
    cl_email                        : 'メールアドレス形式をご確認ください。',
    cl_its_kana_k                   : '{0}は全角カタカナで入力してください。',
    cl_its_kana_hk                  : '{0}は半角ｶﾀｶﾅで入力してください。',
    cl_its_kana_h                   : '{0}はひらがなで入力してください。',
    cl_its_zen                      : '{0}は全角文字列で入力してください。',
    cl_its_han                      : '{0}は半角文字列で入力してください。',
    cl_its_ascii                    : '{0}は英数文字列で入力してください。',
    cl_its_alnum                    : '{0}は英数文字列で入力してください。',
    cl_its_tel                      : '{0}は電話番号形式で入力してください。',
    cl_its_tel_hyphen               : '{0}は電話番号形式ではありません。ハイフン区切りを付けて入力してください。',
    cl_its_tel_nohyphen             : '{0}は電話番号形式ではありません。ハイフン区切りは不要です。',
    cl_its_postal                   : '{0}は郵便番号形式をご確認ください。',
    cl_its_postal_hyphen            : '{0}は郵便番号形式をご確認ください。ハイフン区切りを付けて入力してください。',
    cl_its_postal_nohyphen          : '{0}は郵便番号形式をご確認ください。ハイフン区切りは不要です。',
    cl_its_url                      : '{0}はURL形式ではありません。入力をご確認ください。',
    cl_its_email                    : '{0}はメールアドレス形式ではありません。入力をご確認ください。',

    // ファイル名評価
    cl_invalid_fnamechar            : '記号「<>:*?"/|」はファイル名に使用できません。',
    cl_its_invalid_fnamechar        : '{0}: 記号「<>:*?"/|」はファイル名に使用できません。',
    cl_reserved_fname               : '「{0}」はファイル名に使用できません。',
    cl_its_reserved_fname           : '{0}：「{1}」はファイル名に使用できません。',

    // サジェスト
    cl_suggest_inval                : '適合する選択肢が見つかりません。選択肢の中から選んでください。',
    cl_its_suggest_inval            : '{0} は適合する選択肢が見つかりません。選択肢の中から選んでください。'
});
/**
 * デモ版 clcom 独自プロパティ
 */
var clcom = {
    _preventConfirm: true,              // true: ページを離れるときに「このサイトを離れてもよろしいですか？」を出さない。

    config: {
        enableCheckSourceId: false,     // true:画面遷移元の画面コードチェックを行う。
        disableAuthTokenCheck: true,    // true:AuthTokenチェックしない、false:AuthTokenCheckする。
        disableAbortAction: true,       // true:致命的エラー発生時にエラーページへ飛ばず留まる。false:エラーページジャンプする
        disableCacheBuster: false,      // true:キャッシュバスターOFF、false:キャッシュバスターON
        preventLoadHtmlCache: false,    // clutil.loadHtml キャッシュコントロール。true:キャッシュ阻害する、false:阻害しない
        enableEenterFocusMode: true,    // Enter キーフォーカス制御。true:する、false:しない
        enableDefaultContextMenu: true, // ブラウザデフォルトの右クリックコンテキストメニュー設定、true:有効、false:無効

        // Ajax ローカル API で駆動させるためのホスト名一覧
        // clcom.isLocalHost() で true を返すようになり、テスト仕様 config で駆動するようになる。
        demoHosts: [
            'sevenstarbox:8080',
            'sevenstarbox.suri.co.jp:8080',
            '10.1.3.56:8080'
        ],

        enableFixAbsPath: true,         // 絶対パス補正、true:する、false:しない
        absPathPrefix: function(){
            // ＜絶対パスプレフィックス推測＞
            // 「system」階層がある場合は、「system」階層より上位をプレフィックスとする。
            // 「system」階層が無い場合は、パス全体をプレフィックスとする。（ログインページを想定）
            var p = purl();
            var file = p.data.attr.file;
            var dirs = _.compact(p.data.attr.directory.split('/'));

            var sysIndex = dirs.indexOf('system');
            var prefixPathes;
            if(sysIndex < 0){
                // 「system」階層が無い場合は、dirs そのもので絶対パスプレフィックスとする。
                // 例：ログインページなどを想定。
                prefixPathes = dirs;
            }else{
                // 「system」階層が存在する ⇒ 「system」より前をプレフィックスとする。
                prefixPathes = dirs.splice(0,sysIndex);
            }
            return (prefixPathes.length === 0) ? '' : '/' + prefixPathes.join('/');
        }(),

        loginPagePath: '/login.html',                               // ログインページのパス - enableFixAbsPath[truee] ⇒ clstartでROOT補正
        topPagePath:   '/system/app/SMPLV001/SMPLV00101.html',      // トップページのパス   - enableFixAbsPath[truee] ⇒ clstartでROOT補正

        funcCodeRegEx: /^[A-Z]{5}\d{5}$/,       // 画面機能コード判定用正規表現
        cmHeadPropName: {                       // 共通ヘッダプロパティ名
            req: 'head',
            rsp: 'head'
        },

        storagePrefix: 'clcom.',                // Storage キープレフィックス
        authTokenKey: 'auth_token',             // 認証キー名 (cookie)
        authTokenChkURI: '',                    // 認証キーチェック URI - enableFixAbsPath[true] ⇒ clstartでROOT補正
        authTokenChkFail: true,	                // true: 認証キーチェック失敗させる。ログインページアクセスで失敗強制してセッションリソースを刈り取る
        loginURI : '/system/api/TMBLV10101/D01',        // ログイン処理 URI    - enableFixAbsPath[true] ⇒ clstartでROOT補正
        logoutURI: '/system/api/TMBLV00101/S01',        // ログアウト処理 URI  - enableFixAbsPath[true] ⇒ clstartでROOT補正
        uploaderURI: '/system/api/gs_cm_fileupload',    // ファイルアップロード URI - enableFixAbsPath[true] ⇒ clstart でROOT補正

        // ajax 通信 HTTP エラー検出に関する設定
        // HTTP エラーコード毎に定義する。ここに無いものは、共通応答ヘッダにエラー応答情報を入れて ajax 呼出元の fail 関数へ返す。
        httpErrorPages: {
            defaults: {
                path: '/err/error_occurred.html',   // 汎用エラーページパス
                message: 'cl_http_status_xxx',      // path uri パラメータに付加するメッセージまたはメッセージコード、省略時は uri パラメタ生成しない。
                    // 障害が発生しました。ご迷惑をお掛けしています。システム管理者へご連絡下さい。
                logout: false                       // true: ページ遷移時、ログアウト処理を伴う、false: ページ遷移のみ。default: false。
            },
            // 以下、HTTP エラー値ごとに定義する。プロパティは genericErrorPagePath 汎用エラーページコンフィグと同様。
            // ------------------------------------------------------------------------------
            // path プロパティを省略した場合は、汎用エラーページコンフィグ を適用する。絶対パス記述とする。（外部サイトでも可）
            // message プロパティを指定した場合は、URL パラメタにメッセージ内容を付加する。不要の場合は記述しないこと。
            // logout プロパティは、true: ページ遷移時、ログアウト処理を伴う、false: ページ遷移のみ。default: false。
            401: {
//                path: '/err/accessError.html',
                message: 'cl_http_status_unauthorized',
                    // ログイン情報が確認できませんでした。しばらくお待ち下さい。
                logout: true,
                description: 'Unauthorized'
            },
            500: {
//                path: '/err/systemError.html',
                description: 'Internal Server Error'
            },
            503: {
//                path: '/err/error_occurred.html',
                message: 'メンテナンス中のため利用できません。',
                description: 'Service Unavailable'
            }
        },

        // 全画面で必須なテンプレートがあればここに記述
        // 実体: system/template/template_alert_message.html
        template: [],

        /*
         * パス情報
         * ['apiRoot', 'appRoot', 'templateRoot', 'loginPagePath', 'topPagePath', 'loginURI', 'logoutURI']
         */
        urlRoot: location.protocol + "//" + location.host,      // enableFixAbsPath[true] ⇒ clstartでROOT補正
        apiRoot: '/system/api',                 // enableFixAbsPath[true] ⇒ clstartでROOT補正
        appRoot: '/system/app',                 // enableFixAbsPath[true] ⇒ clstartでROOT補正
        templateRoot: '/system/template',       // enableFixAbsPath[true] ⇒ clstartでROOT補正

        apiRootMatcher: null,                   // clstart で RegExp を設定: new RegExp('^' + clcom.config.apiRoot)
        appRootMatcher: null,                   // clstart で RegExp を設定: new RegExp('^' + clcom.config.appRoot)

        /*
         * View Configurations
         */
        modal: {
            // View Configuration[modal] セクション
            enableEnterFocusMode: true,         // true: モーダルコンテント内部に閉じたフォーカス制御 enterFocusMode を適用する
            enablePreferredFocus: true          // true: モーダル表示 shown の前処理で、自動で初期フォーカスをセットする。
        },

        debugFlg: true                          // true:debug、false;release
    },

    /**
     * 製品情報
     */
    productInfo: {
        uuid: "__clcom_productInfo_uuid__",     // % cat /proc/sys/kernel/random/uuid
        packageName: 'Astro Blaster Solutions',
        serviceName: 'ABUI & CLCOM'
    },

    /**
     * 閾値集
     */
    limits: {
        // 最小日付(固定)
        min_date: 19900102,
        minDate: new Date(1990,(1-1),2,0,0,0,0),        // min_date の Date オブジェクト

        // 最終日付(固定)
        max_date: 20701231,
        maxDate: new Date(2070,(12-1),31,23,59,59,999), // max_date の Date オブジェクト

        // 営業時間帯
        s_time: 700,
        e_time: 2300,

        // INT32の MIN/MAX
        int32Min: -2147483648,  //0x80000000,
        int32Max:  2147483647,  //0x7fffffff,

        // INT64の MIN/MAX
//      int64Min: -9223372036854775808, //0x8000000000000000,
//      int64Max:  9223372036854775807  //0x7fffffffffffffff
        // JavaScript において、Number 型は double に相当。
        // ±1 算術で正確な値を出せる範囲の Min-Max を定義値としておく。
        int64Min: -1 * Math.pow(2,52),
        int64Max:      Math.pow(2,52)

        // TODO: その他 Limits 系定数は、ココの Namespace に置いておきたい。
    },

    /**
     * 画面機能コードから画面 URL を推測する。
     * @param funcCode 画面コード
     * @param altAppRootPath app パス（省略可、通常は省略）
     * @return 指定画面コードを呼び出す URL
     */
    buildScreenUrl: function(funcCode, altAppRootPath){
        // 標準: /system/app/<画面コード>/<画面コード>.html
        //        clutil.fmt('{0}/{1}/{1}.html',clcom.config.appRoot, funcCode);
        // Rzp版:/system/app/<画面主コード>/<画面コード>.html
        //        clutil.fmt('/{0}/{1}/{2}.html', pathes.join('/'), primaryCode, funcCode);
        var primaryCode = funcCode.substring(0,8);
        if(_.isEmpty(altAppRootPath)){
            return clutil.fmt('{0}/{1}/{2}.html', clcom.config.appRoot, primaryCode, funcCode);
        }else{
            var pathes = _.chain([ clcom.config.absPathPrefix, altAppRootPath.split('/') ]).flatten().compact().value();
            return clutil.fmt('/{0}/{1}/{2}.html', pathes.join('/'), primaryCode, funcCode);
        }
    },

    /**
     * リクエスト共通ヘッダデフォルトを生成する
     */
    getDefaultCmReqHead: function(){
        var funcCd = clcom.pageId || '';
        var userData = clcom.getUserData();
        var head = (userData != null) ? _.pick(userData, 'user_id') : {};
        if(clcom.config.funcCodeRegEx.test(funcCd)){
            head.view_id = funcCd;
        }
        return head;
    },
    /**
     * レスポンス共通ヘッダ前処理
     */
    preProcessCmRspHead: function(rsp){
        var rspHead = rsp[clcom.config.cmHeadPropName.rsp];

        // 運用日を更新する
        // ope_iymd 値チェックは内部関数 setOpeDate で行っているので、素のまま入れる。
        if(rspHead){
            clcom.setOpeDate(rspHead.ope_iymd);
        }
    },

    /**
     * URLパスから当該ページIDを推測する。
     * @param pathname
     * @return pageId
     */
    getPageIdFromPath: function(pathname){
        pathname = pathname || location.pathname;
        var pageId = (pathname.split('/').pop() || '').split('.').shift() || '';
        if(_.isEmpty(pageId)){
            pageId = '/';
        }
        return pageId;
    },

    /**
     * 初期化データ取得項目
     * resId	: 取得方法(POST 先リソースID)
     * once		: サーバ取得頻度 - true:ログインセッション間で一度きり、true:毎ページ遷移毎
     * cacheable: 一時キャッシュ。webストレージと別にメモリキャッシュする。true:キャッシュを保持する、false:保持しない。
     * 			  マスタ検索日違いのデータ保持などで利用することを想定。
     * buildReq	: リクエストデータビルダー
     * success	: サーバ応答成功時のアクション（ストレージへ保存する方法）
     */
    getIniExtra: {
        'typelist': {
            description: '区分リスト',
            resId: 'TMCN_90001/S01',
            storedkeys: [ 'typelist' ],
            once: true,
            buildReq: function(){
                var req = {
                    head: { view_id: 'TMCN_900' }
                };
                return req;
            },
            success: function(data, me){
                clcom.saveStorageData('typelist', data.results);
            }
        },
        'prefectures': {
            description: '都道府県',
            resId: 'TMCN_00101/S01',
            storedkeys: [ 'prefectures' ],
            once: true,
            buildReq: function(){
                var req = {
                    head: { view_id: 'TMCN_001' }
                };
                return req;
            },
            success: function(data, me){
                clcom.saveStorageData('prefectures', data.results);
            }
        },
        'municipalities': {
            description: '市区町村:全件',
            resId: 'TMCN_00101/S02',
            storedkeys: [ 'municipalities' ],
            once: true,
            buildReq: function(){
                var req = {
                    head: { view_id: 'TMCN_001' },
                    prefecture_id: 0,
                    city_name: ''
                };
                return req;
            },
            success: function(data, me){
                clcom.saveStorageData('municipalities', data.results);
            }
        }
    },

    /**
     * オプショナルな初期化タスク。
     */
    getIniOption: {
//        'ticket': {
//            description: '更新チケット取得',
//            resId: 'shared/index/update_csrf_token',
//            buildReq: function(){
//                var req = {
//                    head: { view_id: 'TMCN_900' }
//                };
//                return req;
//            },
//            success: function(data, me){
//                me.ticket = data.ticket || data.head.ticket;
//            }
//        }
    },

    // ------------------------------------------------------------
    // ストレージアクセスメソッド

    /**
     * 区分リストを取得
     * @param predicate 絞込条件、省略時は全部返す。
     */
    getTypeList: function(predicate) {
        var list = clcom.getStorageData('typelist');
        if(predicate){
            list = _.filter(list, predicate);
        }
        return list;
    },
    /**
     * 区分検索
     */
    findTypeBy: function(predicate){
        if(predicate == null){
            throw 'clcom.findTypeBy(predicate): be sure spacify an argument "predicate".';
        }
        var list = clcom.getTypeList(predicate);
        if(list.length === 0){
            console.warn('findTypeBy(' + JSON.stringify(predicate) + '): not found.');
        }else if(list.length > 1){
            console.warn('findTypeBy(' + JSON.stringify(predicate) + '): ' + list.length + ' items duplicated.');
        }else{
            return list[0];
        }
    },
    /**
     * 都道府県リスト取得
     */
    getPrefectures: function(predicate){
        var list = clcom.getStorageData('prefectures');
        if(predicate){
            list = _.filter(list, predicate);
        }
        return list;
    },
    /**
     * 都道府県検索
     */
    findPrefectureBy: function(predicate){
        if(predicate == null){
            throw 'clcom.findPrefectureBy(predicate): be sure spacify an argument "predicate".';
        }
        var list = clcom.getPrefectures(predicate);
        if(list.length === 0){
            console.warn('findPrefectureBy(' + JSON.stringify(predicate) + '): not found.');
        }else if(list.length > 1){
            console.warn('findPrefectureBy(' + JSON.stringify(predicate) + '): ' + list.length + ' items duplicated.');
        }else{
            return list[0];
        }
    },
    /**
     * 都道府県リスト取得
     */
    getMunicipalities: function(predicate){
        var list = clcom.getStorageData('municipalities');
        if(predicate){
            list = _.filter(list, predicate);
        }
        return list;
    },
    /**
     * 都道府県検索
     */
    findMunicipalityBy: function(predicate){
        if(predicate == null){
            throw 'clcom.findMunicipalityBy(predicate): be sure spacify an argument "predicate".';
        }
        var list = clcom.getMunicipalities(predicate);
        var list = clcom.getPrefectures(predicate);
        if(list.length === 0){
            console.warn('findMunicipalityBy(' + JSON.stringify(predicate) + '): not found.');
        }else if(list.length > 1){
            console.warn('findMunicipalityBy(' + JSON.stringify(predicate) + '): ' + list.length + ' items duplicated.');
        }else{
            return list[0];
        }
    },

    // ------------------------------------------------------------
    // ストレージキャッシュ物のデータがひと通り揃えた後に呼ばれる。
    // clcom にキャッシュプロパティ等を作る等の処理を記述する。
    preInit: function(){
        ; // ここに、ストレージデータ等から clcom.anyProperty へキャッシュを乗せるなどの処理を行う。
    }
};
/**
 * ORIGINAL clcom.js
 *
 * Namespace: clcom.*
 *
 * [概要]
 * clcom.* は下記機能を有します。
 * ・コンフィギュレーション
 * ・製品情報
 * ・webstorage アクセス I/F
 * ・ページ遷移 I/F
 *   ・指定ページへ遷移  : clcom.pushPage(args)
 *   ・前ページへ戻る   : clcom.popPage();
 *   ・ホームへ戻る        : clcom.gohome();
 *   ・ログアウト     : clcom.logout();
 * ・ページ情報（タイトル、機能ID、...etc）
 * ・静的定数定義値
 *
 * [注意]
 * ・製品固有な定義値は、clcom-**.js に置くこと。(BI:clcom-an.js, Dashboad:clcom-da.js)
 * ・View 機能は含まないこと。--- clutil.view.* ネームスペース下で拡張するように。
 */
var clcom = _.extend({}, clcom);

window.onerror = function (msg, file, line, column, err) {
    alert('予期せぬエラーが発生しました。ご迷惑をお掛けしています。システム管理者へご連絡下さい。\n'+ msg + '(' + (file||'???') + ':' + line + ')');
    clutil.unblockUI(true);
};

(function() {

    /** ローカルストレージAPI: store.js */
    var locStore = window.store;

    /** セッションストレージAPI */
    var sesStore = {
        /**
         * ストレージインスタンス（セッションストレージ）
         */
        storage: function(name){
            var s = window[name];
            if(s == null){
                throw 'session storage not supported!!!';
            }
            return s;
        }('sessionStorage'),
        /**
         * 値を保存する
         * @param key キー
         * @param val 保存する値
         */
        set: function(key,val){
            var storage = this.storage;
            if (val === undefined) { return this.remove(key) }
            storage.setItem(key, this._serialize(val))
            return val
        },
        /**
         * 値を取得する。
         * @param key キー
         * @param defaultVal キーが存在しない場合に返す値
         * @param 値
         */
        get: function(key, defaultVal){
            var storage = this.storage;
            var val = this._deserialize(storage.getItem(key));
            return (val === undefined ? defaultVal : val);
        },
        /**
         * キーの存在検査する。
         * @param key 検査キー
         * @return true:値が存在する、false:存在しない
         */
        has: function(key){
            var storage = this.storage;
            return storage.hasOwnProperty(key);
        },
        /**
         * キーを削除する。
         * @param keys 値、または配列でキー名を与える
         */
        remove: function(keys){
            keys = (keys instanceof Array) ? keys : (keys != null) ? [ keys ] : [];
            var storage = this.storage;
            for(var i=0; i<keys.length; i++){
                var key = keys[i];
                storage.removeItem(key);
            }
        },
        /**
         * ストレージに保持した全キーについて逐次参照する。
         * @param callback 参照関数 - function(key, val)
         */
        forEach: function(callback){
            var storage = this.storage;
            for (var i=0; i<storage.length; i++) {
                var key = storage.key(i), val = this.get(key);
                callback(key, val);
            }
        },
        _serialize: function(value){
            return JSON.stringify(value);
        },
        _deserialize: function(value){
            if(typeof value != 'string'){ return undefined; }
            try { return JSON.parse(value); }
            catch(e) { return value || undefined; }
        }
    };

    /** 共通情報 */
    _.extend(clcom, {

        /**
         * 指定キーによるストレージデータを取得する。
         */
        getStorageData: function(key){
            return locStore.get(clcom.config.storagePrefix + key);
        },
        /**
         * ストレージに指定キーで指定データを保存する。
         */
        saveStorageData: function(key, data){
            locStore.set(clcom.config.storagePrefix + key, data);
        },
        /**
         * 指定キーのストレージデータを削除する。
         */
        removeStorageData: function(key){
            locStore.remove(clcom.config.storagePrefix + key);
        },
        /**
         * ストレージに値の有無を確認する。
         */
        hasStorageKey: function(key, prefix) {
            var fixedKey;
            if (_.isString(prefix)) {
                fixedKey = prefix + key;
            } else {
                fixedKey = clcom.config.storagePrefix + key;
            }
            if (_.isEmpty(fixedKey)) {
                // キーが空欄 or null or undefined
                return false;
            }
            return locStore.has(fixedKey);
        },
        /**
         * ストレージをクリアする
         * プレフィックス 'clcom.' から始まるキーが削除対象。
         * ただし、ユーザ情報は (args.removeUserData === true) 明示指定しない限り保護される。
         *
         */
        clearStorage: function(args){
            // クリア除外キー
            var savingKeys = {};
            if(!(args && args.removeUserData === true)){
                // ログイン情報をストレージ - 引数 args.removeUserData で true 明示しない場合は保護対象。
                savingKeys[clcom.config.storagePrefix + 'userdata'] = true;
            }

            // キー一覧からストレージをクリアしていく。
            var regEx = new RegExp('^' + clcom.config.storagePrefix.replace(/\.*$/, '') + '\\.');
            var delKeys = [];
            locStore.forEach(_.bind(function(k,v){
                if(!regEx.test(k)){
                    return;
                }
                if(savingKeys[k]){
                    return;
                }
                // 'this' is delKeys
                this.push(k);
            },delKeys));
            if(!_.isEmpty(delKeys)){
                locStore.remove(delKeys);
            }
        },

        /** ブラウザキャッシュ防止用キーを取得 */
        getAntiCacheKey: function(){
            var seed = '';
            if(clcom.config.disableCacheBuster){
                return seed;
            }
            if(clcom.productInfo){
                seed = clcom.productInfo.uuid;
            }
            if(seed.length > 12){
                // 末尾 12 桁
                seed = seed.substr(-12,12);
            }
            return seed;
        },

        /** 当該ページのID */
        pageId: clcom.getPageIdFromPath(),

        // 当該ページのタイトル
        pageTitle: document.title,  // DBから取得するメニュー情報から現在ページのタイトルを取るのが望ましい

        // 画面遷移パラメタ(clcom定義の下で設定しています)
        pageArgs: undefined,

        // popPage でもどってきた場合、pushPage で保存されたデータが設定される
        pageData: undefined,

        // popPage でもどってきた場合、どこから戻ってきたを設定(ID)
        dstId: undefined,

        // popPage でもどってきた場合、どこから戻ってきたを設定(URL)
        dstUrl: undefined,

        // popPage でもどってきた場合、リターン値が設定される
        returnValue: undefined,

        // 遷移前ページID
        srcId: undefined,

        // 遷移前ページURL
        srcUrl: undefined,

        // 運用日
        ope_date: 0,

        /**
         * 運用日を保存する。
         * @param {Integer} 8桁の運用日
         */
        setOpeDate: function(ope_date) {
            if(!_.isFinite(ope_date)){
                console.warn('Invalid ope_date: '+ ope_date);
                return;
            }
            ope_date = parseInt(ope_date, 10);
            if(this.ope_date === ope_date){
                return;
            }
            console.info('ope_date changed: ' + this.ope_date + ' -> ' + ope_date);

            var key = 'opedate';
            if(ope_date > 0){
                clcom.saveStorageData(key, ope_date);
            }else{
                clcom.removeStorageData(key);
            }
            clcom.ope_date = ope_date;
            if(clutil && clutil.mediator){
                // イベント中継サポートしている場合は運用日変更イベントを発火させる
                clutil.mediator.trigger('clcom.ope_date:change', ope_date);
            }
        },

        /**
         * 運用日をローカルストレージから取得する。
         */
        getOpeDate: function() {
            var key = clcom.config.storagePrefix + 'opedate';
            return locStore.get(key);
        },

        /**
         * ブラウザの状態を取得する         TODO: 撤廃したい
         */
        onMobile    : 0,
        onPC        : 1,

        getAgent: function() {
            var agent = navigator.userAgent;

            if(agent.search(/iPhone/) != -1) {
                return clcom.onMobile;
            } else if(agent.search(/iPad/) != -1) {
                return clcom.onMobile;
            } else if(agent.search(/Android/) != -1) {
                return clcom.onMobile;
            } else {
                return clcom.onPC;
            }
        },

        /**
         * IE かどうかを判定する。ただし、Edge の場合は false (not IE)を返す。
         * @return true: IE、false: その他のブラウザ
         */
        chkIEAgent: function(validator) {
            console.warn('@Deplicated clcom.chkIEAgent(): use clcom.isLegacyIE()');
            // ブラウザチェック
            var userAgent = window.navigator.userAgent.toLowerCase();
            // IE以外の場合はエラー
            if(userAgent.match(/msie/) || userAgent.match(/trident/) ) {
                return true;
            }
            if (validator != null) {
                validator.setErrorInfo({_eb_: clmsg.cl_cng_IE});        // TODO: 削除予定
            }

            return false;
        },

        /**
         * 古い IE かどうかを判定する。
         * @param olderVer 指定のIEバージョン以下かどうかを指定。省略時は単に IE かどうかを判定する。
         * @return IE11(含む)以前の場合は true を返す。それ以外は false を返す。
         */
        isLegacyIE: function(olderVer){
            var ua = window.navigator.userAgent.toLowerCase();
            var anyIE = _.first(ua.match(/trident/));
            if(anyIE == null){
                return false;
            }
            if(olderVer == null){
                return true;
            }
            var ieVer = 11;
            var legacyIE = _.first(ua.match(/msie\ [0-9]*/));
            if(legacyIE != null){
                ieVer = parseInt(legacyIE.replace(/msie\ /,''), 10);
            }
            return (ieVer <= olderVer);
        },

        /**
         * ローカルホスト判定
         * @return true:ローカルホスト
         */
        isLocalHost: function(){
            var hostname = location.hostname;
            if(hostname == 'localhost' || hostname == '127.0.0.1'){
                return true;
            }
            if(!_.isEmpty(clcom.config.demoHosts)){
                var port = location.port;
                if(!_.isEmpty(port)){
                    hostname += ':' + port;
                }
                return _.contains(clcom.config.demoHosts, hostname);
            }
            return false;
        },

        /**
         * ユーザ情報をwebストレージに保存する。
         */
        setUserData: function(userData) {
            clcom.saveStorageData('userdata', userData);
        },

        /**
         * ユーザ情報をwebストレージから取得する。
         */
        getUserData: function() {
            return clcom.getStorageData('userdata');
        },

        //////////////////////////////////////////////////////////////////////
        // 関数

        /**
         * ページの状態をページスタックにつみ、ページ遷移する
         * @param {String} url 遷移先ページURL
         * @param {Mixed} args 遷移先ページへの引数
         * @param {Mixed} data 保存データ
         */
        pushPage: function(url, args, data) {},

        /**
         * ページスタックのトップのページに戻る。スタックは1つ減る
         *
         * @param {Mixed} returnValue リターン値
         * @param {Integer} n いくつ戻るか
         */
        popPage: function(returnValue, n) {},

        /**
         * 指定されたクッキーの有無を検査する。
         * @param cookey
         * @returns {Boolean} 値があれば True を返す。
         */
        hasCookie: function(cookey) {
            var arg = cookey+'=';
            var alen = arg.length;
            var clen = document.cookie.length;
            var i = 0;
            while (i < clen) {
                var j = i + alen;
                if (document.cookie.substring(i, j) == arg) {
                    return true;
                }
                i = document.cookie.indexOf(" ", i) + 1;
                if (i === 0) {
                    break;
                }
            }
            return false;
        },

        /**
         * 指定されたクッキーを削除する。
         * @param cookey
         */
        delCookie: function(cookey) {
            var path = clcom.config.absPathPrefix || '/';
            document.cookie = cookey + "=dummy;expires=Thu, 01 Jun 1970 00:00:00 GMT; path=" + path;
        },

        /**
         * クッキーに認証情報があるかどうかを判定する。
         * @returns {Boolean} 認証情報がある場合は True を返す。
         */
        hasAuthCookies: function() {
            if(clcom.config.disableAuthTokenCheck === true){
                console.info('clcom.config.disableAuthTokenCheck[TRUE]');
                return true;
            }
            if (!clcom.hasCookie(clcom.config.authTokenKey/*'auth_token'*/)) {
                return false;
            }
            return true;
        },

        cancelBeforeUnload: function(url) {
            var target;
            if (url instanceof $) {
                target = url.attr('target');
                url = url.attr('href');
            }

            if (target) {
                return;
            }

            if (url === undefined || /^#/.test(url))
                return;

            if (/^javascript:/.test(url)) {
                clcom._preventConfirmOnce = true;
                return;
            }

            clcom._preventConfirm = true;
        },

        /**
         * 確認ダイアログなしでURLを変更する。
         */
        location: function(url) {
            clcom.cancelBeforeUnload(url);
            window.location.replace(url);
        },

        /**
         * ログアウトする
         */
        logout: function(url/*省略可*/, keepPage/*省略可、true:ログアウト処理後、現在のページ維持*/){
            // 引数整理
            var o = {
                    url: clcom.config.loginPagePath,
                    keepPage: false
            };
            if(arguments.length === 1 && _.isObject(arguments[0])){
                _.extend(o, arguments[0]);
            }else{
                o = _.defaults({
                    url: arguments[0],
                    keepPage: arguments[1]
                }, o);
            }

            // ストレージクリア
            clcom.clearStorage({ removeUserData: true});
            clcom.sesStore.clearStorage();

            // ログアウト処理
            var d = $.Deferred();
            var uri = clcom.config.logoutURI;

            // テストデータ用モック応答を返す機構
            do{
                var mock = window.TestAjaxMock;
                if(mock != null && mock.enabled === true){
                    var url = purl(uri),
                    path = '/' + (url.data.seg.path||[]).join('/'),
                    res;
                    if(clcom.config.apiRootMatcher.test(path)){
                        res = path.replace(clcom.config.apiRootMatcher, '');
                    }else{
                        res = _.last(url.data.seg.path);
                    }
                    var rspData;
                    if(_.isFunction(mock[res])){
                        rspData = mock[res]();
                    }else if(_.isObject(mock[res])){
                        rspData = mock[res];
                    }else if(_.isString(mock[res])){
                        rspData = JSON.parse(mock[res]);
                    }else{
                        break;      // Exit: do-while(false)
                    }
                    clutil.blockUI(uri);
                    setTimeout(function(){
                        clutil.unblockUI(uri);
                        d.resolve(rspData);
                        if(!o.keepPage){
                            clcom.location(o.url);
                        }
                    },500);
                    return d.promise();
                }
            }while(false);

            clutil.blockUI(uri);
            $.ajax(uri, {
                type: 'GET',
                cache: false,
                timeout: 5000,
                success: function(){
                    console.info('logout success: ', arguments);
                },
                error: function(xhr, status, exp){
                    // HTTP 系異常
                    console.error('[' + status + '] logout failed.');
                },
                complete: function(xhr, status){
                    clutil.unblockUI(uri);
                    d.resolve('OK');
                    if(!o.keepPage){
                        clcom.location(o.url);
                    }
                }
            });
            return d.promise();
        },

        /**
         * トップページへ戻る
         */
        gohome: function(url){
            clcom.pushPage({
                url: url || clcom.config.topPagePath,
                clear: true
            });
        },

        /**
         * clcom セッションストレージアクセス I/F
         */
        sesStore: {
            /**
             * 指定キーによるセッションストレージデータを取得する。
             */
            getStorageData: function(key, defaultVal){
                return sesStore.get(clcom.config.storagePrefix + key, defaultVal);
            },
            /**
             * セッションストレージに指定キーで指定データを保存する。
             */
            saveStorageData: function(key, data){
                sesStore.set(clcom.config.storagePrefix + key, data);
            },
            /**
             * 指定キーのストレージデータを削除する。
             */
            removeStorageData: function(key){
                sesStore.remove(clcom.config.storagePrefix + key);
            },
            /**
             * セッションストレージに値の有無を確認する。
             */
            hasStorageKey: function(key, prefix) {
                var fixedKey;
                if (_.isString(prefix)) {
                    fixedKey = prefix + key;
                } else {
                    fixedKey = clcom.config.storagePrefix + key;
                }
                if (_.isEmpty(fixedKey)) {
                    // キーが空欄 or null or undefined
                    return false;
                }
                return sesStore.has(fixedKey);
            },
            /**
             * セッションストレージをクリアする
             * プレフィックス 'clcom.' から始まるキーが削除対象。
             */
            clearStorage: function(){
                // キー一覧からストレージをクリアしていく。
                var regEx = new RegExp('^' + clcom.config.storagePrefix.replace(/\.*$/, '') + '\\.');
                var delKeys = [];
                sesStore.forEach(_.bind(function(k,v){
                    if(!regEx.test(k)){
                        return;
                    }
                    // 'this' is delKeys
                    this.push(k);
                },delKeys));
                if(!_.isEmpty(delKeys)){
                    sesStore.remove(delKeys);
                }
            }
        },

        /**
         * ブラウザデフォルトの右クリックコンテキストメニューの有効/無効を設定する。
         * @param {boolean} enable true:コンテキストメニュー有効、false:コンテキストメニュー無効
         */
        setDefaultContextMenu: function(enable){
            if(enable){
                $('html').removeAttr('oncontextmenu');
            }else{
                $('html').attr('oncontextmenu', 'return false;');
            }
        }
    });

    /*
     * 運用日初期設定
     */
    var opeIymd = clcom.getOpeDate();   // from Storage
    if(opeIymd != null){
        clcom.ope_date = opeIymd;
    }

    /*
     * 他ページ移動ブロッキング
     * window .load / .unload
     */
    $(document).on('click', 'a', function(e) {
        clcom.cancelBeforeUnload($(e.currentTarget));
    });
    $(window).on('beforeunload', function beforeunload(e){
        if(clcom._preventConfirm){
            return;
        }
        var prevent = clcom._preventConfirmOnce;
        clcom._preventConfirmOnce = false;
        if(prevent){
            return;
        }

        // ページ移動前の確認
        return 'ブラウザの【戻る】【進む】【リロード】ボタンを使用しています。入力したデータは保存されません。現在のページから移動しないでください。';
    });

}());
/**
 * pushpop: ページ遷移制御モジュール
 * clcom.pushPage(), .popPage()
 */
(function() {
    function removeHash(url) {
        var href = url.split(/[\?#]/)[0];
        var qParams = purl(url).data.param.query;
        if(!_.isEmpty(qParams)){
            var kvArray = _.reduce(qParams, function(kv, v, k){
                if(k != 'z'){   // z=<文字列> は本Appのものなのでスキップ。
                    kv.push(k + '=' + v);
                }
                return kv;
            }, []);
            if(kvArray.length > 0){
                href = href + '?' + kvArray.join('&');
            }
        }
        return href;
    }

    function generateHash() {
        return String(new Date().getTime());
    }

    function getPageHash() {
        var ret = location.hash;
        if (!ret) {
            return null;
        }
        ret = ret.replace(/^#/, "");
        return decodeURIComponent(ret);
    }
    // 絶対パスURLに変換
    function toAbsURL(url, opt){
        // <a> タグ href に入れて再取得すると絶対パス化される！
        var url = $('<a href="' + url + '">').get(0).href;
        if(opt && opt.trimParams){
            // ハッシュやパラメタを削除
            var hIdx = url.lastIndexOf('#');
            var qIdx = url.indexOf('?');
            var idx = (hIdx >= 0 && qIdx >= 0) ? Math.min(hIdx, qIdx) : (hIdx >= 0) ? hIdx : qIdx;
            if(idx >= 0){
                url = url.slice(0,idx);
            }
        }
        return url;
    }
    // 同一URLかどうか判定する。ただし、DNS, IPAddr マッピングは解けない。
    function isSameURL(url1, url2){
        var absUrl1 = toAbsURL(url1, {trimParams: true}).toLowerCase(),
            absUrl2 = toAbsURL(url2, {trimParams: true}).toLowerCase();
        return (absUrl1 == absUrl2);
    }
    function go(url, hash) {
        hash = encodeURIComponent(hash);
        url = removeHash(url);
        var antiCacheKey = clcom.getAntiCacheKey();
        if(antiCacheKey.length > 0){
            url = url + '?z=' + antiCacheKey;
        }

        clcom._preventConfirmOnce = true;

        window.location.replace(url + '#' + hash);
        if(isSameURL(url, location.href)){
            location.reload();
        }
    }
    function newWindow(url, hash, target){
        hash = encodeURIComponent(hash);
        url = removeHash(url);
        var antiCacheKey = clcom.getAntiCacheKey();
        if(antiCacheKey.length > 0){
            url = url + '?z=' + antiCacheKey;
        }
        window.open(url + '#' + hash, target);
    }

    // ------------------------------------------
    // ページ遷移用スタック操作
    // 初期化、clcom へ export 処理を行っている。
    var pushpop = (function() {
        var store = window.store,
        stack,
        current,
        savedData,
        history;

        stack = store.get('clcom.pushpop') || [];
        // store.remove('clcom.pushpop');
        if (stack.length > 0) {
            var last = _.last(stack),
            type = last.type,
            hash = last.hash;

            if (last.hash !== getPageHash() && !getPageHash()) {
                store.remove('clcom.pushpop');
                stack = [];
            } else {
                if (last.type === 0) {
                    savedData = stack.pop();
                }
            }
        }
        savedData = savedData || {};
        current = _.last(stack) || {};

        history = _(stack)
            .chain()
            .filter(function(value, index) {return index % 2 === 1;})
            .map(function(value) {
                return {
                    url: value.srcUrl,
                    label: value.srcPageTitle
                };
            })
            .value();

        function push(){
            var opts;
            if(arguments.length === 1 && _.isObject(arguments[0])){
                opts = arguments[0];
            }else{
                opts = {
                    url: arguments[0],
                    args: arguments[1],
                    saved: arguments[2],
                    download: arguments[3],
                    clear: arguments[4]
                };
            }
            var s = stack.slice();
            /* clearフラグを追加 2013/10/17 */
            if (opts.clear) {
                store.remove('clcom.pushpop');
                s = [];
            }
            var dstId = clcom.getPageIdFromPath(opts.url);
            var hash = generateHash();

            s.push({
                type: 0,
                saved: opts.saved,
                dstId: dstId,
                dstUrl: opts.url
            });
            s.push({
                type: 1,
                args: opts.args,
                srcUrl: location.href,
                srcId: clcom.pageId,
                srcPageTitle: clcom.pageTitle,
                hash: hash
            });

            store.set('clcom.pushpop', s);
            if (opts.download) {
                clcom._preventConfirmOnce = true;
                location.href = opts.url;
            } else if(opts.newWindow) {
                var target = null;
                if(_.isString(opts.newWindow)){
                    target = opts.newWindow;
                }else if(_.has(opts.newWindow, 'target')){
                    target = opts.newWindow.target;
                }
                newWindow(opts.url, hash, target);
            } else {
                go(opts.url, hash);
            }
        }

        function pop(returnValue, n) {
            var hash = generateHash();
            n = n != null ? n : 1;
            if (stack.length - 2 * n + 1 < 0)
                return;
            var s = stack.slice(0, stack.length - 2 * n + 1);
            var l = s[s.length - 1];
            l.returnValue = returnValue;
            l.hash = hash;
            store.set('clcom.pushpop', s);
            go(current.srcUrl, hash);
        }

        // 戻り先 srcId(画面コード) とマッチするまで pop し続ける
        function popTo(srcId, returnValue){
            var opt;
            if(arguments.length === 1 && _.isObject(arguments[0])){
                opt = arguments[0];
            }else{
                opt = {
                    srcId: srcId,
                    returnValue: returnValue,
                    lostToHome: false
                };
            }

            var hash = generateHash();
            if(_.isEmpty(stack)){
                if(opt.lostToHome) clcom.gohome();
                return false;
            }
            var findIdx = stack.length-1;
            for(;findIdx >= 0; findIdx-=2){
                if(stack[findIdx].srcId == opt.srcId){
                    break;
                }
            }
            if(findIdx < 0){
                if(opt.lostToHome) clcom.gohome();
                return false;
            }
            var s = stack.slice(0, findIdx);
            var fixCurrent = stack[s.length];
            var l = s[s.length - 1];
            l.returnValue = opt.returnValue;
            l.hash = hash;  // カレントの hash 値 ???
            store.set('clcom.pushpop', s);
            go(fixCurrent.srcUrl, hash);
            return true;
        }

        // 引数: null ⇒ 削除、undefined ⇒ 何もしない（前の設定値を保持する）
        function savePageData(pageData, retVal) {
            var hash = clcom.pageHash;
            if(_.isEmpty(hash)){
                return;
            }
            // update storage resource.
            var s = store.get('clcom.pushpop') || [];
            var x = _.find(s, {type:0, hash:hash});
            if(x == null){
                if(pageData == null && retVal == null){
                    return;
                }
                x = {
                    hash: hash,
                    saved: pageData,
                    type: 0
                };
                if(pageData){
                    x.saved = pageData;
                }
                if(retVal){
                    x.returnValue = retVal;
                }
                s.push(x);
            }else{
                if(pageData === undefined && retVal === undefined){
                    return;
                }
                if(pageData){
                    x.saved = pageData;
                }else if(pageData === null){
                    delete x.saved;
                }
                if(retVal){
                    x.returnValue = retVal;
                }else if(retVal === null){
                    delete x.returnValue;
                }
            }
            store.set('clcom.pushpop', s);

            // update local resource.
            if(pageData !== undefined){
                savedData.saved = pageData;
                clcom.pageData = pageData;
            }
            if(retVal !== undefined){
                savedData.returnValue = retVal;
                clcom.returnValue = retVal;
            }
        }

        // clcom へエクスポート
        return {
            pageHash: getPageHash(),
            pushPage: push,
            popPage: pop,
            popPageTo: popTo,
            go: go,
            newWindow: newWindow,
            transfer: push,
            srcId: current.srcId,
            srcPageTitle: current.pageTitle,
            srcUrl: current.srcUrl,
            dstId: savedData.dstId,
            dstUrl: savedData.dstUrl,
            returnValue: savedData.returnValue,
            pageArgs: current.args,
            pageData: savedData.saved,
            savePageData: savePageData,
            _history: history
        };
    }());
    _.extend(clcom, pushpop);

}());
/**
 * ORIGINAL clcom.js
 *
 * Namespace: clutil.*
 *
 * [概要]
 * clutil.* は下記機能を有します。
 * ・ajax サポート関数
 *   ・postJSON()
 * ・文字列操作、文字種判定、フォーマッタユーティリティ群
 * ・Data-View マッピングユーティリティ
 *   ・clutil.view2data()
 *   ・clutil.data2view() .... etc
 * ・input 活性/非活性化ユーティリティ
 *   ・clutil.viewReadonly()
 *   ・clutil.viewRemoveReadonly()
 * ・キーバインド
 * ...etc
 *
 * [注意]
 * 今後、機能拡張する際は機能毎にネームスペースを切って棲み分けるように。
 */
var clutil = _.extend({}, clutil);

(function() {

    /*
     * clutil.blockUI() ローディングイメージを生成する。
     */
    var blockUILoadingMessage = function(){
        // 旧 Gif イメージのバージョン
        // '<div id="loading" class=""><img src="' + clcom.config.urlRoot + '/images/loader.gif"></div>'
        //
        // 現 ABUIバージョン
        // <div class="sk-fading-circle"><div class="sk-circle[n] sk-circle"></div></div>
        var msgs = [];
        msgs.push('<div class="sk-fading-circle">');
        for(var i=1; i<=12; i++){
            msgs.push('<div class="sk-circle' + i + ' sk-circle"></div>');
        }
        msgs.push('</div>');
        return msgs.join('');
    }();

    /*
     * フォーカス要素の退避/復元サポートAPI
     */
    var FocusSaver = function(){
    };
    _.extend(FocusSaver.prototype, {
        /**
         * フォーカス要素を退避する
         */
        escape: function(){
            var $f = $(':focus');
            if($f.length > 0 && $f.is(':focusable')){
                this.$savedFocus = $f.blur();
            }else{
                this.$savedFocus = null;
            }
        },
        /**
         * フォーカス要素を復元する
         * @param lazy true:遅延作動する
         */
        restore: function(lazy){
            if(lazy === true){
                _.defer(_.bind(this.restore,this));
                return;
            }
            var $f = this.$savedFocus;
            if($f){
                $f.focus();
                this.$savedFocus = null;
            }
        }
    });

    /** ユーティリティ */
    _.extend(clutil, {
        /** UIブロッキング中の呼び出し元マップ */
        UIBlockSlot: {},
        /** UIブロッキング中のフォーカス要素退避/復元 */
        UIBlockFocusSaver: new FocusSaver(),
        /** 画面をブロックする */
        blockUI: function(a){
            var caller = a || 'anonymous';
            var slotKey = _.uniqueId('blockUI_');
            console.log('blockUI called, [' + slotKey + '], caller[' + caller + ']');
            if(_.isEmpty(clutil.UIBlockSlot)){
                // フォーカス要素保存
                clutil.UIBlockFocusSaver.escape();

                var msg = blockUILoadingMessage;
                $.blockUI({
                    baseZ: 10000,
                    css: { 'backgroundColor':'none', 'border':'none' },
                    message: msg
                });
                console.log('blockUI blocked');
            }
            clutil.UIBlockSlot[slotKey] = caller;
        },
        /** 画面のブロックを解除する */
        unblockUI: function(a, force) {
            if(a === true || force){
                console.warn('unblockUI: force unblocked, UIBlockSlot:' + JSON.stringify(clutil.UIBlockSlot));
                clutil.UIBlockSlot = {};
                $.unblockUI();

                // フォーカス復元
                clutil.UIBlockFocusSaver.restore(true/*lazy*/);
                return;
            }
            var caller = a || 'anonymous';
            var slotKey;
            _.find(clutil.UIBlockSlot, function(v,k){
                if(v == caller){
                    slotKey = k;
                    return true;
                }
            });
            console.log('unblockUI called, [' + (slotKey||'?') + '], caller[' + caller + ']');
            if(slotKey){
                delete clutil.UIBlockSlot[slotKey];
            }
            if(_.isEmpty(clutil.UIBlockSlot)){
                $.unblockUI();
                console.log('unblockUI unblocked.');

                // フォーカス復元
                clutil.UIBlockFocusSaver.restore(true/*lazy*/);
            }
        },

        /**
         * 要素を指定して入力ブロックする。
         * @param elem ブロック対象の要素
         * @param opt blockUI オプション - http://malsup.com/jquery/block/#options
         */
        blockElem: function(elem, blockOpt){
            var $elem = $(elem);
            if($elem.length > 0){
                $elem.block(_.extend({
                    message: null,
                    overlayCSS: {
                        cursor: 'default',
                        backgroundColor: 'whitesmoke',
                        opacity: 0.6
                    }
                },blockOpt));
            }
        },
        /**
         * blockElem 要素を解除する。
         * @praram elem ブロック解除対象の要素
         */
        unblockElem: function(elem){
            var $elem = $(elem);
            $elem.unblock();
        },

        /**
         * 初期動作時、非表示にしている要素を表示する。
         * 各画面 MainView.render() の最期で呼び出すことを想定。
         * @param $elem         非表示要素を含むコンテント。mainView.$el の指定を想定。
         * @param immediately   [省略可]再表示時、アニメーション効果無しにただちに表示する。
         * @return defer        処理完了同期取りのための $.Deferred オブジェクトを返す。
         */
        removePreInvisible: function($elem, immediately){
            var df = $.Deferred();
            var $target = ($elem) ? $elem.find('.cl_preinvisible') : $('.cl_preinvisible');
            if(immediately){
                $target.removeClass('cl_preinvisible').show();
                _.defer(function(){ df.resolve(); });
            }else{
                _.defer(function(){
                    $target.hide().removeClass('cl_preinvisible').fadeIn('fast', function(){
                        df.resolve();
                    });
                });
            }
            return df.promise();
        },

        /**
         * リクエストデータからリクエスト共通ヘッダを取得する
         */
        getReqHead: function(req){
            if(req != null){
                return req[clcom.config.cmHeadPropName.req];
            }
        },

        /**
         * レスポンスデータからレスポンス共通ヘッダを取得する
         */
        getRspHead: function(rsp){
            if(rsp != null){
                return rsp[clcom.config.cmHeadPropName.rsp];
            }
        },

        /**
         * 何もしない関数
         */
        nullFunc: function(){
        },

        /**
         * サーバ呼び出しラッパ関数。
         * @param t HTTPメソッド
         * @param res リソースパス
         * @param data JSONデータ
         * @param appcallback function(data,dataType)
         * @param {Function} completed
         * @param {String} resType
         * @param {jQuery Object} $form optional
         * data 形式は、
         * {
         *      // ヘッダ情報
         *      head: {
         *          status: {'success' or 'warn' or 'error'},
         *          message: メッセージコード
         *          args: [ val1, val2, ...] // メッセージ引数
         *          fieldMessages : [
         *              {
         *                  name:項目名,
         *                  message:メッセージコード,
         *                  args: [ メッセージ引数... ]
         *              },
         *              ...
         *          ]
         *      },
         *      // アプリデータ
         *      body: {
         *          アプリケーションデータ
         *      }
         * }
         * @return {promise}
         */
        httpcall: function(options, extra) {
            var deferred = $.Deferred();

            var extraFunc = {
                blockUI: clutil.blockUI,        // ajax 処理中、UIブロックする関数
                unblockUI: clutil.unblockUI     // ajax 処理完了後、UIブロックを外す関数
            }
            if(extra){
                if(!extra.blockUI){
                    // UIブロックしない指定
                    extraFunc.blockUI   = clutil.nullFunc;
                    extraFunc.unblockUI = clutil.nullFunc;
                }
            }

            if (!clcom.hasAuthCookies()) {
                var msg = [ 'ログアウトされました。' ];
                var url = clutil.createErrOccurredUrl(msg, '/');
                clcom.logout({url: url });

                var rsp = {};
                rsp[clcom.config.cmHeadPropName.rsp] = {
                    status: 'error',	// 0でないのを入れる
                    message: 'cl_http_status_unauthorized'
                };
                deferred.reject(rsp);
                return deferred.promise();
            }

            // テストデータ用モック応答を返す機構
            do{
                var mock = window.TestAjaxMock;
                if(mock != null && mock.enabled === true){
                    var url = purl(options.url),
                    path = '/' + (url.data.seg.path||[]).join('/'),
                    res;
                    if(clcom.config.apiRootMatcher.test(path)){
                        res = path.replace(clcom.config.apiRootMatcher, '');
                    }else{
                        res = _.last(url.data.seg.path);
                    }
                    var req = options.data;
                    var rspData;
                    if(_.isFunction(mock[res])){
                        rspData = mock[res](req);
                    }else if(_.isObject(mock[res])){
                        rspData = mock[res];
                    }else if(_.isString(mock[res])){
                        rspData = JSON.parse(mock[res]);
                    }else{
                        break;		// Exit: do-while(false)
                    }

                    extraFunc.blockUI(res);
                    setTimeout(function(){
                        extraFunc.unblockUI(res);
                        if(rspData == null){
                            deferred.reject(rspData);
                            return;
                        }
                        if(_.isFunction(clcom.preProcessCmRspHead)){
                            clcom.preProcessCmRspHead(rspData);
                        }
                        var rspHead = clutil.getRspHead(rspData);
                        if(rspHead == null || rspHead.status !== 0){
                            deferred.reject(rspData);
                            return;
                        }
                        rspHead._isDummyAPI = true;
                        deferred.resolve(rspData, 'success');
                            // clutil.view getIniJSON(), getIniJSON2() の $.when 応答判定のため、
                            // 第2引数 'success' を指定。
                    }, 500);

                    return deferred.promise();
                }
            }while(false);

            var params = _.extend({
//              timeout: clutil.ajax_timeout,
                cache: false,
                dataType: 'json',
                async: true,
                contentType: 'application/json'
                    // beforeSend: function(xhr){
                    //  // リクエストヘッダ付加 - GET メソッドでの妙なキャッシュ作用を防ぐため XXXX cache オプションをfalseに指定すれば良いはず
                    //  if (this.type == 'GET') {
                    //      xhr.setRequestHeader("If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT");
                    //      //xhr.setRequestHeader("Cache-Control", "no-cache");
                    //  }
                    // }
            }, options || {});

            var success = params.success;
            params.success = function(data, dataType) {
                var rspHead = clutil.getRspHead(data);

                // 共通応答ヘッダ前処理
                if(_.isFunction(clcom.preProcessCmRspHead)){
                    clcom.preProcessCmRspHead(data);
                }

                if (success)
                    success(data, dataType);
                if (rspHead && rspHead.status !== 0)
                    deferred.reject(data, 'error');
                else
                    deferred.resolve(data, dataType);
            };

            var error = params.error;
            params.error = function(xhr, textStatus, errorThrown) {
                console.log('XXX error, textStatus[' + textStatus + "]");
                // 致命的なエラーということで共通処理する。
                var ret = clutil.ajaxErrorHandler(xhr, textStatus, errorThrown);
                if (ret) {
                    if (success) {
                        success.apply(this, [ret]);
                    }
                    if (error)
                        error(xhr, textStatus, errorThrown);
                    deferred.reject(ret);
                }
            };

            var complete = params.complete;
            params.complete = function(xhr, textStatus) {
                // 呼び出し完了は Indicator 終了処理とかで共通処理する。
                // xhr.status 200 番台でない場合は強制 unblockUI する。
                extraFunc.unblockUI(params.url, (Math.floor(xhr.status / 100) != 2));
                console.log('XXX completed, stat[' + textStatus + ']); statusCode[' + xhr.status + ']');
                if (complete) {
                    complete(xhr, textStatus);
                }
            };

            // Don't process data on a non-GET request.
            if (params.type !== 'GET') {
                var data = options.data || {};
                if(_.isFunction(clcom.getDefaultCmReqHead)){
                    var hdPropName = clcom.config.cmHeadPropName.req;
                    // 利用統計情報をとるための情報を共通ヘッダに設定する。
                    var head = clcom.getDefaultCmReqHead();
                    data = _.extend({}, data);	// 呼出元リクエストを直接編集しないよう、シャローコピーをとる
                    if(data[hdPropName]){
                        data[hdPropName] = _.extend(head, data[hdPropName]);
                    }else{
                        data[hdPropName] = head;
                    }
                }
                params.data = JSON.stringify(data);
                params.processData = false;
            }

            extraFunc.blockUI(params.url);
            $.ajax(params);
            return deferred.promise();
        },

        /**
         * 汎用エラーページへ送るための URL 生成する。
         * @param {array<string>} msgJson エラーページ側に表示するメッセージ。
         * @param {string} nextUrl エラーページ側における復帰先URL（省略可）
         * @return {string} エラーページURL
         */
        createErrOccurredUrl: function(msgJson, nextUrl) {
            var paramMsg = '?msg=' + encodeURI(JSON.stringify(msgJson));
            var paramNextUrl = '';
            if(nextUrl){
                // サブパス対応 - サブパスを有する場合、nextUrl を補正する。
                if(clcom.config.absPathPrefix && clcom.config.absPathPrefix != '/'){
                    do{
                        if(nextUrl.indexOf('://') >= 0){
                            break; // 外部 URL と見做し、サブパス補正しない
                        }
                        var normNextUrl = clutil.text.normalizeUri(nextUrl);
                        if(normNextUrl.startsWith('/') === false){
                            break; // 相対パス表記と見做し、サブパス補正しない
                        }
                        // サブパス先頭一致マッチングテスト
                        var normSubPath = _.compact(clutil.text.normalizeUri(clcom.config.absPathPrefix).split('/'));
                        var normNextPath = _.compact(normNextUrl.replace(/\?.*$/,'').split('/')).slice(0,normSubPath.length);
                        if(_.isEqual(normSubPath, normNextPath)){
                            break; // サブパス部合致のため、サブパス補正しない
                        }
                        // サブパス補完
                        nextUrl = '/' + $.trim(clcom.config.absPathPrefix) + '/' + $.trim(nextUrl);
                        nextUrl = nextUrl.replace(/[\/]{1,}/g, '/');
                    }while(false);
                }
                // ログイン画面経由のときのみ、遷移先のURLを設定する
                var userData = clcom.getUserData();
                if(userData && userData.fromLogin === true){
                    paramNextUrl = '&nexturl=' + nextUrl;
                }
            }

            // エラーページのデフォルトは、clcom.config.httpErrorPages.defaults で定義。
            // path = '/err/error_occurred.html';
            var errPageDefs = clcom.config.httpErrorPages.defaults;
            return clcom.config.absPathPrefix + errPageDefs.path + paramMsg + paramNextUrl;
        },

        /**
         * Ajax 応答におけるエラーページ転送処理
         * エラーページ定義の configuration は、clcom.config.httpErrorPages を参照。
         * @param xhr {object} xhr オブジェクト
         * @return {boolean} true:転送処理をした。false:転送処理をしなかった。（エラーページ定義が無い場合）
         */
        transferErrorPage: function(xhr){
            if(!xhr){
                return false;
            }

            var errPageCnf = clcom.config.httpErrorPages[xhr.status];
            if(errPageCnf == null){
                // 当該 HTTP エラーコードにおけるエラーページ定義が見つからない ⇒ HTTP エラーページへ飛ばさない
                return false;
            }

            // デバッグ用: Localhost 開発環境ではエラーページ遷移するかどうかの confirm 確認を挟んでおく。
            if(clcom.isLocalHost()){
                console.error('[DBG] HTTP ' + xhr.status + ' Error', xhr);
                if(confirm('[DBG] HTTP ' + xhr.status + ' Error, stay?')){
                    return false;
                }
            }

            // URL 生成 - 後述。
            var url = createErrPageUrl();

            // エラーページ転送処理
            if(errPageCnf.logout){
                // たとえば「401 Unauthorized」の場合は、errPageConf に
                // { logout: true } を仕込んでいて、ログアウト処理を伴わせている。
                clcom.logout({ url: url });
            }else{
                // エラーページへ遷移させる。
                clcom.location(url);
            }
            return true;

            // URL 生成
            function createErrPageUrl(){
                var path = errPageCnf.path,
                    msg1 = errPageCnf.message, msg2 = [ xhr.status, xhr.statusText || errPageCnf.description || '' ].join(' ');

                // エラーページパス無しのものは汎用エラーページへ送る。
                if(_.isEmpty(path)){
                    var genCnf = clcom.config.httpErrorPages.defaults;
                    path = genCnf.path;
                    // メッセージ設定が無い場合は、汎用定義のエラーメッセージをとる。
                    msg1 = msg1 || genCnf.message;
                }
                // サブパス補完
                if(path.indexOf('://') >= 0){
                    // 外部 URL と見做し、サブパス補完無用。
                }else{
                    path = clcom.config.absPathPrefix + path;
                }

                // URL パラメタ (1) エラーメッセージ
                var params = {};
                if(msg1){
                    var msgJson = [ clutil.getclmsg(msg1), msg2 ];
                    params.msg = JSON.stringify(msgJson);
                }
                // URL パラメタ (2) TOPページなるURL
                var user = clcom.getUserData();
                if(user.fromLogin === true){
                    // ログインページ経由でのログインを果たしている場合は、nexturl パラメタにログインURLパスを貼りつける。
                    // SSO ログインの場合は、エラーページ表示後の復帰先 URL は与えない。
                    var loginPageUrl = $('<a href="' + clcom.config.loginPagePath + '">')[0].href;
                    params.nexturl = loginPageUrl;
                }

                var url = path;
                if(!_.isEmpty(params)){
                    var paramkv = [];
                    for(var k in params){
                        paramkv.push(encodeURI(k) + '=' + encodeURI(params[k]));
                    }
                    url += '?' + paramkv.join('&');
                }

                return url;
            }
        },

        /**
         * ajax 通信 HTTP エラーハンドリング
         * @param xhr {object} xhr オブジェクト
         * @param textStatus {string} ajax 応答の textStatus
         * @param errorThrow {string} ajax 応答の errorThrow
         * @return {object} エラー応答メッセージをセットした共通応答ヘッダ。これはサーバ応答ではなく、画面生成したもの。
         */
        ajaxErrorHandler: function(xhr, textStatus, errorThrow) {
            var errcode = 'cl_http_status_xxx';
            switch (xhr.status) {
            case 0:
                // ブラウザ内部事情により送信キャンセルされたときは { status: 0 } がセットされている。
                errcode = "cl_http_status_0";
                break;
            default:
                clutil.transferErrorPage(xhr);
            }

            var rsp = {};
            rsp[clcom.config.cmHeadPropName.rsp] = {
                status: "error",    // 0 でないのを入れる
                message: errcode,
                httpStatus: xhr.status
            };
            return rsp;
        },

        download: function(url, id) {
            var url = encodeURI(url);

            function startDownlaod(_url) {
                clcom._preventConfirmOnce = true;
                if (window.navigator.standalone) {
                    clcom.pushPage("dbapi-1://" + _url, null, null, true);
//                  location.href = "dbapi-1://" + _url;
                } else {
                    clcom.pushPage(_url, null, null, true);
//                  location.href = _url;
                }
            }

            function removeDownloadBar() {
                $('.clDownloadBar').remove();
                $('body').removeClass('clDownloadBarAdded');
            }

            function createDownloadBar(_url) {
                var $alert = $(
                    '<div id="clDownloadBar" class="clDownloadBar alert alert-info">' +
                    '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<a href="' + _url + '"><strong>ダウンロードが開始されない場合はここをクリックしてください。</strong></a></div>'
                );

                $alert
                .on('click', 'button,a', function(e) {
                    removeDownloadBar();
                    if ($(e.currentTarget).is('a')) {
                        startDownlaod(_url);
                    }
                    e.preventDefault();
                })
                .appendTo('body');
                $('body').addClass('clDownloadBarAdded');
            }


            // ファイルの存在チェック
            if (id != null) {
                var req = {
                    cond: {
                        file_id: id
                    }
                };
                // データを取得
                clutil.postJSON('gscm_filechk', req, _.bind(function(data, dataType) {
                    var rspHead = clutil.getRspHead(data);
                    if (rspHead.status == 0) {
                        if(clcom.isLegacyIE(8)){
                            removeDownloadBar();
                            createDownloadBar(url);
                        } else {
                            startDownlaod(url);
                        }
                    } else {
                        clutil.ErrorDialog('ファイルがみつかりませんでした。');
                    }
                }, this));
            } else {
                if(clcom.isLegacyIE(8)){
                    removeDownloadBar();
                    createDownloadBar(url);
                } else {
                    startDownlaod(url);
                }
            }
        },

        /**
         * JSONデータ取得関数
         * @param res
         * @param appcallback
         * @returns {promise}
         */
        getJSON: function(res, appcallback, complete) {
            return clutil.httpcall({
                type: 'GET',
                url: clcom.config.apiRoot + '/' + res,
                success: appcallback,
                complete: complete
            });
        },
        /**
         * JSONデータPOST関数
         * @param res 呼出サーバリソース名（必須）
         * @param data リクエストデータ（必須）
         * @param appcallback ajax success コールバック - @Deplicated
         * @param complete ajax complete コールバック - @Deplicated
         * @param extra object おまけオプション（３番目以降の最期の引数）
         *  .blockUI        : ajax 処理中に、true: UIブロックする、false: UIブロックしない、デフォルト true
         * @returns {promise}
         */
        postJSON: function(res, data, appcallback, complete) {
            if(_.isFunction(appcallback) || _.isFunction(complete)){
                console.warn('@Deplicated, use clutil.postJSON(uri,req).done(success).fail(failure).');
            }
            var extra = undefined;
            if(arguments.length > 2){
                var lastArg = _.last(arguments);
                if(_.isObject(lastArg)){
                    extra = lastArg;
                    if(appcallback === extra){
                        appcallback = undefined;
                    }
                    if(complete === extra){
                        complete = undefined;
                    }
                }
            }
            return clutil.httpcall({
                type: 'POST',
                data: data,
                url: clcom.config.apiRoot + '/' + res,
                success: appcallback,
                complete: complete
            }, extra);
        },
        setButtonName4postJSON: function(buttonName) {
            console.warn('@Deplicated: since Rzp.');
//          this.postJsonData.button_name = buttonName;
        },

        /**
         * HTMLファイルの読み込み
         * @param url	// HTMLファイルのURL
         * @param appcallback
         */
        loadHtml: function(url, appcallback) {
            clutil.blockUI(url);

            var fixUrl = url;

            var urlParams = [];
            var antiCacheKey = clcom.getAntiCacheKey();
            if(antiCacheKey.length > 0){
                urlParams.push('uuid=' + antiCacheKey);
            }
            if(clcom.config.preventLoadHtmlCache === true){
                urlParams.push('z=' + Math.floor(Math.random() * 10000000000));
            }
            if(urlParams.length > 0){
                fixUrl = url + '?' + urlParams.join('&');
            }

            var defer = $.Deferred();
            $.ajax({
                type: 'GET',
                url: fixUrl,
                dataType: 'html',
                success: function(data) {
                    if(_.isFunction(appcallback)){
                        appcallback(data);
                    }
                },
                error:function() {
                    //clutil.ErrorDialog('HTMLファイルの読み込みに失敗しました');
                    // 呼出側でエラー表示ハンドリングすることとする。
                    var fname = purl(url).data.attr.file;
                    console.warn(fname + ': 読み込みに失敗しました');
                },
                complete:function(xhr){
                    // xhr.status 200 番台でない場合は強制 unblockUI する。
                    clutil.unblockUI(url, (Math.floor(xhr.status / 100) != 2));
                }
            }).done(function(data){
                defer.resolve(data);
            }).fail(function(xhr, status, emsg){
                var fname = purl(url).data.attr.file;
                var detail = clutil.fmt('{0}: {1} {2}', fname, xhr.status, emsg);
                var fakeRsp = {
                    head: {
                        status: xhr.status,
                        message: detail
                    }
                };
                defer.reject(fakeRsp);
            });
            return defer.promise();
        },

        /**
         * TODO: 試行中！！！
         * 外部サービスによるデータ取得
         * 同一生成元ポリシーに反するため、httpd config にて、HTTP ヘッダのアクセスコントロール行を付加するためのコンフィギュレーションが必要。
         * @param args  呼出先サービス等
         *      .url    : [必須] 呼出先サービスURL
         *      .type   : 'GET', 'POST', ... 等、HTTP メソッド名、デフォルト 'GET'
         *      .data   : リクエストオブジェクト（POST 指定のとき）
         */
        callService: function(args){
            var uri = args.url;
            clutil.blockUI(uri);

            var defer = $.Deferred();
            var ajaxArgs = _.defaults(args, {
                type: 'GET',
                cache: false,
                dataType: 'json',
                async: true,
                processData: false,
                complete: function(xhr){
                    clutil.unblockUI(uri, true);
                }
            });
            if(ajaxArgs.type == 'GET'){
                delete ajaxArgs.data;
                delete ajaxArgs.contentType;
            }else{
                if(_.isObject(ajaxArgs.data)){
                    ajaxArgs.data = JSON.stringify(ajaxArgs.data);
                    ajaxArgs.contentType = 'application/json';
                }
            }
            $.ajax(ajaxArgs).done(function(data){
                defer.resolve(data);
            }).fail(function(xhr, status, emsg){
                var fname = purl(url).data.attr.file;
                var detail = clutil.fmt('{0}: {1} {2}', fname, xhr.status, emsg);
                var fakeRsp = {
                        head: {
                            status: xhr.status,
                            message: detail
                        }
                };
                defer.reject(fakeRsp);
            });
            return defer.promise();
        },
        /**
         * TODO: 試行中！！！
         * JSONP 呼出
         * @param uri 外部サービスURI
         * @return promise 応答待ち合わせするための promise オブジェクト
         */
        callJSONP: function(uri){
            // TODO: blockUI
            var defer = $.Deferred();

            // JSONP リソースコンテキスト作成 - グローバルに晒すコールバック関数の入れ物を用意。
            var jsonp = window.MyJSONP;
            if(jsonp == null){
                jsonp = {};
                window.MyJSONP = jsonp;
            }

            // global 参照できるネームスペース下に callback を置く
            var myCallbackName = _.uniqueId('myCallback');
            jsonp[myCallbackName] = function(data){
                // 呼出元へ応答を返す
                defer.resolve(data);

                // グローバル化した appcallback スロットを削除
                delete window.MyJSONP[myCallbackName];
                // 生成した <script> タグ削除
                $('#' + myCallbackName).remove();

                // TODO: unblockUI
            }

            // uri に callback パラメタを付加
            var fixedUri;
            if(uri.indexOf('?') > 0){
                fixedUri = uri + '&callback=MyJSONP.' + myCallbackName;
            }else{
                fixedUri = uri + '?callback=MyJSONP.' + myCallbackName;
            }

            console.info('callJSNP: ' + fixedUri);

            // JSONP 呼出 <script> タグ生成
            var $sc = $('<script src="' + fixedUri + '" id="' + myCallbackName + '">');
            $('body').append($sc);

            // TODO: 通信エラー等のハンドリングはどうする？

            return defer.promise();
        },

        /**
         * 書式つき文字列
         * @param {String} fmt フォーマット
         * @returns 文字列
         * @example
         * <pre>
         * > clutil.fmt("range: {0}-{1}", 123, 456);
         * > range: 123-456
         * </pre>
         * TODO:エラーメッセージをID化するか文字列化するか？
         */
        fmt: function(fmt) {
            var i;
            for (i = 1; i < arguments.length; i++) {
                var reg = new RegExp("\\{" + (i - 1) + "\\}", "g");
                fmt = fmt.replace(reg,arguments[i]);
            }
            return fmt;
        },

        fmtargs: function(fmt, args) {
            var i;

            if (!fmt)
                return '';

            if(args instanceof Array){
                for (i = 0; i < args.length; i++) {
                    var reg = new RegExp("\\{" + i + "\\}", "g");
                    fmt = fmt.replace(reg,args[i]);
                }
            }
            return fmt;
        },

        /**
         * <IMG>タグ要素に対して画像切替操作のユーティリティを提供。
         * @param imgElem <IMG>タグ要素
         * @returns {___anonymous92_104}
         */
        imgViewUtil: function(imgElem, errImg){
            var util = {
                imgEl: imgElem,
                errImg: errImg,
                data: null,	// 任意データを一時的に預ける領域

                initialize: function() {
                    var me = this;
                    this.imgEl.error(function(e){
                        me.setImage(me.errImg);
                    });

                    this.imgEl.load(function(){
                        me.onImgLoaded();
                    });
                    return this;
                },

                /**
                 * 再読み込みする
                 */
                reload: function() {
                    var curSrc = this.imgEl.attr('src');
                    if (curSrc.length > 0) {
                        // 一旦 src 属性を削除してから再設定
                        this.imgEl.removeAttr('src').attr('src', curSrc);
                    }
                },

                /**
                 * 画像を設定する
                 * @param srcURI
                 */
                setImage: function(srcURI) {
                    return this.imgEl.attr('src', srcURI);
                },
                /**
                 * 表示中の画像URIを取得する
                 */
                getImage: function() {
                    return this.imgEl.attr('src');
                },
                /**
                 * 画像を削除する
                 */
                removeImage: function() {
                    return this.imgEl.removeAttr('src');
                },
                /**
                 * 画像読み込み通知
                 */
                onImgLoaded: function() {
                    // this = imgEl
                    console.log('Loaded: ' + $(this).attr('src'));
                }
            };
            return util.initialize();
        },

        /**
         * 実際の画像のサイズを取得する
         * @param image
         * @returns
         */
        getActualDimension: function(image) {
            var mem, w, h, key = "actual";

            // for Firefox, Safari, Google Chrome
            if ("naturalWidth" in image) {
                return {width: image.naturalWidth, height: image.naturalHeight};
            }
            if ("src" in image) { // HTMLImageElement
                if (image[key] && image[key].src === image.src) {return  image[key];}

                if (document.uniqueID) { // for IE
                    w = $(image).css("width");
                    h = $(image).css("height");
                } else { // for Opera and Other
                    mem = {w: image.width, h: image.height}; // keep current style
                    $(this).removeAttr("width").removeAttr("height").css({width:"",  height:""});    // Remove attributes in case img-element has set width  and height (for webkit browsers)
                    w = image.width;
                    h = image.height;
                    image.width  = mem.w; // restore
                    image.height = mem.h;
                }
                return image[key] = {width: w, height: h, src: image.src}; // bond
            }

            // HTMLCanvasElement
            return {width: image.width, height: image.height};
        },

        /**
         * 日付形式の判定をする
         */
        checkDate: function(value, minDate, maxDate) {
            if (value === '') {
                // 空欄はOKとする。必須とするなら、cl_required と併用すること
                return;
            }

            minDate = (minDate == null) ? clcom.limits.minDate : new Date(minDate);
            maxDate = (maxDate == null) ? clcom.limits.maxDate : new Date(maxDate);

            var date, srcymd;
            if (~value.indexOf('/')) {
                // slashが含まれている場合
                date = new Date(value);
                srcymd = value.split('/');
            } else {
                // /なしの場合
                date = clutil.date.toDate(parseInt(value,10));
                srcymd = [value.substring(0,4), value.substring(4,6), value.substring(6)];
            }

            if (!_.isDate(date) || isNaN(date.getTime())) {
                return false;
            }

            var chkymd = [date.getFullYear(), date.getMonth()+1, date.getDate()];
            if (srcymd.length != chkymd.length) {
                return false;
            } else {
                for (var i=0; i<chkymd.length; i++) {
                    if (parseInt(srcymd[i], 10) !== chkymd[i]) {
                        return false;
                    }
                }
            }

            if (date.getTime() > maxDate.getTime() || date.getTime() < minDate.getTime()) {
                return false;
            }
        },

        /**
         * 指定した日付形式に変換する
         * @param {string}
         *					・int型8桁	または
         *					・yyyy/mm/dd
         * @param {string} format yyyy-mm-dd, yyyy/mm/dd, yyyy/mm/dd hh:ss, yyyymmdd
         * @returns 文字列
         */
        dateFormat: function(obj, format) {
            console.warn('@Deplicated: clutil.dateFormat, use clutil.date.dateFormat');

            if (typeof obj === 'string' && !~obj.indexOf('/')) {
                obj = parseInt(obj, 10);
            }

            if (!obj) {
                return "";
            } else if (typeof obj == "number") {
                // サーバーからは8桁の数値で来る事を想定
                var n_year = Math.floor(obj/10000);
                var n_month = Math.floor((obj%10000)/100);
                var n_day = Math.floor(obj%100);

                obj = clutil.fmt('{0}/{1}/{2}', n_year, n_month, n_day);
            }

            var dateObj = new Date(obj);
            if (! dateObj.valueOf()) {
                var sqlDateStr = obj.replace(/:| |T/g,"-");
                var YMDhms = sqlDateStr.split("-");
                dateObj.setFullYear(parseInt(YMDhms[0], 10), parseInt(YMDhms[1], 10)-1, parseInt(YMDhms[2], 10));
                dateObj.setHours(parseInt(YMDhms[3], 10), parseInt(YMDhms[4], 10), parseInt(YMDhms[5], 10), 0/*msValue*/);
            }
            var year = dateObj.getFullYear();
            var month = clutil.text.zeroPadding(dateObj.getMonth()+1, 2);
            var day = clutil.text.zeroPadding(dateObj.getDate(), 2);
            var hours = clutil.text.zeroPadding(dateObj.getHours(), 2);
            var minutes = clutil.text.zeroPadding(dateObj.getMinutes(), 2);


            switch (format) {
            case 'yyyymmdd':		  // サーバーに送る型
                return Number(clutil.fmt('{0}{1}{2}', year, month, day));
            case 'yyyy-mm-dd':	  // サーバーに送る型OLD
                return clutil.fmt('{0}-{1}-{2}', year, month, day);
            case 'yyyy/mm/dd':
                return clutil.fmt('{0}/{1}/{2}', year, month, day);
                //case 'yyyy/mm/dd hh:ss':
            case 'yyyymm':
                return Number(clutil.fmt('{0}{1}', year, month));
            case 'yyyy':
                return Number(clutil.fmt('{0}', year));
            case 'mm':
                return Number(clutil.fmt('{0}', month));
            case 'dd':
                return Number(clutil.fmt('{0}', day));
            default:
                return clutil.fmt('{0}/{1}/{2} {3}:{4}'
                        , year, month, day, hours, minutes);
            }
        },

        /**
         * 指定した日付を月形式に変換する
         * @param {string}
         *					・int型6桁、8桁	または
         *					・yyyy/mm
         * @param {string} format yyyy/mm, yyyymm
         * @returns {String}
         */
        monthFormat: function(obj, format) {
            var year;
            var month;
            if (obj == null || obj === "") {
                return "";
            }

            var date = obj;

            if (typeof obj === 'string') {
                date = obj.replace("/", "");
            }

            // 5桁の場合
            if (date < 100000) {
                year = Math.floor(date/10);
                month = clutil.text.zeroPadding(Math.floor(date%10), 2);
            } else if (date > 9999999) {
                // 8桁の場合も対応
                year = Math.floor(date/10000);
                month = clutil.text.zeroPadding(Math.floor((date%10000)/100), 2);
            } else {
                year = Math.floor(date/100);
                month = clutil.text.zeroPadding(Math.floor(date%100), 2);
            }

            switch (format) {
            case 'yyyymm':		// サーバーに送る型
                return Number(clutil.fmt('{0}{1}', year, month));
            case 'yyyy/mm':
                return clutil.fmt('{0}/{1}', year, month);
            default:
                return clutil.fmt('{0}/{1}', year, month);
            }
        },

        /**
         * 指定した時刻を時刻形式に変換する
         * @param {string}
         *					・int型4桁	または
         *					・hh:mm
         * @param {string} format hh:mm, hhmm
         * @returns {String}
         */
        timeFormat: function(obj, format) {
            var hour;
            var minute;
            if (obj == null || obj === "") {
                return "";
            }

            var time = obj;

            if (typeof obj === 'string') {
                time = obj.replace(":", "");
            }

            hour = Math.floor(time/100);
            minute = clutil.text.zeroPadding(Math.floor(time%100), 2);

            switch (format) {
            case 'hhmm':		  // サーバーに送る型
                return Number(clutil.fmt('{0}{1}', hour, minute));
            case 'hh:mm':
            default:
                return clutil.fmt('{0}:{1}', hour, minute);
            }
        },

        /**
         * 文字列取得
         */
        cStr: function(str) {
            if(str == null){
                return "";
            }
            if(_.isString(str)){
                return str;
            }
            return str.toString();
        },

        /**
         * 文字列判定
         */
        chkStr: function(str) {
            if (typeof str != "string") {
                return false;
            }
            var cstr = str.replace(/\s+/g, "");
            if (cstr == null || cstr.length === 0) {
                return false;
            } else {
                return true;
            }
        },

        /**
         * 数値取得
         */
        cInt: function(intValue) {
            if (typeof intValue != "number" || isNaN(intValue)) {
                return 0;
            } else {
                return intValue;
            }
        },

        /**
         * 数値変換
         */
        pInt: function(intValue) {
            if (isNaN(parseInt(intValue))) {
                return 0;
            } else {
                return parseInt(intValue);
            }
        },

        /** 要素ID付けプレフィックスマッチャー */
        defaultCaPrefix: /^ca_/,

        /**
         * 表示エリアよりデータオブジェクトを作成する
         * 指定したプレフィックスを除去したidの名前で作成する
         * ラジオボタンのみnameで検索する
         * 引数
         * ・$view		: jQueryオブジェクト (例：$('#viewarea'))
         * ・prefix		: アプリ固有のプレフィックス 指定されない場合は 'ca_'
         * ・resultdata : 作成するデータオブジェクトを上書きする場合はオブジェクトを指定
         *							  なにも指定されていない場合は新しいオブジェクトを作成する
         * 戻り値
         * ・オブジェクト
         */
        view2data: function($view, prefix, resultdata) {
            resultdata = resultdata == null ? {} : resultdata;
            var _prefix = clutil.text.buildPrefixMatcher(prefix, clutil.defaultCaPrefix);
            var _id = '';
            $view
            .find("input[id]")
            .filter('[type="text"]').each(function(){
                var $this = $(this);
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }

                // domain 付きの場合は controller から設定値取得
                var domain = clutil.getDomain($this);
                if(domain && domain.controller){
                    var v = domain.controller.getValue($this);
                    if(v != null){
                        resultdata[_id] = v;
                    }
                    return;
                }

                var fixval = $.trim(this.value);
                if ($this.hasClass('cl_date') || $this.hasClass('hasDatepicker')) {
                    // datepickerの場合は日付をサーバーに送る型に変換する
                    resultdata[_id] = clutil.date.toIymd(fixval);
                } else if ($this.hasClass('cl_month')) {
                    // 月選択の場合は月をサーバーに送る型に変換する
                    resultdata[_id] = clutil.monthFormat(fixval, 'yyyymm');
                } else if ($this.hasClass('cl_time')) {
                    // 時刻選択の場合は時刻をサーバーに送る型に変換する
                    resultdata[_id] = clutil.timeFormat(fixval, 'hhmm');
                } else if ($this.hasClass('cl_MtSuggestView')) {
                	// サジェスト
                    var api = $this.data('cl_MtSuggestView');
                    if(api){
                        resultdata[_id] = api.getValue();
                    }
                } else {
                    resultdata[_id] = $.inputlimiter.unmask(fixval, {
                        limit: $this.attr('data-limit'),
                        filter: $this.attr('data-filter')
                    });
                }
            })
            .end()
            .filter('[type="hidden"]').each(function(){
                // hiddenのものに対しても処理を行う(idなど)
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                resultdata[_id] = this.value;
            })
            .end()
            .filter('[type="password"]').each(function(){
                // パスワード
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                resultdata[_id] = this.value;
            })
            .end()
            .filter('[type="checkbox"]').each(function(){
                // チェックボックス
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                resultdata[_id] = this.checked ? 1 : 0;
            })
            .end()
            .end()
            .find("input[name]")
            .filter('[type="radio"]').each(function(){
                // ラジオボタン: name 属性で取得する
                if(this.checked){
                    // プレフィックスを除去する
                    _id = clutil.text.isMatchThenTrim(_prefix, this.name);
                    if(_.isEmpty(_id)){
                        return;
                    }
                    // チェックされていたらデータ化する
                    resultdata[_id] = this.value;
                }
            })
            .end()
            .filter('[type="checkbox"].cl_cbgroup').each(function(){
                // チェックボックスグループ: name 属性で取得する。
                if(this.checked && this.value){
                    _id = clutil.text.isMatchThenTrim(_prefix, this.name);
                    if(_.isEmpty(_id)){
                        return;
                    }
                    var cbVal = this.value;
                    if(/^\d{1,}$/.test(cbVal)){
                        cbVal = parseInt(cbVal, 10);
                    }
                    if(!_.isArray(resultdata[_id])){
                        resultdata[_id] = [];
                    }
                    var results = resultdata[_id];
                    if(!_.contains(results, cbVal)){
                        results.push(cbVal);
                    }
                }
            })
            .end()
            .end()
            .find("textarea[id]").each(function(){
                // テキストエリア
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                resultdata[_id] = $.trim(this.value);
            })
            .end()
            .find("select[id]").each(function(){
                // valueの値が取得される
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                var $this = $(this);
                if($this.data('selectpicker')){
                    var _v = $this.selectpicker('val');
//                  【検討】 選択肢なの場合、null で返すので嬉しいか？
                    if(true/* 2017-05-25: 選択ナシ ⇒ 作らない方で修正しておく。 */){
                        if(_v != null)	resultdata[_id] = _v;
                    }else{
                        resultdata[_id] = _v;
                    }
                }else{
                    resultdata[_id] = this.value;
                }
            })
            .end()
            .find("span[id]").each(function(){
                var $this = $(this);
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if ($(this).hasClass('cl_date')) {
                    // datepickerの場合は日付をサーバーに送る型に変換する
                    resultdata[_id] = clutil.dateFormat($(this).text(), 'yyyymmdd');
                } else {
                    resultdata[_id] = $.inputlimiter.unmask($(this).text(), {
                        limit: $this.attr('data-limit'),
                        filter: $this.attr('data-filter')
                    });
                }
            })
            .end()
            .find("p[id]").each(function(){
                var $this = $(this);
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if ($(this).hasClass('cl_date')) {
                    // datepickerの場合は日付をサーバーに送る型に変換する
                    resultdata[_id] = clutil.dateFormat($(this).text(), 'yyyymmdd');
                } else {
                    resultdata[_id] = $.inputlimiter.unmask($(this).text(), {
                        limit: $this.attr('data-limit'),
                        filter: $this.attr('data-filter')
                    });
                }
            })
            .end();
            return resultdata;
        },
        /**
         * データオブジェクトより表示エリアを作成する
         * 指定したプレフィックスを追加したidの名前で作成する
         * ラジオボタンのみnameで検索する
         * 引数
         * ・$view : 表示エリアのjQueryオブジェクト (例：$('#viewarea'))
         * ・resultdata : データオブジェクト
         * ・prefix		: アプリ固有のプレフィックス 指定されない場合は 'ca_'
         * 戻り値
         * ・なし
         */
        data2view: function($view, resultdata, prefix) {
            var _prefix = clutil.text.buildPrefixMatcher(prefix, clutil.defaultCaPrefix);
            var _id = '';

            function mask($el, value) {
                return $.inputlimiter.mask(value, {
                    limit: $el.attr('data-limit'),
                    filter: $el.attr('data-filter')
                });
            }

            $view
            .find("input[id]")
            .filter('[type="text"]').each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                var $this = $(this);
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                }

                // domain 付きの場合は controller から設定値入力
                var domain = clutil.getDomain($this);
                if(domain && domain.controller){
                    domain.controller.setValue($this, resultdata[_id]);
                    return;
                }

                if ($this.hasClass('hasDatepicker')) {
                    // datepickerの場合は、datepicker('setDate', dateObj) APIを使ってセットする。
                    var dt = clutil.date.toDate(resultdata[_id]);
                    if(_.isNaN(dt.getTime())){
                        // Invalid Date
                        this.value = '';
                    }else{
                        $this.datepicker('setDate', dt);
                    }
                    return;
                }
                if ($this.hasClass('cl_MtSuggestView')) {
                	// サジェスト
                    // TODO: サジェストの場合は、Model オブジェクト介入でセットすることが前提なので、data2view サポートは見送る。
                    var api = $this.data('cl_MtSuggestView');
                    if(api){
                        switch(api.getValueType()){
                        case 'obj':
                        case 'id':
                            break;
                        case 'text':
                        default:
                            // valueType がテキストサジェストな場合は入れておく。
                            this.value = resultdata[_id];
                        }
                    }
                    return;
                }
                if ($this.hasClass('cl_date')) {
                    // 非datepickerの場合は日付を表示用に変換する
                    this.value = clutil.date.dateFormat('yyyy/MM/dd', resultdata[_id]);
                } else if ($this.hasClass('cl_month')) {
                    // 月指定の場合は日付を表示用に変換する
                    this.value = clutil.monthFormat(resultdata[_id], 'yyyy/mm');
                } else if ($this.hasClass('cl_time')) {
                    // 時刻指定の場合は時刻を表示用に変換する
                    this.value = clutil.timeFormat(resultdata[_id], 'hh:mm');
                } else {
                    this.value = clutil.cStr(mask($this, resultdata[_id]));
                }
            })
            .end()
            .filter('[type="hidden"]').each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                }
                // hidden
                this.value = clutil.cStr(resultdata[_id]);
            })
            .end()
            .filter('[type="password"]').each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                }
                // パスワード
                this.value = clutil.cStr(resultdata[_id]);
            })
            .end()
            .filter('[type="checkbox"]').each(function(){
                //---------------------------
                // チェックボックス単体
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                }
                var $this = $(this);
                var val = resultdata[_id], f_check = _.isFinite(val) ? parseInt(val, 10) : null;

                var $swApi = $this.data('cl_switch');
                if($swApi){
                    // Bootstrap switch
                    $swApi.bootstrapSwitch('setState', f_check);
                    return;
                }else{
                    // Bootstrap checkbox
                    if(f_check && f_check !== 0){
                        $this.prop("checked", true);
                        $this.checkbox('check');
                    }else{
                        $this.prop("checked", false);
                        $this.checkbox('uncheck');
                    }
                }
            })
            .end()
            .end()
            .find("input[name]")
            .filter('[type="radio"]').each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.name);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null && _.has(resultdata, _id) === false) {
                    // なにもしない
                    return;
                }
                // ラジオボタン
                var $this = $(this);
                var val = resultdata[_id];
                if(val == null/* null 明示 */){
                    // null or undefined 明示の場合はクリアと見做す
                    if(this.checked === true){
                        $this.prop('checked', false);
                        $this.radio('uncheck');
                    }
                }else{
                    // value 値が同じならば checked にする。
                    if(val == this.value){
                        $this.prop('checked', true);
                        $this.radio('check');
                    }
                }
            })
            .end()
            .filter('[type="checkbox"].cl_cbgroup').each(function(){
                //---------------------------
                // チェックボックスグループ
                // <input type="checkbox" name="_id" value="anyTypeOfValue">
                _id = clutil.text.isMatchThenTrim(_prefix, this.name);
                if(_.isEmpty(_id)){
                    return;
                }
                var cbVal = this.value;

                // name, value 属性が設定されていること
                if(!_.isEmpty(_id) && !_.isEmpty(cbVal)){
                    var results = resultdata[_id];	// Array<string> or Array<int> 型を期待。
                    if(!_.isArray(results)){
                        results = [ results ];
                    }
                    var $this = $(this);
                    // ↓_.contains() を使わない理由: 「123 == "123"」(数:数文字列)比較を有効とするため。
                    var f_check = _.some(results, function(rv){ return rv == cbVal; });
                    if(f_check){
                        // チェックをつける
                        $this.prop("checked", true);
                        $this.checkbox('check');
                    } else {
                        // チェックを外す
                        $this.prop("checked", false);
                        $this.checkbox('uncheck');
                    }
                }
            })
            .end()
            .end()
            .find("textarea[id]").each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                }
                // テキストエリア
                this.value = clutil.cStr(resultdata[_id]);
            })
            .end()
            .find("select[id]").each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                }
                var $this = $(this);
                if($this.data('selectpicker')){
                    // clutil.view.XXX 対応
                    if($this.hasClass('cl_YMDSelect')){
                        if($this.hasClass('cl_YMDSelect-year')){
                            // 年部
                            var api = $this.data('cl_YMDSelect');
                            if(api){
                                api.data2view(resultdata, _prefix);
                            }
                        }else{
                            // 月日部
                            // 年セレクタの検出により値をセットする
                        }
                    }else if($this.hasClass('cl_hierarchy')){
                        var hiCtx = $this.data('cl_hierarchy');
                        if(hiCtx && hiCtx.level === 0){
                            // 階層ルート部
                            hiCtx.api.data2view(resultdata, _prefix);
                        }else{
                            // 階層ルート部セレクタの検出により値をセットする
                        }
                    }else{
                        $this.selectpicker('val', resultdata[_id]);
                    }
                }else{
                    var str = clutil.cStr(resultdata[_id]);
                    this.value = str;
                }
            })
            .end()
            .find("span[id]").each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                } else if ($(this).hasClass('cl_date')) {
                    // datepickerの場合は日付を表示用に変換する
                    $(this).html(clutil.dateFormat(resultdata[_id], 'yyyy/mm/dd'));
                } else if ($(this).hasClass('cl_month')) {
                    // 月指定の場合は日付を表示用に変換する
                    $(this).html(clutil.monthFormat(resultdata[_id], 'yyyy/mm'));
                } else if ($(this).hasClass('cl_time')) {
                    // 時刻指定の場合は時刻を表示用に変換する
                    $(this).html(clutil.timeFormat(resultdata[_id], 'hh:mm'));
                } else {
                    $(this).text(clutil.cStr(mask($(this), resultdata[_id])));
                }
            })
            .end()
            .find("p[id]").each(function(){
                // プレフィックスを除去する
                _id = clutil.text.isMatchThenTrim(_prefix, this.id);
                if(_.isEmpty(_id)){
                    return;
                }
                if (resultdata[_id] == null) {
                    // なにもしない
                    return;
                } else if ($(this).hasClass('cl_date')) {
                    // datepickerの場合は日付を表示用に変換する
                    $(this).html(clutil.dateFormat(resultdata[_id], 'yyyy/mm/dd'));
                } else if ($(this).hasClass('cl_month')) {
                    // 月指定の場合は日付を表示用に変換する
                    $(this).html(clutil.monthFormat(resultdata[_id], 'yyyy/mm'));
                } else if ($(this).hasClass('cl_time')) {
                    // 時刻指定の場合は時刻を表示用に変換する
                    $(this).html(clutil.timeFormat(resultdata[_id], 'hh:mm'));
                } else {
                    $(this).html(clutil.cStr(mask($(this), resultdata[_id])));
                }
            })
            .end();
        },

        /**
         * 表示エリアよりデータオブジェクトを作成する(table版)
         * 指定したプレフィックスを除去したnameの名前で作成する
         * @param {jQuery} $trobj trのjQueryオブジェクト (例：$('#ca_tbody_dlv').children())
         * @param {Object} options clutil.serializeへのオプション
         * @return {Object} シリアライズ結果
         */
        tableview2data: function($trobj, options) {
            return _.map($trobj, function(el) {
                var data = clutil.serialize(el, options);
                return data;
            });
        },

        /**
         * 指定Viewになにか入力されたかの判断を行う
         * @param {jQuery} $view jQueryオブジェクト (例：$('#ca_tbody_dlv tr:last')
         * @param {Object} options clutil.serializeへのオプション
         * @param {Array} options.exclude 除外フィールドのキー(name属性)のリスト
         * @return {boolean} 入力されたかどうか
         * @example
         *	 var isEmpty = clutil.isViewEmpty($('#ca_tbody_dlv_info_div > tr:last'), {exclude: ['ca_dlv_place_sw']});
         */
        isViewEmpty: function($view, options) {
            var myOptions = options || {},
            selectNames = _.reduce($view.find('select[name]'), function(memo, select) {
                var name = select.name;
                if (!~_.indexOf(myOptions.exclude || [], name))
                    memo.push(name);
                return memo;
            }, []),
            data = clutil.serialize($view, _.extend({}, myOptions, {
                exclude: selectNames.concat(myOptions.exclude)
            })),
            selectData = clutil.serialize($view, _.extend({}, myOptions, {
                include: selectNames
            })),
            x = _.all(data, function(value) {return !value}),
            y = _.all(selectData, function(value) {return value === '0'});
            return x && y;
        },

        _readonlySupportedList: [
            {
                // bootstrap checkbox / bootstrap switch
                target: 'input:checkbox',
                setter: function($input, ro){
                    var $bsSw = $input.data('cl_switch');
                    if($bsSw){
                        // bootstrap switch
                        var active = !ro;
                        $bsSw.bootstrapSwitch('setActive', active);
                    }else{
                        // bootstrap checkbox
                        $input.prop('disabled', ro);
                        var $label = $input.closest('label');
                        if(ro){
                            $label.addClass('disabled');
                        }else{
                            $label.removeClass('disabled');
                        }
                    }
                }
            },
            {
                // bootstrap radio
                target: 'input:radio',
                setter: function($input, ro){
                    $input.prop('disabled', ro);
                    var $label = $input.closest('label');
                    if(ro){
                        $label.addClass('disabled');
                    }else{
                        $label.removeClass('disabled');
                    }
                }
            },
            {
                target: 'input:text.hasDatepicker',
                setter: function($input, ro){
                    $input.prop('disabled', ro);
                    $input.datepicker(ro ? 'disable' : 'enable');
                }
            },
            {
                target: 'input:text.cl_MtSuggestView',
                setter: function($input, ro){
                    var v = $input.data('cl_MtSuggestView');
                    if(v){
                        v.setReadonly(ro);
                    }else{
                        $input.prop('readonly', ro);
                    }
                }
            },
            {
                target: 'input[type=text],[type=password]',
                setter: 'readonly'
            },
            {
                target: 'input',
                setter: 'disabled'
            },
            {
                target: 'textarea',
                setter: 'readonly'
            },
            {
                target: 'select',
                setter: function($input, ro){
                    $input.prop('disabled', ro);
                    if($input.data('selectpicker')){
                        $input.selectpicker('refresh');
                    }
                }
            },
            {
                target: 'button',
                setter: 'disabled'
            }
        ],

        /**
         * 指定されたエレメントをリードオンリーにする。
         * @param input 操作対象の input 系要素
         * @param ro true: readonly 化、false: readonly 解除、default[true]
         */
        inputReadonly: function($input, ro) {
            var ro = (ro == null) ? true : ro;
            var supportedList = clutil._readonlySupportedList;
            var isApplied = false;
            _.some(supportedList, function(args) {
                if ($input.is(args.target)) {
                    if(_.isFunction(args.setter)){
                        args.setter($input, ro);
                    }else if(!_.isEmpty(args.setter)){
                        $input.prop(args.setter, ro);
                    }
                    isApplied = true;
                    return true;
                }
            });
            if(!isApplied){
                console.warn('clutil.inputReadonly[' + ro + ']: ' + $input[0] + ' -- not supported yet!!!');
            }
            return isApplied;
        },

        /**
         * 指定されたエレメントのリードオンリーを解除する。
         * @param input 操作対象の input 系要素
         */
        inputRemoveReadonly: function($input) {
            return this.inputReadonly($input, false);
        },

        /**
         * 表示エリアを読み取り専用にする
         * 引数
         *
         * @param {jQuery} $view 表示エリアのjQueryオブジェクト (例：$('#viewarea'))
         * @param {Object} [options] オプション
         * @param {String,Function} [options.filter] jQuery.filter関数への引数
         */
        viewReadonly: function($view, options) {
            options = options || {};
            _.defaults(options, {
                filter: '*'
            });

            $view
            .filter(options.filter)
            .find('.cl-a-tag').each(function() {
                var $this = $(this);
                $(this).attr('data-href-orig', $this.attr('href'));
                $(this).attr('href', 'javascript:void(0);');
                $this.addClass('cl-a-disabled');
                $(this).attr('disabled', true);
            })
            .end()
            .find('.cl-file-attach [type=file]').each(function() {
                $(this).attr('disabled', true)
                .closest('.cl-file-attach').attr('disabled', 'disabled');
            })
            .end()
            .find("input[id],.cl-file-table input")
            .filter('[type="text"]').each(function(){
                var $this = $(this);
                clutil.inputReadonly($this);
            })
            .end()
            .filter('[type="password"]').each(function(){
                // パスワード
                $(this).attr("readonly", "readonly");
            })
            .end()
            .filter('[type="checkbox"]').each(function(){
                // id 付きのチェックボックス
                clutil.inputReadonly($(this), true);
            })
            .end()
            .end()
            .find("input[name]")
            .filter('[type="radio"]').each(function(){
                // ラジオボタン
                clutil.inputReadonly($(this), true);
            })
            .end()
            .filter('[type="checkbox"].cl_cbgroup').each(function(){
                // チェックボックスグループ
                $(this).prop('disabled', true).closest('label').addClass('disabled');
            })
            .end()
            .end()
            .find("textarea[id]").each(function(){
                // テキストエリア
                $(this).attr("readonly", "readonly");
            })
            .end()
            .find("select[id]").each(function(){
                // コンボボックス(マルチセレクト未サポート)
                var $this=$(this);
                if($this.hasClass('cl_YMDSelect-year')){
                    // 年月日
                    var api = $this.data('cl_YMDSelect');
                    if(api != null){
                        api.setEnable(/* enabled */false);
                    }
                }else if($this.hasClass('cl_hierarchy')){
                    // 階層
                    var hiCtx = $this.data('cl_hierarchy');
                    if(hiCtx && hiCtx.level === 0){
                        hiCtx.api.setEnable(/* enabled */false);
                    }
                }else{
                    $this.prop('disabled',true);
                    if($this.data('selectpicker')){
                        $this.selectpicker('refresh');
                    }
                }
            })
            .end()
            .find("button[id],.cl-file-delete,tr.addrow,a.btn").each(function(){
                // ボタン
                $(this).attr("disabled", true);
            })
            .end()
            .find('.cl_pager').each(function(){
                // ページャ
                var $this = $(this);
                var pv = $this.data('cl_pager');
                if(pv != null){
                    pv.setEnable(false);
                }
            })
            .end()
            .find('table.cl_tableview').each(function(){
                // clutil.view.TableView
                var $this = $(this);
                var tv = $this.data('cl_tableview');
                if(tv != null){
                    tv.setEnable(false);
                }
            })
            .end();
        },
        /**
         * 表示エリアを読み取り専用から戻す
         * 引数
         * ・$view : 表示エリアのjQueryオブジェクト (例：$('#viewarea'))
         * 戻り値
         * ・なし
         */
        viewRemoveReadonly: function($view) {
            $view
            .find('.cl-a-tag').each(function() {
                var $this = $(this);
                $this.attr('href', $this.attr('data-href-orig'));
                $this.removeClass('cl-a-disabled');
            })
            .end()
            .find('.cl-file-attach [type=file]').each(function() {
                $(this).attr('disabled', false)
                .closest('.cl-file-attach').attr('disabled', false);
            })
            .end()
            .find("input[id],.cl-file-table input")
            .filter('[type="text"]').each(function(){
                var $this = $(this);
                clutil.inputRemoveReadonly($this);
            })
            .end()
            .filter('[type="password"]').each(function(){
                // パスワード
                $(this).removeAttr("readonly");
            })
            .end()
            .filter('[type="checkbox"]').each(function(){
                // id 付きのチェックボックス
                clutil.inputReadonly($(this), false);
            })
            .end()
            .end()
            .find("input[name]")
            .filter('[type="radio"]').each(function(){
                // ラジオボタン
                clutil.inputReadonly($(this), false);
            })
            .end()
            .filter('[type="checkbox"].cl_cbgroup').each(function(){
                // チェックボックスグループ
                $(this).prop('disabled', false).closest('label').removeClass('disabled');
            })
            .end()
            .end()
            .find("textarea[id]").each(function(){
                // テキストエリア
                $(this).removeAttr("readonly");
            })
            .end()
            .find("select[id]").each(function(){
                // コンボボックス(マルチセレクト未サポート)
                var $this=$(this);
                if($this.hasClass('cl_YMDSelect-year')){
                    // 年月日
                    var api = $this.data('cl_YMDSelect');
                    if(api != null){
                        api.setEnable(/* enabled */true);
                    }
                }else if($this.hasClass('cl_hierarchy')){
                    // 階層
                    var ctx0 = $this.data('cl_hierarchy');
                    if(ctx0 && ctx0.level === 0){
                        ctx0.api.setEnable(/* enabled */true);
                    }
                }else{
                    $this.prop('disabled',false);
                    if($this.data('selectpicker')){
                        $this.selectpicker('refresh');
                    }
                }
            })
            .end()
            .find("button[id],.cl-file-delete,tr.addrow,a.btn").each(function(){
                // ボタン
                $(this).removeAttr("disabled");
            })
            .end()
            .find('.cl_pager').each(function(){
                // ページャ
                var $this = $(this);
                var pv = $this.data('cl_pager');
                if(pv != null){
                    pv.setEnable(true);
                }
            })
            .end()
            .find('table.cl_tableview').each(function(){
                // clutil.view.TableView
                var $this = $(this);
                var tv = $this.data('cl_tableview');
                if(tv != null){
                    tv.setEnable(true);
                }
            })
            .end();
        },
        /**
         * 表示エリアをクリアする
         * 引数
         * ・$view : 表示エリアのjQueryオブジェクト (例：$('#viewarea'))
         * ・f_opedate : 日付を運用日に戻すフラグ デフォルトはtrue
         * 戻り値
         * ・なし
         */
        viewClear: function($view, f_opedate) {
            f_opedate = f_opedate == null ? true : f_opedate;
            $view
            .find("input[id]")
            .filter('[type="text"]').each(function(){
                this.value = "";

                // datepickerの場合は運用日を入れなおす
                var $this = $(this);
                if (f_opedate && $this.hasClass('cl_date')) {
                    if($this.hasClass('hasDatepicker')){
                        var dt = clutil.date.toDate(clcom.ope_date);
                        $this.datepicker('setDate', dt);
                    }else{
                        this.value = clutil.date.dateFormat('yyyy/MM/dd', clcom.ope_date);
                    }
                }
            })
            .end()
            .filter('[type="hidden"]').each(function(){
                this.value = "";
            })
            .end()
            .filter('[type="password"]').each(function(){
                // パスワード
                this.value = "";
            })
            .end()
            .filter('[type="checkbox"]').each(function(){
                var $this = $(this), $swApi = $this.data('cl_switch');
                if($swApi){
                    // Bootstrap Switch
                    $swApi.bootstrapSwitch('setState', false);
                    return;
                }else{
                    // チェックボックス
                    $this.attr("checked", "false");
                    $this.checkbox('uncheck');
                }
            })
            .end()
            .end()
            .find('input[name]')
            .filter('[type="radio"]').each(function(){
                // ラジオボタン
                $(this).attr("checked", "false");
                $(this).radio('uncheck');
            })
            .end()
            .end()
            .find("textarea[id]").each(function(){
                // テキストエリア
                this.value = "";
            })
            .end()
            .find("select[id]").each(function(){
                // コンボボックス(マルチセレクト未サポート)
                $(this).prop('selectedIndex', 0);
                $(this).selectpicker('val', 0);

                // datepickerの場合は運用日を入れなおす
                if (f_opedate && $(this).hasClass('cl_yyyy')) {
                    var ope_year = clutil.date.dateFormat('yyyy/MM/dd', clcom.ope_date);
                    $(this).selectpicker('val', ope_year);
                } else if (f_opedate && $(this).hasClass('cl_mm')) {
                    var ope_month = clutil.date.dateFormat('MM', clcom.ope_date);
                    $(this).selectpicker('val', ope_month);
                }
            })
            .end()
            .find("span[id]").each(function(){
                $(this).html("");
            });
        },

        /**
         * オンライン入力制限
         * キャラマップによる制限(全角、半角、アルファベット、数字、正規表現)と
         * 文字数による制限をする。
         *
         *
         * @param {jQuery Object} $el
         * @return {undefined}
         * @example
         * <form id="myForm">
         * <input type="text" data-limit="len:5 zenkaku">
         * <input type="text" data-limit="digit">
         * </form>
         * clcom.inputlimiter($('#myForm'));
         *
         * data-limit に指定できるのは以下
         * len:長さ 長さ制限
         * zenkaku 全角のみ
         * hankaku 半角のみ
         * alpha アルファベットのみ
         * alnum 英数字のみ
         * digit 数値のみ
         */
        inputlimiter: function($el) {
            var $elements = $el.find('[data-limit],[data-filter]'),
            fn = $.fn.inputlimiter;
//              args = Array.prototype.slice.call(arguments, 1);

            if ($elements.length > 0) {
                $elements.each(function(i, e) {
                    fn.call($(e), 'update');
                });
            } else if ($el.is('[data-limit],[data-filter]')) {
                return fn.call($el, 'update');
            }
//              else {

//              }
        },

        /**
         * 数値をカンマ区切りにする。
         *
         * @param {String} 数値の文字列形式
         * @example
         * clutil.comma('123456789') //=> 123,456,789
         * clutil.comma('12345.67890') //=> 12,345.67890
         */
        comma: function(value) {
            console.warn('@Deplicated: use clutil.text.comma()');
            if (value == null) return '';
            return $.inputlimiter.Filters.comma().mask(value.toString());
        },

        /**
         * 全角を半角に変換する
         */
        inputzen2han: function(e) {
            console.warn('@Deplicated: ....');
            var addFlag = false;

            var input = $(e.target).closest('input');

            if (addFlag) {
                return;
            }
            addFlag = true;

            // 入力値を取得
            var val = $(input).val();

            // イベントリセット
            e.preventDefault();
            $(input).val(clutil.zen2han(val));
            addFlag = false;
        },


        /**
         * メールアドレス形式をチェック				TODO: 削除予定
         */
        chkemailfmt: function(e) {
            console.warn('@Deplicated: use clutil.text.CommonRegEx.mail.text(text)');
            var addFlag = false;

            var $input = $(e.target).closest('input');

            if (addFlag) {
                return;
            }
            addFlag = true;

            // 入力値を取得
            var val = $input.val();

            // イベントリセット
            e.preventDefault();

            // エラーを解除
            $input
            .removeClass('cl_email_error_field')
            .removeAttr('data-cl-errmsg');
            $('div.tooltip').remove(); // XXXX なぜか消えない不具合対策

            if (val === '') {
                // 空欄はOKとする。
                addFlag = false;
                return;
            }
            var reg = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
            if (!reg.test(val)) {
                $input
                .addClass('cl_email_error_field')
                .attr('data-cl-errmsg', clmsg.cl_email);
            }
            addFlag = false;

        },

        /**
         * 検索条件、条件結果表示エリアをコントロールする
         * @return controller
         *  .init()                 初期表示状態にする
         *  .showCond(callback)     検索条件エリアを表示する
         *  .showResult(callback)   検索結果エリアを表示する
         *  .focus()    [検索条件を再設定ボタン」にフォーカスする。
         *              [～再設定ボタン] が hide の場合、[検索ボタン] を推測してフォーカスを入れる。
         */
        controlSrchArea: function(wrapper) {
            var controller = {
                $filter         : wrapper.find('.js-filter'),
                filterHeight    : wrapper.find('.js-filter').height(),
                $secSubmit      : wrapper.find('.js-section-submit'),
                $secFilter      : wrapper.find('div.js-section-filter'),
                $btnFilterAgain : wrapper.find('.js-btn-filter-again'),
                $filterResult   : wrapper.find('.js-filter-result'),
                init: function(){
                    var me = this;
                    me.$btnFilterAgain.hide();
                    me.$filterResult.hide();
                    me.$filter.css({height: 'auto'});
                    me.$secFilter.show();
                    me.$secSubmit.show();
                    $('html,body').animate({scrollTop: 0}, 500, 'swing');
                },
				showResult: function(callback){
                    var me = this;
                    if (!me.$secFilter.hasClass('section-filter-fixed')) {
                      me.$secSubmit.hide();
                    }
                    me.$filter.animate({
                        height: "0px"
                    }, 200, function(){
                        me.$secFilter.hide();
                        if(_.isFunction(callback)){
                            callback();
                        }
                    });
                    me.$btnFilterAgain.fadeIn();
                    if (me.$secFilter.hasClass('section-filter-fixed')) {
                      me.$filterResult.fadeIn();
                    } else {
                      me.$filterResult.show();
                    }
                },
                showCond: function(callback){
                    var me = this;
                    me.$btnFilterAgain.hide();
                    me.$filter.animate({
                        height: me.filterHeight
                    }, 200, function(){
                        me.$filter.css({height: 'auto'});
                        me.$secFilter.fadeIn('normal', callback);
                    });
                    me.$secSubmit.fadeIn();
                    $('html,body').animate({scrollTop: 0}, 500, 'swing');
                },
                focus: function(){
                    var me = this;
                    if(me.$btnFilterAgain.css('display') !== 'none'){
                        // 「検索条件を再設定ボタン」にフォーカス
                        me.$btnFilterAgain.find('button').focus();
                    }else if(me.$secSubmit.css('display') !== 'none'){
                        // 検索ボタンにフォーカス
                        me.$secSubmit.find('button:not(.btn-flat-default)').get(0).focus();
                    }
                }
            };
            clutil.bindAll(controller);
//              controller.$btnFilterAgain.click(_.bind(function(){
//              this.showCond();
//              },controller));
            controller.init();

            return controller;
        },

        /**
         * 区分selectorの作成
         * 引数
         * ・$div            : 区分selectを作成する親divのjQueryオブジェクト (例：$('#viewarea'))、または <select> 自身
         * ・kind            : 区分名
         * ・unselectedflag  : 未選択値 0:なし 1:あり
         * ・namedisp        : 名称のみを取得したい場合1を設定。なにも指定されていない場合はコード：名称で返す
         * ・option          : 内部生成する<select>要素の attributes (id名、nameなど) を記述
         *                   : 例 { id : "ca_select", name : "info" }
         * ・cls             : classに追加したいものを文字列で羅列
         *                   : 例 "mbn wt280 flleft"
         * ---
         * 引数(Object指定)
         * {
         *   .$el            : 区分 select を生成するラッパの jQuery オブジェクト、または <select> 自身
         *   .typeTypeId     : 区分種別 ID
         *   .hasNullItem    : 未選択要素有無、省略時は true
         *   .labelFormat    : 選択肢のラベルフォーマット -- 'code:name', 'name or 'code', 省略時は 'name'
         *   .select         : 内部生成する <select> 要素の生成ルール
         *      .attributes  : 内部 <select> の attributes (id名、nameなど) を記述
         *      .className   : 内部 <select> の class を記述
         *   .selectpicker   : selectpicker へ渡す引数、省略時は { style: 'button-input' }
         * }
         */
        initcltypeselector: function($div, kind, unselectedflag, namedisp, option, cls){
            console.warn('@Deplicated clutil.initcltypeselector(): use ...<準備します>...');
            if(clutil.view == null || !_.isFunction(clutil.view.initSelectpicker)){
                throw 'clutil.view.initSelectpicker() not supported.';
            }
            var fixOpt;
            if(arguments.length === 1 && _.isObject(arguments[0])){
                fixOpt = _.defaults(arguments[0], {
                    hasNullItem: true,
                    labelFormat: 'name',
                    select: {},
                    selectpicker: { style: 'btn-input' }
                });
            }else{
                //html_source += key + '="' + value + '"';
                fixOpt = {
                    $el: $div,
                    typeTypeId: kind,
                    hasNullItem: (unselectedflag === 1),
                    labelFormat: (namedisp === 1) ? 'name' : 'code:name',
                    select: {}
                };
                if(option){
                    fixOpt.select.attributes = option;
                }
                if(cls){
                    fixOpt.select.className = cls;
                }
            }

            // 区分名より区分リストを取得する
            var typenamelist = clcom.getTypeList(fixOpt.typeTypeId);
            if (typenamelist == null) {
                return;
            }

            // selectpicker 初期化
            var o = {
                $el: fixOpt.$el,
                select: fixOpt.select,
                selection: {
                    items: typenamelist,
                    altPropName: { id:'type_id' },
                    hasNullItem: fixOpt.hasNullItem,
                    labelFormat: fixOpt.labelFormat
                },
                selectpicker: fixOpt.selectpicker
            };
            if(!o.$el.is('select')){
                o.select = fixOpt.select;
            }
            clutil.view.initSelectpicker(o);
        },

        /**
         * 区分selectorの作成
         * 引数
         * ・$div            : 区分selectを作成する親divのjQueryオブジェクト (例：$('#viewarea'))、または <select> 自身
         * ・list            : 区分リスト
         * ・unselectedflag  : 未選択値 0:なし 1:あり
         * ・namedisp        : 名称のみを取得したい場合1を設定。なにも指定されていない場合はコード：名称で返す
         * ・idname          : IDのid名（defaultは"id")
         * ・namename        : 名称のid名（defaultは"name")
         * ・codename        : コードのid名（defaultは"code")
         * ・option          : id名、nameなどを記述
         *                   : 例 { id : "ca_select", name : "info" }
         * ・cls             : classに追加したいものを文字列で羅列
         *                   : 例 "mbn wt280 flleft"
         * ・size            : コンボボックスサイズ指定を追加
         */
        initcltypeselector2: function($div, list, unselectedflag, namedisp,
                idname, namename, codename, option, cls, size) {
            console.warn('@Deplicated clutil.initcltypeselector2(): use ...<準備します>...');
            if(clutil.view == null || !_.isFunction(clutil.view.initSelectpicker)){
                throw 'clutil.view.initSelectpicker() not supported.';
            }

            // clutil.view.initSelectpicker() へ委譲
            var o = {
                $el: $div,
                selection: {
                    items: list,
                    altPropName: {
                        id: idname || 'id',
                        code: codename || 'code',
                        name: namename || 'name'
                    },
                    hasNullItem: (unselectedflag == 1)
                },
                selectpicker: {
                    size: size || 'auto'
                }
            };
            if(!$div.is('select')){
                o.select = {
                    attributes: option,
                    className: cls
                };
            }
            clutil.view.initSelectpicker(o);
        },

        /**
         * 年月別selectorの作成
         * 引数
         * ・$y_select			: 年表示用selectのjQueryオブジェクト (例：$('#viewarea'))
         * ・$m_select			: 月表示用selectのjQueryオブジェクト (例：$('#viewarea'))
         * ・unselectedflag		: 未選択値 0:なし 1:あり
         * ・past		: 過去何年から defaultは10年
         * ・future		: 未来何年まで defaultは0年
         * ・yyyymm		: 初期選択設定値、省略時は運用日年月
         */
        clyearmonthselector: function($y_select, $m_select, unselectedflag, past, future, yyyymm) {
            console.warn('@Deplicated clutil.clyearmonthselector(): use ...<準備します>...');
            if(clutil.view == null || !_.isFunction(clutil.view.initSelectpicker)){
                throw 'clutil.initcltypeselector() not supported.';
            }

            // 引数解析
            var o;
            if(arguments.length === 1){
                o = arguments[0];
            }else{
                o = {
                    $y_select: $y_select,
                    $m_select: $m_select,
                    hasNullItem: (unselectedflag == 1),
                    pYear: 2006,
                    future: future,
                    yyyymm: yyyymm
                };
            }
            o = _.defaults(o, {
                hasNullItem: false,
                pYear: 2006,			// Default: 2006年～にする 5/8
                future: 0,
                yyyymm: Math.floor(clcom.ope_date / 100)
            });

            var ope_year = Math.floor(clcom.ope_date / 10000);
            if(o.past > 0){
                o.pYear = ope_year - o.past;
            }

            // 引数チェック
            if(!o.$y_select.is('select')){
                throw 'clutil.clyearmonthselector(): invalid arguments, $y_select is not <select>.';
            }
            if(!o.$m_select.is('select')){
                throw 'clutil.clyearmonthselector(): invalid arguments, $m_select is not <select>.';
            }

            // -------------------------------------------------------
            // 年selectorの作成
            var yyyy = Math.floor(o.yyyymm/100);	// 初期値(年)
            var n_past = o.past;					// 過去何年から
            var n_future = o.future;				// 未来何年まで

            // 選択肢(年)を作る
            var yItems = [];
            for (var y = ope_year + n_future; y >= o.pYear; y--) {
                yItems.splice(0,0,{
                    id: y,
                    name: y + '年'
                });
            }
            // selectpicker 初期化
            var oy = {
                $el: o.$y_select,
                selection: {
                    items: yItems,
                    hasNullItem: o.hasNullItem
                }
            };
            clutil.view.initSelectpicker(oy);
            o.$y_select.selectpicker('val', yyyy);	// 初期値は運用日

            // -------------------------------------------------------
            // 月selectorの作成
            var ope_month = Math.floor(clcom.ope_date / 100) % 100;
            var mm = (o.yyyymm == null) ? ope_month : Math.floor(o.yyyymm%100);	// 初期値(月)

            // 選択肢(月)を作る
            var mItems = [];
            for (var m = 1; m <= 12; m++) {
                mItems.push({
                    id: m,
                    name: m+'月'
                });
            }
            // selectpicker 初期化
            var om = {
                $el: o.$m_select,
                selection: {
                    items: mItems,
                    hasNullItem: o.hasNullItem
                }
            };
            clutil.view.initSelectpicker(om);
            o.$m_select.selectpicker('val', mm);	// 初期値は運用日
        },

        /**
         * 時間selectorの作成
         * 引数
         * ・$hh_select          : 時区分selectのjQueryオブジェクト (例：$('#viewarea'))
         * ・$mm_select          : 分区分selectのjQueryオブジェクト (例：$('#viewarea'))
         * ・unselectedflag      : 未選択値 0:なし 1:あり
         * ・start       : 開始時刻から defaultは900
         * ・end         : 終了時刻まで defaultは2200
         * ・minute      : 刻み分数 defaultは30
         */
        cltimeselector: function($hh_select, $mm_select, unselectedflag, start, end, minute) {
            console.warn('@Deplicated clutil.cltimeselector(): use ...<準備します>...');
            if(clutil.view == null || !_.isFunction(clutil.view.initSelectpicker)){
                throw 'clutil.cltimeselector() not supported.';
            }

            // 引数解析
            var o;
            if(arguments.length === 1){
                o = arguments[0];
            }else{
                o = {
                    $hh_select: $hh_select,
                    $mm_select: $mm_select,
                    hasNullItem: (unselectedflag == 1),
                    start: start,
                    end: end,
                    minute: minute
                };
            }
            o = _.defaults(o, {
                hasNullItem: false,
                start: clcom.limits.s_time,
                end: clcom.limits.e_time,
                minute: 1       // 1分刻み
            });
            if(o.hasNullItem === true){
                o.hasNullItem = {
                    id: NaN     // 0時指定があり得るため、未設定選択肢 <option value>&nbsp;</option> に value を入れないよう NaN を指定する。
                };
            }

            if(!o.$hh_select.is('select')){
                throw 'clutil.cltimeselector(): invalid arguments, $hh_select is not <select>';
            }
            if(!o.$mm_select.is('select')){
                throw 'clutil.cltimeselector(): invalid arguments, $mm_select is not <select>';
            }

            var n_start = o.start;      // 開始時刻から
            var n_end = o.end;          // 終了時刻まで
            var n_minute = o.minute;    // 刻み分数

            // -------------------------------------------------------
            // 時セレクターの作成
            var h_start = Math.floor(n_start/100);
            var h_end = Math.floor(n_end/100);

            // 選択肢(時)を作る
            var hItems = [];
            for (var h = h_start; h <= h_end; h++) {
                hItems.push({
                    id: h,
                    name: clutil.text.zeroPadding(h, 2)
                });
            }
            var oh = {
                $el: o.$hh_select.addClass('cl_timeselect'),
                selection: {
                    items: hItems,
                    hasNullItem: o.hasNullItem
                }
            };
            clutil.view.initSelectpicker(oh);

            // -------------------------------------------------------
            // 分セレクターの作成
            var mItems = [];
            for (var m = 0; m < 60; m += n_minute) {
                mItems.push({
                    id: m,
                    name: clutil.text.zeroPadding(m, 2)
                });
            }
            var om = {
                $el: o.$mm_select.addClass('cl_timeselect'),
                selection: {
                    items: mItems,
                    hasNullItem: o.hasNullItem
                }
            };
            clutil.view.initSelectpicker(om);
        },

        /**
         * bootstrap対応
         * 選択画面起動時などに呼ぶ
         */
        initUIelement: function($view) {
            $view.find('[data-toggle="checkbox"]').each(function() {
                $(this).checkbox();
            });
            $view.find('[data-toggle="radio"]').each(function() {
                $(this).radio();
            });
            $view.find('select').each(function() {
//                  $(this).selectpicker();
                var $this = $(this);
                clutil.view.initSelectpicker({ $el: $this});
                $this.selectpicker('refresh');
            });
            var tdovercells = $("table.hilight td"),
            hoverClass = "hover",
            current_r,
            current_c;
            tdovercells.hover(
                function(){
                    var $this = $(this);
                    (current_r = $this.parent().children("table td")).addClass(hoverClass);
                    (current_c = tdovercells.filter(":nth-child("+ (current_r.index($this)+1) +")")).addClass(hoverClass);
                },
                function(){
                    if (current_r) current_r.removeClass(hoverClass);
                    if (current_c) current_c.removeClass(hoverClass);
                }
            );
            var tdRovercells = $("table.hilightRow td"),
            hoverClass = "hover",
            current_r,
            current_c;
            tdRovercells.hover(
                function(){
                    var $this = $(this);
                    (current_r = $this.parent().children("table td")).addClass(hoverClass);
                },
                function(){
                    if (current_r) current_r.removeClass(hoverClass);
                }
            );

            $view.find('#selected').undelegate('.btn-delete', 'mouseover');
            $view.find('#selected').undelegate('.btn-delete', 'mouseout');
            $view.find('#selected').undelegate('.btn-delete', 'mousedown');
            //選択した内容の削除ボタン
            $view.find('#selected').delegate('.btn-delete', 'mouseover', function(){
                $(this).parent('li').toggleClass('ovr');
            });
            $view.find('#selected').delegate('.btn-delete', 'mouseout', function(){
                $(this).parent('li').toggleClass('ovr');
            });
            $view.find('#selected').delegate('.btn-delete', 'mousedown', function(){
                $(this).parent('li').addClass('active');
            });
        },

        /**
         * 初期フォーカスの設定をする
         * @deplicated - use setPreferredFocus()
         */
        setFirstFocus: function($obj){
            console.warn('@Deplicated: use clutil.setPreferredFocus()');
            var agent = clcom.getAgent();
            // PCの場合はフォーカス設定
            if (agent == clcom.onPC) {
                clutil.setFocus($obj);
            }
            // mobile, iPadの場合はなにもしない
        },

        /**
         * 指定 $form 領域内で好ましい要素へフォーカスをセットする。初期フォーカスセットでの利用を想定。
         * @param $form 指定領域、省略時は body 全体。
         * @param lazy 遅延フラグ、true:_.defer() にフォーカスセット処理を積み込む、false:即実行する。デフォルト[false]
         */
        setPreferredFocus: function($form, lazy){
            if(lazy){
                _.defer(clutil.setPreferredFocus, $form);
                return;
            }
            if($form == null || $form.length === 0){
                $form = $('body');
            }
            var $f = $form.find(':tabbable:not([readonly]):not(:disabled):eq(0)');
            clutil.setFocus($f);
        },

        /**
         * フォーカスの設定をする
         */
        setFocus: function($obj){
            var agent = clcom.getAgent();
            var $button = $obj;

            // bootstrap-select 対応
            if($obj.data('selectpicker')){
                $button = $obj.data('selectpicker').$button;
            }

            if (agent == clcom.onPC) {
                // PCの場合はフォーカス設定
//                  $obj.focus();
                $button.focus();
            } else if (agent == clcom.onMobile && !$obj.is('input') && !$obj.is('select')) {
                // mobile, iPadの場合はinput,selectにはfocusしない
//                  $obj.focus();
                $button.focus();
            }
        },

        // @Deplicated - clutil.text.* へ移設
        han_txt: "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｧｨｩｪｫｬｭｮｯ､｡ｰ｢｣0123456789ﾞﾟ ",
        zen_txt: "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲンァィゥェォャュョッ、。ー「」０１２３４５６７８９"
            + "　　　　　ガギグゲゴザジズゼゾダヂヅデド　　　　　バビブベボ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　"
            + "　　　　　　　　　　　　　　　　　　　　　　　　　パピプペポ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　",

        zen2han: function(txt) {
            console.warn('@Deplicated: use clutil.text.zen2han()');
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = clutil.zen_txt.indexOf(c, 0);

                if (n == 70) {
                    // 空白対応
                    c = clutil.han_txt.charAt(72);
                } else if (n >= 130) {
                    c = clutil.han_txt.charAt(n-130);
                    c += clutil.han_txt.charAt(71);
                } else if (n >= 70) {
                    c = clutil.han_txt.charAt(n-70);
                    c += clutil.han_txt.charAt(70);
                } else if (n >= 0) {
                    c = clutil.han_txt.charAt(n);
                }

                retStr += c;
            }
            return retStr;
        },

        han2zen: function(txt) {
            console.warn('@Deplicated: use clutil.text.han2zen()');
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var cnext = txt.charAt(i+1);
                var n = clutil.han_txt.indexOf(c, 0);
                var nnext = clutil.han_txt.indexOf(cnext,0);
                if (n >= 0) {
                    if (nnext == 70) {
                        c = clutil.zen_txt.charAt(n+70);
                        i++;
                    } else if (nnext == 71) {
                        c = clutil.zen_txt.charAt(n+130);
                        i++;
                    } else {
                        c = clutil.zen_txt.charAt(n);
                    }
                }
                if ((n != 70) && (n != 71)) {
                    retStr += c;
                }
            }
            return retStr;
        },

        /**
         * 全角が入力されているかチェック
         * 入力されていた場合retStatにfalseを返す
         */
        chkzen2han: function(txt) {
            console.warn('@Deplicated: use clutil.text.chkzen2han()');
            var retStat = 1;
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = clutil.han_txt.indexOf(c, 0);
                if (c == ',' || c == '"') {
                    retStat = -1;
                    break;
                }
                if (n < 0) {
                    if (!c.match(/[^A-Za-z\s.-]+/)){
                    } else if (!c.match( /[^0-9]+/)){
                    } else {
                        retStat = 0;
                    }
                }
            }
            return retStat;
        },

        /**
         * 半角文字チェック
         */
        isHalf: function(c) {
            console.warn('@Deplicated: use clutil.text.isHalf()');
            c = c.charCodeAt(0);
            return (c >= 0x0 && c < 0x81) || (c == 0xf8f0) ||
            (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4);
        },

        /**
         * エラーメッセージ定義を取るためのラッパ関数
         * @param anyMsg
         * typeof String...	: 任意メッセージ文字列、第１引数はフォーマットパターン、第２引数以降は引数文字列
         * typeof Object	: サーバ応答レスポンスまたは共通レスポンスヘッダ
         * typeof Array<String>: フォーマットパターン＋引数文字列．．．
         */
        getclmsg: function(anyMsg) {
            var ss;
            if(_.isArray(anyMsg)){
                // anyMsg[0] - メッセージフォーマット
                // anyMsg[i>0] - メッセージ引数
                ss = anyMsg;
            }else if(_.isObject(anyMsg)){
                // 共通レスポンスヘッダ
                // gs_proto_common_rsp
                //	.message
                //	.args
                var rspHead = clutil.getRspHead(anyMsg) || anyMsg;
                var ss = [ rspHead.message || '' ];
                if(_.isArray(rspHead.args)){
                    ss = ss.concat(rspHead.args);
                }
            }else{
                // anyMsg: string を期待。
                ss = _.chain(arguments).flatten().value();
            }
            if(!_.isEmpty(ss[0]) && _.has(clmsg, ss[0])){
                // メッセージ定義: clmsg から取る。
                var cd = ss[0];
                ss[0] = clmsg[cd];
            }
            return clutil.fmt.apply(null, ss);
        }
    });
}());


//フォーカス遷移部品
(function() {
    var ENTER = 13, TAB = 9;

    var tabbableSelector = 'input,select,a,button,textarea,[tabindex]';
    function tabbable(el) {
        var $el = $(el),
        exclude = $el.is('[readonly],[type="hidden"]'),
        isTabbable = $el.is(':tabbable'),
        isCmButton = $el.hasClass('cm_button_single');
        return !exclude && isTabbable && !isCmButton;
    }

    var findNextElement = function($el, shift, options) {
        var i, input = $el.get(0);
        if (!input)
            return;

        var focusableElements = options.elements || (options.view ?
                options.view.find('input,select,a,button,textarea,[tabindex]') :
                    $('input,select,a,button,textarea,[tabindex]')),
                    numElements = focusableElements.length;

        if (!numElements)
            return;

        if (options.useTabindex) {
            focusableElements = _.sortBy(focusableElements, function(el) {
                return el.tabIndex || Infinity;
            });
        }

        for (i = 0; i < numElements; i += 1) {
            if (focusableElements[i] === input) {
                break;
            }
        }
        var nowIndex = i;

        var n1, n2, inc = shift < 0 ? -1 : 1,
                nextIndex = nowIndex + inc;

        if (nowIndex === numElements) {
            nextIndex = inc > 0 ? 0 : -1;
        }

        if (shift < 0) {
            inc = -1;
            n1 = -1;
            n2 = numElements;
        } else {
            inc = 1;
            n1 = numElements;
            n2 = -1;
        }

        var target;
        i = nextIndex;
        while (i !== n1) {
            if (tabbable(focusableElements[i])) {
                target = focusableElements[i];
                break;
            }
            i += inc;
        }

        if (!target) {
            i = n2 + inc;
            while (i !== nextIndex) {
                if (tabbable(focusableElements[i])) {
                    target = focusableElements[i];
                    break;
                }
                i += inc;
            }
        }

        return target;
    };

    var focus = function($input, shift, options) {
        if ($input) {
            options = options || {};

            if (shift) {
                var target = findNextElement($input, shift, options);
                if (target) {
                    $(target).focus();
                }
            } else {
                $input.focus();
            }
        }
    };

    function buildCache($elements) {
        if (!$elements) {
            $elements = $(tabbableSelector);
        }

        var i, l, el;
        var cache = [];
        for (i = 0, l = $elements.length; i < l; i += 1) {
            el = cache[i] = $elements[i];
            $(el).attr('data-tabindex', i);
        }

        return cache;
    }

    function findNextElementCache($input, shift, options) {
        var index = $input.attr('data-tabindex');
        if (!index) {
            if (shift > 0)
                index = -1;
            else
                index = 0;
        }

        index = parseInt(index, 10);
        var cache = options._cache;
        var l = cache.length;
        return cache[(index + shift + l) % l];
    }

    /**
     * Enter、 上下キーによるフォーカス移動を可能にする。
     * @deprecate {jQuery} $el 使用していません
     * @deprecate {jQuery,Function} [options.filter=false] 使用していません
     * @param {Boolean} [options.useTabindex=false] tabIndexによる順序付けするか
     * @param {Boolean} [options.convtab=false] Enterキーをタブにコンバート(msieオンリー)
     * @returns {jQuery} $el
     * @example
     * <pre>
     * clutil.enterFocusMode($('#myForm'), { useTabindex: false, convtab: false });
     * </pre>
     */
    var enterFocusMode = (function() {

        var callback = function(options, ev) {
            if (clutil._modalDialogShown && !options.view) {
                return;
            }

            var el = ev.target,
            $el = $(el),
            tagName = el.tagName.toLowerCase(),
            isButton =
                tagName === 'button' ||
                $el.hasClass('btn') ||
                $el.is('input[type=button],input[type=submit]') ||
                tagName === 'a',
                isNoEnterFocus = $el.hasClass('cl_noEnterFocus'),
                shift = 0,
                upOrDown = false,
                keyCode = ev.which,
                shiftKey = ev.shiftKey;

            switch (keyCode) {
            case 9:  // TAB
            case 13: // ENTER
                shift = shiftKey ? -1 : 1;
                break;
//              case 39: // 右
//              if (isButton) {
//              shift = 1;
//              }
//              break;
//              case 40: // 下
//              shift = 1;
//              upOrDown = true;
//              break;
//              case 37: // 左
//              if (isButton) {
//              shift = -1;
//              }
//              break;
//              case 38: // 上
//              shift = -1;
//              upOrDown = true;
//              break;
            }

            if (!shift) {
                return;
            }

            if (options.beforeFocus) {
                options.beforeFocus(ev);
            }

            if (options.upDownRow && upOrDown) {
                var $table = $el.closest('.updownfocus');
                if ($table.length && $table.get(0) !== el) {
                    var $tr = $el.closest('tr'),
                    index = $tr.find(tagName).index($el),
                    $nextTr = shift > 0 ? $tr.next() : $tr.prev(),
                            $next;
                    while ($nextTr.length) {
                        $next = $nextTr.find(tagName).eq(index);
                        if (tabbable($next)) {
                            break;
                        }
                    }
                    if ($next && $next.length) {
                        ev.preventDefault();
                        ev.stopImmediatePropagation();
                        $next.focus();
                    }
                    return;
                }
            }

            // textareaはEnterのときのみ無視
            if ('textarea' === tagName && keyCode == 13) {
                return;
            }

            if (isButton) {
                if (keyCode === 13 && !shiftKey) {   // エンター時
                    return;
                }
            }

            /** Enter時ボタン以外はなにもしない **/
            if (keyCode === 13 && isNoEnterFocus) {
                return;
            }
            /** Enter時ボタン以外はなにもしない **/
            if (options.convtab && window.event) {
                if (shift > 0 || keyCode === ENTER || keyCode === TAB) {
                    window.event.keyCode = 9;
                    return;
                }
            }

            var target;
            if (options._cache) {
                target = findNextElementCache($el, shift, options);
            } else {
                target = findNextElement($el, shift, options);
            }
            // 遷移先 target 補正
            // モーダル表示時、遷移先がモーダル外かどうか判定
            if(target && clutil.view.modal){
                var modal = clutil.view.modal.getModalView();
                if(modal != null){
                    // モーダルあり：target がモーダル内部要素かどうか判定
                    var isModalOuter = modal.$el.has(target).length === 0;
                    if(isModalOuter){
                        target = modal.getPreferredFocus();
                    }
                }
            }
            if (target) {
                ev.preventDefault();
                ev.stopImmediatePropagation();
                $(target).focus();
            }
        };

        return function(el, options) {
            if (!options)
                options = el;
            options = options || {};
            _.defaults(options, {filter: function() {return true}});
            options.convtab = options.convtab && $.browser.msie;

            var cb = _.bind(callback, null, options);
            if (options.convtab) {
                $(document).off('.clEnterFocusMode')
                .on('keydown.clEnterFocusMode', cb);
                clutil._enterFocusMode = 'convertTab';
            } else if (options.view) {
                $(options.view).off('.clEnterFocusMode')
                .on('keydown.clEnterFocusMode', cb);
            } else if (options.cache) {
                options._cache = buildCache();
                $(document).off('.clEnterFocusMode')
                .on('keydown.clEnterFocusMode', cb);
            } else {
                $(document).off('.clEnterFocusMode')
                .on('keydown.clEnterFocusMode', cb);
                clutil._enterFocusMode = 'emulateTab';
            }
            return el;
        };
    }());

    var leaveEnterFocusMode = function() {
        $(document).off('.clEnterFocusMode');
    };

    _.extend(clutil, {
        focus: focus,
        enterFocusMode: enterFocusMode,
        leaveEnterFocusMode: leaveEnterFocusMode
    });
}());


//form serializer, deserializer
(function() {
    _.extend(clutil, (function(Syphon) {
        var inputReaders = new Syphon.InputReaderSet(),
        inputWriters = new Syphon.InputWriterSet(),
        keyExtractors = new Syphon.KeyExtractorSet(),
        elementExtractor = function($view) {
            return $view.find('input,textarea,select,span[data-name]');
        },
        defaultOptions = {
            inputReaders: inputReaders,
            inputWriters: inputWriters,
            keyExtractors: keyExtractors,
            elementExtractor: elementExtractor
        },
        unmaskValue = function($el, value) {
            if ($el.hasClass('cl_date')) {
                value = clutil.dateFormat(value, 'yyyymmdd');
            } else if ($el.hasClass('cl_month')) {
                value = clutil.monthFormat(value, 'yyyymm');
            } else if ($el.hasClass('cl_time')) {
                value = clutil.timeFormat(value, 'hhmm');
            } else {
                value = $.inputlimiter.unmask(value, {
                    limit: $el.attr('data-limit'),
                    filter: $el.attr('data-filter')
                });
            }
            return clutil.cStr(value);
        },
        maskValue = function($el, value) {
            if ($el.hasClass('cl_date')) {
                value = clutil.dateFormat(value, 'yyyy/mm/dd');
            } else if ($el.hasClass('cl_month')) {
                value = clutil.monthFormat(value, 'yyyy/mm');
            } else if ($el.hasClass('cl_time')) {
                value = clutil.timeFormat(value, 'hh:mm');
            } else {
                value = $.inputlimiter.mask(value, {
                    limit: $el.attr('data-limit'),
                    filter: $el.attr('data-filter')
                });
            }
            return clutil.cStr(value);
        },

        buildOptions = function() {

            // inputReaders
            inputReaders.registerDefault(function($el) {
                return $el.val();
            });
            inputReaders.register('checkbox', function($el) {
                return $el.prop('checked') ? 1 : 0;
            });
            inputReaders.register('text', function($el) {
                var val = $el.val();
                return unmaskValue($el, val);
            });
            inputReaders.register('span', function($el) {
                var val = $el.text();
                return unmaskValue($el, val);
            });

            // inputWriters
            inputWriters.registerDefault(function($el, value) {
                $el.val(clutil.cStr(value));
            });
            inputWriters.register('text', function($el, value) {
                value = maskValue($el, value);
                clutil.inputlimiter($el, 'set', value);
            });
            inputWriters.register('checkbox', function($el, value) {
                $el.prop('checked', value);
            });
            inputWriters.register('radio', function($el, value) {
                $el.prop('checked', $el.val() === value);
            });
            inputWriters.register('span', function($el, value) {
                value = maskValue($el, value);
                $el.text(value);
            });

            // KeyExtractor
            keyExtractors.registerDefault(function($el) {
                return $el.prop('name');
            });
            keyExtractors.register('span', function($el) {
                return $el.attr('data-name');
            });
        };

        buildOptions();

        var deserialize = function($view, resultdata, options) {
            options = _.extend({}, defaultOptions, options);
            return Syphon.deserialize($view, resultdata, options);
        };

        var serialize = function($view, options, resultdata) {
            resultdata = resultdata || {};
            options = _.extend({}, defaultOptions, options);
            var serialized = Syphon.serialize($view, options);
            return _.extend(resultdata, serialized);
        };

        return {
            /**
             * データをビューに反映する
             * @param {jQuery or Backbone.View} $view 表示エリアのjQueryオブジェクト (例：$('#viewarea'))
             * @param {Object or Array} resultdata
             * @param {Object} options
             */
            deserialize: deserialize,

            /**
             * ビューからデータオブジェクトを作成する
             * @param {jQuery or Backbone.View} $view jQueryオブジェクト (例：$('#viewarea'))
             * @param {Object} resultdata 作成するデータオブジェクトを上書きする場合はオブジェクトを指定
             *							なにも指定されていない場合は新しいオブジェクトを作成する
             * @return {Object}
             */
            serialize: serialize
        };
    }(Backbone.Syphon)));

}());

(function() {
    /** キー用イベント作成関数
     * @param {jQuery Object} $el 監視ターゲットエレメント
     * @returns {Event Object} Backbone.Eventをmixinしたオブジェクト
     */
    var makeKeyvent = (function() {

        var keyMap = {
            //function keys
            "112": ["f1"],
            "113": ["f2"],
            "114": ["f3"],
            "115": ["f4"],
            "116": ["f5"],
            "117": ["f6"],
            "118": ["f7"],
            "119": ["f8"],
            "120": ["f9"],
            "121": ["f10"],
            "122": ["f11"],
            "123": ["f12"]
        };

        //a-z and A-Z
        for (var aI = 65; aI <= 90; aI += 1) {
            keyMap[aI] = String.fromCharCode(aI + 32);
        }

        return function($el) {
            var registeredKeys = {},

            vent = _.extend({}, Backbone.Events),

            toKeyStr = function(ev, key) {
                var code = ev.keyCode,// || e.which;
                codes = [];
                if (ev.ctrlKey)
                    codes.push('C');
                if (ev.shiftKey)
                    codes.push('S');
                if (ev.altKey)
                    codes.push('M');
                codes.sort();
                codes.push(key);
                return codes.join('-');
            },

            normalizeKey = function(key) {
                var codes = key.split('-'),
                k = codes.pop();
                codes.sort();
                codes.push(k);
                return codes.join('-');
            },

            keydownCallback = function(ev) {
                var code = ev.keyCode,// || e.which;
                keys = keyMap[code] || [];

                if (_.any(keys, function(key) {return registeredKeys[toKeyStr(ev, key)]})) {
                    ev.preventDefault();
                }
            },

            keyupCallback = function(ev) {
                var code = ev.keyCode,// || e.which;
                keys = keyMap[code];

                _.each(keys, function(key) {
                    key = toKeyStr(ev, key);
                    vent.trigger(key, ev, key);
                });
            };

            vent.on = function(name, callback, context) {
                if (typeof name !== 'object') {
                    name = normalizeKey(name);
                    registeredKeys[name] = true;
                }
                Backbone.Events.off.call(this, name);
                Backbone.Events.on.call(this, name, callback, context);
            };

            vent.off = function(name, callback, context) {
                if (typeof name !== 'object') {
                    name = normalizeKey(name);
                    delete registeredKeys[name];
                }
                Backbone.Events.off.call(this, name);
            };

            vent.stop = function() {
                $el.off('keydown', keydownCallback);
                $el.off('keyup', keyupCallback);
            };

            vent.start = function() {
                vent.stop();
                $el.on('keydown', keydownCallback);
                $el.on('keyup', keyupCallback);
            };

            vent.start();

            return vent;
        };
    }());


    // グローバルなキーイベント
    var globalKeyvent;

    $(function() {
        globalKeyvent = makeKeyvent($('body'));
    });

    ////////////////////////////////////////////////////////////////
    // public
    _.extend(clutil, {
        /**
         * body にキーイベント処理を割りあてる。
         *
         * 注意: ブラウザに割りあてられたキーが上書きされる。
         * @param {String} key キー
         * @param {Function} callback 処理関数
         * @param {Object} context コンテキスト
         * @example
         * <pre>
         * // f1 キーでアラート
         * clutil.globalSetKey("f1", function() {alert('f1 pressed')});
         * // Ctrlとfキー同時押しでアラート
         * clutil.globalSetKey("C-f", function() {alert('C-f pressed')});
         * // Ctrl, Shift, Alt と g 同時押しでアラート
         * clutil.globalSetKey("C-M-S-g", function() {alert('S-M-S-g pressed')});
         * </pre>
         */
        globalSetKey: function(key, callback, context) {
            globalKeyvent.on(key, callback, context);
        },

        /**
         * body に割り当てたキーイベント処理を解除する。
         * @param {String} key キー
         * @param {Function} callback 処理関数
         * @param {Object} context コンテキスト
         * @example
         * <pre>
         * clutil.globalUnsetKey("f1");
         * clutil.globalUnsetKey("C-f");
         * </pre>
         */
        globalUnsetKey: function(key, callback, context) {
            globalKeyvent.off(key, callback, context);
        }
    });
}());

//↓不要？
$(function() {
    $('.cl_check').click(function(e){
        var $chkbox = $(this).find('input:checkbox');
        if ($(e.target).get(0).type == 'checkbox') {
            return;
        }
        if ($chkbox.is(":checked")){
            $chkbox.attr('checked', false);
        } else {
            $chkbox.attr('checked', true);
        }
    });
    $('.cl_radio').click(function(e){
        var $chkbox = $(this).find('input:radio');
        if ($(e.target).get(0).type == 'radio') {
            return;
        }
        if (!$chkbox.is(":checked")){
            $chkbox.attr('checked', true);
        }
    });
});
/**
 * clutil 拡張
 */
var clutil = _.extend({}, clutil);
$(function(){

    // Namespace: clutil.text -- テキスト処理に関するユーティリティ
    if(!clutil.text){
        clutil.text = {};
    }
    _.extend(clutil.text, {
        /**
         * Properties {id,code,name} ラベルテキストを作る
         * @param fmt: フォーマット, ['name','code','code:name'] のどれか。
         * @param item: ラベル生成元のプロパティオブジェクト
         * @param altNameDef: プロパティ {id,code,name} の別プロパティ名。(省略可)
         */
        labelTextBuilder: function(fmt, item, altNameDef){
            var alt = _.defaults(altNameDef||{}, { id: 'id', code: 'code', name: 'name' });
            switch(fmt) {
            default:
            case 'name':
                return clutil.getObjProperty(item,alt.name) || "";
            case 'code':
                return clutil.getObjProperty(item,alt.code) || "";
            case 'code:name':
            case 'code：name':
                var ss = [];
                var v = clutil.getObjProperty(item,alt.code);
                if (!_.isEmpty(v)) {
                    ss.push(v);
                }
                v = clutil.getObjProperty(item,alt.name);
                if (!_.isEmpty(v)) {
                    ss.push(v);
                }
                var delim = (fmt == 'code:name') ? ': ' : '：';
                return ss.join(delim);
            }
        },

        /**
         * JSオブジェクトのプロパティを key1="value1" key2="value2" ... 列挙文字列変換する。
         * 入力プロパティの値型に関して、オブジェクト型は toString() を値とする。
         * 配列型の場合は join(" ") 連結する(keyX="value[0] value[1] ...")。
         * ツリー構造のような入れ子展開は考慮しない。
         *
         * ＜用途＞
         * DOM要素へ属性を展開するために用いる。
         * { k1: "val1", k2: "val2", ...} → <p k1="val1" k2="val2" ...></p>
         *                                      ^^^^^^^^^^^^^^^^^^^^^^^
         *
         * @param props プロパティ
         * @returns プロパティを key=val "key1=val1 key2=val2 ..."
         */
        toElemAttrs: function(props){
            if(!_.isObject(props) || _.isEmpty(props)){
                return '';
            }
            var kvArray = _.reduce(props, function(ss, v, k){
                var fixVal = null;
                if(v == null){
                    ;
                }else if(_.isArray(v)){
                    fixVal = v.join(' ');
                }else{
                    fixVal = v.toString();
                }
                if(!_.isEmpty(fixVal)){
                    ss.push(k + '="' + (_.isArray(fixVal) ? fixVal.join(' ') : fixVal) + '"');
                }
                return ss;
            },[]);
            return kvArray.join(' ');
        },

        /**
         * ゼロ埋め（64桁まで対応）
         * @param str		ゼロ埋め文字列または数値
         * @param n		桁数
         */
        zeroPadding: function(str, n){
            var s = _.isString(str) ? str.replace(/^0*/, '') : (str||'').toString();
            if(s.length > n){
                return str;
            }
            return ('0000000000000000000000000000000000000000000000000000000000000000' + s).slice(-n);
        },

        /**
         * 数値文字列をカンマ区切りにする。数値文字として解釈できない場合はそのまま返す。
         * @param value 文字列
         * @return カンマ区切り形式
         */
        comma: function(value){
            if(_.isFinite(value)){
                value = Number(value).toString();
                var dotIdx = value.indexOf('.'), ss =  [];
                if(dotIdx >= 0){
                    ss.push(value.slice(0, dotIdx));
                    ss.push(value.slice(dotIdx+1));
                }else{
                    ss.push(value);
                }
                ss[0] = ss[0].replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){
                    return t + ',';
                });
                value = ss.join('.');
            }
            return value;
        },

        /**
         * 正規表現集: テキスト入力形式チェック用
         * 全角英数記号 → [、！＂＃＄％＆＇（）＊＋，－．／０-９：；＜＝＞？＠Ａ-Ｚ［＼］＾＿｀ａ-ｚ｛｜｝～] -- キーボードから打てるもの適当に選出
         */
        CommonRegEx: {
            notempty: /.{1}/,                   // 空文字以外全部マッチする

            num: /^\d{1,}$/,                    // 数値文字のみチェック
            decimal: /^[+-]{0,1}\d+(\.\d+)?$/,  // 整数チェック ±0123456789.0123456789
            hhmm: /^\d{1,2}:\d{1,2}$/,          // <数値>:<数値>

            zen: /^[^ -~｡-ﾟ]*$/,                // 全角
            han: /^[ -~｡-ﾟ]*$/,                 // 半角 - 半角ｶﾀｶﾅも含む
            kana_k: /^[ァ-ヴー、・！＂＃＄％＆＇（）＊＋，－．／０-９：；＜＝＞？＠Ａ-Ｚ［＼］＾＿｀ａ-ｚ｛｜｝～]*$/,     // 全角カナ - 全角英数記号も含める
            kana_hk: /^[ｦ-ﾟ!-~]*$/,             // 半角ｶﾅ   - ASCII 記号系も含める
            kana_h: /^[ぁ-んー、・！＂＃＄％＆＇（）＊＋，－．／０-９：；＜＝＞？＠Ａ-Ｚ［＼］＾＿｀ａ-ｚ｛｜｝～]*$/,     // ひらがな - 全角英数記号も含める
            alpha: /^[a-zA-Z]*$/,               // 英字
            alnum: /^[a-zA-Z\d]*$/,             // 英数
            ascii: /^[\x20-\x7E]*$/,            // ascii
            passwd: /^^(?=.*[0-9])(?=.*[A-Za-z])(?=.*[!\x22\#$%&@'()*+,-./_])[\w!\x22\#$%&@'()*+,-./]*$/,
                // パスワード: 英数記号あり（大文字小文字の区別なし）
                // 文字数制限は別途、rule: { min:8, max:24 } で指定する。
            url: /^https?(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/,
                // URL形式チェック
            mail: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/,
                // メールアドレス形式チェック
            filename_invalchars: /^.*[\\\\|/|:|\\*|?|\”|<|>|\\|].*$/,
                // ファイル名: ウィンドウズで使えない記号 [<>:*?"/\|]
            postal: /^\d{3,3}-?\d{4,4}$/,               // 郵便番号
            postal_hyphen: /^\d{3,3}-\d{4,4}$/,         // 郵便番号（ハイフン区切り必須）
            postal_nohyphen: /^\d{3,3}\d{4,4}$/,        // 郵便番号（ハイフン区切り無し）
            tel: /^0\d{1,4}-?\d{1,4}-?\d{3,4}$/,        // 電話番号
            tel_hyphen: /^0\d{1,4}-\d{1,4}-\d{3,4}$/,   // 電話番号（ハイフン区切り必須）
            tel_nohyphen: /^0\d{1,4}\d{1,4}\d{3,4}$/    // 電話番号（ハイフン区切り無し）
        },

        /**
         * 文字種別セット
         */
        Charactors: {
            kanah_txt: "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもや　ゆ　よらりるれろわゐ　ゑをんがぎぐげござじずぜぞだぢづでどばびぶべぼぱぴぷぺぽぁぃぅぇぉっゃゅょゎ",
            kanak_txt: "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤ　ユ　ヨラリルレロワヰ　ヱヲンガギクゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォッャュョヮ",
            asciiHan_txt: " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
            asciiZen_txt: "　！”＃＄％＆’（）＊＋，－．／０１２３４５６７８９：；＜＝＞？＠" +
            "ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ［＼］＾＿‘" +
            "ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ｛｜｝～”",

            han_txt: "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｧｨｩｪｫｬｭｮｯ､｡ｰ｢｣0123456789ﾞﾟ ",
            zen_txt: "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲンァィゥェォャュョッ、。ー「」０１２３４５６７８９" +
            "　　　　　ガギグゲゴザジズゼゾダヂヅデド　　　　　バビブベボ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　" +
            "　　　　　　　　　　　　　　　　　　　　　　　　　パピプペポ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　"
        },

        /**
         * ひらがな→カタカナ変換（全角のみ）
         * @param txt 入力文字列（かな）
         * @return 出力文字列（カナ）
         */
        kanah2k: function(txt) {
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = this.Charactors.kanah_txt.indexOf(c);
                retStr += (n >= 0) ? this.Charactors.kanak_txt.charAt(n) : c;
            }
            return retStr;
        },
        /**
         * カタカナ→ひらがな変換（全角のみ）
         * @param txt 入力文字列（カナ）
         * @return 出力文字列（かな）
         */
        kanak2h: function(txt) {
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = this.Charactors.kanak_txt.indexOf(c);
                retStr += (n >= 0) ? this.Charactors.kanah_txt.charAt(n) : c;
            }
            return retStr;
        },
        /**
         * 全角ASCiiを半角へ変換する
         * @param txt 入力文字列（ＡＳＣＩＩ）
         * @return 出力文字列（ASCII）
         */
        asciiZen2Han: function(txt) {
            retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = this.Charactors.asciiZen_txt.indexOf(c, 0);
                retStr += (n >= 0) ? this.Charactors.asciiHan_txt.charAt(n) : c;
            }
            return retStr;
        },
        /**
         * 半角ASCIIを全角へ変換する
         * @param txt 入力文字列（ASCII）
         * @param 出力文字列（ＡＳＣＩＩ）
         */
        asciiHan2Zen: function(txt) {
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = this.Charactors.asciiHan_txt.indexOf(c, 0);
                retStr += (n >= 0) ? this.Charactors.asciiZen_txt.charAt(n) : c;
            }
            return retStr;
        },
        /**
         * 全角カタカナを半角ｶﾀﾅｶへ変換する
         * @param txt 入力文字列（カナ）
         * @return 出力文字列（ｶﾅ）
         */
        zen2han: function(txt) {
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = this.Charactors.zen_txt.indexOf(c, 0);

                if (n == 70) {
                    // 空白対応
                    c = this.Charactors.han_txt.charAt(72);
                } else if (n >= 130) {
                    c = this.Charactors.han_txt.charAt(n-130);
                    c += this.Charactors.han_txt.charAt(71);
                } else if (n >= 70) {
                    c = this.Charactors.han_txt.charAt(n-70);
                    c += this.Charactors.han_txt.charAt(70);
                } else if (n >= 0) {
                    c = this.Charactors.han_txt.charAt(n);
                }

                retStr += c;
            }
            return retStr;
        },
        /**
         * 拡張: zen2han と asciiZen2Han を施す
         */
        zen2hanEx: function(txt) {
            var retStr = this.asciiZen2Han(txt);
            return this.zen2han(retStr);
        },
        /**
         * 半角ｶﾀﾅｶを全角カタカナへ変換する
         */
        han2zen: function(txt) {
            var retStr = "";
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var cnext = txt.charAt(i+1);
                var n = this.Charactors.han_txt.indexOf(c, 0);
                var nnext = this.Charactors.han_txt.indexOf(cnext,0);
                if (n >= 0) {
                    if (nnext == 70) {
                        c = this.Charactors.zen_txt.charAt(n+70);
                        i++;
                    } else if (nnext == 71) {
                        c = this.Charactors.zen_txt.charAt(n+130);
                        i++;
                    } else {
                        c = this.Charactors.zen_txt.charAt(n);
                    }
                }
                if ((n != 70) && (n != 71)) {
                    retStr += c;
                }
            }
            return retStr;
        },
        /**
         * 拡張: han2zen と asciiHan2Zen を施す
         */
        han2zenEx: function(txt) {
            var retStr = this.han2zen(txt);
            return this.asciiHan2Zen(retStr);
        },
        /**
         * 全角が入力されているかチェック
         * 入力されていた場合retStatにfalseを返す
         */
        zenContains: function(txt) {
            var retStat = 1;
            for (var i = 0; i < txt.length; i++) {
                var c = txt.charAt(i);
                var n = this.Charactors.han_txt.indexOf(c, 0);
                if (c == ',' || c == '"') {
                    retStat = -1;
                    break;
                }
                if (n < 0) {
                    if (!c.match(/[^A-Za-z\s.-]+/)){

                    } else if (!c.match( /[^0-9]+/)){

                    } else {
                        retStat = 0;
                    }
                }
            }
            return retStat === 0;
        },
        /**
         * 半角文字チェック
         * @param c １文字
         * @return 判定結果 - true:半角文字、false:半角じゃない文字
         */
        isHalf: function (c) {
            c = c.charCodeAt(0);
            return (c >= 0x0 && c < 0x81) || (c == 0xf8f0) ||
            (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4);
        },

        /**
         * 前方一致マッチャーを作成する
         * @param prefix 前方一致文字列（素の文字列、正規表現エスケープは考慮せず）
         * @param defaultRegExp prefix 指定無しの場合に適用する正規表現
         * @return 前方一致正規表現を返す。null を返した場合はマッチャー無しに相当。
         */
        buildPrefixMatcher: function(prefix, defaultRegExp){
            if(_.isRegExp(prefix)){
                return prefix;
            }
            if(defaultRegExp == null){
                return _.isEmpty(prefix) ? null : new RegExp('^' + prefix);
            }
            if(prefix == null){
                return defaultRegExp;
            }
            if(prefix.length === 0){
                // 空文字指定 ⇒ マッチャー無しの明示指定に相当
                return null;
            }
            prefix = '^' + prefix;
            if(prefix == defaultRegExp.source){
                return defaultRegExp;		// マッチングパターンが同等内容
            }
            return new RegExp(prefix);
        },
        /**
         * 指定テキストがマッチャーに合致する場合、合致部を取り除いた文字列を返す。合致しない場合は null を返す。
         * @param macher 正規表現、未指定の場合はマッチしたものと見做し、入力文字列をそのまま返す。
         * @param text テスト文字列
         * @return 合致部を取り除いた文字列。マッチャーに合致しない場合 null を返す。
         */
        isMatchThenTrim: function(matcher, text){
            if(matcher == null || text == null){
                return text;
            }
            if(!matcher.test(text)){
                return null;
            }
            text = text.replace(matcher, '');
            return _.isEmpty(text) ? null : text;
        },
        /**
         * 文字列正規化: テキストソートを想定した正規化文字列を生成する。
         * @param text 変換対象文字列、可変引数対応。
         * @return 文字列マッチングに最適化した正規化文字列を返す。
         */
        normalize: function(text){
            text = _.chain(arguments).toArray().flatten().compact().value().join('').normalize('NFKC');
            return $.trim(text);
        },
        /**
         * 文字列正規化: テキストマッチングを想定した正規化文字列を生成する。
         * @param text 変換対象文字列、可変引数対応。
         * @return 文字列マッチングに最適化した正規化文字列を返す。
         */
        normalizeForSearch: function(text){
            text = _.chain(arguments).toArray().flatten().compact().value().join('').normalize('NFKC').toLowerCase();
            // さらに、かな → カナ変換
            text = this.kanah2k(text);
            return $.trim(text);
        },
        /**
         * URI文字列正規化: テキストマッチングを想定した URI における正規化文字列を生成する。
         * @param uri URI文字列、またはパス文字列
         * @return 文字列マッチングに最適化した正規化文字列を返す。
         */
        normalizeUri: function(uri){
            uri = $.trim(uri);
            // 「プロトコル部」と「ロケーション＆パス部」に分離
            var protoDelim = '://', delimIdx = uri.indexOf(protoDelim), protoWithDelim = '', locationPath = uri;
            if(delimIdx >= 0){
                delimIdx += protoDelim.length;
                protoWithDelim = uri.substring(0,delimIdx);
                locationPath = uri.substring(delimIdx);
            }
            // 「ロケーション＆パス部」から、パラメタ部を分離
            var paramDelim = '?', urlParams = '';
            delimIdx = locationPath.indexOf(paramDelim);
            if(delimIdx >= 0){
                urlParams = locationPath.substring(delimIdx);
                locationPath = locationPath.substring(0, delimIdx);
            }

            // lowercace で正規化とする。パラメタ部はそっとしておく。
            protoWithDelim = protoWithDelim.toLowerCase();
            locationPath = locationPath.toLowerCase();

            // '/' スラッシュの重複記述をひとつにする
            locationPath = locationPath.replace(/[\/]{1,}/g, '/');

            // URI 正規化結果を返す。
            return protoWithDelim + locationPath + urlParams;
        },
        /**
         * スネークケース変換
         */
        snake_case: function(str){
            if(arguments.length > 1){
                var wkArray = [];
                for(var i=0; i<arguments.length; i++){
                    var str = arguments[i];
                    var snake = this.snake_case(str);
                    wkArray.push(snake);
                }
                return _.compact(wkArray).join('.');
            }
            var camel = this.camelCase(str);
            return camel.replace(/[A-Z]/g, function(s){
              return "_" + s.charAt(0).toLowerCase();
            });
        },
        /**
         * キャメルケース変換
         */
        camelCase: function(str){
            if(arguments.length > 1){
                var wkArray = [];
                for(var i=0; i<arguments.length; i++){
                    var str = arguments[i];
                    var camel = this.camelCase(str);
                    wkArray.push(camel);
                }
                return _.compact(wkArray).join('.');
            }
            str = str.charAt(0).toLowerCase() + str.slice(1);
            return str.replace(/[-_](.)/g, function(match, group1) {
                return group1.toUpperCase();
            });
        },
        /**
         * パスカルケース変換
         */
        PascalCase: function(str){
            if(arguments.length > 1){
                var wkArray = [];
                for(var i=0; i<arguments.length; i++){
                    var str = arguments[i];
                    var pascal = this.PascalCase(str);
                    wkArray.push(pascal);
                }
                return _.compact(wkArray).join('.');
            }
            var camel = this.camelCase(str);
            return camel.charAt(0).toUpperCase() + camel.slice(1);
        }
    });

    // Namespace: clutil.date -- 日付処理に関するユーティリティ
    if(!clutil.date){
        clutil.date = {};
    }
    _.extend(clutil.date, {
        /** よく利用するフォーマット集 */
        CommonDateFormat: function(){
            var dfMap = {
                form01: new DateFormat("yyyy/MM/dd HH:mm:ss SSS"),
                DateTimeSec: new DateFormat("yyyy/MM/dd HH:mm:ss"),
                DateTime: new DateFormat("yyyy/MM/dd HH:mm"),
                'yyyy/MM/dd HH:mm:ss SSS': new DateFormat("yyyy/MM/dd HH:mm:ss SSS"),
                'yyyy/MM/dd HH:mm:ss': new DateFormat("yyyy/MM/dd HH:mm:ss"),
                'yyyy/MM/dd HH:mm': new DateFormat("yyyy/MM/dd HH:mm"),
                'yyyy年MM月dd日 HH時mm分': new DateFormat('yyyy年MM月dd日 HH時mm分'),
                'yyyy/MM/dd': new DateFormat("yyyy/MM/dd"),
                'yyyy年MM月dd日': new DateFormat('yyyy年MM月dd日'),
                'yyyyMMdd': new DateFormat("yyyyMMdd"),
                'yyyy/MM': new DateFormat("yyyy/MM"),
                'yyyyMM': new DateFormat("yyyyMM"),
                'HH:mm': new DateFormat("HH:mm"),
                'HHmm': new DateFormat('HHmm'),
                rfc3339: new DateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            };
            return _.extend(dfMap, {
                'yyyy/mm/dd hh:mm:ss sss': dfMap['yyyy/MM/dd HH:mm:ss SSS'],
                'yyyy/mm/dd hh:mm:ss': dfMap['yyyy/MM/dd HH:mm:ss'],
                'yyyy/mm/dd hh:mm': dfMap['yyyy/MM/dd HH:mm'],
                'yyyy年mm月dd日 HH時mm分': dfMap['yyyy年MM月dd日 HH時mm分'],
                'yyyy/mm/dd': dfMap['yyyy/MM/dd'],
                'yyyy年mm月dd日': dfMap['yyyy年MM月dd日'],
                'yyyymmdd': dfMap['yyyyMMdd'],
                'yyyy/mm': dfMap['yyyy/MM'],
                'yyyymm': dfMap['yyyyMM'],
                'hh:mm': dfMap['HH:mm'],
                'hhmm': dfMap['HHmm']
            });
        }(),
        /**
         * 8桁数値-年月日(4桁数値-時分) → Date オブジェクト変換
         * @param ymd	年月日: Dateオブジェクト、Number(8桁:yyyyMMdd)、文字列(new Date(文字列)) に対応。
         * @param hhmm 時分: Number に対応。省略可。
         * @param side 時分秒以下の桁の寄せポリシー。
         * 					'from':0時0分0秒000 に寄せる。
         * 					'to':23時59分59秒999に寄せる。
         * 				hhmm 引数指定時は秒以下に適用。省略可(Defaultは'from')。
         */
        toDate: function(ymd, hhmm, side) {
            var ymdDate;

            function num8ToDate(dt) {
                var fixYmd = _.isNumber(dt) ? dt : parseInt(dt,10);
                var y = Math.floor(fixYmd / 10000);
                var m = (Math.floor(fixYmd / 100) % 100) -1;
                var d = fixYmd % 100;
                return new Date(y,m,d);
            }

            // 第１引数評価: 年月日 -- Date, String, Number に対応
            if(_.isDate(ymd)){
                ymdDate = ymd;
            }else if(_.isNumber(ymd)){
                ymdDate = num8ToDate(ymd);
            }else if(_.isString(ymd)){
                var fixStr = $.trim(ymd);
                // Datepicker表示フォーマット対策: 「～ (<曜日>)」曜日表示があれば取り除く
                fixStr = fixStr.replace(/\s{0,}[(（].*[）)]\s{0,}$/, '');
                if(/\d{8}/.test(fixStr)){
                    // 8桁数文字は yyyyMMdd フォーマットと見做す。
                    ymdDate = num8ToDate(fixStr);
                }else{
                    ymdDate = new Date(fixStr);
                }
            }

            // 第２引数評価: 時分
            var hour = 0;
            var min = 0;
            var sec = 0;
            var msec = 0;
            if(_.isDate(ymd)){
                hour = ymd.getHours();
                min = ymd.getMinutes();
                sec = ymd.getSeconds();
                msec = ymd.getMilliseconds();
            }
            if(hhmm != null){
                var fixHHmm = parseInt(hhmm, 10);
                hour = Math.floor(fixHHmm / 100);
                min  = fixHHmm % 100;
            }

            // 第３引数評価: From 寄せ、To 寄せ
            var fixSide = (side || '').toLowerCase();
            switch(side){
            case 'from':
                if(hhmm == null){
                    hour = 0;
                    min = 0;
                    sec = 0;
                    msec = 0;
                }
                break;
            case 'to':
                if(hhmm == null){
                    hour = 23;
                    min = 59;
                }
                sec = 59;
                msec = 999;
                break;
            }

            // 時分以下補正
            ymdDate.setHours(hour);
            ymdDate.setMinutes(min);
            ymdDate.setSeconds(sec);
            ymdDate.setMilliseconds(msec);

            return ymdDate;
        },
        /**
         * 指定年月に対する Date オブジェクトを返す。
         * @param ym 数値型の場合 yyyyMM とみなす。6桁数文字列指定の場合 "yyyyMM" とみなす。
         * @param side	'from': 1日に設定（デフォルト）、'to': 指定月の最終日に設定
         * @return Date オブジェクト
         */
        toDateByYM: function(ym, side){
            var fixDate = ym;
            if(_.isNumber(ym)){
                fixDate = clutil.date.toDate(ym * 100 + 1);
            }else if(!_.isDate(ym)){
                if(_.isFinite(ym)){
                    if(_.isString(ym)){
                        if(ym.length >= 8/* yyyyMMdd 想定 */){
                            fixDate = clutil.date.toDate(ym);
                        }else{
                            /* yyyyMM 想定 */
                            fixDate = clutil.date.toDate(parseInt(ym, 10) * 100 + 1);
                        }
                    }else{
                        fixDate = clutil.date.toDate(parseInt(ym, 10) * 100 + 1);
                    }
                }else{
                    fixDate = new Date(ym);
                }
            }
            return (side == 'to') ? clutil.date.getLastDateOfMonth(fixDate) : fixDate;
        },
        /**
         * 指定書式 pattern の日付形式を返す。
         * @param pattern 日付フォーマット
         * @param ymd 8桁数値表現による年月日、または Date オブジェクト
         * @param hhmm 4桁数値表現による時分（省略可）
         */
        dateFormat: function(pattern, ymd, hhmm){
            var date = clutil.date.toDate(ymd, hhmm);
            var fmt = clutil.date.CommonDateFormat[pattern];
            if(fmt == null){
                fmt = new DateFormat(pattern);
                clutil.date.CommonDateFormat[pattern] = fmt;
            }
            var fmtStr = fmt.format(date);
            if(/^\d{1,}$/g.test(fmtStr)){
                // 数文字のみで成り立つ場合は数値型に変換して返す
                return Number(fmtStr);
            } else {
                return fmtStr;
            }
        },
        /**
         * 年月日を８桁数値に変換する。変換できない場合は undefined を返す。
         */
        toIymd: function(anyYmd){
            // 引数評価ショートカット
            // 空白
            if(anyYmd == null){
                return;	// undefined.
            }
            // 数値
            if(_.isNumber(anyYmd)){
                return anyYmd;
            }
            // 数字列のみ（前後空白を含めてもいい）
            if(_.isString(anyYmd)){
                if(/^\s{0,}\d{1,}\s{0,}$/.test(anyYmd)){
                    return parseInt(anyYmd,10);
                }
                if(anyYmd.length === 0){
                    return;	// undefined
                }
            }

            var dt = clutil.date.toDate(anyYmd);
            if(_.isNaN(dt.getTime())){
                return;		// Invalid Date
            }
            var iYmdStr = clutil.date.dateFormat('yyyyMMdd', dt);
            return parseInt(iYmdStr,10);
        },
        /**
         * 時刻（時分）を４桁数値に変換する。変換できない場合は undefined を返す。
         * @param anyTime 文字列、数値、Date オブジェクトなど
         * @param loose true:緩い、false:厳格チェック 、default: false
         * @return HHmm ４桁数値 or undefined
         */
        toHHmm: function(anyTime, loose){
            // 空白
            if(anyTime == null){
                return;
            }

            function toHHmmLoose(t){
                // 数値
                if(_.isNumber(t)){
                    return t;
                }
                // Date
                if(_.isDate(t)){
                    if(_.isNaN(t.getTime())){
                        return; // Invalid date
                    }
                    var h = t.getHours(), m = t.getMinutes();
                    return h * 100 + m;
                }
                if(_.isString(t)){
                    var fixStr = $.trim(t);
                    if(fixStr.length === 0){
                        return; // undefined
                    }
                    // 数字列のみ
                    if(/^\s{0,}\d{1,}\s{0,}$/.test(fixStr)){
                        return parseInt(fixStr,10);
                    }
                    // HH:mm 形式な文字列
                    var ss = fixStr.split(':');
                    if(ss.length != 2){
                        return; // undefined, invalid format.
                    }
                    if(_.isFinite(ss.join(''))){
                        var h = parseInt(ss[0],10), m = parseInt(ss[1],10);
                        return h * 100 + m;
                    }
                }
            }

            var hhmm = toHHmmLoose(anyTime);
            if(loose){
                // 緩いチェック
                return hhmm;
            }
            if(hhmm != null){
                var h = Math.floor(hhmm / 100), m = hhmm % 100;
                if(0 <= h && h < 24 && 0 <= m && m < 60){
                    return hhmm;
                }
                console.warn('Invalid range hours[' + h + '] minutes[' + m + ']');
            }
        },
        /**
         * Date オブジェクトベースで日付算出する
         * @param date yyyyMMdd(文字列 or 数値) or 日付になる文字列 or Date オブジェクト
         * @param dYear 年差分、null は 0 として扱う。
         * @param dMonth 月差分、null は 0 として扱う。
         * @param dDay 日差分、null は 0 として扱う。
         * @param dHour 時間差分、null は 0 として扱う。
         * @param dMin 分差分、null は 0 として扱う。
         * @praram dSec 秒差分、null は 0 として扱う。
         * @param dMsec ミリ秒差分、null は 0 として扱う。
         * @return Date オブジェクト、null は 0 として扱う。
         */
        computeDate: function(date, dYear, dMonth, dDay, dHour, dMin, dSec, dMsec){
            var fixDate = date;
            if(_.isNumber(date)){
                // yyyymmdd 8桁数値と見做す
                fixDate = clutil.date.toDate(date);
            }else if(!_.isDate(date)){
                // 任意日付表現テキストと見做す
                fixDate = new Date(date);
            }
            if(_.isNaN(fixDate.getTime())){
                return;		// Invalid date
            }

            var y = fixDate.getFullYear() + (dYear || 0);
            var m = fixDate.getMonth() + (dMonth || 0);
            var d = fixDate.getDate() + (dDay || 0);
            var hh = fixDate.getHours() + (dHour || 0);
            var mm = fixDate.getMinutes() + (dMin || 0);
            var ss = fixDate.getSeconds() + (dSec || 0);
            var sss = fixDate.getMilliseconds(); + (dMsec || 0);

            return new Date(y, m, d, hh, mm, ss, sss);
        },
        /**
         * Date オブジェクトベースで月の加減操作をする。月日数が合わない月への加減は、対象月の最終日へ丸める。
         * 操作例：
         *   2012/3/31 の前月 → 2012/2/28
         *   2020/2/29 うるう年の前年 → 2019/2/28
         * @param date yyyyMMdd(文字列 or 数値) or 日付になる文字列 or Date オブジェクト
         * @param dYear 年差分、null は 0 として扱う。
         * @param dMonth 月差分、null は 0 として扱う。
         * @return Date オブジェクト、null は 0 として扱う。
         */
        computeMonth:function(date, dYear, dMonth){
            var curDate = clutil.date.toDate(date);
            var newDate = this.computeDate(curDate, dYear, dMonth);
            if(newDate == null){
                return;
            }

            // 算出後の期待月
            var expectedMonth = curDate.getMonth() + (dMonth || 0);
            while(expectedMonth < 0){
                expectedMonth += 12;
            }
            while(expectedMonth >= 12){
                expectedMonth -= 12;
            }

            var newMonth = newDate.getMonth();
            if(newMonth != expectedMonth){
                // 補正
                var dMonth2 = expectedMonth - newMonth;
                newDate = this.getLastDateOfMonth(newDate, dMonth2);
            }

            return newDate;
        },
        /**
         * yyyyMM 年月の相対年月を算出する
         * @param ym 基準とする年月
         * @param dYear 年差分、null は 0 として扱う。
         * @param dMonth 月差分、null は 0 として扱う。
         */
        computeYM: function(ym, dYear, dMonth){
            var date = clutil.date.toDateByYM(ym);
            var fixDate = clutil.date.computeDate(date, dYear, dMonth);
            var fixYM = fixDate.getFullYear() * 100 + (fixDate.getMonth() + 1);
            return fixYM;
        },
        /**
         * 指定日付の最終日を表す Date オブジェクトを返す。
         * @param date 指定日付
         * @param dMonth date日付より、±月の最終日を取る場合に指定。省略可
         */
        getLastDateOfMonth: function(date, dMonth){
            var fixDate = date;
            if(_.isNumber(date)){
                // yyyymmdd 8桁数値と見做す
                fixDate = clutil.date.toDate(date);
            }else if(!_.isDate(date)){
                // 任意日付表現テキストと見做す
                fixDate = new Date(date);
            }
            if(_.isNaN(fixDate.getTime())){
                return;		// Invalid date
            }

            var y = fixDate.getFullYear();
            var m = fixDate.getMonth();
            if(dMonth != null){
                m += dMonth;
            }

            // 翌月 0:00.00.000 から 1 ミリ秒減算することで同月最終日の Date を取る。
            return new Date(y, m+1, 1, 0, 0, 0, -1);
        },
        /**
         * 指定日付 date から、時分秒ミリ秒を 0 に設定する。
         * @param date 指定日付
         * @return Date オブジェクト
         */
        clearHms: function(date){
            return clutil.date.toDate(date, 0/* 0時0分0秒000 */);
        },
        /**
         * うるう年判定
         */
        isLeapYear: function(year){
            var y = _.isDate(year) ? year.getFullYear() : year;
            return (y % 4 == 0 && y % 100 != 0) || y % 400 == 0;
        },
        /**
         * 正しい日付かどうか判定する。
         * @param year  検査対象年
         * @param month 検査対象月
         * @param day   [省略可]検査対象日
         * または、
         * @param year  検査対象 Date オブジェクト
         *
         * @return true:正しい日付、false:Invalid date
         */
        isValidDate: function(year, month, day){
            if(arguments.length === 1 && _.isDate(arguments[0])){
                var dt = arguments[0], tm = dt.getTime();
                return _.isNaN(tm) === false;
            }
            year = parseInt(year,10), month = parseInt(month, 10);
            if(_.isFinite(day)){
                day = parseInt(day, 10);
            }else{
                day = 1;
            }
            var dt = new Date(year, month-1, day);
            return (dt.getFullYear() === year && dt.getMonth() === (month-1) && dt.getDate() === day);
        }
    });

    // Namespace: clutil.color -- 色定義に関するユーティリティ
    var colorsamples = [
        { name: 'default',		fg: null,		bg: null },
        { name: 'blue',			fg: 0x0283cc,	bg: 0xd7effd },
        { name: 'bluegreen',	fg: 0x088aa7,	bg: 0xd8f1f7 },
        { name: 'green',		fg: 0x0e9286,	bg: 0xd9f3f1 },
        { name: 'greenyellow',	fg: 0x4cb244,	bg: 0xe6f5e5 },
        { name: 'yellow',		fg: 0x97a510,	bg: 0xf4f7d9 },
        { name: 'yelloworange',	fg: 0xb99b1d,	bg: 0xf9f4dc },
        { name: 'orange',		fg: 0xff9308,	bg: 0xfff1de },
        { name: 'orangered',	fg: 0xf95f21,	bg: 0xfeeae2 },
        { name: 'red',			fg: 0xf32c3e,	bg: 0xfde3e6 },
        { name: 'redpurple',	fg: 0xab4786,	bg: 0xf4e5ef },
        { name: 'purple',		fg: 0x674cca,	bg: 0xebe7f8 },
        { name: 'purpleblue',	fg: 0x206dde,	bg: 0xe1ebfb }
        ];
    if(!clutil.color){
        clutil.color = {};
    }
    _.extend(clutil.color, {
        /**
         * 色定義サンプル（Number 型）
         */
        samples: colorsamples,
        /**
         * 色定義サンプルマップ(プロトコル準拠) - Map<colorName, 色定義情報>
         */
        sampleMap: _.reduce(colorsamples, function(map, obj){
            map[obj.name] = {
                fgcolor: obj.fg,
                bgcolor: obj.bg
            };
            return map;
        }, {}),
        /**
         * 色定義サンプルマップ（CSS 用）- Map<colorName, 色定義情報>
         */
        cssSampleMap: _.reduce(colorsamples, function(map, obj){
            map[obj.name] = {
                'color': (obj.fg == null) ? '' : '#' + obj.fg.toString(16),
                'background-color': (obj.bg == null) ? '' : '#' + obj.bg.toString(16)
            };
            return map;
        },{}),
        /**
         * 色設定用 css クラス名リストを生成する。
         * 色設定クラスを削除する際の利用想定。
         *
         * 使用例：
         *      $cell.removeClass(clutil.color.cssClasses('cell-').join(' ));
         *
         * @param prefix
         * @param suffix
         * @return カラー名配列 - 例: [ 'blue', 'bluegreen', ... 'purpleblue']
         */
        cssClasses: function(prefix, suffix){
            return _.reduce(colorsamples, function(ss, c){
                if(c.fg != null && c.bg != null){
                    var cname = c.name;
                    if(prefix != null)	cname = prefix + cname;
                    if(suffix != null)	cname += suffix;
                    ss.push(cname);
                }
                return ss;
            }, []);
        },
        /**
         * 指定 jQuery 要素に対して、カラー名で背景色、前景色を設定する。
         * @param colorName カラー名
         * @param $elem 適用対象の jQuery オブジェクト
         */
        applyColor: function(colorName, $elem){
            var c = clutil.color.cssSampleMap[colorName];
            if(c == null){
                c = clutil.color.cssSampleMap['default'];
            }
            for(var i=1; i<arguments.length; i++){
                var $x = arguments[i];
                $x.css(c);
            }
        },
        /**
         * 指定カラー名を適用するための style 属性を文字列で出力する。
         * @param colorName カラー名
         * @return スタイル属性 - 例: 「style="color: #eeeeee; background-color: #ffffff;"」
         */
        styleAttr: function(colorName){
            var c = clutil.color.cssSampleMap[colorName];
            if(c == null){
                return '';
            }
            var ss = [];
            for(var k in c){
                var v = c[k];
                if(v != null){
                    ss.push(k + ': ' + v + ';');
                }
            }
            return _.isEmpty(ss) ? '' : 'style="' + ss.join(' ') + '"';
        },
        /**
         * $elem.css('color') で取得できる色定義 "rgb(255, 255, 255)" 文字列を Number 型へ変換する。
         * @param rgb -- 例: "rgb(255, 255, 255)" 文字列
         * @return Number 変換した値
         */
        rgb2Number: function(rgb){
            var rgbValues = rgb.replace(/^rgb\(/,'').replace(/\)$/,'').split(',');
            var hexStr = '0x' + _.chain(rgbValues).map(function(c){ return parseInt(c,10).toString(16); }).value().join('');
            return parseInt(hexStr,16);
        }
    });

    // clutil 拡張
    _.extend(clutil, {
        /**
         * 指定テンプレートをロードする。
         */
        loadTemplate: function(arg){
            var tmplPath = clcom.config.templateRoot || '/system/template';
            if(clcom.templates == null){
                clcom.templates = {};
            }

            var dd = [];
            _.chain(arguments).toArray().flatten().each(function(tcode){
                if(clcom.templates[tcode] != null){
                    return;	// キャッシュ済
                }
                var uri = clutil.fmt('{0}/{1}.html', tmplPath, tcode);
                dd.push(clutil.loadHtml(uri, function(data){
                    clcom.templates[tcode] = data;
                }));
            });

            var defer = $.Deferred();
            $.when.apply($, dd).done(function(){
                defer.resolve(clcom.template);
            }).fail(function(data){
                defer.reject(data);
            });
            return defer.promise();
        },

        /**
         * Simple underscore function to create secure inmutable (recursive deep copy) JavaScript Objects
         * @param {Object/Array} obj Target Object. Required
         * @return {Object/Array) Super inmutable new Object
         * @method deepClone
         */
        deepClone: function deepClone(obj) {
            var clone, key;
            if (typeof obj !== 'object' || obj === null) { return obj; }
            if (_.isString(obj)) { return obj.splice(); }
            if (_.isDate(obj)) { return new Date(obj.getTime()); }
            if (_.isFunction(obj.clone)) { return obj.clone(); }
            clone = _.isArray(obj) ? obj.slice() : _.extend({}, obj);
            for (key in clone) {
                clone[key] = deepClone(clone[key]);
            }
            return clone;
        },

        /**
         * 引数指定 obj 内の全ての関数の this を obj に束縛する。
         * underscore.js 1.5.0 より、_.bindAll(obj,funcNames,..) は関数名指定が必須となり、
         * _.bindAll(this) は廃止されたための措置。
         * @param instance bind 対象の関数を含むオブジェクト
         * @param ctx bind 先のオブジェクト。省略時は instance 自身が bind 先オブジェクトとなる。
         */
        bindAll: function(instance, ctx){
            if(ctx == null){
                ctx = instance;
            }
            var funcNames = _.functions(instance);
            if(instance === ctx){
                var args = [instance].concat(funcNames);
                _.bindAll.apply(this, args);
            }else{
                for(var i=0; i<funcNames.length; i++){
                    var fname = funcNames[i];
                    var fn = instance[fname];
                    instance[fname] = _.bind(fn, ctx);
                }
            }
        },

        /**
         * Object操作: 指定 obj のプロパティパス値を取得する。
         * 例: obj = { id:1, attr:{ qy:1234, memos:["memo1","memo2"] } }
         *   clutil.getValueByPath(obj, "attr.qy") ⇒ 1234
         *   clutil.getValueByPath(obj, "attr.memos[1]") ⇒ "memo2"
         */
        getObjProperty: function(obj, propPath) {
            return _.reduce(propPath.split('.'), function(o, propName){
                if (o == null) {
                    return o;
                }
                if (/.*\[[0-9].*\]$/.test(propName)) {
                    // 配列アクセス
                    var braIdx = propName.indexOf('[');
                    var ketIdx = propName.indexOf(']');
                    var name = propName.substr(0, braIdx);
                    var idx = propName.substr(braIdx+1,ketIdx-braIdx-1);
                    var oByName = o[name];
                    return (oByName != null) ? oByName[idx] : undefined;
                } else {
                    return o[propName];
                }
            }, obj);
        },
        /**
         * Object操作: 指定 obj 内のプロパティパスに値をセットする。
         * 例：
         *   clutil.setValueOnPath({}, "path.to.value", 1234) ⇒ {path:{to:{value: 1234}}}
         *   clutil.setValueOnPath({}, "path.collection[2]", 1234) ⇒ { path:{collection:[undefined, 1234] }
         */
        setObjProperty: function(obj, propPath, value) {
            var pathNames = propPath.split('.');
            var baseName = _.last(pathNames);
            var parentObj = null;

            if (pathNames.length > 1) {
                pathNames.length -= 1;
                parentObj = _.reduce(pathNames, function(o, propName){
                    if (/.*\[[0-9].*\]$/.test(propName)) {
                        // 配列アクセス
                        var braIdx = propName.indexOf('[');
                        var ketIdx = propName.indexOf(']');
                        var name = propName.substr(0, braIdx);
                        var idx = propName.substr(braIdx+1,ketIdx-braIdx-1);
                        if (o[name] == null) {
                            o[name] = [];
                        }
                        if (o[name][idx] == null) {
                            o[name][idx] = {};
                        }
                        return o[name][idx];
                    } else {
                        if (o[propName] == null) {
                            o[propName] = {};
                        }
                        return o[propName];
                    }
                }, obj);
            } else {
                parentObj = obj;
            }

            if (/.*\[[0-9].*\]$/.test(baseName)) {
                // 配列アクセス
                var braIdx = parentObj.indexOf('[');
                var ketIdx = parentObj.indexOf(']');
                var name = baseName.substr(0, braIdx);
                var idx = baseName.substr(braIdx+1,ketIdx-braIdx-1);
                if (parentObj[name] == null) {
                    parentObj[name] = [];
                }
                parentObj[name][idx] = value;
            } else {
                parentObj[baseName] = value;
            }
        },
        /**
         * オブジェクト配列の行列転置する
         * list: [ {id:1,code:'001',name'aaa',iymd:20160101,hhmm:1225}, {id:2,code:'00002',name'bbb',iymd:20160102,hhmm:1226},... ]
         *   ↓
         * tlist: { id:[1,2,... ], name:['aaa','bbb',...], iymd:[ 20160101,20160102,.. ], hhmm:[ 1225,1226,... ] }
         *
         * @param opt 複合列データ定義（省略可)
         * 例: { idcdnm: ['id','code','name'], datetime: [ 'iymd', 'hhmm' ] }
         */
        trList: function(list, opt){
            if(_.isEmpty(list)){
                return list;
            }
            var colNames = _.keys(list[0]);
            var opt = opt || {};
            return _.reduce(list, function(tobj, row){
                for(var colIdx=0; colIdx<colNames.length; colIdx++){
                    var colName = colNames[colIdx];
                    var dstList = tobj[colName];
                    if(dstList == null){
                        dstList = [];
                        tobj[colName] = dstList;
                    }
                    dstList.push(row[colName]);
                }
                for(var k in opt){
                    var inColNames = opt[k];
                    var dstList = tobj[k];
                    if(dstList == null){
                        dstList = [];
                        tobj[k] = dstList;
                    }
                    var obj = {};
                    for(var i=0;i<inColNames.length;i++){
                        var colName = inColNames[i];
                        obj[colName] = row[colName];
                    }
                    dstList.push(obj);
                }
                return tobj;
            },{});
        },
        /**
         * 数値化する。数値化できない場合は 0 を返す。
         */
        valueToNumber: function(x) {
            var fixedVal;
            if(_.isBoolean(x)){
                // boolean は true[1], false[0] とでもしておく。
                fixedVal = x ? 1 : 0;
            }else if(_.isArray(x)){
                fixedVal = 0;
            }else{
                if(_.isString(x) && x.indexOf('.') > 0){
                    fixedVal = parseFloat(x);
                }else{
                    fixedVal = parseInt(x,10);
                }
            }
            return _.isNaN(fixedVal) ? 0 : fixedVal;
        },
        /**
         * @param obj
         * 	obj がプリミティブ型の場合、可能な限り Number 型に変換する。
         * 	obj が配列型の場合、内部要素に対して Number 型に変換する。コピー配列を返す。
         * 	obj がオブジェクト型の場合、内部プロパティに対して Number 型にする。コピーオブジェクトを返す。
         * @opt
         * 	.exclude array<string>	: Number 型変換しないプロパティ名を指定する。
         */
        valuesToNumber: function(obj, opt) {
            var excludeMap = _.reduce(((opt||{}).exclude || []), function(map, v){
                map[v] = true;
                return map;
            }, {});

            function myValuesToNumber(obj,exMap){
                if(obj == null){
                    return obj;
                }
                if(_.isNumber(obj)){
                    return obj;
                }
                if(_.isEmpty(obj)){
                    return obj;
                }
                // 配列
                if(_.isArray(obj)){
                    var xdst = new Array();
                    for(var k in obj){
                        var v = obj[k];
                        xdst[k] = myValuesToNumber(v,exMap);
                    }
                    return xdst;
                }
                // オブジェクト
                if(_.isObject(obj)){
                    var xdst = new Object();
                    for(var k in obj){
                        var v = obj[k];
                        if(_.isObject(v)){
                            xdst[k] = myValuesToNumber(v, exMap);
                        }else if(_.isArray(v)){
                            var v0 = _.first(v);
                            if(_.isObject(v0) || _.isArray(v0)){
                                xdst[k] = myValuesToNumber(v, exMap);
                            }else{
                                if(exMap[k]){
                                    xdst[k] = v;
                                }else{
                                    xdst[k] = myValuesToNumber(v, exMap);
                                }
                            }
                        }else{
                            if(exMap[k]){
                                xdst[k] = v;
                            }else{
                                xdst[k] = myValuesToNumber(v, exMap);
                            }
                        }
                    }
                    return xdst;
                }
                // プリミティブ型
                var num = Number(obj);
                return _.isNaN(num) ? obj : num;
            }

            return myValuesToNumber(obj, excludeMap);
        },
        /**
         * Int32 サイズの Number 値を返す。最大、最小値を超える場合は切り詰める。
         * MIN(-2147483648) - MAX(2147483647)
         */
        valueToInt32: function(val){
            var fixedval = clutil.valuesToNumber(val);
            if(fixedval > 2147483647/*INT32_MAX*/){
                fixedval = 2147483647;
            }else if(fixedval < -2147483648/*INT32_MIN*/){
                fixedval = -2147483648;
            }
            return fixedval;
        }
    });
});
/**
 * clutil.mediator イベント
 * clstart 直前に配置する。
 */
$(function () {

    /**
     * イベント中継の Backbone.Event インスタンス
     */
    if(clutil == null){
        throw 'clutil context not loaded yet.';
    }
    _.extend(clutil, {
        /**
         * イベント中継の Backbone.Event インスタンス
         */
        mediator: _.extend({}, Backbone.Events),

        /**
         * フォーカスヒストリー -- 20件くらい
         */
        focusHistory: [],

        /**
         * Datepicker カレンダーの内部要素かどうか判定する。
         */
        isDatepickerCalendar: function (el){
            var $el = (el instanceof $) ? el : $(el);
            if($el.hasClass('.ui-datepicker')){
                return true;
            }
            var $w = $el.closest('.ui-datepicker');
            return $w.length > 0;
        }
    });

    /**
     * Extra リフレッシュイベント
     *
     * イベント名: refresh:<clcom.getIniExtra.resId>, subscribe:<clcom.getIniExtra.resId>
     *
     * [発火] clutil.mediator.trigger('refresh:<clcom.getIniExtra.resId>')
     *      マスタ検索日違いのデータを一時キャッシュ上だけ更新したい場合は、
     *          clutil.mediator.trigger('refresh:<clcom.getIniExtra.resId>', { req:{cond:{srch_iymd: xxx}}, preventSuccessProc:true } );
     *                  options.req: マスタ検索日違いなリクエストを指定する。
     *                  options.preventSuccessProc: 成功応答時、Webストレージへの保存を阻害する。
     * [購読] anyView.listenTo(clutil.mediator, 'subscribe:<clcom.getIniExtra.resId>', function(ev){
     *          var data = ev.data;     // 応答データ
     *          var extra = ev.extra;   // 初期化データ取得項目
     *          if(ev.status == 'failure'){
     *                  // エラー応答 -- (data.head.status != OK) と等価
     *          }
     *        });
     */
    var m = clutil.mediator;
    var extras = clcom.getIniExtra || {};
    _.each(extras, function(extra, key){
        var refresh = 'refresh:' + extra.resId;
        m.on(refresh, function(argOptoin){
            // this = extra[i]
            var option = argOptoin || {};
            var subscribe = 'subscribe:' + this.resId;
            var req = _.isFunction(this.buildReq) ? this.buildReq() : {};
            if(option.req){
                _.extend(req, option.req);
            }
            var successProc = (option.preventSuccessProc) ? null : this.success;
            var $df = clutil.postJSON(this.resId, req).done(_.bind(function(data){
                if (_.isFunction(successProc)) {
                    successProc(data, this);    // ストレージへ save
                }
                // 応答ヘッダ以外を揮発キャッシュに乗せる
                if(_.isFunction(this.cacheable)){
                    this.cacheable(data, this);
                }
                m.trigger(subscribe, {data: data, status:'success', extra:this});
            },this)).fail(_.bind(function(data){
                m.trigger(subscribe, {data: data, status:'failure', extra:this});
            },this));
        }, extra);
    });

    /**
     * ウィンドウイベント
     *
     * イベント名: window:lazyresize、window:lazyresize、screen-mode:change
     *
     * 'window:resize'          - $window.resize() のイベント中継する。
     * 'window:lazyresize'      - リサイズ操作が終わったタイミングで発火する。
     * 'screen-mode:change'     - スクリーンモード変更
     */
    var rszTimer = false, savedScreenMode = null;
    $(window).resize(function(ev){
        clutil.mediator.trigger('window:resize', ev);
        if(rszTimer !== false){
            clearTimeout(rszTimer);
            rszTimer = false;
        }
        rszTimer = setTimeout(function(){
            clutil.mediator.trigger('window:lazyresize', ev);
            rszTimer = false;
        },300);

        // スクリーンモード定義がある場合
        if(savedScreenMode != null){
            var curMode = clutil.view.getScreenMode();
            if(savedScreenMode !== curMode){
                // イベント発火
                console.info('[screen-mode:change]: ' + JSON.stringify(savedScreenMode) + ' -> ' + JSON.stringify(curMode.name));
                clutil.mediator.trigger('screen-mode:change', curMode, savedScreenMode);
                savedScreenMode = curMode;
            }
        }
    });
    // スクリーンモード定義がある場合
    if(clutil.view && clutil.view.ScreenModes){
        savedScreenMode = clutil.view.getScreenMode();
    }

    // datepicker 内部要素かどうか判定、内部要素である場合はラッパー要素を返す。
    function isDatepickerInner($target){
        if($target.is('.datepicker_wrap')){
            return $target;
        }
        if($target.hasClass('hasDatepicker') || $target.hasClass('ui-datepicker-trigger')){
            var $wrap = $target.closest('.datepicker_wrap');
            if($wrap.length > 0){
                return $wrap;
            }
        }
    }

    // selectpicker 内部要素かどうか判定、内部要素である場合はラッパー要素を返す。
    function isSelectpickerInner($target){
        if($target.is('.bootstrap-select')){
            return $target;
        }
        var $wrap = $target.closest('.bootstrap-select');
        if($wrap.length > 0){
            return $wrap;
        }
    }

    /**
     * フォーカスインイベント
     *
     * イベント名: focushistory:focusin
     *
     * body 部全体で、focus の入りを監視する。
     * selectpicker や datepicker などの複合コンポーネントでの内部フォーカス移動についてはヒストリに積まない。
     *
     * 利用想定:
     * 「Input の Editing 状態が刈り取られる」判定に利用。
     * 「自分以外の要素にフォーカスが入る」ことで感知することにする。
     *
     * 実装例:
     * myView.listenTo(clutil.mediator, 'focushistory:focusin', function(ev, prevEv, focusHistory){
     *  var isMyContent = myView.$el.has(ev.target).length > 0;
     *  var prevIsMyContent = (prevEv != null) && (myView.$el.has(prevEv.target).length > 0);
     *  if(isMyContent && prevIsMyContent){
     *      // 内部フォーカス移動である - focusHistory の prevEv 要素を取り除く。
     *      var hist = clutil.focusHistory;
     *      hist.pop();
     *      hist.pop();
     *      hist.push(ev);
     *      return;
     *  }
     *  if(prevIsMyContent && isMyContent === false){
     *      // 余所へフォーカス移動した
     *      // ⇒ 内部状態が「編集中状態」から「確定状態」に変化した。よって、確定処理を施す。
     *      //    この際、フォーカスに影響する処理はやらないこと。
     *  }
     * });
     */
    $('body').on('mousedown', function(ev){
        var $target = $(ev.target);
        var hist = clutil.focusHistory;
        var prevEv = _.last(hist);

        // datepicker のカレンダーポップアップからの focus 発火は無視
        // TODO: AlertView, tooltip も同様な気がする
        if(clutil.isDatepickerCalendar(ev.target)){
            return;
        }

        // mousedown - フォーカスロスト契機を判断する。
        // フォーカスロストと判定した場合はフォーカス履歴に null を登録する。
        if($target.is(':focusable')){
            // フォーカス可能な要素は focusin で拾うのでスキップ
            return;
        }

        // focasable でないものでも、複合コンポーネントからのイベントならスキップする。
        if(isDatepickerInner($target)){
            // datepicker 内部要素である
            return;
        }
        if(isSelectpickerInner($target)){
            // selectpicker 内部要素である
            return;
        }

        // ヒストリー最後要素がクリック要素と合致しない場合はフォーカスロスト判定とする。
        if(prevEv == null){
            // どこもフォーカスをもっていない状態
            return;
        }

        // ヒストリーへ null を積み込む
        if(hist.length >= 20){
            hist.shift();
        }
        hist.push(null/*ev*/);

        // イベント発火
        console.info('[focushistory:focusin]: null');
        clutil.mediator.trigger('focushistory:focusin', null, prevEv, hist);

    }).on('focusin', function(ev){
        var $target = $(ev.target);
        var hist = clutil.focusHistory;
        var prevEv = _.last(hist);

        // IE11 Datepicker特別対処
        // focusin 発火元がフォーカス保持できない要素なら focus 発火は無視。
        // そもそも、focus が入らない要素に対して focusin イベント発生することは期待していない。
        // しかしながら、IE11 の場合、input 欄外クリックすると BODY に対して focusin 発火する。
        // BODY は focus 保持できないものとして、ここで除外しておく。
        if($target.is(':focusable') === false){
            return;
        }

        // datepicker のカレンダーポップアップからの focus 発火は無視
        // TODO: AlertView, tooltip も同様な気がする
        if(clutil.isDatepickerCalendar(ev.target)){
            return;
        }

        if(prevEv && (ev.target === prevEv.target)){
            // ブラウザ window が focus lost してから、再び focus 復帰した場合想定
            return;
        }

        // 複合コンポーネント対応
        do{
            var $wrap;

            // datepicker 発の場合
            $wrap = isDatepickerInner($target);
            if($wrap){
                if(prevEv && prevEv.cl_target.has($target).length > 0){
                    // 内部コンポーネント間のフォーカス移動のため、history 最後を入れ替えておくだけにする。
                    ev.cl_target = prevEv.cl_target;
                    hist.shift();
                    hist.push(ev);
                    return;
                }
                ev.cl_target = $wrap;
                break;
            }

            // selectpicker 発の場合
            $wrap = isSelectpickerInner($target);
            if($wrap){
                if(prevEv && prevEv.cl_target.has($target).length > 0){
                    // 内部コンポーネント間のフォーカス移動のため、history 最後を入れ替えておくだけにする。
                    ev.cl_target = prevEv.cl_target;
                    hist.shift();
                    hist.push(ev);
                    return;
                }
                ev.cl_target = $wrap;
                break;
            }
        }while(false);

        // ヒストリーへ積み込む
        if(hist.length >= 20){
            hist.shift();
        }
        if(ev.cl_target == null){
            ev.cl_target = $target;
        }
        hist.push(ev);

        // イベント発火
        console.info('[focushistory:focusin]: ', ev.target);
        clutil.mediator.trigger('focushistory:focusin', ev, prevEv, hist);
    });

    /**
     * focus 出入で input:text 入力値の 編集用 ⇔ 表示用 フォーマット切り替え処理を行うための
     * clutil.mediator "focushistory:focusin" イベントハンドラ。
     */
    var vent = _.extend({}, Backbone.Events);
    vent.listenTo(clutil.mediator, 'focushistory:focusin', function(ev, prevEv, focusHistory){

        // 複合コンポーネント対応の target 要素取得
        function getTargetElem(ev){
            if(ev == null) return;

            var $target = $(ev.target);

            // datepicker カレンダー toggle ボタン
            if($target.hasClass('ui-datepicker-trigger')){
                $target = $target.prev();
            }

            // selectpicker の内部ボタンなら．．．
            if($target.is('button.dropdown-toggle[data-toggle="dropdown"]')){
                var $wrap = isSelectpickerInner($target);
                if($wrap){
                    var $select = $wrap.children('select');
                    if($select.length === 1){
                    	// ほぼ、selectpicker の元 <select> 要素と断定
                    	$target = $select;
                    }
                }
            }

            return $target;
        }

        var $outInput = getTargetElem(prevEv), $inInput = getTargetElem(ev);

        // フォーカスが抜けた要素 : 編集中状態を刈り取る
        if($outInput/* focusout */){
            var $input = $outInput;

            // IE11 Datepicker特別対処 [1]
            // 欄外クリックのとき、change イベント発火しないので、発火させる。
            if(clcom.isLegacyIE() && $input.is('.hasDatepicker:not(:disabled)')){
                var rawval = $input.val(), savedVal = $input.data('cl_datepicker_savedval');
                $input.removeData('cl_datepicker_savedval');
                // 最終入力値 - 日付フォーマット補正が入る場合に限るとき、IE11 では change イベント発火しないため、
                // 明示的に $input.change() イベント発火させる！
                var dt = clutil.date.toDate(rawval);
                if (rawval != savedVal && clutil.date.isValidDate(dt)) {
                    $input.change();
                }
            }

            var domain = clutil.getDomain($input);
            if(domain && domain.controller && _.isFunction(domain.controller.editStop)){
                domain.controller.editStop($input);
            }
        }

        // フォーカスが入った要素 : 編集中状態の表示
        if($inInput){
            var $input = $inInput;

            var domain = clutil.getDomain($input);
            if(domain && domain.controller && _.isFunction(domain.controller.editStart)){
                domain.controller.editStart($input);
            }

            // IE11 Datepicker特別対処 [2]
            // 欄外クリックのとき、change イベント発火しない。editStop なタイミング [1] のときに、
            // change イベント発火させる / させない判定のため、編集前の入力値を記録しておく。
            if(clcom.isLegacyIE() && $input.is('.hasDatepicker:not(:disabled)')){
                var rawval = $input.val();
                $input.data('cl_datepicker_savedval', rawval);
            }
        }
    });

    /**
     * 広くクリックイベントを監視する
     *
     * イベント名: 'html:click'
     *
     * View 自身の外のクリックイベントを監視する場合に利用する。
     * Backbone.View インスタンスにおいて、イベント発信元が自身の el の外かどうかの判定は下記で。
     * 実装例:
     * myView.listenTo(clutil.mediator, 'html:click', function(ev){
     *  var isMyContent = myView.$el.has(ev.target).length > 0;
     *  if(isMyContent === false){
     *      // 自身の外からクリックイベント発信
     *  }
     * }
     */
    $('html').on('click', function(ev){
        clutil.mediator.trigger('html:click', ev);
    });

});
/**
 * 汎用入出力コントローラ     XXX
 */
var clutil = _.extend({}, clutil);
if(!clutil.view){
    clutil.view = {};
}
$(function(){


    // 数値化可能判定関数 - id 値体系を想定。
    // -----------------------------------------------------------------
    // 先頭 0 からはじまる数文字列はコードと見做し、数値化不可としておく。
    // [例]
    //  '0'     : true - 数値化可能
    //  '1234'  : true - 数値化可能
    //  '0001'  : false - 数値化不可
    //  '123a'  : false - 数値化不可
    function isNumerable(str){
        return  (str == '0') ? true : /^[1-9]\d*$/.test(str);
    }

    var abstractTextUICtroller = {
        // 設定値を取得する
        getValue: function($input){
            var val = $input.val();
            return $.trim(val);
        },
        // 値を設定する
        setValue: function($input, val){
            val = val || '';
            $input.val(val);
        },
        // 活性・非活性化する
        setEnable: function($input, enable){
            // TODO
        },
        // クリア
        clear: function($input){
            if($input.is('textarea')){
                $input.text('');
            }else{
                $input.val('');
            }
        },
        // 入力値素のまま
        getRawValue: function($input){
            var val = $input.val();
            return $.trim(val);
        },
        // 編集時のコンバータ
        // 数値表示の変換例: 1,234,567.89 → 1234567.89
        editStart: function($input){
            $input.trigger('cl_edit:start');
        },
        // 表示時のコンバーター
        // 数値表示の変換例: 1234567.89 → 1,234,567.89
        editStop: function($input){
            $input.trigger('cl_edit:stop');
        }
    };
    var abstractSelectUICtroller = {
        // 設定値を取得する
        getValue: function($select){
            var val = $select.selectpicker('val');
            if(val == null || val == 'NaN' || val == 'null' || val == 'undefined' || val == ''){
                return;
            }

            var isNum = true;
            if(_.isArray(val) /* $select.is('[multiple') */){
                // 複数選択 - select[multiple]
                var vals = val;
                for(var i=0; i<vals.length; i++){
                    var v = vals[i];
                    if((isNum = isNumerable(v)) === false){
                        break;
                    }
                }
            }else{
                isNum = isNumerable(val);
            }

            return (isNum) ? clutil.valuesToNumber(val) : val;
        },
        // 値を設定する
        setValue: function($input, val){
            $input.selectpicker('val', val);
        },
        // 活性・非活性化する
        setEnable: function($input, enable){
            // TODO
        },
        // クリア
        clear: function($select){
            // 選択値 NaN を未選択として固定してしまおう。
            $select.selectpicker('val', NaN);
        },
        // 入力値素のまま
        getRawValue: function($input){
            return $input.selectpicker('val');
        },
        // 入力値素のまま
        getRawValue: function($input){
            var val = $input.is('textarea') ? $input.text() : $input.val();
            return $.trim(val);
        },
        // 編集時のコンバータ
        editStart: function($input){
            $input.trigger('cl_edit:start');
        },
        // 表示時のコンバーター
        editStop: function($input){
            $input.trigger('cl_edit:stop');
        }
    };

    _.extend(clutil.view, {

        // [メンツ]
        // [済] input:text
        // [済] input:text (数値)
        // [済] input:text.hasDatepicker
        // [済] select               ... selectpicker
        // [未] input:checkbox       TODO
        // [済] checkboxgroup        ... ラッパー div に data-cl_domain を仕掛ける
        // [済] radiogroup           ... ラッパー div に data-cl_domain を仕掛ける
        // [未] switch               TODO
        // ...その他部品APIは都度追加する...
        // ----------------------
        // [共通 API]
        // getValue(), setValue(val), setEnable(boolean), clear()   -- データ入出力用
        // getRawValue()                                            -- 入力チェック用
        // editStart(), editStop()                                  -- 編集開始、終了ハンドラ: 数値入力のフォーマット変換とか・・・
        ctrl: {
            /**
             * 文字列入出力 - input:text 系、textarea に対応。
             */
            text: _.defaults({}, abstractTextUICtroller),

            /**
             * 数値入出力 - input:text 系、フォーマット適用無し
             */
            number: _.defaults({
                // 設定値を取得する
                getValue: function($input){
                    var val = $.trim($input.val());
                    if(_.isFinite(val)){
                        return Number(val);
                    }
                    // return undefined;
                },
                // 値を設定する
                setValue: function($input, val){
                    val = (val == null) ? '' : val;
                    $input.val(val);
                }
            }, abstractTextUICtroller),

            /**
             * 数値入出力 - input:text 系、カンマ区切りフォーマット
             */
            decimal: _.defaults({
                // 設定値を取得する
                getValue: function($input){
                    var val = $.trim($input.val()).replace(/,/g,'');
                    if(_.isFinite(val)){
                        return Number(val);
                    }
                    // return undefined;
                },
                // 値を設定する
                setValue: function($input, val){
                    val = val || '';
                    if(_.isFinite(val)){
                        val = clutil.text.comma(val);
                    }
                    $input.val(val);
                },
                // 編集時のコンバータ
                // 数値表示の変換例: 1,234,567.89 → 1234567.89
                editStart: function($input){
                    var raw = this.getRawValue($input).replace(/,/g, '');
                    if(_.isFinite(raw)){
                        $input.val(raw).select();
                    }
                    $input.trigger('cl_edit:start');
                },
                // 表示時のコンバーター
                // 数値表示の変換例: 1234567.89 → 1,234,567.89
                editStop: function($input){
                    var raw = this.getRawValue($input);
                    if(_.isFinite(raw)){
                        $input.val(clutil.text.comma(raw));
                    }
                    $input.trigger('cl_edit:stop');
                }
            }, abstractTextUICtroller),

            /**
             * datepicker: 年月日 iymd 入力
             */
            datepicker: _.defaults({
                // 設定値を取得する
                getValue: function($input){
                    var val = this.getRawValue($input);
                    return clutil.date.toIymd(val);
                },
                // 値を設定する
                setValue: function($input, val){
                    var dt = clutil.date.toDate(val);
                    if(_.isNaN(dt.getTime())){
                        // Invalid Date
                        if(!_.isString(val)){
                            val = '';
                        }
                        $input.val(val);
                    }else{
                        $input.datepicker('setDate', dt);
                    }
                },
                // 活性・非活性化する
                setEnable: function($input, enable){
                    // TODO
                },
                // 入力値素のまま: 空白区切りで前方の部分文字列をとらす。
                // datepicker の dateFormat 指定について、、、
                // 補助的な日付情報は、空白文字 " " で区切り、後方へ記述することとする。
                getRawValue: function($input, real){
                    var val = $.trim($input.val());
                    if(real){
                        return val; // 本当に素のまま
                    }
                    var blankIndex = val.indexOf(' ');
                    return (blankIndex > 0) ? val.slice(0,blankIndex) : val;
                },
                // 編集時のコンバータ
                editStart: function($input){
                    // 日付として正しい値に変換可能ならば、空白 " " 区切り前方のテキストをセットする。
                    //  ⇒ 編集時は、曜日要素をトリミングして、「yyyy/mm/dd」形式で表示する。
                    var rval = this.getRawValue($input);
                    if(!_.isEmpty(rval)){
                        var dt = clutil.date.toDate(rval);
                        if(!_.isNaN(dt.getTime())){
                            $input.val(rval);
                        }
                    }
                    $input.trigger('cl_edit:start');
                },
                // 表示時のコンバーター
                editStop: function($input){
                    // Datepicker の内部フォーマッタを使って、フォーマット済テキストを表示する
                    var rval1 = this.getRawValue($input);
                    var dt1 = clutil.date.toDate(rval1);
                    if(clutil.date.isValidDate(dt1)){
                        var dpSingleton = $.datepicker, dpInstance  = $input.data('datepicker');
                        if(dpSingleton && dpInstance){
                            var dpFormatConfig = dpSingleton._getFormatConfig(dpInstance);
                            var dpFormat = $input.datepicker('option', 'dateFormat');
                            try{
                                var rval2 = dpSingleton.formatDate(dpFormat, dt1, dpFormatConfig);
                                if(!_.isEmpty(rval2) && rval1 != rval2){
                                    dpInstance.input.val(rval2);
                                }
                            }catch(ex){ ; }
                        }
                    }
                    $input.trigger('cl_edit:stop');
                }
            }, abstractTextUICtroller),

            /**
             * 時分入出力 - input:text 系
             */
            hhmm: _.defaults({
                // 設定値を取得する
                getValue: function($input){
                    var val = this.getRawValue($input);
                    return clutil.date.toHHmm(val);
                },
                // 値を設定する
                setValue: function($input, val){
                    var fixVal = clutil.date.toHHmm(val);
                    if(fixVal == null){
                        $input.val(val);
                        return;
                    }
                    var h = Math.floor(fixVal / 100), m = fixVal % 100;
                    $input.val(clutil.text.zeroPadding(h, 2) + ':' + clutil.text.zeroPadding(m, 2));
                },
                // 表示時のコンバーター
                // 数値表示の変換例: 0123 → 01:23
                editStop: function($input){
                    var rval = this.getRawValue($input);
                    if(!_.isEmpty(rval)){
                        this.setValue($input, rval);
                    }
                    $input.trigger('cl_edit:stop');
                }
            }, abstractTextUICtroller),

            /**
             * selectpicker
             */
            select: _.defaults({}, abstractSelectUICtroller),

            /**
             * checkboxgroup
             */
            checkboxgroup: {
                // 設定値を取得する
                getValue: function($container){
                    var vals = this.getRawValue($container);

                    if(_.isEmpty(vals)){
                        return;
                    }

                    var isNum = true;
                    for(var i=0; i<vals.length; i++){
                        var v = vals[i];
                        if((isNum = isNumerable(v)) === false){
                            break;
                        }
                    }
                    return (isNum) ? clutil.valuesToNumber(vals) : vals;
                },
                // 値を設定する
                setValue: function($container, vals){
                    if(_.isEmpty(vals)){
                        this.clear();
                        return;
                    }
                    var selectVals = _.isArray(vals) ? vals : [ vals ];
                    $container.find('input:checkbox.cl_cbgroup').each(function(idx, cb){
                        var eigen = cb.value;
                        var nowChecked = cb.checked;
                        //-----------------------
                        var isSelect = _.some(selectVals, function(v){ return v == eigen; });
                        if(isSelect){
                            if(!nowChecked){
                                $(cb).checkbox('check');
                            }
                        }else{
                            if(nowChecked){
                                $(cb).checkbox('uncheck');
                            }
                        }

                    });
                },
                // 活性・非活性化する
                setEnable: function($container, enable){
                    // TODO
                },
                // クリア
                clear: function($container){
                    $container.find('input:checkbox.cl_cbgroup:checked').checkbox('uncheck');
                },
                // 入力値素のまま
                getRawValue: function($container){
                    var vals = [];
                    $container.find('input:checkbox.cl_cbgroup:checked').each(function(idx, cb){
                        if(cb.checked){
                            vals.push(cb.value);
                        }
                    });
                    return vals;
                },
                // 編集時のコンバータ
                editStart: function($container){
                    // nop
                },
                // 表示時のコンバーター
                editStop: function($container){
                    // nop
                }
            },

            /**
             * radiogroup
             */
            radiogroup: {
                // 設定値を取得する
                getValue: function($container){
                    var $radio = $container.find('input:radio.cl_rbgroup:checked');
                    if($radio.length === 0){
                        return;
                    }
                    var val = $radio.val();
                    return isNumerable(val) ? parseInt(val,10) : val;
                },
                // 値を設定する
                setValue: function($container, val){
                    if(val == null /*|| val == 'null' || val == 'undefined' || val == 'NaN' || val == ''*/){
                        this.clear();
                    }else{
                        $container.find('input:radio.cl_rbgroup[value="' + val + '"]').radio('check');
                    }
                },
                // 活性・非活性化する
                setEnable: function($input, enable){
                    // TODO
                },
                // クリア
                clear: function($container){
                    $container.find('input:radio.cl_rbgroup:checked').radio('uncheck');
                },
                // 入力値素のまま
                getRawValue: function($container){
                    return $container.find('input:radio.cl_rbgroup:checked').val();
                },
                // 編集時のコンバータ
                editStart: function($container){
                    // nop
                },
                // 表示時のコンバーター
                editStop: function($container){
                    // nop
                }
            },

            /**
             * suggest
             */
            suggest: _.defaults({
                // 設定値を取得する
                getValue: function($input){
                    var api = $input.data('cl_MtSuggestView');
                    if(api == null){
                        console.warn('Suggest is not ready!!!');
                        return abstractTextUICtroller.getValue($input);
                    }
                    return api.getValue();
                },
                // 値を設定する
                setValue: function($input, value){
                    var api = $input.data('cl_MtSuggestView');
                    if(api == null){
                        console.warn('Suggest is not ready!!!');
                        abstractTextUICtroller.setValue($input, value);
                        return;
                    }
                    if(_.isObject(value)){
                        // オブジェクト型明示の場合はマスタアイテムと見做し、
                        // マスタアイテム用の setter を直接行使する。
                        api.setItem(value);
                    }else{
                        // サジェストの valueType に準じた setter を利用。
                        // 特に、id によるsetter は、非同期駆動することに注意！
                        api.setValue(value);
                    }
                },
                // 活性・非活性化する
                setEnable: function($input, enable){
                    var api = $input.data('cl_MtSuggestView');
                    if(api == null){
                        console.warn('Suggest is not ready!!!');
                        abstractTextUICtroller.setEnable($input, enable);
                        return;
                    }
                    api.setEnable(enable);
                },
                // クリア
                clear: function($input){
                    var api = $input.data('cl_MtSuggestView');
                    if(api == null){
                        console.warn('Suggest is not ready!!!');
                        abstractTextUICtroller.clear($input);
                        return;
                    }
                    // 【検討】クリアオプション？
                    // ひとまず、内部キャッシュまでクリアしておく。
                    api.clear({ cacheClear: true });
                },
                // 編集値のコンバータ
                editStart: function($container){
                    // nop -- MtSuggestView 内部処理に委譲
                },
                // 表示時のコンバーター
                editStop: function($container){
                    // nop -- MtSuggestView 内部処理に委譲
                }
            }, abstractTextUICtroller)
        }
    });
});
/**
 * ドメイン定義（基本セット）
 */
var domain = _.extend({}, domain);
var clutil = _.extend({}, clutil);
$(function(){

    // -----------------------------------------------
    // clcutil.* 拡張 - ドメイン定義取得 I/F
    _.extend(clutil, {
        /**
         * 指定要素のドメイン定義情報を取得する。
         * @return object
         *  .domain ドメイン定義情報
         *  .constraint 制約（required, maxlen, minlen, maxval, minval, maxcount, mincount, decpoint,...等）
         *      required    : 入力必須
         *      minlen=<n>  : 最小文字列長
         *      maxlen=<n>  : 最大文字列長
         *      minval=<n>  : 最小値
         *      maxval=<n>  : 最大値
         *      decpoint=<n>: 小数点以下桁数
         *      mincount=<n>: 最小選択個数（３つ以上選択してください）
         *      maxcount=<n>: 最大選択個数（３つまで選択してください）
         */
        getDomain: function(input){
            var $input;
            if(input instanceof $){
                $input = input;
            }else{
                $input = $(input);
            }
            var domain = $input.data('cl_domain');
            if(_.isEmpty(domain)){
                return;
            }
            if(_.isString(domain)){
                // ドメイン情報 parse 処理
                // ------------------------------------------------------
                // [形式]
                // <ドメイン名>:<制約1>,<制約2>=<val2>,<制約3>=<val3>,...
                var dommy = domain, dommyTail = null;
                var delimIdx = dommy.indexOf(':');
                if(delimIdx >= 0){
                    dommyTail = dommy.slice(delimIdx+1);
                    dommy = dommy.slice(0,delimIdx);
                }
                domain = window.domain[dommy];
                if(domain == null){
                    throw 'DOMAIN[' + dommy + '] not defined.';
                }
                domain = clutil.deepClone(domain);
                if(!_.isEmpty(dommyTail)/*制約オーバーライド部*/){
                    // オプション制約情報を parse する。
                    var opt = {}, optss = dommyTail.split(',');
                    for(var i=0; i<optss.length; i++){
                        var optstr = optss[i];
                        var indexOfEq = optstr.indexOf('=');
                        if(indexOfEq < 0){
                            opt[optstr] = true;
                            continue;
                        }else{
                            var okey = optstr.substring(0,indexOfEq), oval = optstr.substring(indexOfEq + 1);
                            opt[okey] = _.isFinite(oval) ? Number(oval) : oval;
                        }
                    }
                    // 制約情報をオーバーライドする
                    if(!_.isEmpty(opt)){
                        domain.constraint = _.defaults(opt, domain.constraint);
                    }
                }
                // ドメイン定義インスタンスを data-cl_domain に紐付けておく。
                $input.data('cl_domain', domain);
            }
            return domain;
        }
    });

    // -----------------------------------------------
    // ドメイン定義

    // １．基本セット
    _.extend(domain, {
        /* ------------------------------------------------------------
         * テキスト             input:text
         * ------------------------------------------------------------ */
        text: {
            _description: 'テキスト入力',
            itemName: undefined,    // 項目名 - エラーメッセージ中に使用すること想定。
            controller: clutil.view.ctrl.text,
            formatTest: undefined,  // フォーマット - 正規表現 or 関数
            constraint: {
//                required: false,
//                maxlen: 20,     // 最大文字列長
//                minlen: 8,      // 最小文字列長
//                maxval: 10000,  // 最大値（数値）
//                minval: 1000,   // 最小値（数値）
//                decpoint: 2,    // 小数点下桁（数値）
//                mincount: 3,    // 選択個数下限（複数選択肢）
//                maxcount: 8     // 選択個数上限（複数選択肢）
            },
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);
                var rawVal = (_.isFunction(ctrl.getRawValue)) ? ctrl.getRawValue($input) : val.toString();

                // 必須チェック
                if(_.isEmpty(rawVal)){
                    if(c.required){
                        // 入力してください。 / {0}を入力してください。
                        var msg = _.isEmpty(this.itemName) ? 'cl_required' : 'cl_its_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // 文字列長チェック: 形式チェックOKの場合に続けてチェックする。
                var strLen = rawVal.length;
                var minLen = _.has(c,'minlen') ? c.minlen : null;
                var maxLen = _.has(c,'maxlen') ? c.maxlen : null;
                if (minLen != null && maxLen != null) {
                    if(minLen == maxLen){
                        // 固定文字数
                        if(_.isEmpty(this.itemName)){
                            // 項目名無し ------------------------------
                            // cl_length_noteq      : {0}文字で入力してください。
                            if(strLen != minLen){
                                return clutil.getclmsg('cl_length_noteq', minLen);
                            }
                        }else{
                            // 項目名付き -------------------------------
                            // cl_its_length_short0 : {0}が短すぎます。{1}文字で入力してください。
                            if(strLen < minLen){
                                return clutil.getclmsg('cl_its_length_short0', this.itemName, minLen);
                            }
                            // cl_its_length_long0  : {0}が長すぎます。{1}文字で入力してください。
                            if(strLen > maxLen){
                                return clutil.getclmsg('cl_its_length_long0', this.itemName, maxLen);
                            }
                        }
                    }else{
                        // 範囲指定
                        if(_.isEmpty(this.itemName)){
                            // 項目名無し ------------------------------
                            if(strLen < minLen || strLen > maxLen){
                                // cl_length_short2 : {0}～{1}文字で入力してください。
                                return clutil.getclmsg('cl_length_short2', minLen, maxLen);
                            }
                        }else{
                            // 項目名付き -------------------------------
                            if(strLen < minLen){
                                // cl_its_length_short2 : {0}が短すぎます。{1}～{2}文字で入力してください。
                                return clutil.getclmsg('cl_its_length_short2', this.itemName, minLen, maxLen);
                            }
                            if(strLen > maxLen){
                                // cl_its_length_long2  : {0}が長すぎます。{1}～{2}文字で入力してください。
                                return clutil.getclmsg('cl_its_length_long2', this.itemName, minLen, maxLen);
                            }
                        }
                    }
                } else if(minLen != null){
                    if (strLen < minLen) {
                        if(_.isEmpty(this.itemName)){
                            // 項目名無し ---------------------------------
                            // cl_length_short1 : {0}文字以上で入力してください。
                            return clutil.getclmsg('cl_length_short1', minLen);
                        }else{
                            // 項目名付き ---------------------------------
                            // cl_its_length_short1 : {0}が短すぎます。{1}文字以上で入力してください。
                            return clutil.getclmsg('cl_its_length_short1', this.itemName, minLen);
                        }
                    }
                } else if(maxLen != null){
                    if (strLen > maxLen) {
                        if(_.isEmpty(this.itemName)){
                            // 項目名無し ---------------------------------
                            // cl_length_long1      : {0}文字以下で入力してください。
                            return clutil.getclmsg('cl_length_long1', maxLen);
                        }else{
                            // 項目名付き ---------------------------------
                            // cl_its_length_long1  : {0}が長すぎます。{1}文字以下で入力してください。
                            return clutil.getclmsg('cl_its_length_long1', this.itemName, maxLen);
                        }
                    }
                }

                // フォーマットチェック
                if(this.formatTest){
                    if(_.isFunction(this.formatTest)){
                        // チェック関数
                        var emsg = this.formatTest(rawVal, this.itemName);
                        if(!_.isEmpty(emsg)){
                            return emsg;
                        }
                    }else{
                        // 正規表現
                        var regExp = null;
                        if(_.isString(this.formatTest)){
                            // 文字列指定の場合は、正規表現集 - clutil.text.CommonRegEx - からとる。
                            regExp = clutil.text.CommonRegEx[this.formatTest];
                        }else if(_.isRegExp(this.formatTest)){
                            regExp = this.formatTest;
                        }
                        if(regExp == null){
                            throw '不明なフォーマット定義: ' + this.formatTest.toString();
                        }
                        if(!regExp.test(rawVal)){
                            // cl_invalid_format     : 形式が誤っています。
                            // cl_its_invalid_format : {0}の形式が誤っています。入力をご確認ください。
                            var msg = _.isEmpty(this.itemName) ? 'cl_invalid_format' : 'cl_its_invalid_format';
                            return clutil.getclmsg(msg, this.itemName);
                        }
                    }
                }

                // OK
                return null;
            },
        },
        /* ------------------------------------------------------------
         * 数値 -1,234.56       input:text
         * ------------------------------------------------------------ */
        decimal: {
            _description: '数値入力',
            itemName: undefined,
            controller: clutil.view.ctrl.decimal,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);
                var rawVal = $.trim((_.isFunction(ctrl.getRawValue)) ? ctrl.getRawValue($input) : val.toString());

                // 必須チェック
                if(_.isEmpty(rawVal)){
                    if(c.required){
                        var msg = _.isEmpty(this.itemName) ? 'cl_required' : 'cl_its_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // フォーマットチェック
                if(val == null){
                    // cl_num_inval: "数値で入力してください。",
                    // cl_its_num_inval: "{0}は数値で入力してください。",
                    var msg = _.isEmpty(this.itemName) ? 'cl_num_inval' : 'cl_its_num_inval';
                    return clutil.getclmsg(msg, this.itemName);
                }

                // 浮動小数点桁数チェック
                if(_.isFinite(c.decpoint)){
                    var strval = rawVal;
                    var decPtIndex = strval.indexOf('.');
                    if(decPtIndex > 0){
                        // 小数点下無しを明示している
                        if(c.decpoint === 0){
                            if(_.isEmpty(this.itemName)){
                                // cl_num_nodecpt: '小数点以下の入力はできません。',
                                var msg = 'cl_num_nodecpt';
                                return clutil.getclmsg(msg);
                            }else{
                                // cl_its_num_nodecpt: '{0}は小数点以下の入力はできません。',
                                var msg = 'cl_its_num_nodecpt';
                                return clutil.getclmsg(msg, this.itemName);
                            }
                            return;
                        }

                        // 小数点下桁数評価
                        var decLen = (strval.length-1) - decPtIndex;
                        if(decLen > c.decpoint){
                            if(_.isEmpty(this.itemName)){
                                // cl_num_declen_overflow: '小数{0}桁以下で入力してください。',
                                // cl_num_nodecpt: '小数点以下の入力はできません。',
                                var msg = (c.decpoint > 0) ? 'cl_num_declen_overflow' : 'cl_num_nodecpt';
                                return clutil.getclmsg(msg, c.decpoint);
                            }else{
                                // cl_its_num_declen_overflow: '{0}は小数{1}桁以下で入力してください。',
                                // cl_its_num_nodecpt: '{0}は小数点以下の入力はできません。',
                                var msg = (c.decpoint > 0) ? 'cl_its_num_declen_overflow' : 'cl_its_num_nodecpt';
                                return clutil.getclmsg(msg, this.itemName, c.decpoint);
                            }
                        }
                    }
                }

                // 値範囲チェック
                var min = _.has(c,'minval') ? c.minval : null;
                var max = _.has(c,'maxval') ? c.maxval : null;
                if(min != null && max != null){
                    if(val < min || val > max){
                        // 範囲エラー
                        if(_.isEmpty(this.itemName)){
                            // cl_num_outrange: '{0}～{1}の範囲で入力してください。',
                            var msg = 'cl_num_outrange';
                            return clutil.getclmsg(msg, min, max);
                        }else{
                            // cl_its_num_outrange: '{0}は{1}～{2}の範囲で入力してください。',
                            var msg = 'cl_its_num_outrange';
                            return clutil.getclmsg(msg, this.itemName, min, max);
                        }
                    }
                }else if(min != null){
                    if(val < min){
                        // 最小値以下
                        if(_.isEmpty(this.itemName)){
                            // cl_num_less: '{0}以上の値を入力してください。',
                            var msg = 'cl_num_less';
                            return clutil.getclmsg(msg, min);
                        }else{
                            // cl_its_num_less: '{0}は{1}以上の値を入力してください。',
                            var msg = 'cl_its_num_less';
                            return clutil.getclmsg(msg, this.itemName, min);
                        }
                    }
                }else if(max != null){
                    if(val > max){
                        // 最大値超過
                        if(_.isEmpty(this.itemName)){
                            // cl_num_more: '{0}以下の値を入力してください。',
                            var msg = 'cl_num_more';
                            return clutil.getclmsg(msg, this.itemName, max);
                        }else{
                            // cl_its_num_more: '{0}は{1}以下の値を入力してください。',
                            var msg = 'cl_its_num_more';
                            return clutil.getclmsg(msg, this.itemName, max);
                        }
                    }
                }

                // OK
                return null;
            }
        },
        /* ------------------------------------------------------------
         * 年月日 yyyyMMdd      datepicker
         * ------------------------------------------------------------ */
        iymd: {
            _description: '年月日 - datepicker',
            itemName: undefined,
            controller: clutil.view.ctrl.datepicker,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);
                var rawVal = ctrl.getRawValue($input, true/* 非加工 */);

                // 必須チェック
                if(_.isEmpty(rawVal)){
                    if(c.required){
                        var msg = _.isEmpty(this.itemName) ? 'cl_required' : 'cl_its_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // フォーマットチェック
                if(val == null){
                    // cl_date_inval: "日付形式が誤っています。",
                    // cl_its_date_inval: "{0}の日付形式が誤っています。",
                    var msg = _.isEmpty(this.itemName) ? 'cl_date_inval' : 'cl_its_date_inval';
                    return clutil.getclmsg(msg, this.itemName);
                }

                // 値範囲チェック
                var min = _.has(c,'minval') ? c.minval : null;
                var max = _.has(c,'maxval') ? c.maxval : null;
                // 値範囲指定が無い場合、datepicker オプションの設定範囲を適用
                if(min == null){
                    var dpMinDate = clutil.date.toDate($input.datepicker('option', 'minDate'));
                    if(dpMinDate && !_.isNaN(dpMinDate.getTime())){
                        min = clutil.date.toIymd(dpMinDate);
                    }
                }
                if(max == null){
                    var dpMaxDate = clutil.date.toDate($input.datepicker('option', 'maxDate'));
                    if(dpMaxDate && !_.isNaN(dpMaxDate.getTime())){
                        max = clutil.date.toIymd(dpMaxDate);
                    }
                }
                // ここから、範囲チェック
                if(min != null && max != null){
                    if(val < min || val > max){
                        // 範囲エラー
                        var from = clutil.date.dateFormat('yyyy/MM/dd', min)
                            , to = clutil.date.dateFormat('yyyy/MM/dd', max);
                        if(_.isEmpty(this.itemName)){
                            // cl_date_range: "日付が範囲外です。{0}～{1}の日付を入力してください。",
                            var msg = 'cl_date_range';
                            return clutil.getclmsg(msg, from, to);
                        }else{
                            // cl_its_date_range: "{0}の日付が範囲外です。{1}～{2}の日付を入力してください。",
                            var msg = 'cl_its_date_range';
                            return clutil.getclmsg(msg, this.itemName, from, to);
                        }
                    }
                }else if(min != null){
                    if(val < min){
                        // 開始日以前
                        var from = clutil.date.dateFormat('yyyy/MM/dd', min);
                        if(_.isEmpty(this.itemName)){
                            // cl_date_min: "日付が範囲外です。{0}後の日付を入力してください。",
                            var msg = 'cl_date_min';
                            return clutil.getclmsg(msg, from);
                        }else{
                            // cl_its_date_min: "{0}の日付が範囲外です。{1}後の日付を入力してください。",
                            var msg = 'cl_its_date_min';
                            return clutil.getclmsg(msg, this.itemName, from);
                        }
                    }
                }else if(max != null){
                    if(val > max){
                        // 最終日以後
                        var to = clutil.date.dateFormat('yyyy/MM/dd', max);
                        if(_.isEmpty(this.itemName)){
                            // cl_date_max: "日付が範囲外です。{0}前の日付を入力してください。",
                            var msg = 'cl_date_max';
                            return clutil.getclmsg(msg, from, to);
                        }else{
                            // cl_its_date_max: "{0}の日付が範囲外です。{1}前の日付を入力してください。",
                            var msg = 'cl_its_date_max';
                            return clutil.getclmsg(msg, this.itemName, to);
                        }
                    }
                }

                // OK
                return null;
            }
        },
// [Peding]
//        /* ------------------------------------------------------------
//         * 年月日 yyyyMM        input:text
//         * ------------------------------------------------------------ */
//        ym: {
//            _description: '年月 - テキスト入力',
//            itemName: undefined,
//        },
        /* ------------------------------------------------------------
         * 時分 HHmm            input:text
         * ------------------------------------------------------------ */
        hhmm: {
            _description: '時分 - テキスト入力',
            itemName: undefined,
            controller: clutil.view.ctrl.hhmm,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);
                var rawVal = ctrl.getRawValue($input);

                // 必須チェック
                if(_.isEmpty(rawVal)){
                    if(c.required){
                        var msg = _.isEmpty(this.itemName) ? 'cl_required' : 'cl_its_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // フォーマットチェック
                if(val == null){
                    // cl_hhmm_inval: "時刻形式が誤っています。",
                    // cl_its_hhmm_inval: "{0}の時刻形式が誤っています。",
                    var msg = _.isEmpty(this.itemName) ? 'cl_hhmm_inval' : 'cl_its_hhmm_inval';
                    return clutil.getclmsg(msg, this.itemName);
                }

                function fmtTime(ihhmm){
                    var h = Math.floor(ihhmm / 100), m = ihhmm % 100;
                    return clutil.text.zeroPadding(h,2) + ':' + clutil.text.zeroPadding(m,2);
                }

                // 値範囲チェック
                var min = _.has(c,'minval') ? c.minval : null;
                var max = _.has(c,'maxval') ? c.maxval : null;
                if(min != null && max != null){
                    if(val < min || val > max){
                        // 範囲エラー
                        var from = fmtTime(min), to = fmtTime(max);
                        if(_.isEmpty(this.itemName)){
                            // cl_hhmm_range: "時刻が範囲外です。{0}～{1}の時刻を入力してください。",
                            var msg = 'cl_hhmm_range';
                            return clutil.getclmsg(msg, from, to);
                        }else{
                            // cl_its_hhmm_range: "{0}の時刻が範囲外です。{1}～{2}の時刻を入力してください。",
                            var msg = 'cl_its_hhmm_range';
                            return clutil.getclmsg(msg, this.itemName, from, to);
                        }
                    }
                }else if(min != null){
                    if(val < min){
                        // 開始時刻以前
                        var from = fmtTime(min);
                        if(_.isEmpty(this.itemName)){
                            // cl_hhmm_min: "時刻が範囲外です。{0}後の時刻を入力してください。",
                            var msg = 'cl_hhmm_min';
                            return clutil.getclmsg(msg, from);
                        }else{
                            // cl_its_hhmm_min: "{0}の時刻が範囲外です。{1}後の時刻を入力してください。",
                            var msg = 'cl_its_hhmm_min';
                            return clutil.getclmsg(msg, this.itemName, from);
                        }
                    }
                }else if(max != null){
                    if(val > max){
                        // 終端時刻以降
                        var to = fmtTime(max);
                        if(_.isEmpty(this.itemName)){
                            // cl_hhmm_max: "時刻が範囲外です。{0}前の時刻を入力してください。",
                            var msg = 'cl_hhmm_max';
                            return clutil.getclmsg(msg, from, to);
                        }else{
                            // cl_its_hhmm_max: "{0}の時刻が範囲外です。{1}前の時刻を入力してください。",
                            var msg = 'cl_its_hhmm_max';
                            return clutil.getclmsg(msg, this.itemName, to);
                        }
                    }
                }

                // OK
                return null;
            }
        },
        /* ------------------------------------------------------------
         * セレクト             selectpicker
         * ------------------------------------------------------------ */
        select: {
            _description: 'セレクト - selectpicker',
            itemName: undefined,
            controller: clutil.view.ctrl.select,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);

                // 必須チェック
                {
                    var $option = (val == null) ? null : $input.find('option[value="' + val + '"]');
                    if($option == null || $option.hasClass('cl_nullitem')){
                        if(c.required){
                            // cl_select_required: "選択してください。",
                            // cl_its_select_required: "{0}を選択してください。",
                            var msg = _.isEmpty(this.itemName) ? 'cl_select_required' : 'cl_its_select_required';
                            return clutil.getclmsg(msg, this.itemName);
                        }
                        return null;    // 空入力 OK
                    }
                }

                // 選択個数チェック
                if(_.isArray(val)){
                    var count = val.length;
                    var min = _.has(c, 'mincount') ? c.mincount : null;
                    var max = _.has(c, 'maxcount') ? c.maxcount : null;

                    if(min != null && max != null){
                        if(count < min || count > max){
                            // 範囲エラー
                            if(_.isEmpty(this.itemName)){
                                // cl_select_range: '{0}～{1}個まで選んでください。',
                                var msg = 'cl_select_range';
                                return clutil.getclmsg(msg, min, max);
                            }else{
                                // cl_its_select_range: '{0}は{1}～{2}個まで選んでください。',
                                var msg = 'cl_its_select_range';
                                return clutil.getclmsg(msg, this.itemName, min, max);
                            }
                        }
                    }else if(min != null){
                        if(count < min){
                            // 最小値以下
                            if(_.isEmpty(this.itemName)){
                                // cl_select_less: '{0}個以上選んでください。',
                                var msg = 'cl_select_less';
                                return clutil.getclmsg(msg, min);
                            }else{
                                // cl_its_select_less: '{0}は{1}個以上選んでください。',
                                var msg = 'cl_its_select_less';
                                return clutil.getclmsg(msg, this.itemName, min);
                            }
                        }
                    }else if(max != null){
                        if(count > max){
                            // 最大値超過
                            if(_.isEmpty(this.itemName)){
                                // cl_sleect_more: '{0}個以下選んでください。',
                                var msg = 'cl_sleect_more';
                                return clutil.getclmsg(msg, this.itemName, max);
                            }else{
                                // cl_its_sleect_more: '{0}は{1}個以下選んでください。',
                                var msg = 'cl_its_sleect_more';
                                return clutil.getclmsg(msg, this.itemName, max);
                            }
                        }
                    }
                }

                // OK
                return null;
            }
        },
// [Peding]
//        checkbox: {
//            _description: 'input:checkbox - チェックボックス（単体）',
//        },
        /* ------------------------------------------------------------
         * チェックボックスグループ
         * ------------------------------------------------------------ */
        checkboxgroup: {
            _description: 'セレクト - チェックボックスグループ',
            itemName: undefined,
            controller: clutil.view.ctrl.checkboxgroup,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);

                // 必須チェック
                if(val == null){
                    if(c.required){
                        // cl_select_required: "選択してください。",
                        // cl_its_select_required: "{0}を選択してください。",
                        var msg = _.isEmpty(this.itemName) ? 'cl_select_required' : 'cl_its_select_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // 選択個数チェック
                var count = val.length;
                var min = _.has(c, 'mincount') ? c.mincount : null;
                var max = _.has(c, 'maxcount') ? c.maxcount : null;

                if(min != null && max != null){
                    if(count < min || count > max){
                        // 範囲エラー
                        if(_.isEmpty(this.itemName)){
                            // cl_select_range: '{0}～{1}個まで選んでください。',
                            var msg = 'cl_select_range';
                            return clutil.getclmsg(msg, min, max);
                        }else{
                            // cl_its_select_range: '{0}は{1}～{2}個まで選んでください。',
                            var msg = 'cl_its_select_range';
                            return clutil.getclmsg(msg, this.itemName, min, max);
                        }
                    }
                }else if(min != null){
                    if(count < min){
                        // 最小値以下
                        if(_.isEmpty(this.itemName)){
                            // cl_select_less: '{0}個以上選んでください。',
                            var msg = 'cl_select_less';
                            return clutil.getclmsg(msg, min);
                        }else{
                            // cl_its_select_less: '{0}は{1}個以上選んでください。',
                            var msg = 'cl_its_select_less';
                            return clutil.getclmsg(msg, this.itemName, min);
                        }
                    }
                }else if(max != null){
                    if(count > max){
                        // 最大値超過
                        if(_.isEmpty(this.itemName)){
                            // cl_sleect_more: '{0}個以下選んでください。',
                            var msg = 'cl_sleect_more';
                            return clutil.getclmsg(msg, this.itemName, max);
                        }else{
                            // cl_its_sleect_more: '{0}は{1}個以下選んでください。',
                            var msg = 'cl_its_sleect_more';
                            return clutil.getclmsg(msg, this.itemName, max);
                        }
                    }
                }

                // OK
                return null;
            }
        },
        /* ------------------------------------------------------------
         * ラジオボタングループ
         * ------------------------------------------------------------ */
        radiogroup: {
            _description: 'セレクト - radiogroup',
            itemName: undefined,
            controller: clutil.view.ctrl.radiogroup,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;

                var val = ctrl.getValue($input);

                // 必須チェック
                if(val == null){
                    if(c.required){
                        // cl_select_required: "選択してください。",
                        // cl_its_select_required: "{0}を選択してください。",
                        var msg = _.isEmpty(this.itemName) ? 'cl_select_required' : 'cl_its_select_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // OK
                return null;
            }
        },
        /* ------------------------------------------------------------
         * サジェスト
         * ------------------------------------------------------------ */
        suggest: {
            _description: 'サジェスト - suggest',
            itemName: undefined,
            controller: clutil.view.ctrl.suggest,
            constraint: {},
            // @return エラーメッセージ or null
            isValid: function($input){
                var c = this.constraint;
                var ctrl = this.controller;
                var api = $input.data('cl_MtSuggestView');

                var rawVal = ctrl.getRawValue($input);

                // 必須チェック
                if(_.isEmpty(rawVal)){
                    if(c.required){
                        // 入力してください。 / {0}を入力してください。
                        var msg = _.isEmpty(this.itemName) ? 'cl_required' : 'cl_its_required';
                        return clutil.getclmsg(msg, this.itemName);
                    }
                    return null;    // 空入力 OK
                }

                // 入力値の整合性チェック
                if(api && !api.isValid()){
                    // cl_suggest_inval: '適合する選択肢が見つかりません。選択肢の中から選んでください。',
                    // cl_its_suggest_inval: '{0} は適合する選択肢が見つかりません。選択肢の中から選んでください。'
                    var msg = _.isEmpty(this.itemName) ? 'cl_suggest_inval' : 'cl_its_suggest_inval';
                    return clutil.getclmsg(msg, this.itemName);
                }

                // OK
                return null;
            }
        }

    });

    // ２．拡張基本セット
    _.extend(domain, {
        /**
         * 数値入力 - カンマ区切りフォーマット無し
         */
        number: _.defaults({
            _description: '数値入力',
            controller: clutil.view.ctrl.number
        }, domain.decimal),
        /**
         * テキスト入力 - コード（[0-9]数文字列）
         */
        code: _.defaults({
            _description: 'テキスト入力 - コード',
            formatTest: 'num',  // clutil.text.CommonRegEx.num
            constraint: {
                // 継承側において、コード桁数を指定する
                // minlen: <n>,
                // maxlen: <n>
            }
        }, domain.text),
        /**
         * テキスト入力 - カナ
         */
        kana_k: _.defaults({
            _description: 'テキスト入力 - カナ',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.kana_k;
                if(!regExp.test(text)){
                    // cl_kana_k: '全角カタカナで入力してください。',
                    // cl_its_kana_k: '{0}は全角カタカナで入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_kana_k' : 'cl_its_kana_k';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        },domain.text),
        /**
         * テキスト入力 - 半角ｶﾅ
         */
        kana_hk: _.defaults({
            _description: 'テキスト入力 - 半角ｶﾅ',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.kana_hk;
                if(!regExp.test(text)){
                    // cl_kana_hk: '半角ｶﾀｶﾅで入力してください。',
                    // cl_its_kana_hk: '{0}は半角ｶﾀｶﾅで入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_kana_hk' : 'cl_its_kana_hk';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - かな
         */
        kana_h: _.defaults({
            _description: 'テキスト入力 - かな',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.kana_h;
                if(!regExp.test(text)){
                    // cl_kana_h: 'ひらがなで入力してください。',
                    // cl_its_kana_h: '{0}はひらがなで入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_kana_h' : 'cl_its_kana_h';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        },domain.text),
        /**
         * テキスト入力 - 全角
         */
        zen: _.defaults({
            _description: 'テキスト入力 - 全角',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.zen;
                if(!regExp.test(text)){
                    // cl_zen: '全角文字列で入力してください。',
                    // cl_its_zen: '{0}は全角文字列で入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_zen' : 'cl_its_zen';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 半角（半角ｶﾅ含む）
         */
        han: _.defaults({
            _description: 'テキスト入力 - 半角',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.han;
                if(!regExp.test(text)){
                    // cl_han: '半角文字列で入力してください。',
                    // cl_its_han: '{0}は半角文字列で入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_han' : 'cl_its_han';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        },domain.text),
        /**
         * テキスト入力 - 英数記号
         */
        ascii: _.defaults({
            _description: 'テキスト入力 - 英数記号',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.ascii;
                if(!regExp.test(text)){
                    // cl_ascii: '英数文字列で入力してください。',
                    // cl_its_ascii: '{0}は英数文字列で入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_ascii' : 'cl_its_ascii';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 英数
         */
        alnum: _.defaults({
            _description: 'テキスト入力 - 英数',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.alnum;
                if(!regExp.test(text)){
                    // cl_alnum: '英数文字列で入力してください。',
                    // cl_its_alnum: '{0}は英数文字列で入力してください。',
                    var msg = _.isEmpty(name) ? 'cl_alnum' : 'cl_its_alnum';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 電話番号
         */
        tel: _.defaults({
            _description: 'テキスト入力 - 電話番号',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.tel;
                if(!regExp.test(text)){
                    // cl_tel: '電話番号形式をご確認ください。',
                    // cl_its_tel: '{0}は電話番号形式ではありません。入力をご確認ください。',
                    var msg = _.isEmpty(name) ? 'cl_tel' : 'cl_its_tel';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 電話番号（ハイフン区切り必須）
         */
        tel_hyphen: _.defaults({
            _description: 'テキスト入力 - 電話番号（ハイフン区切り必須）',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.tel_hyphen;
                if(!regExp.test(text)){
                    // cl_tel_hyphen: '電話番号形式をご確認ください。ハイフン区切りを付けて入力してください。',
                    // cl_its_tel_hyphen: '{0}は電話番号形式ではありません。ハイフン区切りは不要です。',
                    var msg = _.isEmpty(name) ? 'cl_tel_hyphen' : 'cl_its_tel_hyphen';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 電話番号(ハイフン区切り無し)
         */
        tel_nohyphen: _.defaults({
            _description: 'テキスト入力 - 電話番号（ハイフン区切り無し）',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.tel_nohyphen;
                if(!regExp.test(text)){
                    // cl_tel_nohyphen: '電話番号形式をご確認ください。ハイフン区切りは不要です。',
                    // cl_its_tel_nohyphen: '{0}は電話番号形式ではありません。ハイフン区切りは不要です。',
                    var msg = _.isEmpty(name) ? 'cl_tel_nohyphen' : 'cl_its_tel_nohyphen';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 郵便番号
         */
        postal: _.defaults({
            _description: 'テキスト入力 - 郵便番号',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.postal;
                if(!regExp.test(text)){
                    // cl_postal: '郵便番号形式をご確認ください。',
                    // cl_its_postal: '{0}は郵便番号形式ではありません。入力をご確認ください。',
                    var msg = _.isEmpty(name) ? 'cl_postal' : 'cl_its_postal';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 郵便番号（ハイフン区切り必須）
         */
        postal_hyphen: _.defaults({
            _description: 'テキスト入力 - 郵便番号（ハイフン区切り必須）',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.postal_hyphen;
                if(!regExp.test(text)){
                    // cl_postal_hyphen: '郵便番号形式をご確認ください。',
                    // cl_its_postal_hyphen: '{0}は郵便番号形式ではありません。入力をご確認ください。',
                    var msg = _.isEmpty(name) ? 'cl_postal_hyphen' : 'cl_its_postal_hyphen';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - 郵便番号（ハイフン区切り無し）
         */
        postal_nohyphen: _.defaults({
            _description: 'テキスト入力 - 郵便番号（ハイフン区切り無し）',
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.postal_nohyphen;
                if(!regExp.test(text)){
                    // cl_postal_nohyphen: '郵便番号形式をご確認ください。',
                    // cl_its_postal_nohyphen: '{0}は郵便番号形式ではありません。入力をご確認ください。',
                    var msg = _.isEmpty(name) ? 'cl_postal_nohyphen' : 'cl_its_postal_nohyphen';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - URL
         */
        url: _.defaults({
            _description: 'テキスト入力 - ＵＲＬ',
            constraint: {
                maxlen: 2083
            },
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.url;
                if(!regExp.test(text)){
                    // cl_url: 'URL形式をご確認ください。',
                    // cl_its_url: '{0}はURL形式ではありません。入力をご確認ください。',
                    var msg = _.isEmpty(name) ? 'cl_url' : 'cl_its_url';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - メールアドレス
         */
        mail: _.defaults({
            _description: 'テキスト入力 - メールアドレス',
            constraint: {
                maxlen: 256
            },
            formatTest: function(text, name){
                var regExp = clutil.text.CommonRegEx.mail;
                if(!regExp.test(text)){
                    // cl_email: "メールアドレス形式をご確認ください。",
                    // cl_its_email: "{0}はメールアドレス形式ではありません。入力をご確認ください。",
                    var msg = _.isEmpty(name) ? 'cl_email' : 'cl_its_email';
                    return clutil.getclmsg(msg, this.itemName);
                }
            }
        }, domain.text),
        /**
         * テキスト入力 - ファイル名
         */
        filename: _.defaults({
            _description: 'テキスト入力 - ファイル名',
            constraint: {
                maxlen: 80  // パス全体で 260 文字まで。basename ファイル名長のデフォルトは 80 文字に設定しておく。
            },
            /** ファイル名予約名集 */
            reservedNames: {
                CON: {
                    regExp: /^CON$/i,
                    msgArg: 'CON'
                },
                PRN: {
                    regExp: /^PRN$/i,
                    msgArg: 'PRN'
                },
                AUX: {
                    regExp: /^AUX$/i,
                    msgArg: 'AUX'
                },
                NUL: {
                    regExp: /^NUL$/i,
                    msgArg: 'NUL'
                },
                COM: {
                    regExp: /^COM[0-9]$/i,
                    msgArg: 'COM[0-9]'
                },
                LPT: {
                    regExp: /~LPT[0-9]$/i,
                    msgArg: 'LPT[0-9]'
                }
            },
            formatTest: function(text, name){
                // 1) 使用不可な記号マッチングチェック
                var regExp = clutil.text.CommonRegEx.filename_invalchars;
                if(regExp.test(text)/* Match したら NG */){
                    // cl_invalid_fnamechar     : '記号「<>:*?"/|」はファイル名に使用できません。',
                    // cl_its_invalid_fnamechar : '{0}: 記号「<>:*?"/|」はファイル名に使用できません。',
                    var msg = _.isEmpty(name) ? 'cl_invalid_fnamechar' : 'cl_its_invalid_fnamechar';
                    return clutil.getclmsg(msg, this.itemName);
                }

                // 2) 予約語チェック
                // CON、PRN、AUX、NUL、COM0～9、LPT0～9
                // reserved_filename: /^(CON|PRN|AUX|NUL|COM[0-9]|LPT[0-9])/i,
                for(var k in this.reservedNames){
                    var v = this.reservedNames[k];

                    // ファイル拡張子部を除く。
                    var fname = text;
                    var extIdx = fname.lastIndexOf('.');
                    if(extIdx >= 0){
                        fname = fname.slice(0, extIdx);
                    }

                    // 予約名評価
                    if(v.regExp.test(fname)/* Match したら NG */){
                        // cl_reserved_fname     : '「{0}」はファイル名に使用できません。',
                        // cl_its_reserved_fname : '{0}：「{1}」はファイル名に使用できません。'
                        var msg;
                        if(_.isEmpty(name)){
                            msg = clutil.getclmsg('cl_reserved_fname', v.msgArg);
                        }else{
                            msg = clutil.getclmsg('cl_its_reserved_fname', name, v.msgArg);
                        }
                        return msg;
                    }
                }
            }
        }, domain.text)
    });

});
/**
 * バリデータ（ab-ui準拠）
 */
var clutil = _.extend({}, clutil);
$(function(){
    /*
     * 指定要素に対する <div class="form-group"> ラッパ要素を検索する。
     * 指定要素より、親方向へ jQuery セレクタ検索する。
     */
    function getFormGroupByInput($input){
        return $input.hasClass('form-group') ? $input : $input.closest('.form-group');
    }
    /*
     * 指定エリア内の <div class="form-group"> ラッパ要素を一覧する。
     * 指定要素より、子方向へ jQuery セレクタ検索する。
     */
    function findFormGroupsByForm($form){
        return $form.hasClass('form-group') ? $form : $form.find('.form-group');
    }
    /*
     * 指定要素に対する <td class="cell-control"> 要素を検索する。
     * 指定要素より、親方向へ jQuery セレクタ検索する。
     */
    function getCellControllByInput($input){
        // cell-control
        return $input.hasClass('.cell-control') ? $input : $input.closest('.cell-control');
    }
    /*
     * 指定エリア内の <anytag class="cell-control"> 要素一覧する。
     * 指定要素より、子方向へ jQuery セレクタ検索する。
     */
    function findCellControllByForm($form){
        return $form.hasClass('cell-control') ? $form : $form.find('.cell-control');
    }

    /**
     * @param elem チェック対象要素
     * @param emsgfunc エラーメッセージを注入する方法: 通常型（setError） or コレクション型（setErrorCell）
     * @returns エラー検出件数、0 ⇒ OK
     */
    function isValidInner(elem, emsgfunc){
        var $elem = (elem instanceof $) ? elem : $(elem);

        // 検査対象 elem にクラス "cl_valid_ignore" が付いている場合はチェック処理をスキップする。
        if($elem.hasClass('cl_valid_ignore')){
            return 0;
        }

        // ドメイン定義情報をとる。
        var domain = clutil.getDomain($elem);
        if(domain == null){
            console.warn('DOMAIN not found!!!! -- ' + $elem.data('cl_domain'));
            return 0;
        }

        var emsg = domain.isValid($elem);
        if(!_.isEmpty(emsg)){
            // メッセージを設定する
            emsgfunc($elem, emsg);
            return 1;   // エラー検出数 = 1
        }

        return 0;   // OK
    }

    // -------------------------------------------
    // clutil へインポート
    _.extend(clutil, {
        validator: {
            /**
             * 指定要素(input等)に対する form-block ラッパに対して、エラー状態 class 属性と注釈エラーメッセージを付加する。
             * @param $input	エラー状態付けする input 要素
             * @param msg		[省略可] 注釈に付けるエラーメッセージ
             * @param msgArgs	[省略可] エラーメッセージのメッセージ引数
             *
             * [想定DOM構成]
             * <div class="form-group ..." ...>
             *   <input type="text" />
             * </div>
             */
            // setError: function($input, msg, msgArgs)
            setError: function($input, msg, msgArgs){
                var $fm = getFormGroupByInput($input);

                // 既存注釈部(p.help-block)マーカークラス付け
                // cl-errmsg:付加, cl-errhidden:クラスを付加してhide()
                var $ann = $fm.find('.help-block');
                $ann.each(function(idx, elem){
                    var $elem = $(elem);
                    if($elem.hasClass('cl-errmsg')){
                        // エラー注釈が存在する ⇒ 削除
                        $elem.remove();
                    }else{
                        // 隠し注釈化する。これはエラークリア時に通常注釈として復活させる。
                        $elem.addClass('cl-errhidden').hide();
                    }
                });
                // 新たに作る注釈
                if(msg){
                    var ann = '<p class="help-block cl-errmsg">' + clutil.getclmsg(msg, msgArgs) + '</p>';
                    $fm.append(ann);
                }
                $fm.addClass('has-error');
            },
            /**
             * 指定要素(input等)のエラー状態 class 属性と注釈エラーメッセージを取り除く。
             * @param $input エラー状態付けされた input 要素
             */
            removeError: function($input){
                var $fm = getFormGroupByInput($input);
                // エラー注釈を削除
                $fm.find('.help-block.cl-errmsg').remove();
                // 通常注釈の復帰
                var $hiddenAnn = $fm.find('.help-block.cl-errhidden');
                if($hiddenAnn.length > 0){
                    $hiddenAnn.fadeIn('fast');
                }
                // エラー表示クラス
                $fm.removeClass('has-error');
            },
            /**
             * テーブル内セルを想定した、指定要素(input等)に対して、エラー状態 class 属性とエラーメッセージ（ツールチップ）を付加する。
             * @param $input    エラー状態付けする input 要素
             * @param msg       [省略可] 注釈に付けるエラーメッセージ
             * @param msgArgs   [省略可] エラーメッセージのメッセージ引数
             *
             * [想定DOM構成]
             * <td class="... cell-control" ... >
             *   <input type="text" ... />
             * </td>
             */
            setErrorCell: function($input, msg, msgArgs){
                var $cell = getCellControllByInput($input);

                // ツールチップを付ける
                if(msg){
                    if($input.is('select')){
                        // selectpicker - dropdown ボタンにエラー付けする。
                        var api = $input.data('selectpicker');
                        if(api != null){
                            $input = api.$button;
                        }
                    }
                    $input.attr('data-cl-errmsg', clutil.getclmsg(msg, msgArgs));
                }
                $cell.addClass('has-error');
            },
            /**
             * テーブル内セルを想定した、指定要素(input等)のエラー状態 class 属性と注釈エラーメッセージを取り除く。
             * @param $input エラー状態付けされた input 要素
             */
            removeErrorCell: function($input){
                var $cell = getCellControllByInput($input);
                // エラーツールチップを削除
                if($input.is('select')){
                    // selectpicker - dropdown ボタンにエラー付けされている。
                    var api = $input.data('selectpicker');
                    if(api){
                        $input = api.$button;
                    }
                }
                $input.removeAttr('data-cl-errmsg');
                // エラー表示クラス
                $cell.removeClass('has-error');
            },
            /**
             * 共通応答ヘッダのエラーメッセージを一斉に設定する。
             * @param $form エラー設定する form 領域
             * @param rspData 応答データ、共通応答ヘッダ、またはフィールドメッセージ配列
             * @param prefix [省略可]対象入力要素 id のプレフィックス。デフォルトは 'ca_'
             */
            setFieldMessages: function($form, rspData, prefix){
                var fieldMessages;
                if(_.isArray(rspData)){
                    fieldMessages = rspData;
                }else{
                    var hd = (_.has(rspData, 'status') && _.has(rspData, 'fieldMessages')) ? rspData : clutil.getRspHead(rspData);
                    if(hd == null || hd.status == 0){
                        return;
                    }
                    fieldMessages = hd.fieldMessages;
                }
                if(_.isEmpty(fieldMessages)){
                    return;
                }

                function dotTail(str){
                    var idx = str.lastIndexOf('.');
                    return (idx < 0) ? str : str.slice(idx+1);
                }

                prefix = prefix || 'ca_';

                // エラーメッセージを仕分ける
                // lineno === 0 のものは struct_name 付きでも通常フィールドと見做す。
                // lineno > 0 かつ、struct_name 付きのものはコレクション構造体と見做す。
                // lineno > 0 かつ、struct_name が無いものは規約違反として、無視する。

                // 通常のフィールドへのエラーメッセージ適用
                // ------------------------------------------------------------
                // [DOM構成]
                //
                // ◆原則
                // ・チェック対象 input は、<div class="form-group"> でラップされていること（AB-UI準拠）
                //
                // 例）
                // <div class="form-group required">
                //   <label for="ca_family_name_e">フィールド名</label>
                //   <input type="text" class="form-control" placeholder="field name" id="ca_field_name" data-cl_domain="text:required,maxlen=32">
                // </div>
                // ------------------------------------------------------------
                var structFldMsgs = [];
                for(var i=0; i<fieldMessages.length; i++){
                    var fmsg = fieldMessages[i];

                    // 通常フィールド
                    if(fmsg.lineno === 0 || fmsg.lineno == null){
                        var $elem = $form.find('#' + prefix + fmsg.field_name);
                        this.setError($elem, fmsg.message, fmsg.args);
                        continue;
                    }

                    // コレクション構造体
                    if(!_.isEmpty(fmsg.struct_name) && fmsg.lineno > 0){
                        structFldMsgs.push(fmsg);   // 後で仕分けながらエラーメッセージ適用する。
                        continue;
                    }

                    // 規約違反
                    console.warn('fieldMessages[' + i + '] is BAD, ignore!!! -- ' + JSON.stringify(fmsg));
                }

                // コレクション構造体へのエラーメッセージ適用
                // ------------------------------------------------------------
                // [DOM構成] 必ずしも<table>タグとは限らないので．．．
                //
                // ◆原則
                //  ・コレクションＵＩのコンテナ要素（<table>タグなど）に struct_name に因んだ data-struct_name 属性を付与する。(例: data-struct_name="struct_name"）
                //  ・内部の行要素に class="cl_rowform" を付与する。
                //  ・評価対象となる input に対して、field_name に因んだ data-field_name 属性を付与する。（例 <input data-cl_field_name="field_name" />）
                //
                // 例１）
                // <table data-struct_name="struct_name">
                //   <thead> ... カラムヘッダ ... </thead>
                //   <tbody>
                //     <tr class="cl_rowform">
                //       <td class="cell-control">
                //          <input type="text" data-field_name="field_name" data-cl_domain="text:required" />
                //       </td>
                //       ...
                //     </tr>
                //     <tr class="cl_rowform">...</tr>
                //     ...
                //   </tbody>
                // </table>
                //
                // 例２）
                // <ul data-struct_name="struct_name">
                //   <li class="cl_rowform">
                //     <input data-field_name="field_name" />
                //   </li>
                //   <li class="cl_rowform">...</li>
                //   ...
                // </ul>
                // ------------------------------------------------------------
                var fmsgby = _.groupBy(structFldMsgs, 'struct_name');
                for(var structName in fmsgby){
                    var fmsgs = fmsgby[structName];

                    // コレクション - コンテナ要素を特定
                    var structName = dotTail(structName);
                    var $conts = $form.find('[data-struct_name="' + structName + '"]');
                    if($conts.length === 0){
                        // エラーメッセージ適用対象が見つからない＃１
                        console.warn('struct_name[' + structName + ']: container([data-struct_name="' + structName + '"]) not found. #1');
                        continue;
                    }

                    // 行番別に仕分け
                    var fmsgsLnMap = _.groupBy(fmsgs, 'lineno'), that = this;
                    $conts.each(function(index, cont){
                        // コレクション - 行要素を収集
                        var $rows = $(cont).find('.cl_rowform');
                        if($rows.length === 0){
                            // エラーメッセージ適用対象が見つからない＃２
                            console.warn('struct_name[' + structName + ']: at $container[' + index + '], $row(.cl_rowform)  not found. #2');
                            return;
                        }
                        for(var lineno in fmsgsLnMap){
                            var $row = $rows.eq(lineno -1);     // lineno は 1 オリジンにつき、-1 する。
                            if($row.length === 0){
                                // エラーメッセージ適用対象が見つからない＃３
                                console.warn('struct_name[' + structName + ']: $row of lineno[' + lineno + '] not found. #3');
                                continue;
                            }
                            // メッセージを入れる
                            // setErrorCell: function($input, msg, msgArgs)
                            var fmsgsLn = fmsgsLnMap[lineno];
                            for(var i=0; i<fmsgsLn.length; i++){
                                var fmsg = fmsgsLn[i];
                                var $cells = $row.find('[data-field_name="' + fmsg.field_name + '"]');
                                if($cells.length === 0){
                                    // エラーメッセージ適用対象が見つからない＃４
                                    console.warn('struct_name[' + structName + '] lineno[' + lineno + ']: field_name[' + fmsg.field_name + '] not found. #4');
                                    continue;
                                }
                                $cells.each(function(celIndex, cell){
                                    var $input = $(cell);
                                    that.setErrorCell($input, fmsg.message, fmsg.args);
                                });
                            }
                        }
                    });
                }
            },
            /**
             * 指定入力要素に対して、エラー状態がセットされているかどうか判定。
             * エラーチェックは行わない。
             * @param $form エラー状態検査対象要素
             * @return true:エラー状態がセットされている。false:正常（ただし、エラーチェック結果ではない）
             */
            hasError: function($input){
                var $fm = null;
                do{
                    $fm = getFormGroupByInput($input);
                    if($fm.length > 0){
                        break;
                    }
                    $fm = getCellControllByInput($input);
                }while(false);
                return $fm.hasClass('has-error');
            },

            /**
             * 指定フォーム領域下の要素に対して、ドメイン定義を設定している要素に対してエラーチェックを行う。
             * エラーを検出した場合は、ＵＩ効果を施す（赤ボーダー、ヒントメッセージの付加など）。
             * 入力要素間の関連性チェック（大小反転など）は行わないので、別途App層で行うこと。
             * @param $form 検査対象ラッパ要素
             * @return エラー検出件数
             */
            isValid: function($form){
                this.clearErrors($form);

                var invalCount = 0, emsgFunc;
                if($form.is('[data-cl_domain]')){
                    // 指定要素 $form 自身がチェック対象の場合
                    var $rowForm = $form.closest('.cl_rowform');
                    emsgFunc = ($rowForm.length === 0) ? this.setError : this.setErrorCell;
                    invalCount += isValidInner($form, emsgFunc);
                }else if($form.is('.form-group')){
                    // 指定要素 $form 自身がチェック対象を束ねる form-group の場合 ...非コレクションと見做す
                    emsgFunc = this.setError;
                    var $dommys = $form.find('[data-cl_domain]');
                    $dommys.each(function(idx, elem){
                        invalCount += isValidInner(elem, emsgFunc);
                    });
                }else{
                    // 指定要素 $form がコンテナ要素の場合
                    var that = this;

                    emsgFunc = this.setErrorCell;   // コレクションUIの部からチェック処理する。
                    if($form.is('.cl_rowform')){
                        // -----------------------
                        // コレクションUIの部（１）
                        //   $form自身がコレクション中の行要素の場合
                        var $rowDommys = $form('[data-cl_domain]');
                        $rowDommys.each(function(idx, elem){
                            invalCount += isValidInner(elem, emsgFunc);
                        });
                    }else{
                        // -----------------------
                        // コレクションUIの部（２）
                        var $rowDommys = $form.find('.cl_rowform [data-cl_domain]');
                        var checkedElems = [];
                        $rowDommys.each(function(idx, elem){
                            invalCount += isValidInner(elem, emsgFunc);
                            checkedElems.push(elem);
                        });

                        // -----------------------
                        // 単体フィールドの部
                        emsgFunc = this.setError;
                        var $dommys = $form.find('[data-cl_domain]');
                        $dommys.each(function(idx, elem){
                            if(checkedElems.length > 0){
                                var chkIdx = checkedElems.indexOf(elem);
                                if(chkIdx >= 0){
                                    checkedElems.splice(chkIdx,1);
                                    return;
                                }
                            }
                            invalCount += isValidInner(elem, emsgFunc);
                        });
                    }
                }

                return invalCount;
            },
            /**
             * 指定フォーム領域下の要素に対して、エラー表示を解除する。
             * @param $form 対象領域
             */
            clearErrors: function($form){
                // $form 自身がドメイン要素である場合 → 個別にエラー解除関数を呼び出す。
                if($form.is('[data-cl_domain]')){
                    var $rows = $form.closest('.cl_rowform');  // コレクションUIチェック
                    var removeErrorFunc = ($rows.length > 0) ? this.removeErrorCell : this.removeError;
                    removeErrorFunc($form);
                    return;
                }

                // 脚注部(p.help-block)のマーカークラス
                // cl-errmsg:削除, cl-errhidden:クラスを落として表示
                var $fm = findFormGroupsByForm($form);
                $fm.each(function(idx, elem){
                    var $f = $(elem);
                    if($f.hasClass('has-error')){
                        // エラーメッセージを削除
                        $f.find('p.help-block.cl-errmsg').remove();
                        // 退避している脚注を表示
                        $f.find('p.help-block').removeClass('cl-errhidden').fadeIn('fast');
                    }
                }).removeClass('has-error');

                // セル内エラーツールチップを解除する
                var $cells = findCellControllByForm($form);
                $cells.each(function(idx, elem){
                    var $cell = $(elem);
                    $cell.removeClass('has-error').find('[data-cl-errmsg]').removeAttr('data-cl-errmsg');
                });
            },
            /**
             * 指定フォーム領域下の要素に対して、エラー表示がセットされた入力要素の個数を数える。
             * エラーチェックは行わない。
             * @param $form 検査対象
             * @return エラー表示の件数
             */
            countErrors: function($form){
                var errCount = 0;
                if($form.hasClass('has-error')){
                    errCount++;
                }else{
                    errCount = $form.find('.has-error').length;
                }
                return errCount;
            },
            /**
             * エラー検出位置へフォーカスを設定する。
             */
            setPrefferredFocus: function($form){
                $form = $form || $('body');
                var $fm = $form.find('.has-error');
                if($fm.length > 0){
                    var $fm0 = $fm.eq(0);

                    // フォーカスを入れる input
                    var $input = $fm0.find(':focusable').eq(0);

                    // スクロールトップの要素
                    var $scrollTop = $fm0.closest('.row');
                    if($scrollTop.length === 0){
                        $scrollTop = $fm0;
                    }
                    // スクロール
                    clutil.view.scrollTop({ target: $scrollTop }, function(){
                        // フォーカス
                        clutil.setFocus($input);
                    });
                }

            }
        }
    });
});
/**
 * 共通 View クラスなど
 */
var clutil = _.extend({}, clutil);
$(function () {
    if(!clutil.view){
        clutil.view = {};
    }

    // clutil.view.appStart() の処理関数
    function buildAuthFailedDefer(){
        var d = $.Deferred();

        var rsp = {};
        rsp[clcom.config.cmHeadPropName.rsp] = {
            status: "error",    // 0 でないのを入れる
            message: 'cl_http_status_unauthorized'
        };
        d.reject(rsp);
        return d.promise();
    }
    /*
     * 初期データ取得用 GET メソッド
     * @see clcom.getIniExtra
     */
    function getIniJSON(){
        // Cookie: AuthToken 有無チェック
        if (!clcom.hasAuthCookies()) {
            // ログインしていない場合
            var msg = [ 'ログアウトされました。' ];
            var url = clutil.createErrOccurredUrl(msg, '/');
            clcom.logout({url: url });

            return buildAuthFailedDefer();
        }
        // ユーザ情報を clom.userInfo プロパティへキャッシュ。
        clcom.userInfo = clcom.getUserData();

        // 呼び出し元画面チェック
        if (clcom.config.enableCheckSourceId === true && _.isEmpty(clcom.srcId)){
            var msg = [ 'ログアウトされました。' ];
            var url = clutil.createErrOccurredUrl(msg, '/');
            clcom.logout({url: url });

            return buildAuthFailedDefer();
        }

        // 初期パラメータの取得を仕掛ける
        var dd = [];
        var extras = clcom.getIniExtra || {};
        _.each(extras, function(extra, k, src){
            if(extra.once && _.every(extra.storedkeys, function(k){ return clcom.hasStorageKey(k); })){
                // １発もので、全てのキーがストレージに入っている
                console.log('getIniJSON: [' + JSON.stringify(_.pick(extra, 'description', 'resId')) + '] skip.');
                if(_.isFunction(extra.cacheable)){
                    extra.cacheable(null, extra);
                }
                return;
            }
            var req = _.isFunction(extra.buildReq) ? extra.buildReq() : {};
            dd.push(clutil.postJSON(extra.resId, req).then(function(data){
                var rspHead = clutil.getRspHead(data);
                // ログを出すだけ。
                if(rspHead && rspHead.status === 0){
                    // success
                    console.log('getIniJSON: ' + JSON.stringify(_.pick(extra, 'description', 'resId')) + ' success.');
                }else{
                    // fail
                    console.log('getIniJSON: ' + JSON.stringify(_.pick(extra, 'description', 'resId')) + ' failed.');
                }
                return this;
            }).done(_.bind(function(data){
                if(_.isFunction(this.success)){
                    this.success(data,this);
                }
                if(_.isFunction(extra.cacheable)){
                    extra.cacheable(data,this);
                }
            }, extra)));
            console.log('getIniJSON: [' + JSON.stringify(_.pick(extra, 'description', 'resId')) + '] enqueued.');
        });

        var defer = $.when.apply($, dd);
        return defer.promise().then(function(){
            // --------------------
            // clcom.preInit
            var failCount = 0;

            do{
                var getIniEntryCount = dd.length;
                var rspArray;
                if(getIniEntryCount <= 0){
                    break;
                }else if(getIniEntryCount === 1){
                    rspArray = [ arguments ];
                }else{
                    rspArray = arguments;
                }

                for(var i=0; i<rspArray.length; i++){
                    var rsp = rspArray[i];
                    var data = rsp[0];
                    var statText = rsp[1];
                    var rspHead = clutil.getRspHead(data);
                    if(!_.isArray(rsp) && data == null){
                        // ダミー API 応答対応
                        data = rsp;
                        rspHead = clutil.getRspHead(data);
                        if(rspHead._isDummyAPI){
                            statText = statText || 'success';
                        }
                    }
                    if(statText != 'success'){
                        failCount += 1;
                        continue;
                    }
                    if(rspHead && rspHead.status !== 0){
                        failCount += 1;
                        continue;
                    }
                }
                console.info('getIniJSON: #of task=' + rspArray.length + ', failCount=' + failCount);
            }while(false);

            if(failCount === 0 && _.isFunction(clcom.preInit)){
                clcom.preInit();
            }
            return this;
        });
    }
    /*
     * 初期データ取得用 GET メソッド２
     * @see clcom.getIniOption
     */
    function getIniJSON2(iniOptions){
        if(iniOptions == null){
            throw 'getIniJSON2: invalid arguments';
        }
        // Cookie: AuthToken 有無チェック
        if (!clcom.hasAuthCookies()) {
            // ログインしていない場合
            var msg = [ 'ログアウトされました。' ];
            var url = clutil.createErrOccurredUrl(msg, '/');
            clcom.logout({url: url });

            return buildAuthFailedDefer();
        }
        // ユーザ情報を clom.userInfo プロパティへキャッシュ。
        clcom.userInfo = clcom.getUserData();

        // 呼び出し元画面チェック
        if (clcom.config.enableCheckSourceId === true && _.isEmpty(clcom.srcId)){
            var msg = [ 'ログアウトされました。' ];
            var url = clutil.createErrOccurredUrl(msg, '/');
            clcom.logout({url: url });

            return buildAuthFailedDefer();
        }

        // clcom.getIniOption エントリに登録されたタスクかどうかチェック
        var tasks = _.reduce(iniOptions, function(tasks, key){
            var task = clcom.getIniOption[key];
            if(task == null){
                throw 'getIniJSON2: Invalid task name [' + key + '], task instance not found.';
            }
            if(task.status == null){
                task.status = 'select';
                tasks.push(task);
            }else{
                console.warn('getIniJSON2: duplicated task name [' + key + '].');
            }
            return tasks;
        }, []);
        if(tasks.length === 0){
            throw 'getIniJSON2: empty tasks.';
        }

        // API 呼出処理
        var dd = [];
        for(var i=0; i<tasks.length; i++){
            var task = tasks[i];
            var req = _.isFunction(task.buildReq) ? task.buildReq() : {};
            var d = clutil.postJSON(task.resId, req).then(function(data){
                // ログを出すだけ。
                var rspHead = clutil.getRspHead(data);
                if(rspHead && rspHead.status === 0){
                    // success
                    console.log('getIniJSON2: ' + JSON.stringify(_.pick(task, 'description', 'resId')) + ' success.');
                }else{
                    // fail
                    console.log('getIniJSON2: ' + JSON.stringify(_.pick(task, 'description', 'resId')) + ' failed.');
                }
                return this;
            }).done(_.bind(function(data){
                if(_.isFunction(this.success)){
                    this.success(data,this);
                }
            },task));
            dd.push(d);
            console.log('getIniJSON2: [' + JSON.stringify(_.pick(task, 'description', 'resId')) + '] enqueued.');
        }

        return $.when.apply($,dd).promise();
    }

    /*
     * Backbone.View 拡張 View 規定クラス定義
     * ・子 View の events を継承できるようにする。
     * ・親クラス prototype を su プロパティに持つようにする。[super] は予約語につき [su] とした。
     */
    var AbstractView = Backbone.View.extend();
    AbstractView.extend = function(child){
        var v = Backbone.View.extend.apply(this, arguments);
        v.prototype.events = _.extend({}, this.prototype.events, child.events);
        v.prototype.su = this.prototype;
        return v;
    };

    /*
     * エコーバック
     */
    var AlertView = AbstractView.extend({
        className: 'alert al1 displaynone',
        attribute: {
            role: 'alert'
        },
//        model: {
//            level: 'danger',      // [省略可] dismissable, default, primary, success, info, warning, danger、省略時は danger(赤）
//            duration: 3000,       // [省略可] 表示期間、デフォルト3000[msec]、0以下を明示指定すると無期限。
//            priority: 'high',     // [省略可] 優先順位付け。デフォルト: 'normal'、clutil.view.showAlert() によって 'normal' 以外が指定されているアラートメッセージが存在している場合は後発アラートメッセージを表示しない。
//            blockElem: $elem,     // [省略可] アラート表示中、$elem への入力をブロックするフィルム層を被せる。
//            message: 'メッセージ' // エラーメッセージ（文字列）、または、共通応答ヘッダを含む応答データ、共通応答ヘッダ、エラーコード(messages_ja.js 参照)
//        },
        initialize: function(){
            if(this.model == null){
                this.model = {};
            }
            _.defaults(this.model, {
                level: 'danger',	//'default'
                duration: 3000		// 表示期間 3[sec]
            });
        },
        render: function(){
            var lvClassName = 'alert-' + this.model.level;
            var msg = clutil.getclmsg(this.model.message);
            // メモ: スタイル補正のため、'cl_AlertView' クラス属性を追加。
            this.$el.addClass(lvClassName + ' cl_AlertView').text(msg).data('cl_AlertView', this);
            return this;
        },

        /**
         * 表示優先度を取得する。
         * @return 'normal' - 優先度指定なし、other - 優先度指定あり。
         */
        getPriority: function(){
            return this.model.priority || 'normal';
        },

        /**
         * アラートメッセージを表示する。
         */
        show: function(onClosedCallback){
            if(this.status == 'shown' || this.status == 'destroyed'){
                return;
            }

            // blockElem
            if(this.model.blockElem){
                clutil.blockElem(this.model.blockElem);
            }

            this.status = 'shown';

            var $body = $('body');
            $body.append(this.$el);

            this.$el.stop().show().animate({
                bottom: '0'
            }, 300, 'swing');

            // デフォルトで3秒後にフェードアウト
            var duration = this.model.duration;
            if(duration > 0){
                var myView = this;
                setTimeout(function() {
                    myView.$el.fadeOut('fast', function(ev){
                        myView.close(onClosedCallback);
                    });
                }, duration);
            }
        },

        /**
         * 表示されているアラートメッセージを閉じる
         */
        close: function(onClosed){
            if(this.status != 'shown'){
                return;
            }
            var myView = this;
            myView.$el.fadeOut('fast', function(ev){
                // fadeOut 後に破棄
                myView.destroy();
                // unblockElem
                if(myView.model.blockElem){
                    clutil.unblockElem(myView.model.blockElem);
                }
                if(_.isFunction(onClosed)){
                    onClosed();
                }
            });
        },

        /**
         * 破棄する
         */
        destroy: function(){
            this.status = 'destroyed';
            // unblockElem（念のため）
            if(this.model.blockElem){
                clutil.unblockElem(this.model.blockElem);
            }
            this.remove();
        }
    });

    /**
     * 致命的エラーなメッセージ表示
     * メッセージ表示後、トップページへ移動する。
     */
    var AbortView = AbstractView.extend({
        el: 'body',
        templates: {
            wrap: ''
                + '<div class="wrapper-page" style="height: 100%;">'
                +   '<div class="wrapper with-navbar-fixed-top-bslg" style="height: 100%;">'
                +      '<div class="container-fluid" style="height: 100%;">'
                +         '<div id="ca_main">'
                +            '<div id="cl_message_canvas"></div>'
                +         '</div>'
                +    '</div>'
                +  '</div>'
                + '</div>',
            messages: _.template(''
                + '<p>'
                +   '<%= _.map(messages, function(msg){ return _.escape(clutil.getclmsg(msg)); }).join("<br>") %>'
                + '</p>')
        },
        style: {
            '#cl_message_canvas': {
                'text-align': 'center',
                'font-size': 14,
                'color': '#b5c1c6;',
                'padding': 32
            }
        },

//        clutil.view.abort({
//            el: 'body',
//            messages: [ anyMsgs, ... ],	// エラーメッセージ内容(rsp.head, array<string>, string)。content 指定時は無効。
//            content: '<p>あああ</p>',	// エラーメッセージ内容(html)。
//            action: {
//                delayMsec: 5000,
//                command: 'logout'		// 'popPage', 'popPageTo', 'logout', 'home' -- デフォルトは 'home'
//                    popPageTo: 'GSANV9999'	// commaond == popPageTo 指定時の戻り先の画面コード
//            }
//        });

        initialize: function(opt){
            var o = opt || {};

            // モデルデータ整理
            this.model = {};
            if(o.content){
                this.model.content = o.content;
            }else if(o.messages){
                this.model.messages = _.isArray(o.messages) ? o.messages : [ o.messages ];
            }

            // オプション整理
            this.options = {};
            if(o.action == null){
                this.options.action = {};
            }else{
                this.options.action = o.action;
            }
            _.defaults(this.options.action, {
                delayMsec: 5000,
                command: 'home'
            });
        },
        render: function(){
            this.$el.html(this.templates.wrap);
            var $canvas = this.$('#cl_message_canvas');

            var dto = this.model;
            if(dto.content != null){
                $canvas.html(dto.content);
            }else if(dto.messages != null){
                $canvas.html(this.templates.messages(dto));
            }

            for(var k in this.style){
                var style = this.style[k];
                this.$(k).css(style);
            }

            return this;
        },
        startTimer: function(){
            var timeout = this.options.action.delayMsec;
            var myView = this;
            if(timeout > 0){
                setTimeout(_.bind(function(){
                    this.doAction();
                },this), timeout);
            }
            return this;
        },
        doAction: function(){
            // 'popPage', 'popPageTo', 'logout', 'home' -- デフォルトは 'home'
            if(clcom.config.disableAbortAction === true){
                var msg = 'clcom.config.disableAbortAction[TRUE]: command=' + JSON.stringify(this.options.action.command);
                console.info(msg);
                if(confirm(msg) === false){
                    return;
                }
            }
            switch(this.options.action.command){
            case 'logout':
                clcom.logout();
                break;
            case 'popPage':
                clcom.popPage();
                break;
            case 'popPageTo':
                var toAppCode = this.options.action.popPageTo;
                if(toAppCode != null){
                    clcom.popPageTo({
                        srcId: toAppCode,

                        lostToHome: true
                    });
                    break;
                }
                // fall through
            default:
            case 'home':
                clcom.gohome();
            }
        }
    });

    _.extend(clutil.view, {
        /**
         * Backbone.View 拡張 View 規定クラス定義。
         * ・子 View の events を継承する。
         * ・親クラス prototype を su プロパティに持つ。[super] は予約語につき [su] とした。
         */
        AbstractView: AbstractView,

        /**
         * 機器モード定義
         * ブラウザ Window サイズでスタイル等切り替えるための判断にあたり、window 幅閾値を定義する。
         */
        ScreenModes: _.each([
                {
                    // 幅[0-767]px: スマホ
                    name: 'xs',
                    description: 'Smart Phone',
                    minWidth: 0
                    //maxWidth: 768-1
                },
                {
                    // 幅[768-991]px: タブレット
                    name: 's',
                    description: 'Tablet',
                    minWidth: 768
                    //maxWidth: 1366-1
                },
                {
                    // 幅[992-1365]: PC一般
                    name: 'm',
                    description: 'Generic Screen',
                    minWidth: 992
                    // maxWidth: 1366-1
                },
                {
                    // 幅[1366-∞]px: PC最大化
                    name: 'xl',
                    description: 'Generic Screen: Maximized',
                    minWidth: 1366
                    //maxWidth: Number.POSITIVE_INFINITY
                }
            ],
            function(cur,index,array){
                var next = array[index+1];
                cur.maxWidth = (next == null) ? Number.POSITIVE_INFINITY : next.minWidth-1;
            }
        ),
        /**
         * 機器モードを取得する。
         * @param width ウィンドウ幅、省略時は現在の window 幅（innerWidth）
         * @return 機器モードを返す。
         */
        getScreenMode: function(width){
            if(width == null){
                width = window.innerWidth;
            }
            for(var i=this.ScreenModes.length-1; i>=0; i--){
                var def = this.ScreenModes[i];
                if(width >= def.minWidth){
                    return def;
                }
            }
            return this.DevSpaceDefs[0];
        },
        /**
         * スクロール
         * @param opt
         * 	.container	: スクロール親要素、省略時は $('html,body')
         * 	.target		: スクロール子要素、省略時は 0
         * @param scrollCompleteCallback スクロールアニメーションの終了コールバック
         */
        scrollTop: function(opt, scrollDoneCallback){
            var o = opt || {};
            var $c;
            if(o.container instanceof $){
                $c = o.container;
            }else if(_.isElement(o.container)){
                $c = $(o.container);
            }else if(_.isString(o.container)){
                $c = $(o.container);
            }else{
                $c = $('html,body');
            }

            var offset = 0;
            if(o.target instanceof $){
                offset = o.target.offset().top;
            }else if(_.isElement(o.target)){
                offset = $(o.target).offset().top;
            }else if(_.isString(o.target)){
                offset = $(o.target).offset().top;
            }else if(o.target != null){
                offset = o.offset;
            }

            // fixtop 補正項
            if(offset > 0){
                $('.cl_fixtop').each(function(idx, elem){
                    offset -= $(elem).outerHeight();
                });
                if(offset < 0) offset = 0;
            }

            $c.stop().animate({scrollTop: offset}, function(){
                if(_.isFunction(scrollDoneCallback)){
                    scrollDoneCallback(this);
                }
            });
        },

        /**
         * アラートメッセージ表示
         * @param level     レベル
         * @param message   メッセージ
         * @param onClosed  [省略可]メッセージを閉じた後に呼び出すコールバック
         * または、
         * @param object
         *  .level      [省略可] dismissable, default, primary, success, info, warning, danger、省略時は danger(赤）
         *  .duration   [省略可] 表示期間、デフォルト3000[msec]、0以下を明示指定すると無期限。
         *  .priority   [省略可] 優先順位付け。優先順位を指定すると、後続 showAlert() は表示されない。高優先指定する場合は慣例的に 'high' を指定すること。デフォルト: 'normal'。
         *  .blockElem  [省略可] アラート表示中、$elem への入力をブロックするフィルム層を被せる。
         *  .message    エラーメッセージ（文字列）、または、共通応答ヘッダを含む応答データ、共通応答ヘッダ、エラーコード(messages_ja.js 参照)
         * @return アラートメッセージのコントローラインスタンス（Backbone.View）
         */
        showAlert: function(level, message, onClosed){
            var defaultModel = {
                level: 'danger'
            };
            var model, cbFunc = onClosed;
            if(arguments.length === 1 && _.isObject(arguments[0])){
                model = _.defaults(_.omit(arguments[0],'onClosed'), defaultModel);
                cbFunc = arguments[0].onClosed;
            }else if(_.isObject(message) || _.isArray(message)){
                model = _.defaults({
                    level: level,
                    message: message
                }, defaultModel);
            }else{
                var args = _.toArray(arguments);
                model = _.defaults({
                    level: args[0],
                    message: args.splice(1)
                }, defaultModel);
            }

            // 高優先メッセージが表示されている場合は表示をあきらめる。
            // コールバック指定している場合はコールバックを実行しておく。
            var priorityBlocking = false;
            $('body').children('.alert').each(function(idx,elem){
                var $el = $(elem);
                var api = $el.data('cl_AlertView');
                if(api != null && api.getPriority() != 'normal'){
                    priorityBlocking = true;
                    return false;
                }
            });
            if(priorityBlocking){
                var msg = clutil.getclmsg(model.message);
                console.warn('reject alert: ' + msg);
                if(_.isFunction(cbFunc)){
                    cbFunc();
                }
                return;
            }

            // メッセージ表示する。
            var api = new AlertView({model: model});
            api.render().show(cbFunc);

            return api;
        },
        /**
         * 全アラートメッセージを消す
         */
        clearAlert: function(){
            $('body').children('.alert').each(function(idx, elem){
                var $el = $(elem);
                var api = $el.data('cl_AlertView');
                if(api){
                    api.destroy();
                }else{
                    $el.remove();
                }
            });
        },

        /**
         * アラートメッセージの表示をチェックする。
         */
        isActiveAlert: function(){
            var b = false;
            $('body').children('.alert').each(function(idx,elem){
                var $el = $(elem);
                var api = $el.data('cl_AlertView');
                if(api != null){
                    b = true;
                    return false;
                }
            });
            return b;
        },

        /**
         * 致命的エラーにつき続行不能なメッセージを表示する。
         */
        abort: function(arg){
            var av = new AbortView(arg);
            av.render();
            av.startTimer();
        },

        /**
         * アプリスタート
         * ・clcom 初期化タイミングを保証する。
         */
        appStart: function(appFunc, opt) {
            var o = opt || {};
//          ▼ @since Rzp
            if (!o.template) {
                o.template = [];
            }
            if (!_.isArray(o.template)) {
                o.template = [o.template];
            }
            if(!_.isEmpty(clcom.config.template)){
                o.template.push(clcom.config.template);
            }
//          ▲ @since Rzp
            $(function(){
                getIniJSON().always(function(data){
                    ; // ココは共通処理を記述できる隙間
//                    var userData = clcom.getUserData();
//                    if (!userData) {
//                        var url = clutil.createErrOccurredUrl({}, '/');
//                        clcom.location(url);
//                        return null;
//                    }
                    return this;
                }).then(function(){
                    if(!_.isEmpty(o.iniOptions)){
                        return getIniJSON2(o.iniOptions);
                    }
                    return this;
                }).then(function(){
                    var tmplCodes = clutil.view.preLoadTemplateCodes.concat(o.template || []);
                    if(!_.isEmpty(tmplCodes)){
                        return clutil.loadTemplate(tmplCodes);
                    }
                    return this;
                }).done(function() {
                    if(window.TestAjaxMock && window.TestAjaxMock.ope_date){
                        // モックAjax応答テスト
                        var iymd = clutil.date.toIymd(window.TestAjaxMock.ope_date);
                        clcom.setOpeDate(iymd);
                    }
                    clutil.mediator.trigger('clcom:ready');
                    if(_.isFunction(appFunc)) {
                        appFunc();
                    }
                }).fail(function(data){
                    clutil.view.abort({
                        el: 'body',
                        messages: 'cl_sys_error',
                        action: {
                            command: 'logout'
                        }
                    });
                });
            });
        },

        /** 事前ロードするテンプレートコード */
        preLoadTemplateCodes: [],

        /**
         * Template View クラス
         * <table>タグを $el とし、行選択、行追加のあるテーブルの動作をサポートする。
         * TODO: ネームスペースとか、配置すべき js とか再考。
         */
        TemplateView : AbstractView.extend({
            events: {
            },
            /**
             * opt: オプション
             * .template	: 行生成のためのテンプレート
             */
            initialize: function(opt){
                var o = opt || {};
                if(o){
                    if(o.template){
                        if(_.isString(opt.template)){
                            this.template = _.template(opt.template);
                        }else{
                            this.template = opt.template;
                        }
                    }
                }
                if(this.collection == null){
                    this.collection = [];
                }else{
                    this.collection = _.clone(this.collection);
                }
            },
            render: function(){
                if(!_.isEmpty(this.collection)){
                    this.setRecs(this.collection);
                }
                return this;
            },

            /**
             * 行データをセットする。
             * ただし、テンプレート指定されていること。
             */
            setRecs: function(rows){
                if(this.template == null){
                    throw 'clutil.view.TemplateView: template';
                }
                this.clear();
                if(_.isEmpty(rows)){
                    return;
                }

                this.collection = _.clone(rows);

                for (var i = 0; i < rows.length; i++) {
                    var rec = rows[i];
                    this.$el.append(this.template(rec));
                }

            },

            /**
             * 行追加する。 ただし、行テンプレートが指定されていること。
             */
            appendRows: function(rows) {
                if(this.template == null){
                    throw 'clutil.view.TemplateView: template';
                }
                if(_.isEmpty(rows)){
                    return;
                }

                this.collection = _.clone(rows);

                for (var i = 0; i < rows.length; i++) {
                    var rec = rows[i];
                    this.$el.append(this.template(rec));
                }
            },

            /**
             * 行追加する。 ただし、行テンプレートが指定されていること。
             */
            appendRow: function(row) {
                if(this.template == null){
                    throw 'clutil.view.TemplateView: template';
                }
                if(_.isEmpty(row)){
                    return;
                }

                var rec = row;
                this.$el.append(this.template(rec));
            },

            /**
             * クリアする。
             */
            clear: function(){
                this.$el.empty();
            },

            /**
             * 全データを返す。
             */
            getRecs: function(){
                return clutil.deepClone(this.collection);
            }
        })

    });

}());
/**
 * 共通 View クラス - 選択ＵＩ関連
 */
var clutil = _.extend({}, clutil);
$(function () {

    // ローカルテンプレート集
    var MyTemplates = {
        // 曜日定義
        dayOfWeeks: [
            // getDayメソッドは曜日の値を取得します。取得できる値は0から6までの数値で日曜日なら0、土曜日なら6を取得します。
            { id:0, code:'0', name:'日', name_m: '日曜', name_l:'日曜日' },
            { id:1, code:'1', name:'月', name_m: '月曜', name_l:'月曜日' },
            { id:2, code:'2', name:'火', name_m: '火曜', name_l:'火曜日' },
            { id:3, code:'3', name:'水', name_m: '水曜', name_l:'水曜日' },
            { id:4, code:'4', name:'木', name_m: '木曜', name_l:'木曜日' },
            { id:5, code:'5', name:'金', name_m: '金曜', name_l:'金曜日' },
            { id:6, code:'6', name:'土', name_m: '土曜', name_l:'土曜日' }
        ],
        /**
         * チェックボックスグループ用テンプレート
         * @param properties
         *  .name       : name 属性       ①
         *  .value      : value 属性      ②
         *  .label      : 表示用テキスト  ③
         *  .classNames : class 属性      ④
         *  .attributes : その他属性      ⑤
         *  .display    : 横並び＝"checkbox-inline" ⑥
         * @return
         * <label class="checkbox ⑥">
         *	<input type="checkbox" name="①" class="cl_cbgroup ④" data-toggle="checkbox" value="②" ⑤>③
         * </label>
         */
        buildCheckboxGroup: _.template(''
            + '<label class="checkbox <%= display %>">'
            +	'<input type="checkbox" name="<%= name %>" class="cl_cbgroup <%= classNames.join(" ") %>" <%= clutil.text.toElemAttrs(attributes) %> data-toggle="checkbox" value="<%- value %>"><%- label %>'
            + '</label>'),

        /**
         * ラジオボタングループ用テンプレート
         * @param properties
         *  .name           : name 属性       ①
         *  .value          : value 属性      ②
         *  .label          : 表示用テキスト  ③
         *  .classNames     : class 属性      ④
         *  .attributes     : その他属性      ⑤
         *  .display        : 横並び＝"radio-inline" ⑥
         * @return
         * <label class="checkbox ⑥">
         *	<input type="radio" name="①" class="cl_rbgroup ④" data-toggle="radio" value="②" ⑤>③
         * </label>
         */
        buildRadioGroup: _.template(''
            + '<label class="radio <%= display %>">'
            +	'<input type="radio" name="<%= name %>" class="cl_rbgroup <%= classNames.join(" ") %>" <%= clutil.text.toElemAttrs(attributes) %> data-toggle="radio" value="<%- value %>"><%- label %>'
            + '</label>')
    };

    /**
     * 階層関係を持ったマスタ用セレクタ（静的）
     * @param args Array
     * [
     *    {
     *      $el             : <select> 要素
     *      hasNullItem     : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
     *                        第２要素以降は必ず空選択が入る。
     *      altPropName     : 当該マスタにおける { id, code, name } に相当するプロパティ名
     *      collection      : 当該階層の全選択肢                                                 ... TODO 動的取得対応にも検討する。
     *      labelFormat     : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
     *    },
     *    ...
     *  ]
     *  @return api
     *
     *  イベント -- 選択値変更イベントは内部 model の change イベントを監視することで実現可能。
     *  api.model.listenTo(listener, 'change', callback)
     */
    HierarcicalSelectpickers = function(args){
        var defaultDto = {};
        var ctxs = [];
        for(var i=0; i<args.length; i++){
            var av = args[i];

            // clutil.view.initSelectpicker() 引数をつくる。
            var iniSelectArg = {
                $el: av.$el,
                selection: {
                    altPropName: av.altPropName || {id: 'id', code: 'code', name: 'name'},
                    items: [],
                    labelFormat: av.labelFormat,
                    hasNullItem: av.hasNullItem
                }
            };

            // 各 Lv 個々のコンテキスト
            var ctx = {
                level: i,
                api: this,
                arg: av,
                initSelectArg: iniSelectArg,
                idPropName: iniSelectArg.selection.altPropName.id
            };
            ctxs.push(ctx);

            // デフォルト設定値
            defaultDto[ctx.idPropName] = undefined;
        }

        // デフォルト設定値: 初期値整理
        var iniDto = _.clone(defaultDto);
        var ctx0 = ctxs[0], iniSelectArg = ctx0.initSelectArg;
        if(!iniSelectArg.selection.hasNullItem){
            var item0 = ctx0.arg.collection[0];
            if(item0){
                var idPropName = ctx0.idPropName;
                iniDto[idPropName] = item0[idPropName];
            }
        }

        // API 内部プロパティ
        this.ctxs = ctxs;
        this.model = new Backbone.Model(defaultDto);

        // 初期化
        this._initialize(iniDto);
    }
    _.extend(HierarcicalSelectpickers.prototype, {
        _initialize: function(mdto){
            var ctxs = this.ctxs;

            // initSelectpicker 初期化
            // Lv[0] - 全選択肢を注入する
            var ctx0 = ctxs[0];
            var iniSelectArg = ctx0.initSelectArg;
            iniSelectArg.selection.items = ctx0.arg.collection;
            iniSelectArg.$el
                .change(this._selectChangedHandler)         // change イベントハンドラ
                .addClass('cl_hierarchy').data('cl_hierarchy', ctx0);
            clutil.view.initSelectpicker(iniSelectArg);

            // Lv[1-n] 空初期化して disabled 化。
            for(var i=1; i<ctxs.length; i++){
                var ctx = ctxs[i];
                var iniSelectArg = ctx.initSelectArg;
                iniSelectArg.selection.items = [];
                iniSelectArg.$el
                    .change(this._selectChangedHandler)     // change イベントハンドラ
                    .data('cl_hierarchy', ctx)
                    .prop('disabled', true);
                clutil.view.initSelectpicker(iniSelectArg);
            }

            // change イベント監視
            // なお、change:<プロパティ> で、特定プロパティの変更のみを取ることができるが、
            // <select> UI では、変更連鎖するため、change で一括してイベントハンドリングする。
            this.model.listenTo(this.model, 'change', _.bind(this._modelDataChanged, this));

            // 内部 Backbone.Model へ初期設定値をセットして、選択肢取込イベント発火の連鎖を起こす
            if(!_.chain(mdto).compact().isEmpty().value()){
                this.model.set(mdto);
            }
        },
        // <select> の change イベントハンドラ
        _selectChangedHandler: function(ev){
            var $elem = $(ev.currentTarget);
            if($elem.hasClass('cl_hierarchy:model_data_changed')){
                // モデルデータ change イベント中から呼ばれた。これは、選択肢設定処理済のため無視。
                $elem.removeClass('cl_hierarchy:model_data_changed');
                return;
            }

            var ctx = $elem.data('cl_hierarchy');
            if(ctx == null){
                return;
            }
            var myApi = ctx.api;
            var myLevel = ctx.level;
            var mySelectArg = ctx.initSelectArg;
            var myAltPropName = mySelectArg.selection.altPropName;

            // -----------------------
            // DTO 上で設定値編集
            var mdto = myApi.model.toJSON();
            var mval = ev.currentTarget.value;

            // <select> 設定値を model へセットする。
            mdto[myAltPropName.id] = _.isFinite(mval) ? parseInt(mval,10) : undefined;

            // 子要素の設定値をクリアする。
            var ctxs = myApi.ctxs;
            for(var i=myLevel+1; i<ctxs.length; i++){
                var ctx = ctxs[i];
                var idPropName = ctx.idPropName;
                mdto[idPropName] = undefined;
            }

            var v = ev.currentTarget.value;
            myApi.model.set(mdto, { srcEvent: ev });    // 独自オプション: 他 <select> の change 発火を行うためのヒント
                // ↑ model の change イベントハンドラ「_modelDataChanged」によって選択肢の設定を伝播させる。
        },

        // this.model (Backbone.Model) の change イベント監視
        // 各レベル毎、一斉に選択肢の入れ替え処理を行う。
        _modelDataChanged: function(m, o){
            var changed = m.changed, curDto = m.toJSON(), fixDto = {};
            var ctxs = this.ctxs;

            // 配列 items 中に { [propName]: val } 相当の要素が存在する index を返す。
            function findIndexOf(items, propName, val){
                return _.findIndex(items, function(it){ return it[propName] == val; });
            }
            function fixVal(val){
                return (val == null || val == '') ? undefined : val;
            }

            // ルート
            if(ctxs.length > 0){
                var ctx = ctxs[0];
                var propName = ctx.idPropName;
                var val = curDto[propName];
                var items = ctx.initSelectArg.selection.items;
                if(_.has(changed, propName)){
                    var findIndex = findIndexOf(items, propName, val);
                    if(findIndex >= 0){
                        ctx.initSelectArg.$el.selectpicker('val', val);
                    }else{
                        // ルート階層、選択肢中に val 要素が存在しない ⇒ 変更できないので、そのまま維持。
                        val = ctx.initSelectArg.$el.selectpicker('val');
                        delete changed[propName];   // 変更を無かったことに。
                    }
                }
                fixDto[propName] = fixVal(val);
            }

            // 親Lvに変更がある場合は選択肢を再構築する。
            // 自身の設定値を設定する。
            for(var i=1; i<ctxs.length; i++){
                // 親階層
                var pctx = ctxs[i-1], pidPropName = pctx.idPropName, pval = curDto[pidPropName];
                // 子階層
                var cctx = ctxs[i], cidPropName = cctx.idPropName, cval = curDto[cidPropName];

                // 親が未設定、かつ、子に選択肢を持っている場合、子をクリアする。
                if(pval == null){
                    var initSelectArg = cctx.initSelectArg;

                    initSelectArg.$el.prop('disabled', true);
                    if(!_.isEmpty(cctx.initSelectArg.selection.items)){
                        initSelectArg.selection.items = [];
                        clutil.view.initSelectpicker(initSelectArg);
                    }else{
                        initSelectArg.$el.selectpicker('refresh');
                    }

                    cval = undefined;
                    changed[cidPropName] = cval;
                    fixDto[cidPropName] = cval;
                    continue;
                }

                // 親変更あり ⇒ 子の選択肢を再設定する。
                if(_.has(changed, pidPropName)){
                    // 選択肢を入れ直す
                    // TODO: 動的にサーバ取得する系も実装検討する。
                    var cItems = _.filter(cctx.arg.collection, function(it){
                        return it[pidPropName] == pval;
                    });
                    var initSelectArg = cctx.initSelectArg;
                    initSelectArg.selection.items = cItems;
                    initSelectArg.$el.prop('disabled', _.isEmpty(cItems));
                    clutil.view.initSelectpicker(initSelectArg);

                    // 子階層で選択可能な値を選択する。
                    if(cItems.length > 0){
                        // 新しい選択肢 cItems の中に cval が存在している場合は当該要素を選択。
                        if(curDto[cidPropName] != null){
                            var findIndex = findIndexOf(cItems, cidPropName, cval);
                            if(findIndex >= 0){
                                initSelectArg.$el.selectpicker('val', cval);
                                changed[cidPropName] = cval;
                                fixDto[cidPropName] = cval;
                                continue;
                            }
                        }

                        // 未選択用の選択肢が無い場合は要素[0]番目を選択。
                        if(!initSelectArg.selection.hasNullItem){
                            cval = cItems[0][cidPropName];
                            //initSelectArg.$el.selectpicker('val', cval); -- [0]番目はUI上で既に選択済
                            changed[cidPropName] = cval;
                            fixDto[cidPropName] = cval;
                            continue;
                        }
                    }

                    // 選択肢入れ替え済につき、items[0] が選択された状態になっている。
                    // changed、fixDto を items[0] 値に変更しておく。
                    cval = initSelectArg.$el.selectpicker('val');   // items[0] または、未選択
                    if(cval == null) cval = undefined;
                    changed[cidPropName] = cval;
                    fixDto[cidPropName] = cval;
                    continue;
                }

                // 親変更なし ⇒ 今セットされている選択肢中から選択可能な値を選択する。
                // 選択可能な値が無ければ現状維持としておく。
                var initSelectArg = cctx.initSelectArg;
                var val = initSelectArg.$el.selectpicker('val');
                if(val != cval){
                    var cItems = initSelectArg.selection.items || [];
                    var findIndex = findIndexOf(cItems, cidPropName, cval);
                    if(findIndex >= 0){
                        initSelectArg.$el.selectpicker('val', cval);
                        changed[cidPropName] = cval;
                        fixDto[cidPropName] = cval;
                        continue;
                    }
                }
                // 現状維持
                fixDto[cidPropName] = cval;
            }

            // 補正値をセット: change イベント発火は抑制。
            m.set(fixDto, {silent: true});

            // イベントトリガが <select> の change イベントで発火している場合、他 <select> の change イベントを発火してまわる。
            if(o && o.srcEvent){
                var $elem = $(o.srcEvent.currentTarget);
                var srcCtx = $elem.data('cl_hierarchy');
                if(srcCtx == null){
                    return;
                }
                for(var i=0; i<ctxs.length; i++){
                    var ctx = ctxs[i];
                    if(ctx === srcCtx){
                        // change イベント発火元自身につき、change 発火省略。
                        continue;
                    }

                    // 値変更チェック
                    var propName = ctx.idPropName;
                    if(curDto[propName] != fixDto[propName]){
                        // アプリ層の change イベント監視のために、$.trigger('change') 発火する。
                        // 本 API 内部でも change イベント listen しているが、選択肢設定処置済のため、
                        // 無視するようにマーカークラス 'cl_hierarchy:model_data_changed' を付与しておく。
                        ctx.initSelectArg.$el.addClass('cl_hierarchy:model_data_changed').trigger('change');
                    }
                }
            }
        },
        /**
         * 内部モデルデータを取得する。
         */
        getModelDto: function(){
            return this.model.toJSON();
        },
        /**
         * 内部モデルデータを設定する。
         */
        setModelDto: function(mdto){
            this.model.set(mdto);
        },
        /**
         * 指定階層へ値を設定する。
         */
        setValueOf: function(hLevel, val){
            var ctxs = this.ctxs;
            var ctx = ctxs[hLevel];
            if(ctx == null){
                return;
            }
            var mdto = this.model.toJSON();

            // [hLevel]の設定値をセットする。
            mdto[ctx.idPropName] = val;

            // [hLevel+1] 以降の設定値をクリアする。
            for(var i=hLevel+1; i<ctxs.length; i++){
                var ctx = ctxs[i];
                mdto[ctx.idPropName] = undefined;
            }

            // change イベント発火、発火イベントの延長で選択肢を入れ替える。
            this.model.set(mdto);
        },
        /**
         * 指定階層の設定値を取得する。
         * @param hLevel 階層レベル(0オリジン)
         */
        getValueOf: function(hLevel){
            var ctx = this.ctxs[hLevel];
            if(ctx != null){
                var initSelectArg = ctx.initSelectArg;
                var val = initSelectArg.$el.selectpicker('val');
                return _.isFinite(val) ? parseInt(val,10) : val;
            }
        },
        /**
         * clutil.data2view のためのヘルパー関数
         */
        data2view: function(data, prefix){
            prefix = clutil.text.buildPrefixMatcher(prefix, clutil.defaultCaPrefix);

            data = data || {};
            var ctxs = this.ctxs, mdto = {};

            var i;
            for(i=0; i<ctxs.length; i++){
                var ctx = ctxs[i];
                var $elem = ctx.initSelectArg.$el;
                var propName = clutil.text.isMatchThenTrim(prefix, $elem.attr('id'));
                var val = data[propName];
                mdto[ctx.idPropName] = _.isFinite(val) ? parseInt(val,10) : val;
                if(val == null){
                    break;  // 以降の階層では値をクリアする。
                }
            }
            // 未設定値以降の階層はクリアする。
            for(; i<ctxs.length; i++){
                if(i==0){
                    continue;
                }
                var ctx = ctxs[i];
                mdto[ctx.idPropName] = undefined;
            }

            // change イベント発火、発火イベントの延長で選択肢を入れ替える。
            this.model.set(mdto);
        },
        /**
         * clutil.view2data のためのヘルパー関数
         */
        view2data: function(prefix){
            prefix = clutil.text.buildPrefixMatcher(prefix, clutil.defaultCaPrefix);

            var data = {}
            var ctxs = this.ctxs;
            for(var i=0; i<ctxs.length; i++){
                var ctx = ctxs[i], initSelectArg = ctx.initSelectArg, $elem = initSelectArg.$el;
                var myId = $elem.attr('id'), val = $elem.selectpicker('val');
                var propName = clutil.text.isMatchThenTrim(prefix, myId);
                if(propName && _.isFinite(val)){
                    data[propName] = parseInt(val,10);
                }else{
                    // 値が取れない階層以降は設定値収集しない。
                    break;
                }
            }

            return data;
        },
        /**
         * 活性/不活性状態を設定する
         * @param enable true:活性化、false:不活性化
         */
        setEnable: function(enable){
            function _setEnable(iniArg, en){
                var disabled = (en === false || _.isEmpty(iniArg.selection.items));
                iniArg.$el.prop('disabled', disabled).selectpicker('refresh');
            }
            for(var i=0; i<this.ctxs.length; i++){
                var ctx = this.ctxs[i];
                _setEnable(ctx.initSelectArg, enable)
            }
        }
    });

    /**
     * 年月日セレクタ
     * @param args Object
     *  .start              : [省略可] 開始年月日 - default 19900102
     *  .end                : [省略可] 終了年月日 - default 20701231
     *  .year
     *      .$el            : 年セレクタ <select> 要素
     *      .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
     *  .month
     *      .$el            : 月セレクタ <select> 要素
     *      .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
     *  .date
     *      .$el            : 日セレクタ <select> 要素
     *      .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
     */
    var YMDSelectPicker = function(arg){
        // --------------------------------------
        // 引数チェック
        if(!(_.isObject(arg) && _.isObject(arg.year) && _.isObject(arg.month) && _.isObject(arg.date))){
            throw 'clutil.view.YMDSelect: invalid arguments.';
        }
        // <select> 要素チェック
        if(!(arg.year && arg.year.$el && arg.year.$el.is('select'))
                || !(arg.month && arg.month.$el && arg.month.$el.is('select'))
                || !(arg.date && arg.date.$el && arg.date.$el.is('select'))){
            throw 'clutil.view.YMDSelect: invalid arguments, year.$el, month.$el or day.$el must be <select> element.';
        }
        var stDate = clutil.date.toDate(arg.start || clcom.limits.min_date, null, 'from')
            , edDate = clutil.date.toDate(arg.end   || clcom.limits.max_date, null, 'to')
            , iniDate = clutil.date.toDate(clcom.ope_date);
        if(iniDate.getTime() < stDate.getTime()){
            iniDate = new Date(stDate);
        }else if(iniDate.getTime > edDate.getTime()){
            iniDate = new Date(edDate);
        }
        // 開始日 <= 終了日チェック
        if(stDate.getTime() > edDate.getTime()){
            throw 'clutil.view.YMDSelect: invalid arguments, start > end.';
        }

        function buildItems(num,unitLabel){
            var array = [];
            for(var i=1; i<=num; i++){
                array.push({
                    id:i,
                    name:i + unitLabel
                });
            }
            return array;
        }
        var monthItems = buildItems(12,'月');	// 月選択肢全部
        var dayItems = buildItems(31,'日');		// 日選択肢全部

        // 内部コンテキスト
        var ctx = {
            stDate: stDate,		// 下限
            edDate: edDate,		// 上限
            iniDate: iniDate,	// 初期設定値
            // 年セレクタ用の初期化パラメタ
            year: {
                $el: arg.year.$el,
                selection: {
                    items: function(minYear, maxYear){
                        var array = [];
                        for(var y=minYear; y<=maxYear; y++){
                            array.push({ id: y, name: y + '年' });
                        }
                        return array;
                    }(stDate.getFullYear(), edDate.getFullYear())
                }
            },
            // 月セレクタ用の初期化パラメタ
            month: {
                $el: arg.month.$el,
                selection: {
                    items: monthItems
                }
            },
            // 日セレクタ用の初期化パラメタ
            date: {
                $el: arg.date.$el,
                selection: {
                    items: []			// 年、月選択に応じて生成する
                }
            },
            monthItemsAll: monthItems,	// 月選択肢（12ヶ月分）
            dateItemsAll: dayItems		// 日選択肢（31日分)
        };
        var defaultNullItem = { id: NaN };
        if(arg.year.hasNullItem){
            var nullItem = arg.year.hasNullItem;
            ctx.year.selection.hasNullItem = _.isObject(nullItem) ? _.extend({}, nullItem, defaultNullItem) : true;
        }
        if(arg.month.hasNullItem){
            var nullItem = arg.month.hasNullItem;
            ctx.month.selection.hasNullItem = _.isObject(nullItem) ? _.extend({}, nullItem, defaultNullItem) : true;
        }
        if(arg.date.hasNullItem){
            var nullItem = arg.date.hasNullItem;
            ctx.date.selection.hasNullItem = _.isObject(nullItem) ? _.extend({}, nullItem, defaultNullItem) : true;
        }
        this.ctx = ctx;
    }
    _.extend(YMDSelectPicker.prototype, {
        initialize: function(){
            var c = this.ctx;

            clutil.view.initSelectpicker(c.year);
            clutil.view.initSelectpicker(c.month);
            clutil.view.initSelectpicker(c.date);

            // 初期設定運用日、または運用日に近い日付で初設定
            var date = c.iniDate, yVal = date.getFullYear(), mVal = date.getMonth()+1, dVal = date.getDate();
            this._setVal('year', yVal);
            this._buildMonthItems(yVal, mVal);
            this._buildDateItems(yVal, mVal, dVal);

            // イベントハンドリング
            c.year.$el.change(_.bind(function(ev){
                // 年変更 ⇒ 月日選択肢を再設定
                this._buildMonthItems();
                this._buildDateItems();
            },this));
            c.month.$el.change(_.bind(function(ev){
                // 月変更 ⇒ 日選択肢を再設定
                this._buildDateItems();
            },this));

            // セレクタ要素に api インスタンスを仕込む
            c.year.$el.addClass('cl_YMDSelect cl_YMDSelect-year').data('cl_YMDSelect', this);
            c.month.$el.addClass('cl_YMDSelect cl_YMDSelect-month').data('cl_YMDSelect', this);
            c.date.$el.addClass('cl_YMDSelect  cl_YMDSelect-day').data('cl_YMDSelect', this);
        },
        // setter 内部関数
        _setVal: function(slotName, val){
            var c = this.ctx[slotName] ,found = false;
            if(_.isFinite(val)){
                val = parseInt(val,10);
                // 選択肢定義域チェック
                var items = c.selection.items,
                found = _.find(items,{id:val});
            }
            if(found === false){
                // 設定値が選択肢の中に無い場合
                if(c.selection.hasNullItem){
                    val = null;
                }else{
                    var it0 = c.selection.items[0];
                    if(it0 != null){
                        val = it0.id;
                    }
                }
            }
            c.$el.selectpicker('val',val);
        },
        // getter 内部関数
        _getVal: function(slotName){
            var c = this.ctx[slotName]
            , val = c.$el.selectpicker('val');
            return _.isFinite(val) ? parseInt(val,10) : undefined;
        },
        // 月選択肢を再構築する
        _buildMonthItems: function(yVal, iniVal){
            var c = this.ctx.month,
            stYear = this.ctx.stDate.getFullYear(),
            edYear = this.ctx.edDate.getFullYear(),
            curMVal = this.getMonth(),
            curItems = c.selection.items;
            if(arguments.length === 0){
                yVal = this.getYear();
            }

            // 月選択肢入れ換え
            var newItems = null;
            if(yVal == null || yVal == 0){
                // 年未選択 ⇒ 月選択肢を空にして、disabled 化。
                newItems = [];
            }else if(yVal == stYear){
                // 年定義域始点の場合
                var stMonth = this.ctx.stDate.getMonth()+1;
                newItems = this.ctx.monthItemsAll.slice(stMonth-1,12);
            }else if(yVal == edYear){
                // 年定義域終点の場合
                var edMonth = this.ctx.edDate.getMonth()+1;
                newItems = this.ctx.monthItemsAll.slice(0,edMonth);
            }else{
                // 12ヶ月の選択肢をセットする
                if(curItems.length != 12){
                    newItems = this.ctx.monthItemsAll;
                }
            }
            if(newItems != null){
                c.selection.items = newItems;
                clutil.view.initSelectpicker(c);
            }

            // 設定値復元
            this._setVal('month', iniVal || curMVal);

            // 選択肢が空 ⇒ disabled 化
            var disable = _.isEmpty(c.selection.items);
            c.$el.prop('disabled',disable).selectpicker('refresh');
        },
        // 日選択肢を再構築する
        _buildDateItems: function(yVal, mVal, iniVal){
            var c = this.ctx.date,
            curDVal = this.getDate(),
            curItems = c.selection.items;
            if(arguments.length === 0){
                yVal = this.getYear();
                mVal = this.getMonth();
            }

            // 日選択肢入れ替え
            var newItems = null;
            if(mVal == null || mVal == 0){
                // 月未選択 ⇒ 日選択肢を空にして、disabled 化
                newItems = [];
            }else{
                // 設定範囲評価
                var stYear = this.ctx.stDate.getFullYear(),
                stMonth = this.ctx.stDate.getMonth()+1,
                edYear  = this.ctx.edDate.getFullYear(),
                edMonth = this.ctx.edDate.getMonth()+1;

                // 設定範囲両端月かどうかで、開始日、終端日を見極める
                var stDate, edDate;
                if(yVal == stYear && mVal == stMonth){
                    stDate = this.ctx.stDate;
                }else{
                    stDate = new Date(yVal, (mVal-1), 1);
                }
                if(yVal == edYear && mVal == edMonth){
                    edDate = this.ctx.edDate;
                }else{
                    edDate = clutil.date.getLastDateOfMonth(new Date(yVal, (mVal-1), 1));
                }

                var stDate = stDate.getDate(), edDate = edDate.getDate();
                newItems = this.ctx.dateItemsAll.slice(stDate-1, edDate);
            }
            c.selection.items = newItems;
            clutil.view.initSelectpicker(c);

            // 設定値復元
            this._setVal('date', iniVal || curDVal);

            // 選択肢が空 ⇒ disabled 化
            var disable = _.isEmpty(c.selection.items);
            c.$el.prop('disabled', disable).selectpicker('refresh');
        },
        /**
         * 年設定、指定年が定義域外の場合は未設定化する。
         * @param year 設定年
         */
        setYear: function(year){
            this._setVal('year',year);
            // 月、日選択肢を入れ替え
            this._buildMonthItems();
            this._buildDateItems();
        },
        /**
         * 年設定値取得
         * @return year 設定年、未設定の場合は undefined を返す
         */
        getYear: function(){
            return this._getVal('year');
        },
        /**
         * 月設定、指定値が定義域外の場合は未設定化する。
         * @param month 設定月
         */
        setMonth: function(month){
            this._setVal('month',month);
            // 日選択肢を入れ替え
            this._buildDateItems();
        },
        /**
         * 月設定値取得
         * @return month 設定月、未設定の場合は undefined を返す
         */
        getMonth: function(){
            return this._getVal('month');
        },
        /**
         * 日設定、指定値が定義域外の場合は未設定化する。
         * @param date 設定日
         */
        setDate: function(date){
            this._setVal('date',date);
        },
        /**
         * 日設定値取得
         * @return month 設定日、未設定の場合は undefined を返す
         */
        getDate: function(){
            return this._getVal('date');
        },
        /**
         * 年月日 yyyyMMdd 8桁整数形式で取得する。但し、年月日が揃っていない場合は null を返す。
         * @return iymd yyyyMMdd 8桁整数
         */
        getIymd: function(){
            var y = this.getYear(), m = this.getMonth(), d = this.getDate();
            var iymd = (y * 10000) + (m * 100) + d;
            return _.isFinite(iymd) ? iymd : null;
        },
        /**
         * 年月日 yyyyMMdd 8桁整数形式値 setter
         */
        setIymd: function(yyyyMMdd){
            if(!_.isFinite(yyyyMMdd)){
                return;
            }
            yyyyMMdd = parseInt(yyyyMMdd, 10);
            var y = Math.floor(yyyyMMdd / 10000), m = Math.floor(yyyyMMdd / 100) % 100, d = yyyyMMdd % 100;
            this.setYear(y);
            this.setMonth(m);
            this.setDate(d);
        },
        /**
         * 活性/不活性状態を設定する
         * @param enable true:活性化、false:不活性化
         */
        setEnable: function(enable){
            if(enable){
                function _setEnable(c){
                    var disable = _.isEmpty(c.selection.items);
                    c.$el.prop('disabled',disable).selectpicker('refresh');
                }
                _setEnable(this.ctx.year);
                _setEnable(this.ctx.month);
                _setEnable(this.ctx.date);
            }else{
                var ctx = this.ctx;
                ctx.year.$el.prop('disabled',true).selectpicker('refresh');
                ctx.month.$el.prop('disabled',true).selectpicker('refresh');
                ctx.date.$el.prop('disabled',true).selectpicker('refresh');
            }
        },
        /**
         * clutil.data2view のためのヘルパー関数
         */
        data2view: function(data, prefix){
            prefix = clutil.text.buildPrefixMatcher(prefix, clutil.defaultCaPrefix);

            data = data || {};
            var c = this.ctx, propName;

            // year
            propName = clutil.text.isMatchThenTrim(prefix, c.year.$el.attr('id'));
            if(propName){
                var val = parseInt(data[propName],10);
                this.setYear(val);
            }

            // month
            propName = clutil.text.isMatchThenTrim(prefix, c.month.$el.attr('id'));
            if(propName){
                var val = parseInt(data[propName],10);
                this.setMonth(val);
            }

            // date
            propName = clutil.text.isMatchThenTrim(prefix, c.date.$el.attr('id'));
            if(propName){
                var val = parseInt(data[propName],10);
                this.setDate(val);
            }
        },
        /**
         * clutil.view2data のためのヘルパー関数
         */
        view2data: function(prefix){
            prefix = clutil.text.buildPrefixMatcher(prefix, clutil.defaultCaPrefix);

            var c = this.ctx;
            var dto = {};

            // year
            var yId = c.year.$el.attr('id');
            if(yId){
                var propName = clutil.text.isMatchThenTrim(prefix, yId);
                var val = c.year.$el.selectpicker('val');
                if(propName && _.isFinite(val)){
                    dto[propName] = parseInt(val,10);
                }
            }
            // month
            var mId = c.month.$el.attr('id');
            if(mId){
                var propName = clutil.text.isMatchThenTrim(prefix, mId);
                var val = c.month.$el.selectpicker('val');
                if(propName && _.isFinite(val)){
                    dto[propName] = parseInt(val,10);
                }
            }
            // date
            var dId = c.date.$el.attr('id');
            if(dId){
                var propName = clutil.text.isMatchThenTrim(prefix, dId);
                var val = c.date.$el.selectpicker('val');
                if(propName && _.isFinite(val)){
                    dto[propName] = parseInt(val,10);
                }
            }

            return dto;
        }
    });

    // --------------------------------------------------------
    // clutil.view.* エクスポート
    _.extend(clutil.view, {

        /**
         * Bootstrap-Switch 初期化
         * @param $checkbox <input:checkbox> 要素
         * @param bootstrapSwitchOpt
         * @return 引数の $checkbox を返す。
         * @see http://bootstrapswitch.com/
         */
        initBootstrapSwitch: function($checkbox, bootstrapSwitchOpt){
            if(!$checkbox.is('input:checkbox')){
                throw 'clutil.view.initBootstrapSwitch: invalid arguments, $select must be <input:checkbox> element.';
            }

            var $sw = $checkbox.data('cl_switch');
            if($sw == null){
                $sw = $checkbox.wrap('<div class="switch" />').parent();
                if(bootstrapSwitchOpt){
                    $sw.data(bootstrapSwitchOpt);
                }
                $sw.bootstrapSwitch();
                $checkbox.data('cl_switch', $sw);
            }else{
                // 既に初期化済（と見做す）
            }
            return $sw;
        },

        /**
         * checkbox グループビルダ
         * @param arg
         * .checkbox        :
         *   .name          : input 要素の[name]属性 *必須*
         *   .classNames    : input 要素のクラス 省略可、Array<String>
         *   .attributes    : input 要素の属性   省略可、Object
         * .selection       : 選択肢定義
         *   .items         : 選択肢配列。原則 'id', 'name' プロパティを持つものとする。
         *   .altPropName   : items[x] id, code, name の別名プロパティ定義。例えば、選択肢要素 id が 'staff_id' のような場合に id 別名定義として指定することができる。
         *   .hasNullItem   : 未選択値指定、true:未選択つき、false:未選択なし、Object 指定で未選択値の {id, name} の明示ができる。
         *   .labelFormat   : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
         *   .key           : 選択値のキー名: 'id' or 'code' で指定する。デフォルトは 'id' を適用。
         * .display         : 縦並び: 'none'、横並び: 'inline'、省略時＝縦並び。
         *
         * 注意1: 選択肢の id 値が未設定の場合は 0 で補完される。どうしても選択肢 <option value=id> に value を持たせたくない場合は、id = NaN を指定すること。
         * 注意2: Bootstrap 装飾を施すため、呼出側で clutil.initUIelement($el) すること。
         *
         * @return checkbox チェックボックスグループの html を文字列で返す。
         */
        buildCheckboxGroup: function(arg){
            var textHtml = '';
            if(arg.selection){
                var selArg = arg.selection;
                var items = selArg.items || [];
                var altNameDef = _.defaults(selArg.altPropName || {}, { id:'id',code:'code',name:'name' });
                if(selArg.hasNullItem){
                    // 未選択値を追加する
                    var nullObject = _.isObject(selArg.hasNullItem) ? selArg.hasNullItem : {};
                    if(!_.has(nullObject, altNameDef.id)){
                        nullObject[altNameDef.id] = 0;
                    }
                    if(!_.has(nullObject, altNameDef.name)){
                        nullObject[altNameDef.name] = '&nbsp;';
                    }
                    items = [ nullObject ].concat(items);
                }

                // MyTemplates.buildCheckboxGroup テンプレート引数
                var tmplArg = {
                    name: arg.checkbox.name,
                    classNames: arg.checkbox.classNames || [],
                    attributes: arg.checkbox.attributes || {},
                    value: 0,
                    label: '',
                    display: (arg.display == 'inline') ? 'checkbox-inline' : ''
                };
                var labelFmt = selArg.labelFormat || 'name';
                var valkey = altNameDef[ selArg.key || 'id' ];
                for(var i=0; i<items.length; i++){
                    var item = items[i];
                    tmplArg.label = clutil.text.labelTextBuilder(labelFmt, item, altNameDef);
                    tmplArg.value = item[valkey] || 0;
                    textHtml += MyTemplates.buildCheckboxGroup(tmplArg);
                }
            }
            return _.isEmpty(textHtml) ? null : textHtml;
        },

        /**
         * radio グループビルダ
         * @param arg
         * .radio           :
         *   .name          : input 要素の[name]属性 *必須*
         *   .classNames    : input 要素のクラス 省略可、Array<String>
         *   .attributes    : input 要素の属性   省略可、Object
         * .selection       : 選択肢定義
         *   .items         : 選択肢配列。原則 'id', 'name' プロパティを持つものとする。
         *   .altPropName   : items[x] id, code, name の別名プロパティ定義。例えば、選択肢要素 id が 'staff_id' のような場合に id 別名定義として指定することができる。
         *   .hasNullItem   : 未選択値指定、true:未選択つき、false:未選択なし、Object 指定で未選択値の {id, name} の明示ができる。
         *   .labelFormat   : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
         *   .key           : 選択値のキー名: 'id' or 'code' で指定する。デフォルトは 'id' を適用。
         * .display         : 縦並び: 'none'、横並び: 'inline'、省略時＝縦並び
         *
         * 注意1: 選択肢の id 値が未設定の場合は 0 で補完される。どうしても選択肢 <option value=id> に value を持たせたくない場合は、id = NaN を指定すること。
         * 注意2: Bootstrap 装飾を施すため、呼出側で clutil.initUIelement($el) すること。
         *
         * @return checkbox チェックボックスグループの html を文字列で返す。
         */
        buildRadioGroup: function(arg){
            var textHtml = '';
            if(arg.selection){
                var selArg = arg.selection;
                var items = selArg.items || [];
                var altNameDef = _.defaults(selArg.altPropName || {}, { id:'id',code:'code',name:'name' });
                if(selArg.hasNullItem){
                    // 未選択値を追加する
                    var nullObject = _.isObject(selArg.hasNullItem) ? selArg.hasNullItem : {};
                    if(!_.has(nullObject, altNameDef.id)){
                        nullObject[altNameDef.id] = 0;
                    }
                    if(!_.has(nullObject, altNameDef.name)){
                        nullObject[altNameDef.name] = '&nbsp;';
                    }
                    items = [ nullObject ].concat(items);
                }

                // MyTemplates.buildRadioGroup テンプレート引数
                var tmplArg = {
                    name: arg.radio.name,
                    classNames: arg.radio.classNames || [],
                    attributes: arg.radio.attributes || {},
                    value: 0,
                    label: '',
                    display: (arg.display == 'inline') ? 'radio-inline' : ''
                };
                var labelFmt = selArg.labelFormat || 'name';
                var valkey = altNameDef[ selArg.key || 'id' ];
                for(var i=0; i<items.length; i++){
                    var item = items[i];
                    tmplArg.label = clutil.text.labelTextBuilder(labelFmt, item, altNameDef);
                    tmplArg.value = item[valkey] || 0;
                    textHtml += MyTemplates.buildRadioGroup(tmplArg);
                }
            }
            return _.isEmpty(textHtml) ? null : textHtml;
        },

        /**
         * 汎用 selectpicker 初期化ユーティリティ
         * @param arg
         * .$el             : 初期化対象のjQueryオブジェクト
         * .select          : ※非推奨オプション！$el がラッパー <div> の場合の内部 <select> の生成ルール。$el 自身が <select> の場合は不要。
         *   .attributes    : 内部生成 <select> の属性定義で、Object で指定。
         *   .className     : 内部生成 <select> の class 属性で、string で指定。複数クラスは ' ' 区切りで指定。
         * .selection       : 選択肢定義
         *   .items         : 選択肢配列。原則 'id', 'name' プロパティを持つものとする。
         *                      グループ毎に分けたい場合、下記形式のグループ要素を指定する。但し１段までサポート。
         *                      [
         *                          { label:'ラベル名', itmes: [...,選択肢,...] },
         *                          ...
         *                      ]
         *   .altPropName   : items[x] id, code, name の別名プロパティ定義。例えば、選択肢要素 id が 'staff_id' のような場合に id 別名定義として指定することができる。
         *   .hasNullItem   : 未選択値指定、true:未選択つき、false:未選択なし、Object 指定で未選択値の {id, name} の明示ができる。
         *   .labelFormat   : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
         *   .key           : 選択値のキー名: 'id' or 'code' で指定する。デフォルトは 'id' を適用。
         * .selectpicker    : bootstrap.selectpicker の初期化引数。
         *
         * 注意！ 選択肢の id 値が未設定の場合は 0 で補完される。どうしても選択肢 <option value=id> に value を持たせたくない場合は、id = NaN を指定すること。
         */
        initSelectpicker: function(arg){
            var $select;
            if(arg.$el.is('select')){
                $select = arg.$el;
            }else if(arg.select){
                // <select> 構築する。
                console.warn('@Deplicated: 非推奨オプション！ $el に <select> 要素を指定してください。');
                var attrs = clutil.text.toElemAttrs(arg.select.attributes);
                var className = arg.select.className || '';
                var textHtml = clutil.fmt('<select {0} class="{1}"></select>', attrs, className);
                arg.$el.html(textHtml);
                $select = arg.$el.find('select');
            }

            // 選択肢が指定されている場合
            if(arg.selection){
                var selArg = arg.selection;
                var items = selArg.items || [];
                var altNameDef = _.defaults(selArg.altPropName || {}, { id:'id',code:'code',name:'name' });
                var key = selArg.key || 'id', valkey = altNameDef[ key ] || altNameDef.id;
                if(selArg.hasNullItem){
                    // 未選択値を追加する
                    var nullObject = _.isObject(selArg.hasNullItem) ? selArg.hasNullItem : {};
                    if(!_.has(nullObject, altNameDef.id)){
                        nullObject[altNameDef.id] = NaN;
                    }
                    if(!_.has(nullObject, altNameDef.code)){
                        nullObject[altNameDef.code] = '';
                    }
                    if(!_.has(nullObject, altNameDef.name)){
                        nullObject[altNameDef.name] = '&nbsp;';
                    }
                    nullObject._isNullItem = true;
                    items = [ nullObject ].concat(items);
                }
                var labelFmt = selArg.labelFormat || 'name';
                var textHtml = '';
                for(var i=0; i<items.length; i++){
                    var item = items[i];
                    if(_.isArray(item) || _.isArray(item.items)){
                        // optgroup 想定: １段まで。
                        var label, citems;
                        if(_.isArray(item)){
                            label = "";
                            citems = item;
                        }else{
                            label = item.label || "";
                            citems = item.items;
                        }
                        if(citems.length > 1){
                            textHtml += '<optgroup label="' + label + '">';
                        }
                        for(var j=0; j<citems.length; j++){
                            var cItem = citems[j];
                            var labelName = clutil.text.labelTextBuilder(labelFmt, cItem, altNameDef);
                            var fixVal = cItem[ valkey ];
                            if(fixVal == null){
                                fixVal = 0;      // システムとして、区分値系は 0 未選択としている
                            }else if(_.isNaN(fixVal)){
                                fixVal = '';
                            }

                            var attrObj = {};
                            if(!_.isEmpty(cItem.className)){
                                var className = _.isArray(cItem.className) ? cItem.className : cItem.className.toString().split(' ');
                                attrObj['class'] = className;
                            }
                            if(cItem._isNullItem){
                                if(attrObj['class'] == null){
                                    attrObj['class'] = [];
                                }
                                attrObj['class'].push('cl_nullitem');
                            }

                            textHtml += '<option value="' + fixVal + '" ' + clutil.text.toElemAttrs(attrObj) + '>' + labelName + '</option>';
                        }
                        if(citems.length > 1){
                            textHtml += '</optgroup>';
                        }
                    }else{
                        var labelName = clutil.text.labelTextBuilder(labelFmt, item, altNameDef);
                        var fixVal = item[ valkey ];
                        if(fixVal == null){
                            fixVal = 0;      // システムとして、区分値系は 0 未選択としている
                        }else if(_.isNaN(fixVal)){
                            fixVal = '';
                        }

                        var attrObj = {};
                        if(!_.isEmpty(item.className)){
                            var className = _.isArray(item.className) ? item.className : item.className.toString().split(' ');
                            attrObj['class'] = className;
                        }
                        if(item._isNullItem){
                            if(attrObj['class'] == null){
                                attrObj['class'] = [];
                            }
                            attrObj['class'].push('cl_nullitem');
                        }

                        textHtml += '<option value="' + fixVal + '" ' + clutil.text.toElemAttrs(attrObj) + '>' + labelName + '</option>';
                    }
                }
                $select.html(textHtml);
            }else{
                ; // HTML 上に選択肢が予め設定してあるものと見做す。
            }

            // selectpicker 初期化
            var selectpickerApi = $select.data('selectpicker');
            if(selectpickerApi){
                // 既に初期化済の場合はリフレッシュする
                $select.selectpicker('refresh');
            }else{
                // selectpicker をはじめて作る
                var selectpickerOpt = _.extend({
                    noneSelectedText: ''
                }, arg.selectpicker);
                var selectNameAttr = $select.attr('name');
                if(selectNameAttr && _.isEmpty(selectpickerOpt.style)){
                    selectpickerOpt.style = 'btn-' + selectNameAttr;
                }
                $select.selectpicker(selectpickerOpt);

                // カラーピッカーの場合、selectpicker 内部 $button に選択値の色を設定する。
                function __colorPickerButtonCss($c, colorName){
                    if($c.hasClass('select--color')){
                        colorName = colorName || $c.selectpicker('val');
                        var color = clutil.color.sampleMap[colorName];
                        var api = $c.data('selectpicker');
                        var $btn = api.$button;
                        $btn.removeClass(clutil.color.cssClasses().join(' '));
                        if(color != null && (color.bgcolor != null || color.fgcolor != null)){
                            $btn.addClass(colorName);
                        }
                    }
                    return $c;
                }

                // '.open' クラス外しと、選択肢の open/close イベント付け
                $select.parents('.bootstrap-select').find('.open').removeClass('open');
                $select.off('show.bs.select').off('hidden.bs.select');
                $select.on('show.bs.select', function (e) {
                    $(this).parents('.bootstrap-select').find($('.dropdown-menu')).addClass('open');
                }).on('hidden.bs.select', function(e){
                    var $this = $(this);
                    $this.parents('.bootstrap-select').find($('.dropdown-menu')).removeClass('open');

                    // カラーピッカー
                    // selectpicker 内部ボタンの色を選択色にセットする。
                    __colorPickerButtonCss($this);
                });

                // カラーピッカー
                // $select.selectpicker('val') で値変更時、selectpicker 内部ボタンにも選択値の色を設定する。
                if($select.hasClass('select--color')){
                    $select.off('rendered.bs.select').on('rendered.bs.select', function(e){
                        __colorPickerButtonCss($(e.currentTarget));
                    })
                }
            }

            return $select;
        },

        /**
         * selectpicker の選択肢ドロップダウンメニューを開く
         * @param $select selectpicker 初期化済の<select>要素
         */
        showSelectpickerDropdown: function($select){
            if(!$select.is('select')){
                return;
            }
            var selectpickerApi = $select.data('selectpicker');
            if(selectpickerApi == null){
                return;
            }
            if($select.selectpicker('isDisabled')){
                return;
            }
            selectpickerApi.$button.focus().click();
        },

        /**
         * 色指定用 selectpicker 初期化
         * @param $el		: 初期化対象のjQueryオブジェクト
         */
        initColorpicker: function($el){
            if(!$el.is('select')){
                throw 'clutil.view.initColorpicker: Invalid argument, $el is not <select>';
            }

            // 選択肢データ
            var items = _.map(clutil.color.samples, function(c){
                return {
                    id: c.name,
                    name: ''
                };
            });

            // selectpicker 初期化
            clutil.view.initSelectpicker({
                $el: $el.addClass('select--color'),
                selection: {
                    items: items,
                }
            });

            return $el;
        },

        /**
         * 選択アイテムプールView
         */
        SelectionItemsView: clutil.view.AbstractView.extend({
            templates: {
                body: _.template(''
                    + '<div class="col-md-10">'
                    +   '<ul class="drilldown-list" id="ca_drilldownlist"></ul>'
                    + '</div>'
                    + '<div class="col-md-2 text-right">'
                    +   '<button class="btn btn-sm btn-flat-danger" id="ca_drilldown_delall" style="margin-top: 12px;">すべて削除</button>'
                    + '</div>'
                ),
                item: _.template('<li data-key="<%= key %>"><%- label %><span class="icon icon-abui-cross"></span></li>')
            },
            events: {
                'click #ca_drilldown_delall'    : '_onClickDellAll',
                'click #ca_drilldownlist li'    : '_onClickDellItem'
            },
            initialize: function(opt){
                // ＜オプション＞
                // ・ラベルフォーマット              -- 省略時は 「code：name」
                // ・id, code, name プロパティ名別名 -- 省略時は id, code, name
                // ・ユニークキー                    -- 省略時は item.id でマッチングする
                //
                var o = opt || {};
                this.options = _.defaults(o.options || {}, {
                    labelFormat: 'code：name',
                    altNameDef: { code:'code', name:'name' },
                    itemUniqKeys: [ 'id' ]
                });

                this.itemKeyMap = {};		// 重複チェック用マップ
            },
            render: function(){
                this.$el.html(this.templates.body({}));

                // 内部 jQuery 予約
                this.$poolUL = this.$('#ca_drilldownlist');

                // 初期エントリ追加
                _.each(this.collection || [], function(item){
                    this.addItem(item);
                }, this);

                this.$el.addClass('cl_view').data('cl_view', this);

                return this;
            },
            // 全て削除
            _onClickDellAll: function(ev){
                this.clear();
            },
            // 1つ削除
            _onClickDellItem: function(ev){
                var $li = $(ev.currentTarget);
                var key = $li.data('key');

                // view から削除
                $li.remove();

                // 内部データから指定要素を削除
                if(!_.isEmpty(key)){
                    delete this.itemKeyMap[key];
                }
            },
            _buildItemKey: function(item){
                var keys = [];
                var uniqKeys = this.options.itemUniqKeys || [];
                for(var i=0; i<uniqKeys.length; i++){
                    var propName = uniqKeys[i];
                    keys.push(propName + '-' + (item[propName] || 'n/a'));
                }
                return keys.join('_');
            },
            /**
             * アイテムを追加する。
             * @return true:追加された、false:重複につき追加されなかった
             */
            addItem: function(item){
                // 追加アイテムに対するキー生成
                var key = this._buildItemKey(item);

                // 重複チェック
                if(_.has(this.itemKeyMap, key)){
                    return false;
                }
                this.itemKeyMap[key] = item;

                // item を view に追加
                var tmplArg = {
                    key: key,
                    label: clutil.text.labelTextBuilder(this.options.labelFormat, item, this.options.altNameDef)
                };
                var $li = $(this.templates.item(tmplArg)).data('cl_item', item);
                this.$poolUL.append($li);
            },
            /**
             * アイテムをセットする。
             */
            setItems: function(items){
                this.clear();
                for(var i=0; i<items.length; i++){
                    var item = items[i];
                    this.addItem(item);
                }
            },
            /**
             * 全クリアする。
             */
            clear: function(){
                this.itemKeyMap = {};
                this.$poolUL.empty();
            },
            /**
             * 選択アイテム一覧を返す
             */
            getItems: function(){
                var items = [];
                this.$poolUL.find('li').each(function(index, elem){
                    var $elem = $(elem);
                    var item = $elem.data('cl_item');
                    if(item != null){
                        items.push(item);
                    }
                });
                return items;
            },
            /**
             * アイテム個数を取得する。
             */
            countItems: function(){
                return _.keys(this.itemKeyMap).length;
            }
        }),

        /**
         * 年セレクタ
         * 	.$el			: 初期化対象jQueryオブジェクト、<select> 要素であること。
         * 	.hasNullItem	: [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         * .start			: [省略可] 開始日 - default 1990
         * .end				: [省略可] 終了日 - default 2070、(start <= end) であること。
         */
        initYearSelect: function(args){
            var fixArgs = _.defaults(args, {
                start: Math.floor(clcom.limits.min_date / 10000),   // 1990
                end:   Math.floor(clcom.limits.max_date / 10000)    // 2070
            });

            // 未選択要素
            var nullItem = fixArgs.hasNullItem, wkNullItem = { id: NaN, code: '' };
            if(nullItem === true){
                // 未設定選択肢 <option value>&nbsp;</option> に value を入れないよう id:NaN を指定する。
                nullItem = wkNullItem;
            }else if(_.isObject(nullItem)){
                nullItem = _.extend(wkNullItem, nullItem);
            }else if(_.isString(nullItem) && nullItem.length > 0){
                wkNullItem.name = $.trim(nullItem);
                nullItem = wkNullItem;
            }

            // 選択肢生成
            var items = [];
            for(var y=fixArgs.start; y<=fixArgs.end; y++){
                items.push({
                    id: y,
                    code: clutil.text.zeroPadding(y,4),
                    name: y + '年'
                });
            }

            // selectpicker 初期化
            return this.initSelectpicker({
                $el: fixArgs.$el,
                selection: {
                    items: items,
                    hasNullItem: nullItem
                },
                selectpicker: fixArgs.selectpicker
            });
        },

        /**
         * 月セレクタ
         * 	.$el			: 初期化対象jQueryオブジェクト、<select> 要素であること。
         * 	.hasNullItem	: [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         * .start			: [省略可] 開始日 - default 1
         * .end				: [省略可] 終了日 - default 12
         */
        initMonthSelect: function(args){
            // 引数チェック
            var fixArgs = _.defaults(args, {
                start: 1,
                end:   12
            });
            if(!(fixArgs.start >= 1 && fixArgs.start <= 12 && fixArgs.end >= 1 && fixArgs.end <= 12)){
                throw 'clutil.view.initMonthSelect(): invalid Month range[1-12], start=' + fixArgs.start + ' end=' + fixArgs.end;
            }

            // 未選択要素
            var nullItem = fixArgs.hasNullItem, wkNullItem = { id: NaN, code: '' };
            if(nullItem === true){
                // 未設定選択肢 <option value>&nbsp;</option> に value を入れないよう id:NaN を指定する。
                nullItem = wkNullItem;
            }else if(_.isObject(nullItem)){
                nullItem = _.extend(wkNullItem, nullItem);
            }else if(_.isString(nullItem) && nullItem.length > 0){
                wkNullItem.name = $.trim(nullItem);
                nullItem = wkNullItem;
            }

            // 選択肢生成
            var items = [];
            for(var m=fixArgs.start; m<=fixArgs.end; m++){
                items.push({
                    id: m,
                    code: clutil.text.zeroPadding(m,2),
                    name: m + '月'
                });
            }

            // selectpicker 初期化
            return this.initSelectpicker({
                $el: fixArgs.$el,
                selection: {
                    items: items,
                    hasNullItem: nullItem
                },
                selectpicker: fixArgs.selectpicker
            });
        },

        /**
         * 日セレクタ
         * @param args Object
         * 	.$el			: 初期化対象jQueryオブジェクト、<select> 要素であること。
         * 	.hasNullItem	: [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         * .start			: [省略可] 開始日 - default 1
         * .end				: [省略可] 終了日 - default 31、(start <= end) であること。
         * .selectpicker	: [省略可] bootstrap.selectpicker の初期化引数。
         */
        initDateSelect: function(args){
            // 引数チェック
            var fixArgs = _.defaults(args, {
                start: 1,
                end:   31
            });
            if(!(fixArgs.start >= 1 && fixArgs.start <= 31 && fixArgs.end >= 1 && fixArgs.end <= 31)){
                throw 'clutil.view.initDateSelect(): invalid DayOfMonth range[1-31], start=' + fixArgs.start + ' end=' + fixArgs.end;
            }
            if(!(fixArgs.start <= fixArgs.end)){
                throw 'clutil.view.initDateSelect(): invalid arguments [start >= end], start=' + fixArgs.start + ' end=' + fixArgs.end;
            }

            // 未選択要素
            var nullItem = fixArgs.hasNullItem, wkNullItem = { id: NaN, code: '' };
            if(nullItem === true){
                // 未設定選択肢 <option value>&nbsp;</option> に value を入れないよう id:NaN を指定する。
                nullItem = wkNullItem;
            }else if(_.isObject(nullItem)){
                nullItem = _.extend(wkNullItem, nullItem);
            }else if(_.isString(nullItem) && nullItem.length > 0){
                wkNullItem.name = $.trim(nullItem);
                nullItem = wkNullItem;
            }

            // 選択肢生成
            var items = [];
            for(var d=fixArgs.start; d<=fixArgs.end; d++){
                items.push({
                    id: d,
                    code: clutil.text.zeroPadding(d, 2),
                    name: d + '日'
                });
            }

            // selectpicker 初期化
            return this.initSelectpicker({
                $el: fixArgs.$el,
                selection: {
                    items: items,
                    hasNullItem: nullItem
                },
                selectpicker: fixArgs.selectpicker
            });
        },

        /**
         * 年月日セレクタ
         * @param args Object
         *  .start              : [省略可] 開始年月日 - default 19900102
         *  .end                : [省略可] 終了年月日 - default 20701231
         *  .year
         *      .$el            : 年セレクタ <select> 要素
         *      .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         *  .month
         *      .$el            : 月セレクタ <select> 要素
         *      .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         *  .date
         *      .$el            : 日セレクタ <select> 要素
         *      .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         * @return api 年月日セレクタのコントローラを返す。このコントローラ内部で、選択肢入れ替え処理等
         */
        initYMDSelect: function(args){
            var api = new YMDSelectPicker(args);
            api.initialize();
            return api;
        },

        /**
         * 年月セレクタ
         * @param args Object
         *  .$el            : 初期化対象jQueryオブジェクト、<select> 要素であること。
         *  .hasNullItem    : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         *  .start          : [省略可] 開始年月 - default 199001
         *  .end            : [省略可] 終了年月 - default 207012、(start <= end) であること。0時跨ぎは 2400, 翌午前1時は 2500 のように指定する。
         *  .selectpicker   : [省略可] bootstrap.selectpicker の初期化引数。
         */
        initYMSelect: function(args){
            // 引数チェック
            var fixArgs = _.defaults(args, {
                start: Math.floor(clcom.limits.min_date / 100), // 1990-01-02
                end:   Math.floor(clcom.limits.max_date / 100)  // 2070-12-31
            });
            if(!(fixArgs.start <= fixArgs.end)){
                throw 'clutil.view.initDaySelect(): invalid arguments [start >= end], start=' + fixArgs.start + ' end=' + fixArgs.end;
            }

            // 未選択要素
            var nullItem = fixArgs.hasNullItem, wkNullItem = { id: NaN, code: '' };
            if(nullItem === true){
                // 未設定選択肢 <option value>&nbsp;</option> に value を入れないよう id:NaN を指定する。
                nullItem = wkNullItem;
            }else if(_.isObject(nullItem)){
                nullItem = _.extend(wkNullItem, nullItem);
            }else if(_.isString(nullItem) && nullItem.length > 0){
                wkNullItem.name = $.trim(nullItem);
                nullItem = wkNullItem;
            }

            // 選択肢生成
            var items = [];
            for(var ym=fixArgs.start; ym<=fixArgs.end; ){
                var year = Math.floor(ym / 100), month = ym % 100;
                items.push({
                    id: ym,
                    code: clutil.text.zeroPadding(ym, 6),
                    name: year + '年' + month + '月'
                });
                if(month < 12){
                    month++;
                }else{
                    year++;
                    month = 1;
                }
                ym = year * 100 + month;
            }

            // selectpicker 初期化
            return this.initSelectpicker({
                $el: fixArgs.$el,
                selection: {
                    items: items,
                    hasNullItem: nullItem
                },
                selectpicker: fixArgs.selectpicker
            });
        },

        /**
         * 曜日選択用 selectpicker 初期化
         * @param args Object
         *  .$el            : 初期化対象のjQueryオブジェクト、<select> 要素であること。
         *  .hasNullItem    : [省略可] 未選択値指定、true:未選択つき、false:未選択なし、Object で指定: 未選択値の {id, name} の明示ができる。
         */
        initDayOfWeekSelect: function(args){
            // 引数チェック
            var fixArgs = args || {};

            // 未選択要素
            var nullItem = fixArgs.hasNullItem, wkNullItem = { id: NaN, code: '' };
            if(nullItem === true){
                // 未設定選択肢 <option value>&nbsp;</option> に value を入れないよう id:NaN を指定する。
                nullItem = wkNullItem;
            }else if(_.isObject(nullItem)){
                nullItem = _.extend(wkNullItem, nullItem);
            }else if(_.isString(nullItem) && nullItem.length > 0){
                wkNullItem.name = $.trim(nullItem);
                nullItem = wkNullItem;
            }

            // selectpicker 初期化
            return this.initSelectpicker({
                $el: fixArgs.$el,
                selection: {
                    items: MyTemplates.dayOfWeeks,
                    hasNullItem: nullItem
                },
                selectpicker: fixArgs.selectpicker
            });
        },

        /**
         * 時刻選択用 selectpicker 初期化
         * @param args Object
         *  .$el            : 初期化対象のjQueryオブジェクト、<select> 要素であること。
         *  .hasNullItem    : [省略可] 未選択値指定、true:未選択つき、false:未選択なし、Object で指定: 未選択値の {id, name} の明示ができる。
         *  .start          : [省略可] 開始時刻 - default  700
         *  .end            : [省略可] 終了時刻 - default 2300、(start <= end) であること。0時跨ぎは 2400, 翌午前1時は 2500 のように指定する。
         *  .step           : [省略可] 刻み分数 - default 30 (30分)、(step > 0) であること。
         *  .key            : [省略可] 選択値キー名 - "code" or "key" を指定。デフォルト "id" 適用。
         *  .selectpicker   : [省略可] bootstrap.selectpicker の初期化引数。
         */
        initTimeSelect: function(args){
            // 引数チェック
            var fixArgs = _.defaults(args, {
                start: clcom.limits.s_time,
                end: clcom.limits.e_time,
                step: 30,
                key: 'id'
            });
            if(!(fixArgs.start <= fixArgs.end)){
                throw 'clutil.view.initTimeSelect(): invalid arguments [start >= end], start=' + fixArgs.start + ' end=' + fixArgs.end;
            }
            if(!(fixArgs.step > 0)){
                throw 'clutil.view.initTimeSelect(): invalid arguments [step > 0], step=' + fixArgs.step;
            }

            // 未選択要素
            var nullItem = fixArgs.hasNullItem, wkNullItem = { id: NaN, code: '' };
            if(nullItem === true){
                // 未設定選択肢 <option value>&nbsp;</option> に value を入れないよう id:NaN を指定する。
                nullItem = wkNullItem;
            }else if(_.isObject(nullItem)){
                nullItem = _.extend(wkNullItem, nullItem);
            }else if(_.isString(nullItem) && nullItem.length > 0){
                wkNullItem.name = $.trim(nullItem);
                nullItem = wkNullItem;
            }

            // 選択肢生成
            var items = []
                , baseIYmd = 20171201	// 年月日部は適当
                , wkDt = clutil.date.toDate(baseIYmd, fixArgs.start, 'from')
                , edTimeMillis = clutil.date.toDate(baseIYmd, fixArgs.end, 'to').getTime()
                , prevDayOfMonth = wkDt.getDate();
            do{
                var h = wkDt.getHours(), m = wkDt.getMinutes()
                    , dDate = wkDt.getDate() - prevDayOfMonth
                    , fixHour = h + (dDate * 24)
                    , hhmm = fixHour * 100 + m;

                items.push({
                    id: hhmm,
                    code: clutil.text.zeroPadding(hhmm, 4),
                    name: clutil.text.zeroPadding(fixHour, 2) + ':' + clutil.text.zeroPadding(m, 2)
                });

                prevDayOfMonth = wkDt.getDate();
                wkDt.setMinutes(m + fixArgs.step);
            }while(wkDt.getTime() <= edTimeMillis);

            var maxLimitCount = 60 * 24;		// 選択肢上限数: 丸1日の分数としておく
            if(items.length > maxLimitCount){
                throw 'clutil.view.initTimeSelect(): 選択肢数超過(上限 ' + maxLimitCount + ' 個まで)';
            }

            // selectpicker 初期化
            return this.initSelectpicker({
                $el: fixArgs.$el,
                selection: {
                    items: items,
                    hasNullItem: nullItem,
                    key: args.key
                },
                selectpicker: fixArgs.selectpicker
            });
        },

        /**
         * datepickerの作成
         * 引数
         * ・$view : 表示エリアのjQueryオブジェクト (例：$('#viewarea'))
         * 戻り値
         * ・datepickerオブジェクト
         */
        datepicker: function($view, min_date, max_date) {
            if($view.hasClass('hasDatepicker')){
                // datepicker 処理済。
                return $view;
            }
            if(!$view.parent().hasClass('datepicker_wrap')){
                $view.wrap('<div class="datepicker_wrap">');
            }

            var opt;
            if(arguments.length === 2 && _.isObject(arguments[1])){
                opt = arguments[1];
            }else{
                opt = {};
                if(min_date != null){
                    opt.minDate = clutil.date.toDate(min_date);
                }
                if(max_date != null){
                    opt.maxDate = clutil.date.toDate(max_date);
                }
            }

            var dpopt = _.extend({
                dateFormat: 'yy/mm/dd （D）',
                defaultDate: (clcom.ope_date > 0) ? clutil.date.toDate(clcom.ope_date) : null,
//              changeMonth: true,
//              changeYear: true,
                minDate: clcom.limits.minDate,
                maxDate: clcom.limits.maxDate,
                autoSize: false,
                showOn: 'button',
//              buttonImage: '/system/images/icn_s_calendar.png',
                buttonText: '',//clmsg.cl_datepicker_button_text,
//              buttonImageOnly: true,
                firstDay: 1
//              numberOfMonths: 3, // 表示月数
//              showCurrentAtPos: 1 // 先月から表示する
//              defaultDate: clutil.ymd2date(clcom.getOpeDate())
            },opt);
            if(dpopt.minDate != null && !_.isDate(dpopt.minDate)){
                dpopt.minDate = clutil.date.toDate(dpopt.minDate);
            }
            if(dpopt.maxDate != null && !_.isDate(dpopt.maxDate)){
                dpopt.maxDate = clutil.date.toDate(dpopt.maxDate);
            }
            dpopt.yearRange = dpopt.minDate.getFullYear() + ':' + dpopt.maxDate.getFullYear();

            // datepicker のカレンダが閉じたときに focus 行方不明対策
            // カレンダーが閉じた後のフォーカスはカレンダボタンへ付与する
            dpopt.onClose = function(val, inst){
                console.log('datepicker.onClosed: ', arguments);
                var fmt = inst.input.datepicker('option', 'dateFormat') || '';
                if(!_.isEmpty(val) && /^yy\/mm\/dd\ /.test(fmt)){
                    var fixVal = val.split(' ')[0];
                    inst.input.val(fixVal);
                }
                inst.input.focus();
            };

            // ここで Datepicker 初期化
            $view.datepicker(dpopt);

//            // 入力長制限を注入
//            var size = $view.prop('size');
//            if(size){
//                $view.attr('maxlength', size);
//            }

            // 初期値を運用日に設定
            var iniDate = clutil.date.toDate(clcom.ope_date);
            if(iniDate.getTime() < dpopt.minDate.getTime()){
                iniDate = dpopt.minDate;
            }else if(iniDate.getTime() > dpopt.maxDate.getTime()){
                iniDate = dpopt.maxDate;
            }
            $view.datepicker('setDate', iniDate);

            return $view;
        },

        /**
         * 階層関係を持ったマスタ用セレクタ（静的）
         * @param arguments Object, ...
         *  {
         *      $el             : <select> 要素
         *      hasNullItem     : [省略可] 未選択値設定、true:未選択つき、false:未選択なし、Object で指定:未選択値の {id, name} 明示。
         *                        第２要素以降は必ず空選択が入る。
         *      altPropName     : 当該マスタにおける { id, code, name } に相当するプロパティ名
         *      collection      : 当該階層の全選択肢                                                 ... TODO 動的取得対応にも検討する。
         *      labelFormat     : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
         *  },
         *  ...
         *  @return api
         *
         *  イベント
         *  api.model.listenTo(listener, 'change', callback)
         */
        hierarcicalSelectpickers: function(){
            return new HierarcicalSelectpickers(_.toArray(arguments));
        },

        /**
         * 汎用 combobox 初期化ユーティリティ（テキストボックス付き）
         * @param arg
         * .$el             : 初期化対象のjQueryオブジェクト、<div> ラッパー要素であること。
         * .propName        : JSON 化するときのプロパティ名。省略時は el.id から "^ca_" を取り除いた要素をプロパティ名とする。
         * .parentPropName  : 親階層のプロパティ名。リレーション事情がある場合に指定する。
         * .model           : モデルデータ。他セレクタＵＩと連動する変更伝播の仲介役となる。省略時は {propName: value} モデルを内部生成する。
         * .textbox
         *   .placeholder   : プレースホルダ―
         *   .elemId        : 外部ラベル <label for="elem_id"> for 属性との関連付けのための id。
         * .selection       : 選択肢定義
         *   .items         : 選択肢配列。原則 'id', 'name' プロパティを持つものとする。
         *   .altPropName   : items[x] id, code, name の別名プロパティ定義。例えば、選択肢要素 id が 'staff_id' のような場合に id 別名定義として指定することができる。
         *   .hasNullItem   : 未選択値指定、true:未選択つき、false:未選択なし、Object 指定で未選択値の {id, name} の明示ができる。
         *   .labelFormat   : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
         * .retriever       : 選択肢を動的にとる場合は指定。
         *   .matchingPolicy: '' or '' - 先頭一致、中間一致、デフォルトは先頭一致
         *   .triggerLength : 選択肢取得トリガーとする文字数、0 で全選択肢をとる。n-1 長になったら選択肢は０件にして選択状態も未選択にする。
         *   .uri           : クエリ発行先の API 名
         *   .reqBuilder    : クエリのリクエストデータ作成方法を関数で指定する。
         */
        initComboBox: function(arg){
            // TODO
        },
        /**
         * リレーション版の汎用 combobox 初期化ユーティリティ（テキストボックス付き）
         * @param arg
         * .model           : リレーション間で共有するモデルオブジェクト（Backbone.Model）
         * .args            : 個々の combobox 初期化パラメータ。内部 model インスタンスは省略すること。
         */
        rIniComboBox: function(arg){
            var model = arg.model;
            var args = arg.args;

            // model オブジェクトをリレーション combobox 間で共有化する
            if(model == null){
                model = new Backbone.Model();
            }
            var dto = model.toJSON();
            for(var i=0; i<args.length; i++){
                var arg = args[i];
                var propName = arg.propName;
                if(propName == null){
                    propName = arg.$el[0].id.replace(clutil.defaultCaPrefix, '');
                }
                if(!_.has(dto, propName)){
                    model.set(propName, undefined);  // 明示的にプロパティを与える
                }
                arg.model = model;
            }

            // コンボボックス初期化
            for(var i=0; i<args.length; i++){
                var arg = args[i];
                this.initComboBox(arg);
            }
        }
    });

    /**
     * 選択肢の取得エンジン部
     * @param options
     *  .triggerLength
     *  .uri
     *  .reqBuilder         : uri への Ajax リクエストを作る関数。
     *  .resultsPropName    : uri 応答結果の中の選択肢プロパティ名
     */
    var CollectionRetriever = function(options){
        if(options == null || !_.isObject(options)){
            throw 'Invalid Arguments: empty options.';
        }
        if(_.isEmpty(options.uri)){
            throw 'Invalid Arguments: "uri" not specified.';
        }
        if(_.isFunction(options.reqBuilder)){
            throw 'Invalid Arguments: reqBuilder not function.';
        }
        this.options = _.defaults(options, { triggerLength: 1, resultsPropName: 'results' });
        this.vent = new Backbone.Events();

        // TODO: まだまだ

        // reqBuilder はリクエスト条件を作成するビルダ関数
        // @param token 検索トークン
        // @param model 検索トークン以外の検索条件（リレーション情報: 親階層の ID を保持したりなど）
        //reqBuilder = function(token, model);
    };
    _.extend(CollectionRetriever.prototype, {
        /**
         * 取得要求を送出する（非同期）
         * @param token     検索ワード
         * @param model     リレーション情報モデルデータ(POJO)
         * @return
         *  'not_modified'  変更なし → UI 側は何も変更しないでよい
         *  'unaccept'      受入不可（必須条件となる親 id が無いなどの理由）→ UI 側は選択肢は空にして disabled 化する
         *  'accept'        要求送出中 → this.vent で応答イベント上るのを受け取る
         */
        query: function(token, model){
            // 検索ワード評価
            var fixToken = token || '';
            var tlen = this.options.triggerLength;
            if(tlen > 0 && token.length > tlen){
                // トリガー長切り出し
                fixToken = token.substr(0,tlen);
            }
            if(fixToken == this.savedToken){
                // 前回検索結果と同一
                return'not_modified'; // Not Modified
            }

            // 検索リクエストを生成する
            var req = this.reqBuilder(token, model);
            if(req == null){
                // 検索条件生成できない（model.親要id が足りないなど）
                // このとき、関連ずく UI の選択肢は空欄にする。
                console.warn('Unaccepted.');
                return 'unaccept';
            }

            // 検索
            var uri = this.options.uri;
            clutil.postJSON(uri, req).done(_.bind(function(data){
                // 選択肢リストを取り出す
                var results = data[this.options.resultsPropName];
                if(_.isUndefined(results)){
                    console.warn('It may be a protocol gap ?');
                    results = [];
                }

                // 応答通知
                this.savedToken = token;
                this.savedReq = uri;
                this.savedResults = results;
                this.vent.trigger('OK', results, this);
            },this)).fail(_.bind(function(data){
                console.warn('uri[' + this.uri + '] failed: ' + clutil.getclmsg(data));

                // エラー通知: 空選択肢でセットすること想定。
                this.vent.trigger('NG', [], this);
            },this));
            return 'accept';    // 検索要求送出中
        },
        getResults: function(){
            return this.savedResults;
        },
        getSavedReq: function(){
            return this.savedReq;
        },
        getSavedToken: function(){
            return this.savedToken;
        }
    });

    /**
     * TODO: 作業中につき、使用不可！！！
     * 汎用 combobox View クラス
     * opt
     *  .model                  : リレーション情報 Backbone.Model（省略可）リレーション事情はモデル側で吸収する。（子階層の id をクリアするなど）
     *  .options
     *    .propName             : [必須] JSON 化するときのプロパティ名
     *    .parentPropName       : 親階層のプロパティ名。リレーション事情がある場合に指定する。
     *                              ・親選択値が変更 ⇒ 子選択肢変更し、子自身は未選択状態へ。
     *                              ・親が未選択に   ⇒ 子選択肢クリアし、子自身は disabled に。
     *    .matchingPolicy       : 選択肢候補絞込ポリシー - 前方一致:'forwd', 中間一致:'mid', 末尾一致:'last'、デフォルト('forwd')
     *    .retriever            : CollectionRetriever サーバ検索エンジンとなるインスタンス（省略時は selection.items 固定と見做す）
     *    .textbox              : [省略可] textbox オプション
     *      .placeholder        : [省略可] テキスト部のプレースホルダ―
     *      .elemId             : 外部ラベル部と同調させるための <label for="elemId"> に相当する id を想定。
     *    .selection            : [必須] selectpicker オプション
     *      .items              : 選択肢配列。原則 'id', 'name' プロパティを持つものとする。retriever 指定時は空配列を指定すること。
     *      .altPropName        : items[x] id, code, name の別名プロパティ定義。例えば、選択肢要素 id が 'staff_id' のような場合に id 別名定義として指定することができる。
     *      .hasNullItem        : 未選択値指定、true:未選択つき、false:未選択なし、Object 指定で未選択値の {id, name} の明示ができる。
     *      .labelFormat        : 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
     */
    var ComboBoxView = clutil.view.AbstractView.extend({
        className: 'combobox cl_ComboBoxView',
        template: _.template(''
            + '<input type="text" placeholder="<%= placeholder %>" class="form-control cl_v2d_ignore" id="<%= elemId %>">'
            + '<select name="input" class="select-block cl_v2d_ignore"></select>'
        ),
//        model: Backbone.Model({ propName: value })
        events: {
            'keyup input:text'      : '_onKeyup',
            'change select'         : '_onChangeSelect',
            'show.bs.select select' : '_onShowingDropdown'
        },
        /**
         * opt
         */
        initialize: function(opt){
            clutil.bindAll(this);

            // TODO: 引数チェック
            this.options = opt.options;


            // モデルデータを整える
            if(this.model == null){
                this.model = new Backbone.Model();
            }else if(! this.model instanceof Backbone.Model){
                this.model = new Backbone.Model(this.model);
            }

            // モデルの変更イベント購読
            this.listenTo(this.model, 'change', this._onChangeModel);

            // window リサイズイベントでテキストボックス要素幅の調整と連動
            this.listenTo(clutil.mediator, {
                'window:lazyresize': this.adjustSize
            });

            // TODO: 選択肢取得エンジンからの応答監視
            if(this.options.retriever){
                var vent = this.options.retriever.vent;
                this.listenTo(vent, {
                    'OK': this._updateCollection,
                    'NG': this.clear
                });
            }
        },
        render: function(){
            var bindObj = _.defaults(this.options.textbox||{}, {
                placeholder: '',
                elemId: ''
            });
            this.$el.html(this.template(bindObj));

            // インスタンス変数
            this.$text = this.$('input:text');
            this.$select = this.$('select');

            // selectpicker 初期化
            var selectionArg = this.options.selection;
            if(selectionArg.hasNullItem){
                // hasNullItem 指定の場合、NullItem の id 値を強制 NaN とし、value を持たないようにしておく。
                var idPropName = (selectionArg.altPropName) ? selectionArg.altPropName.id || 'id' : 'id';
                if(!_.isObject(selectionArg.hasNullItem) /* hasNullItem === false */){
                    selectionArg.hasNullItem = {};
                }
                selectionArg.hasNullItem[idPropName] = NaN;
            }
            clutil.view.initSelectpicker({
                $el: this.$select,
                selection: this.options.selectionArg
            });

            // bootstrap select API インスタンス
            this.bs = this.$select.selectpicker('selectpicker');

            this.$el.data('cl_ComboBoxView', this);     // api を注入

            // レトリバー指定の場合: 選択肢注入処理
            if(this.options.retriever){
                var retrv = this.options.retriever;
                // TODO -- とりあえず、静的コレクションによるやり方で実装していくか...
            }

            return this;
        },
        /*
         * モデル変更イベント購読
         * <select> 選択肢で選んだ表示ラベルをテキストボックスへコピーする
         */
        _onChangeModel: function(model){
            var changed = model.changedAttributes();
            if(!changed){
                return;
            }

            // 親要素の変更を検知 ⇒ 自身は選択肢を再取得し、設定値をクリアする
            if(this.options.parentPropName){
                var pPropName = this.options.parentPropName;
                if(_.has(changed, pPropName)){
                    // XXX
                    // 親値が undefined ⇒ 自身は空欄選択肢をセットして、自身は undefined になる。
                    // 親値が何か設定変更されていたら、選択肢を作り直して、自身は undefined になる。
                }
                return;
            }

            // 自分自身の変更を反映
            var propName = this.options.propName;
            if(_.has(changed, propName)){
                var val = changed[propName];
                var $op = this.$select.find('option[value="' + val + '"]');
                var text = ($op.length === 0 || $op.hasClass('cl_nullitem')) ? '' : $op.text();
                this.$text.val(text);
            }
        },
       /*
        *
        */
       _onKeyup: function(ev){
           // TODO
           // 選択肢フィルタする
           // 選択肢ヒットするものがあれば、selectpicker を開く

           // 候補選択を retriever へ問い合わせる
       },
        /*
         * セレクト選択変更
         * モデルに変更を反映する。テキストボックスへの表示ラベル反映はモデル経由 change イベントで行う。
         */
        _onChangeSelect: function(ev){
            var val = ev.currentTarget.value;
            console.info(val);

            // モデルに対して変更する。
            // テキスト一致化は、モデル変更イベントを受けて行う。
            var propName = this.options.propName;
            this.model.set(propName, val);
        },
        /*
         * セレクトの選択肢メニューが開くとき
         * ・選択肢未取得の場合は選択肢構築のトリガーを引く
         */
        _onShowingDropdown: function(ev){
            // 選択肢未取得の場合は選択肢構築のトリガーを引く
        },
        /**
         * テキストボックスのサイズ調整をする。
         */
        adjustSize: function(){
            var width = this.bs.$button.outerWidth() - 30;
            this.$text.outerWidth(width);
        },
//        /**
//         * clutil.view2data() ヘルパー
//         */
//        view2data: function(prefix){
//            // TODO
//        },
//        /**
//         * clutil.data2view() ヘルパー
//         */
//        data2view: function(dto, prefix){
//            // TODO
//        },
        /**
         * 活性/非活性状態を設定する。
         * @param enable true:活性化、false:非活性化
         */
        setEnable: function(enable){
            var api = this.$select.data('selectpicker');
            if(api == null){
                return; // selectpicker 化していない
            }
            api.$button.css('visivility', (enable===false) ? 'hidden' : '');
            this.$text.prop('readonly', enable===false);
        },
        /**
         * テキスト入力内容と select 設定値が合致しているかどうか検査する
         * @return true:合致、false:不一致
         */
        isCorrect: function(){
            // テキストの入力内容 / 内部 selectpicker の選択テキスト
            var inputText = this._getInputText(), selectedLabel = this._getSelectedText();
            return (inputText == selectedLabel);
        },
        /**
         * 選択値を設定する
         */
        setValue: function(value){
            if(_.isString(value)){
                value = $.trim(value);
            }
            if(_.isEmpty(value)){
                this.clear();
                return;
            }

            var retriever = this.options.retriever;
            if(retriever != null){
                // TODO: value に対する { id, code, name } を取る、、、どうやって？？？
                // TODO: 選択肢を取りに行く、、、どうやって？？？
            }else{
                // 静的な選択肢
                this.$select.selectpicker('val', value);
                var $option = this.$select.find('option:selected');
                if($option.length === 1){
                    var text = $option.text();
                    this.filterSelection(text);
                    this.$text.text(text);
                }else{
                    this.filterSelection();
                    this.$text.text('');
                    value = undefined;
                }
                this.model.set(this.options.propName, value);   // モデルへ変更を伝播させる
            }
        },
        /**
         * 選択値を取得する
         * @return 選択値、または undefined
         */
        getValue: function(){
            if(this.isCorrect()){
                return this.$select.selectpicker('val');
            }
        },
        /**
         * クリア
         */
        clear: function(){
            var retriever = this.options.retriever;
            if(retriever != null){
                var trigLen = retriever.triggerLength;
                var selectArg = this.options.selection;
                if(trigLen > 0){
                    // XXX 選択肢を空っぽにする
                }else{
                    // XXX はじめから全選択肢を取得済みと想定する。
                }
            }else{
                // 静的選択肢の場合
                this.filterSelection(); // 選択肢は全部表示させる
                this.$select.selectpicker('val', null);
            }
            this.$text.text('');
            this.model.set(this.options.propName, undefined);   // モデルへ変更を伝播させる
        },
        // <select> の選択肢を開く/閉じる
        // @param stat 'open':開く、'close':閉じる、'toggle':トグル
        _switchDropdownMenu: function(stat){
            var api = $select.data('selectpicker');
            if(api == null){
                return; // selectpicker 化していない
            }
            switch(stat){
            case 'open':
                if(!api.$menu.hasClass('open')){
                    api.toggle();
                }
                break;
            case 'close':
                if(api.$menu.hasClass('open')){
                    api.toggle();
                }
                break;
            case 'toggle':
                api.toggle();
                break;
            }
        },
        // テキスト入力値を取得する
        _getInputText: function(){
            return $.trim(this.$text.val());
        },
        // 内部 selectpicker の選択テキストを取得する
        _getSelectedLabel: function(){
            var val = this.$select.selectpicker('val');
            var $op = this.$select.find('option[value=' + val + ']');
            return ($op.hasClass('cl_nullitem')) ? '' : $.trim($op.text());
                // クラス 'cl_nullitem' 付きの場合は未選択肢なので空欄 '' を戻す。
        },
        // 選択肢フィルタ
        // 設定済選択肢内でフィルタ行使する。
        filterSelection: function(token, selectSupport){
            token = $.trim(token);
            var itemCount = 0, $option = this.$select.find('option:not(.cl_nullitem)');
            if(_.isEmpty(token)){
                // 全開示
                itemCount = $option.show().length;
                this.$select.find('optgroup').show();
            }else{
                // フィルタをかける
                // 前方一致:'forwd', 中間一致:'mid', 末尾一致:'last'
                var policy = this.options.matchingPolicy;
                var matcher
                switch(policy){
                case 'mid':     // 中間一致
                    matcher = new RegExp(token);
                    break;
                case 'last':    // 末尾一致
                    matcher = new RegExp(token + '$');
                    break;
                default:
                case 'forwd':   // 前方一致
                    matcher = new RegExp('^' + token);
                }

                // 選択肢要素で for-each してフィルタ対象は show、それ以外は hide する。
                $option.each(function(idx, elem){
                    var $option = $(elem);
                    var label = $option.text();
                    if(matcher.test(label)){
                        $option.removeClass('displaynone').show();
                        itemCount++;
                    }else{
                        // hide 対象は選択状態も刈り取っておく
                        $option.addClass('displaynone').prop('selected', false).hide();
                    }
                });
                // 選択肢グループ: 階層＝１段のみ想定
                this.$select.find('optgroup').each(function(idx, elem){
                    var $optgrp = $(elem);
                    if($optgrp.find('option:not(.displaynone)').length === 0){
                        $optgrp.addClass('displaynone').hide();
                    }else{
                        $optgrp.removeClass('displaynone').show();
                    }
                });
            }
            this.$select.selectpicker('refresh');

            if(selectSupport){
                if(itemCount === 1){
                    // フィルタ結果が１個になったら選択確定させる
                    var label = $option.text();
                    this.$text.text(label);
                    this._switchDropdownMenu('close');  // 選択肢メニューも閉じる
                }else if(itemCount > 1){
                    this._switchDropdownMenu('open');   // 選択肢メニュー開いてあげる
                }else{
                    // itemCount <= 0: 有用な選択肢が無い
                    this._switchDropdownMenu('close');  // 選択肢メニュー閉じる（不要かも）
                }
            }else{
                this._switchDropdownMenu('close');
            }
        }
    });

}());
/**
 * 共通 View クラス - table 関連
 */
var clutil = _.extend({}, clutil);
$(function () {

    /*
     * clutil.view.setHilightTableCell イベントハンドラ群
     */
    var HilightTableCellHandlers = {
        // hover 時に付加する CSS 制御用のクラス
        hoverClass: "hover",
        // hover イベントは、mouseenter, mouseleave イベントのエイリアスであるとのこと。
        hoverEventsMap: {
            // TD 要素にマウスが乗った
            mouseenter: function(ev){
                var $td = $(ev.currentTarget)
                    , $tbody = $td.closest('tbody')
                    , $table = $tbody.closest('table')
                    , $row = $td.siblings()
                    , colIndex = $td.index()
                    , $col = $tbody.find('tr td:nth-child(' + (colIndex + 1) + ')');
                $row.addClass(HilightTableCellHandlers.hoverClass);
                $col.addClass(HilightTableCellHandlers.hoverClass);

                // mouseleave 時の jQuery セレクタ操作を省略するために、
                // 現在の Hilight 対象 TD を $table.data にバインドしておく。
                $table.data('cl_HilightTableCell', {
                    row: $row,
                    col: $col
                });
            },
            // TD 要素からマウスが離れた
            mouseleave: function(ev){
                var $table = $(ev.currentTarget).closest('table');
                var data = $table.data('cl_HilightTableCell');
                if(data){
                    data.row && data.row.removeClass(HilightTableCellHandlers.hoverClass);
                    data.col && data.col.removeClass(HilightTableCellHandlers.hoverClass);
                    $table.removeData('cl_HilightTableCell');
                }
            }
        },
        // HilightTableCell イベントハンドラを破棄する
        destroy: function(ev){
            clutil.view.setHilightTableCell(ev.currentTarget, 'off');
            console.info('HilightTableCell: destroyed.');
        }
    };

    _.extend(clutil.view, {
        /**
         * テーブルセルのハイライトを付加/削除する。
         * @param table table 要素
         * @param enable 'on':ハイライトを設定する、'off':ハイライト設定を解除する。
         */
        setHilightTableCell: function(table, enable){
            var $table = $(table);
            if(!$table.is('table')){
                throw 'clutil.view.setEnableHilightCell(): table is not <table>.';
            }

            // ハイライト設定を削除
            $table.removeClass('table-hover-cell')
                .off(HilightTableCellHandlers.hoverEventsMap, 'tbody td')
                .off('remove', HilightTableCellHandlers.destroy)
                .removeData('cl_HilightTableCell');

            // ハイライト設定を付加
            if(enable === true || enable == 'on'){
                $table.addClass('table-hover-cell')
                    .on(HilightTableCellHandlers.hoverEventsMap, 'tbody td')
                    .on('remove', HilightTableCellHandlers.destroy);
            }
        },

        /**
         * テーブル View クラス
         * <table>タグを $el とし、行選択、行追加のあるテーブルの動作をサポートする。
         */
        TableView: clutil.view.AbstractView.extend({
            events: {
                'toggle thead tr:first-child input:checkbox'    : '_onToggleSelectAll',     // 全選択
                'toggle tbody tr td:first-child input:checkbox' : '_onToggleSelectRow',     // 行選択（multi）
                'change tbody tr td:first-child input:radio'    : '_onToggleSelectARow',    // 行選択（single)
                'click tbody tr td:last-child a.ca_btn_plus:not([disabled])'
                                                                : '_onClickAddItem',        // 選択行アイテム追加ボタン
                'click tbody tr td:last-child a.ca_btn_minus:not([disabled])'
                                                                : '_onClickDelItem',        // 選択行アイテム削除ボタン
                'click tbody tr:not([disabled])'                : '_onClickRow'             // 行クリック
            },
            /**
             * コンフィギュレーション
             * 値を変更して運用する場合は、clstart-[proj].js で、以下のように変更する。
             * ＜設定変更例＞
             * var myTableViewConfig = { selectedRowClass: 'プロジェクトで変更したい値' };
             * _.extend(clutil.view.TableView.prototype.config, myTableViewConfig);
             */
            config: {
                /** 選択行 tr に付与するクラス属性 */
                selectedRowClass: 'checked'
            },
            /**
             * opt: オプション
             * .template    : 行生成のためのテンプレート
             */
            initialize: function(opt){
                var o = opt || {};
                this.options = {
                    rowSelectionPolicy: 'none'  // 行選択ポリシー: 'none', 'single', 'single_oneclick' or 'multi'
                };
                if(o){
                    // 行 <TR> 描画用のテンプレートをセット
                    if(o.template){
                        if(_.isString(opt.template)){
                            this.template = _.template(opt.template);
                        }else{
                            this.template = opt.template;
                        }
                    }
                    // 行選択ポリシー
                    if(o.options){
                        _.extend(this.options, o.options);
                    }
                }
                if(this.collection == null){
                    this.collection = [];
                }else{
                    this.collection = _.clone(this.collection);
                }
            },
            render: function(){
                if(!_.isEmpty(this.collection)){
                    this.setRecs(this.collection);
                }
                clutil.initUIelement(this.$el);
                this.$el.addClass('cl_tableview').data('cl_tableview', this);
                return this;
            },
            // 全選択: on/off
            _onToggleSelectAll: function(ev){
                // 複数選択のみ対応
                if(!this.isMultiSelectable()){
                    return;
                }
                var $cb = $(ev.currentTarget);
                var isSelected = $cb.prop('checked');
//              $cb.checkbox(isSelected ? 'check' : 'uncheck');
                this.setSelectAll(isSelected, ev);
            },
            // 行選択(checkbox): on/off
            _onToggleSelectRow: function(ev){
                if((this.options.rowSelectionPolicy || 'none') == 'none' ){
                    return;
                }
                var $cb = $(ev.currentTarget);
                $cb.closest('tr').toggleClass(this.config.selectedRowClass);

                var selectedCount = this.getSelectedCount();

                var $chkall = this.$('thead tr th:first-child input:checkbox');
                if(selectedCount === this.collection.length){
                    // 全選択チェックボックス [ON]
                    $chkall.checkbox('check');
                }else{
                    // 全選択チェックボックス [OFF]
                    $chkall.checkbox('uncheck');
                }

                // 選択状態の変化通知イベントを発火する
                this.trigger('selectChanged', this, ev);
            },
            // 行選択(radio): on/off
            _onToggleSelectARow: function(ev){
                if((this.options.rowSelectionPolicy || 'none') == 'none' ){
                    return;
                }
                var $rb = this.$(ev.currentTarget);
                var isSelected = $rb.prop('checked');
                $rb.radio(isSelected ? 'check' : 'uncheck');
                $rb.closest('tr').toggleClass(this.config.selectedRowClass);

                // 選択状態の変化通知イベントを発火する
                this.trigger('selectChanged', this, ev);
            },

            // 選択行アイテム追加ボタン
            _onClickAddItem: function(ev){
                var $tr = $(ev.currentTarget).closest('tr');
                var rec = $tr.data('cl_rec');
                if(rec != null){
                    // イベント 'addItemSelected' を発火して、対象行データをリスナに渡す
                    this.trigger('addItemSelected', rec, this);
                }
            },

            // 選択行アイテム削除ボタン
            _onClickDelItem: function(ev){
                var $tr = $(ev.currentTarget).closest('tr');
                this.removeRowAt($tr);
            },

            // 行クリック ⇒ 行選択
            _onClickRow: function(ev){
                var $target = $(ev.target);
                if($target.hasClass('ca_apphandle')){
                    // アプリ固有ハンドラにつき、行選択操作は行わない。
                    return;
                }
                var $tr = (ev.target === ev.currentTarget) ? $target : $(ev.currentTarget);
                if($tr.hasClass('addrow')){
                    // 行追加要求のイベントを発火
                    this.trigger('click:addrow', this);
                }else{
                    // 行選択をトグル
                    var $td = $(ev.target).closest('td');
                    var policy = this.options.rowSelectionPolicy;
                    switch(policy){
                    case 'single':
                        if($td.index() === 0){
                            // 先頭列は行選択コントロール input:checkbox を想定。
                            //   ⇒ それぞれの toggle イベントでハンドリングしているのでスキップ。
                            return;
                        }
                        this.$('tbody tr:not(.addrow)').removeClass(this.config.selectedRowClass);
                        var $radio = $tr.find('td:first-child input:radio');
                        $radio.radio('check');
                        // 直後、toggle イベント発火して、this._onToggleSelectRow() が呼び出される。
                        break;
                    case 'single_radio_only':
                        if($td.index() === 0){
                            // 先頭列は行選択コントロール input:checkbox を想定。
                            //   ⇒ それぞれの toggle イベントでハンドリングしているのでスキップ。
                            return;
                        }
                        // 行クリックのイベントを送出する。
                        var rec = $tr.data('cl_rec');
                        if(rec != null){
                            this.trigger('row:click', rec, this);
                        }
                        break;
                    case 'single_oneclick':
                        var rec = $tr.data('cl_rec');
                        if(rec != null){
                            this.trigger('oneClickSelected', rec, this);
                        }
                        break;
                    case 'multi':
                        if($td.index() === 0){
                            // 先頭列は行選択コントロール input:radio を想定。
                            //   ⇒ それぞれの toggle イベントでハンドリングしているのでスキップ。
                            return;
                        }
                        var $cb = $tr.find('td:first-child input:checkbox');
                        $cb.checkbox('toggle');
                        // 直後、toggle イベント発火して、this._onToggleSelectARow() が呼び出される。
                        break;
                    case 'multi_checkbox_only':
                        if($td.index() === 0){
                            // 先頭列は行選択コントロール input:radio を想定。
                            //   ⇒ それぞれの toggle イベントでハンドリングしているのでスキップ。
                            return;
                        }
                        // 行クリックのイベントを送出する。
                        var rec = $tr.data('cl_rec');
                        if(rec != null){
                            this.trigger('row:click', rec, this);
                        }
                        break;
                    default:
                    case 'none':
                    }
                }
            },
            /**
             * １行描画する。
             * 編集系に利用する場合で、セル要素に datepicker や selectpicker 等初期化処理を要するものは、本関数をオーバーライドして初期化処理を行えばよい。
             * example:
             *   var MyTableView = clutil.view.TableView.extend({
             *       initialize: function(opt){
             *           su.initialize.apply(this,arguments);
             *       },
             *       renderRow: function(rec){
             *           var $row = su.renderRow.apply(this, rec);
             *           clutil.view.initSelectpicker({ $el: $row.find('select要素'), ..初期化引数.. });
             *           clutil.data2view($row, rec);
             *           return $row;
             *       }
             *   });
             * @param rec 行データ
             * @return 行の jQuery オブジェクト
             */
            renderRow: function(rec){
                return $(this.template(rec)).data('cl_rec', rec);
            },
            /**
             * 行データをセットする。
             * ただし、テンプレート指定されていること。
             */
            setRecs: function(rows){
                if(this.template == null){
                    throw 'clutil.view.CommonTableView: template';
                }
                this.clear();
                if(_.isEmpty(rows)){
                    return;
                }

                this.collection = _.clone(rows);

                var $tbody = this.$('tbody');
                var $addRowTR = $tbody.find('tr:last-child.addrow');
                var hasAddRowTR = $addRowTR.length > 0;
                for(var rowIdx=0; rowIdx<rows.length; rowIdx++){
                    var rec = rows[rowIdx];
                    var $tr = this.renderRow(rec);
                    if(hasAddRowTR){
                        $addRowTR.before($tr);
                    }else{
                        $tbody.append($tr);
                    }
                }
                clutil.initUIelement($tbody);

                // 全選択チェックボックスのコントロール
                var $chkall = this.$('thead tr th:first-child input:checkbox');
                if($chkall.length > 0){
                    // enable 設定
                    var chkallEnable = (this.collection.length > 0);
                    this._setBtnEnable($chkall, chkallEnable);

                    // 既に全選択になっていたら $chkall を selected 表示にする。
                    var selectedCount = this.getSelectedCount();
                    if(rows.length === selectedCount){
                        $chkall.checkbox('check');
                    }
                }
            },
            /**
             * 行追加する。
             * ただし、行テンプレートが指定されていること。
             */
            appendRows: function(aRows){
                if(this.template == null){
                    throw 'clutil.view.TableView.appendRow(): null template';
                }

                var $tbody = this.$('tbody');
                var $addRowTR = $tbody.find('tr:last-child.addrow');
                var hasAddRowTR = $addRowTR.length > 0;

                if(!_.isArray(aRows)){
                    aRows = [ aRows ];
                }
                var beforeRowCount = this.collection.length;
                for(var i=0; i<aRows.length; i++){
                    var rec = aRows[i];
                    var $tr = this.renderRow(rec);
                    if(hasAddRowTR){
                        $addRowTR.before($tr);
                    }else{
                        $tbody.append($tr);
                    }
                    clutil.initUIelement($tr);
                    this.collection.push(rec);
                }
                var afterRowCount = this.collection.length;
                if(beforeRowCount === 0 && afterRowCount > 0){
                    // 行数: 0 → N へ変化。全選択チェックボックスを活性化する。
                    var $chkall = this.$('thead tr th:first-child input:checkbox');
                    this._setBtnEnable($chkall, true);
                }
            },
            /**
             * 行追加する。
             * ただし、行テンプレートが指定されていること。
             * @param row 行データ
             */
            appendRow: function(row){
                var aRows = [row];
                this.appendRows(aRows);
            },
            /**
             * クリアする。
             */
            clear: function(){
                var $tbody = this.$('tbody');
                var $addRowTR = $tbody.find('tr:last-child.addrow');

                var beforeSelectedRowCount = this.getSelectedCount();

                $tbody.empty();
                if($addRowTR.length > 0){
                    $tbody.append($addRowTR);
                }
                this.collection = [];

                var $chkall = this.$('thead tr th:first-child input:checkbox');
                if($chkall.length > 0){
                    // 行ヘッダの全選択コントロールチェックボックスが有る場合は非活性にセット
                    $chkall.checkbox('uncheck');
                    this._setBtnEnable($chkall, false);
                }

                // 選択行数: beforeSelectedRowCount 個 → 0 個に変化
                if(beforeSelectedRowCount > 0){
                    // 選択状態の変化通知イベントを発火する
                    this.trigger('selectChanged', this);
                }
            },

            // ラジオ/チェックボックス活性|非活性 enabled を設定する。
            _setBtnEnable: function($btn, enable){
                var enabled = (enable || false);
                $btn.prop('disabled', (enabled === false));
                if(enabled){
                    $btn.parent('label').removeClass('disabled');
                }else{
                    $btn.parent('label').addClass('disabled');
                }
                return $btn;
            },

            /**
             * 選択行数を返す。
             */
            getSelectedCount: function(){
                return this.$('tbody tr td:first-child input:checked').length;
            },
            /**
             * 選択状態の行データを返す。ただし、行データは $tr.data('cl_rec',val) セットしてあること。
             */
            getSelectedRecs: function(){
                var selectedRows = [];
                this.$('tbody tr td:first-child input:checked').each(function(idx, elem){
                    var $input = $(elem);
                    var $tr = $input.closest('tr');
                    var rec = $tr.data('cl_rec'), cpyRec = clutil.deepClone(rec);
                    selectedRows.push(cpyRec);
                });

                // 複数選択以外は先頭 rec のみを返す。
                if(!this.isMultiSelectable()){
                    var rec = _.first(selectedRows);
                    selectedRows = (rec) ? clutil.deepClone(rec) : undefined;
                }

                return selectedRows;
            },
            /**
             * 選択行の行ハッシュマップを返す。
             */
            getSelectedHashMap: function(map){
                map = map || {};
                var hashKeys = this.options.hashKeys;
                this.$('tbody tr td:first-child input').each(_.bind(function(idx, elem){
                    var $input = $(elem);
                    var selected = $input.is(':checked');

                    var $tr = $input.closest('tr');
                    var rec = $tr.data('cl_rec');
                    if(rec != null){
                        var hash = this._hashValue(rec);
                        if(selected){
                            map[hash] = _.pick(rec, hashKeys);
                        }else{
                            delete map[hash];
                        }
                    }
                },this));
                return map;
            },
            // 行データに対するハッシュ値を求める。
            _hashValue: function(rec){
                var hashKeys = this.options.hashKeys;
                if(hashKeys == null){
                    return;
                }
                if(!_.isArray(hashKeys)){
                    throw '[clutil.view.TableView] options.hashKeys is bad, it must be Array<string>.';
                }

                var hvv = [];
                for(var i=0; i<hashKeys.length; i++){
                    var k = hashKeys[i];
                    var v = rec[k];
                    if(v != null){
                        hvv.push(k + '#' + v);
                    }
                }
                return hvv.join(':');
            },
            /**
             * 行数を返す。
             */
            getRecCount: function(){
                return this.$('tbody tr:not(.addrow)').length;
            },
            /**
             * 全データを返す。
             * @param opt object   : 省略可
             *  .viewOrder         : 表示順序通りに行データを返す。
             * @return 行データの配列（複製）
             */
            getRecs: function(opt){
                var recs;
                if(opt && opt.viewOrder){
                    // <tr> 並び順で collection をつくる
                    recs = [];
                    this.$('tbody tr').each(function(idx, elem){
                        var rec = $(elem).data('cl_rec'), cpyRec = clutil.deepClone(rec);
                        recs.push(cpyRec);
                    });
                }else{
                    recs = clutil.deepClone(this.collection);
                }
                return recs;
            },
            /**
             * 選択状態の行の $(TR) を返す。
             */
            $getSelectedRows: function(){
                var $x = $();
                this.$('tbody tr td:first-child input:checked').each(function(idx, elem){
                    var $input = $(elem);
                    var $tr = $input.closest('tr');
                    $x = $x.add($tr);
                });
                return $x;
            },
            /**
             * 行選択: 指定行 index をの選択を ON/OFF する。
             * @param rowIndex 選択除対象とする、行内の任意の要素 or jQuery or 行インデックス
             * @param select true:選択ON、false:選択OFF
             * @param publish selectChanged イベント発火指定、true:イベント発火する、false,default:イベント発火しない。
             */
            setSelectRowAt: function(rowIndex, select, publish){
                var policy = this.options.rowSelectionPolicy;
                switch(policy){
                case 'single':
                case 'single_radio_only':
                case 'multi':
                case 'multi_checkbox_only':
                    break;
                default:
                    return;
                }

                var $tr = this.getRowBy(rowIndex);
                if($tr == null){
                    console.warn('clutil.view.TableView.setSelectRowAt: target TR elem by [' + rowIndex + '] not found.');
                    return;
                }
                var $input = $tr.find('td:first-child input');
                if($tr.length !== 1 || $input.length !== 1){
                    return;
                }

                var beforeSelect = $input.is(':checked'), afterSelect = (select === true);
                if(beforeSelect !== afterSelect){
                    if($input.is('input:radio')){
                        $input.radio(select ? 'check' : 'uncheck');
                    }else if($input.is('input:checkbox')){
                        $input.checkbox(select ? 'check' : 'uncheck');
                        this.syncCheckAll();
                    }
                    $tr.toggleClass(this.config.selectedRowClass);
                    if(publish){
                        // 選択状態の変化通知イベントを発火する
                        this.trigger('selectChanged', this, ev);
                    }
                }
            },
            /**
             * 全行選択 - 但し、複数選択のみ対応。
             * @param select true:選択ON、false:選択OFF
             */
            setSelectAll: function(select, ev){
                // 複数選択のみ対応
                if(!this.isMultiSelectable()){
                    return;
                }

                if(_.isEmpty(this.collection)){
                    return;
                }

                var beforeSelectedCount = this.getSelectedCount();
                var check = (select) ? 'check' : 'uncheck';

                // カラムヘッダの全選択チェックボックス
                var $chkall = this.$('thead tr th:first-child input:checkbox');
                $chkall.checkbox(check);

                // 各行の行選択チェックボックス
                var $tbody = this.$('tbody');
                $tbody.find('tr td:first-child input:checkbox').each(_.bind(function(idx,elem){
                    var $cb = $(elem);
                    $cb.checkbox(check);
                    if(select){
                        $cb.closest('tr').addClass(this.config.selectedRowClass);
                    }else{
                        $cb.closest('tr').removeClass(this.config.selectedRowClass);
                    }
                },this));

                var afterSelectedCount = this.getSelectedCount();
                if(beforeSelectedCount !== afterSelectedCount && ev != null){
                    // 選択状態の変化通知イベントを発火する
                    this.trigger('selectChanged', this, ev);
                }
            },
            /**
             * 指定ハッシュマップに合致する行の選択状態を設定する。
             * ハッシュ値が合致する行にはチェックマークをセットし、合致しない行はチェックを解除する。
             * 但し、複数選択のみ対応。
             * @param map 選択行データのハッシュマップ。
             * @param publish selectChanged イベント発火指定、true:イベント発火する、false,default:イベント発火しない。
             */
            setSelectRowsByHashMap: function(map, publish){
                // 複数選択のみ対応
                if(!this.isMultiSelectable()){
                    return;
                }

                if(_.isEmpty(this.collection)){
                    return;
                }

                if(_.isEmpty(map)){
                    this.setSelectAll(false);
                    return;
                }

                // 各行の行選択チェックボックス
                var $tbody = this.$('tbody');
                var changedCount = 0;
                $tbody.find('tr td:first-child input:checkbox').each(_.bind(function(idx, elem){
                    var $cb = $(elem), beforeSelect = $cb.is(':checked');
                    var $tr = $cb.closest('tr');
                    var rec = $tr.data('cl_rec');
                    if(rec != null){
                        var hash = this._hashValue(rec);
                        var select = (map[hash] != null);
                        if(beforeSelect !== select){
                            if(select){
                                $cb.checkbox('check');
                            }else{
                                $cb.checkbox('uncheck');
                            }
                            $tr.toggleClass(this.config.selectedRowClass);
                            changedCount++;
                        }
                    }
                },this));

                if(changedCount){
                    this.syncCheckAll();
                    if(publish){
                        this.trigger('selectChanged', this);
                    }
                }
            },
            /**
             * 選択行を削除する。
             * ただし、行テンプレートが指定されていること。
             */
            removeSelectedRows: function(){
                if(this.template == null){
                    throw 'clutil.view.TableView.removeSelectedRows(): null template.';
                }

                var $selectedTR = this.$getSelectedRows();
                if($selectedTR.length > 0){
                    $selectedTR.remove();
                    this.collection = [];
                    this.$('tbody tr:not(".addrow")').each(_.bind(function(idx, elem){
                        // 存在している行データで collection を再構築する。
                        var rec = $(elem).data('cl_rec');
                        if(rec){
                            this.collection.push(rec);
                        }
                    },this));

                    // カラムヘッダの全選択チェックボックス
                    var $chkall = this.$('thead tr th:first-child input:checkbox');
                    $chkall.checkbox('uncheck');
                    if(_.isEmpty(this.collection)){
                        this._setBtnEnable($chkall, false);
                    }

                    // 選択状態の変化通知イベントを発火する
                    this.trigger('selectChanged', this);
                }
                return $selectedTR;
            },
            /**
             * 指定行を削除する
             * @param rowIndex 行削除対象とする、行内の任意の要素 or jQuery or 行インデックス
             * @return true:削除した、false:削除しなかった（削除対象が見つからないなど）
             */
            removeRowAt: function(rowIndex){
                var $tr = this.getRowBy(rowIndex);
                if($tr == null){
                    return false;
                }

                // 行にバインドした元データを取得
                var rec = $tr.data('cl_rec');
                if(rec == null){
                    return false;
                }

                // 削除対象行の行選択状態
                var isSelected = $tr.find('td:first-child input').is(':checked');

                // 行削除
                var rowModelIndex = _.indexOf(this.collection,rec);
                if(rowModelIndex < 0){
                    return false;
                }
                this.collection.splice(rowModelIndex, 1);
                $tr.remove();

                // 行選択ポリシー 'multi' の場合、行頭の selectAll チェックボックスの on/off 制御
                this.syncCheckAll();

                // 削除対象行が選択行であった場合、選択状態の変化通知イベントを発火する
                if(isSelected){
                    this.trigger('selectChanged', this);
                }

                return true;
            },

            /**
             * 全選択チェックボックスの ON/OFF を調整する。
             */
            syncCheckAll: function(){
                if(!this.isMultiSelectable()){
                    return;
                }
                var $chkall = this.$('thead tr th:first-child input:checkbox');
                if($chkall.length === 0){
                    return;
                }
                var rowCount = this.collection.length;
                if(rowCount === 0){
                    $chkall.checkbox('uncheck');
                    return;
                }
                var selectedRowCount = this.getSelectedCount();
                if(selectedRowCount === rowCount){
                    $chkall.checkbox('check');
                }else{
                    $chkall.checkbox('uncheck');
                }
            },

            /**
             * 全行選択 input の活性非活性を設定する。
             */
            setEnable: function(enable){
                enable = (enable === true);

                // 各行の行選択 input
                var $tbody = this.$('tbody');
                $tbody.find('tr:not(.addrow)').each(_.bind(function(index, elem){
                    // 行全体
                    var $tr = $(elem);
                    if(enable){
                        $tr.removeAttr('disabled');
                    }else{
                        $tr.attr('disabled', true);
                    }

                    // 行先頭の checkbox or radio
                    var $btn = $tr.find('td:first-child input');
                    this._setBtnEnable($btn, enable);

                    // 行末の [+] ボタン
                    $btn = $tr.find('td:last-child a.ca_btn_plus');
                    if(enable){
                        $btn.removeClass('disabled');
                    }else{
                        $btn.addClass('disabled');
                    }
                },this));

                // 末尾行の［＋追加］ボタン
                var $addRowTR = $tbody.find('tr:last-child.addrow');
                if(enable){
                    $addRowTR.removeAttr('disabled');
                    $addRowTR.find('a.btn').removeAttr('disabled');
                }else{
                    $addRowTR.attr('disabled', true);
                    $addRowTR.find('a.btn').attr('disabled', true);
                }

                // 複数選択: chkall
                if(this.options.rowSelectionPolicy == 'multi'){
                    var $chkall = this.$('thead tr th:first-child input:checkbox');
                    this._setBtnEnable($chkall, enable);
                }
            },

            /**
             * 指定の内部要素に関連する行データを取得する。
             * 注意: セル内部要素に <table> を設置している場合、誤動作の可能性あり。
             * @param elem 行内の任意の要素 or jQuery or 行インデックス
             * @return 指定行の元となるデータ
             */
            getRecBy: function(elem){
                var $tr = this.getRowBy(elem);
                if($tr == null)
                    return;
                return $tr.data('cl_rec');
            },
            /**
             * 任意な要素条件の行要素、$tr オブジェクトを取得する。
             * @param elem 行内の任意の要素 or jQuery or 行インデックス。
             * @return 当該行要素 $tr を返す。不適切な any 条件な場合は undefined を返す。例えば、当 TableView.el の外の要素を指定した場合は undefined を返す。
             */
            getRowBy: function(elem){
                var $tr = null;
                if(_.isNumber(elem)){
                    $tr = this.$('tbody tr:nth(' + elem + ')');
                }else{
                    var $elem = $(elem);
                    $tr = $elem.is('tr') ? $elem : $elem.closest('tr');
                }
                if($tr == null || $tr.length === 0 || this.$('tbody').has($tr).length <= 0){
                    return; // 不適切な行指定条件だった
                }
                return $tr;
            },
            /**
             * 複数行選択可否を判定する。options.rowSelectinPolicy が 'multi' or 'multi_checkbox_only' の場合は複数行選択可と判定する。
             * @return true:複数行選択
             */
            isMultiSelectable: function(){
                switch(this.options.rowSelectionPolicy){
                case 'multi':
                case 'multi_checkbox_only':
                    return true;
                }
                return false;
            },
            /**
             * 指定行データを更新する。
             * @param rowIndex 指定対象とする、行内の任意の要素 or jQuery or 行インデックス
             * @param updRec 更新データ
             */
            updateRow: function(rowIndex, updRec){
                this.fireRowDataChanged(rowIndex, { updRec: updRec });
            },
            /**
             * 指定行データを再描画する。
             * @param rowIndex 指定対象とする、行内の任意の要素 or jQuery or 行インデックス
             * @param opt object オプション
             *  .updRec : 更新行データ、省略時は現在仕込まれている行データの下に再描画する。
             */
            fireRowDataChanged: function(rowIndex, opt){
                // 更新処理対象行要素および行入れ替え前の状態を取得
                var $tr = this.getRowBy(rowIndex);
                if($tr == null){
                    return;
                }
                var rec = $tr.data('cl_rec');
                if(rec == null){
                    return;
                }
                var isRowSelected = $tr.find('td:first-child input').is(':checked');

                // 更新データオプションが指定されている場合は、モデルデータ更新
                if(opt && opt.updRec && rec !== opt.updRec){
                    _.extend(rec, opt.updRec);
                }

                // テンプレートから HTML 展開
                var $newTr = this.renderRow(rec);
                clutil.initUIelement($newTr);
                if(isRowSelected){
                    // 元々の行選択状態を復元
                    var $input = $newTr.find('td:first-child input');
                    if($input.is('input:radio')){
                        $input.radio('check');
                    }else if($input.is('input:checkbox')){
                        $input.checkbox('check');
                    }
                }

                // 行要素入れ替え
                $tr.replaceWith($newTr);
            }
        })
    });

}());
/**
 * モーダル表示
 */
var clutil = _.extend({}, clutil);
if(!clutil.view){
    clutil.view = {};
}
if(!clutil.view.modeal){
    clutil.view.modal = {};
}
$(function(){

    /**
     * モーダル View 抽象クラス
     */
    var AbstractModalView = clutil.view.AbstractView.extend({
        className: 'modal fade',
        events: {
            'show.bs.modal'     : '_onPreShowing',
            'shown.bs.modal'    : '_onPreShown',
            'hide.bs.modal'     : '_onPreClosing',
            'hidden.bs.modal'   : '_onPreClosed'
        },
        initialize: function(opt){
            var o = opt || {};
            if(o.options){
                this.options = o.options;
            }
            if(o.template){
                this.template = o.template;
            }
        },
        render: function(){
            if(_.isString(this.template)){
                this.$el.html(this.template);
            }else if(_.isFunction(this.template)){
                this.$el.html(this.template(this.model || {}));
            }
            this.$el.addClass('cl_modal').data('cl_modal', this);
            return this;
        },
        /**
         * モーダル表示するターゲット<div>を取得する。
         * デフォルトは this.$el を返す。
         */
        getModalElem: function(){
            return this.$el;
        },
        /**
         * モーダル表示内部コンテント<div>を取得する。
         */
        getModalContent: function(){
            if(this.$el.hasClass('modal-content')){
                return this.$el;
            }
            return this.$('.modal-content');
        },
        show: function(onShowing, onShown){
            if(_.isFunction(onShowing)){
                if(this.options == null){
                    this.options = {};
                }
                this.options.onShowing = onShowing;
            }
            if(_.isFunction(onShown)){
                if(this.options == null){
                    this.options = {};
                }
                this.options.onShown = onShown;
            }
            var $body = $('body');
            $body.append(this.$el);
            var $mel = this.getModalElem();
            $mel.modal();
            return this;
        },
        close: function(onClosing, onClosed){
            if(_.isFunction(onClosing)){
                if(this.options == null){
                    this.options = {};
                }
                this.options.onClosing = onClosing;
            }
            if(_.isFunction(onClosed)){
                if(this.options == null){
                    this.options = {};
                }
                this.options.onClosed = onClosed;
            }
            var $mel = this.getModalElem();
            $mel.modal('hide');
        },
        // onShowing 事前の共通処理
        _onPreShowing: function(ev){
            // モーダル表示前に、focus 要素を憶えておき、かつ、フォーカスを刈り取っておく。
            // 本モーダルが閉じた後、focus を戻す。
            this.$__savedFocus__ = $(':focus').blur();
            if(_.isFunction(this.onShowing)){
                return this.onShowing(ev);
            }
        },
        onShowing: function(ev){
            var ret = true;
            if(this.options && _.isFunction(this.options.onShowing)){
                ret = this.options.onShowing(ev, this);
            }
            if(ret === false){
                this.destroy();
            }
        },
        // onShown 事前の共通処理
        _onPreShown: function(ev){
            // 初期フォーカスセット
            if(clcom.config.modal.enablePreferredFocus){
                this.setPreferredFocus();
            }
            // フォーカス制御 enterFocusMode の適用
            this.initEnterFocusMode();
            if(_.isFunction(this.onShown)){
                this.onShown(ev);
            }
        },
        onShown: function(ev){
            if(this.options && _.isFunction(this.options.onShown)){
                return this.options.onShown(ev, this);
            }
        },
        // onClosing 事前の共通処理
        _onPreClosing: function(ev){
            if(_.isFunction(this.onClosing)){
                return this.onClosing(ev);
            }
        },
        onClosing: function(ev){
            var ret = true;
            if(this.options && _.isFunction(this.options.onClosing)){
                ret = this.options.onClosing(ev, this);
            }
            return ret;
        },
        // onClosed 事前の共通処理
        _onPreClosed: function(ev){
            var $saved = this.$__savedFocus__;
            if($saved && $saved.length > 0){
                _.defer(function(){
                    clutil.focus($saved);
                });
            }
            if(_.isFunction(this.onClosed)){
                this.onClosed(ev);
            }
        },
        onClosed: function(ev){
            if(this.options && _.isFunction(this.options.onClosed)){
                this.options.onClosed(ev, this);
            }
            this.destroy();
        },
        /**
         * 初期フォーカス設定 - モーダルコンテント内部の適当な要素にフォーカスをセットする。
         * @param lazy true:遅延
         */
        setPreferredFocus: function(lazy){
            if(lazy){
                _.defer(this.setPreferredFocus);
                return;
            }
            var $f = this.getPreferredFocus();
            clutil.setFocus($f);
        },
        /**
         * モーダルコンテント内部で適当なフォーカス候補を返す。
         */
        getPreferredFocus: function(){
            var $f = this.$('.modal-body :focusable:eq(0)');
            if($f.length === 0){
                $f = this.$('.modal-footer :focusable:eq(0)');
            }
            if($f.length === 0){
                $f = this.$('.modal-header :focusable:eq(0)');
            }
            return $f;
        },
        /**
         * Enter キーフォーカスモードを設定する。
         * フォーカスが動ける範囲は、モーダルコンテント内部に縛られる。
         */
        initEnterFocusMode: function(force){
            if(clcom.config.modal.enableEnterFocusMode || force === true){
                var $c = this.getModalContent();
                clutil.enterFocusMode($c, { view: $c });
            }
        },
        destroy: function(){
            this.remove();
            clutil.mediator.trigger('clutil.view.modal:destroyed', this);
        }
    });
    // 子 View の events を継承できるようにする。
    AbstractModalView.extend = function(child){
        var v = Backbone.View.extend.apply(this, arguments);
        v.prototype.events = _.extend({}, this.prototype.events, child.events);
        return v;
    };

    // モーダルリソースが残留している場合に body.modal-open クラスを補完する。
    clutil.mediator.listenTo(clutil.mediator, 'clutil.view.modal:destroyed', function(modalView){
        _.defer(function(){
            if($('body > .cl_modal').length > 0){
                // モーダルを多重に開いている場合で、前線のモーダルが閉じられた。

                // モーダルが閉じる契機で body.modal-open クラスが刈り取られるので、付け直す。
                $('body').addClass('modal-open');

                // 次に Active になるモーダルに、フォーカス制御 enterFocusMode を付け直す。
                // ただし、clcom.config.modal.* コンフィグ設定で有効化されている場合にのみ適用される。
                var curModal = clutil.view.modal.getModalView();
                if(curModal){
                    curModal.initEnterFocusMode();
                }
            }else{
                // 残留モーダルがいなくなった
                if(clcom.config.enableEenterFocusMode){
                    // body に標準の enterFocusMode を適用する。
                    clutil.enterFocusMode();
                }else{
                    // body に標準の enterFocusMode を適用する。
                    clutil.leaveEnterFocusMode();
                }
            }
        });
    });

    // -------------------------------------------
    // clutil.view.modal へインポート
    _.extend(clutil.view.modal, {
        /**
         * モーダルView基底クラス
         * * 独自にモーダルViewを作る場合はこの View を拡張して作成する。
         */
        AbstractModalView: AbstractModalView,

        /**
         * 指定コンテントをモーダルエリアに表示する。
         * @param content モーダルエリアに表示するコンテント
         *      テキスト html, DOMエレメント、jQuery オブジェクト、Backbone.View インスタンスが指定可能。
         * @param opt オプション(省略可能)
         *  .onShowing  表示直前に呼ばれるコールバック関数。この関数の中で false を返すと表示しない。
         *  .onShown    表示直後に呼ばれるコールバック関数。
         *  .onClosing  閉じる動作中に呼ばれるコールバック関数。この関数の中で false を返すと閉じる処理は中断する。
         *  .onClosed   閉じた後で呼ばれるコールバック関数。
         * それぞれのコールバック関数の引数は以下のとおり。
         *  arg0        Bootstrap からのモーダルイベントオブジェクト
         *  arg1:       モーダルView基底インスタンス
         */
        showModal: function(content, opt){
            var modalView;
            if(_.isString(content)){
                // HTML テキストとみなす
                modalView = new clutil.view.modal.AbstractModalView({
                    el: $(content),
                    options: opt
                }).render();
            }else if(content instanceof $ || _.isElement(content)){
                // jQuery オブジェクト or DOM element
                modalView = new clutil.view.modal.AbstractModalView({
                    el: content,
                    options: opt
                }).render();
            }else if(content instanceof clutil.view.modal.AbstractModalView){
                // モーダルView 基底クラスインスタンス
                modalView = content;
                if(opt){
                    modalView.options = _.extend(modalView.options || {}, opt);
                }
            }else if(content instanceof Backbone.View){
                // その他 Backbone.View: el は内部コンテントと見做す
                var o = opt || {};
                var argEx = _.pick(o, 'id', 'tagName', 'className', 'attributes');
                argEx.options = _.omit(o, 'id', 'tagName', 'className', 'attributes');
                argEx.el = content.el;
                modalView = new clutil.view.modal.AbstractModalView(argExt).render();
            }else{
                throw 'clutil.view.modal.showModal(): Invalid argument.';
            }

            return modalView.show();
        },

        /**
         * モーダルリソース全削除
         */
        closeAll: function(){
            var closedCount = 0;
            $('body > .cl_modal').each(function(idx, elem){
                var $elem = $(elem);
                var v = $elem.data('cl_modal');
                if(v != null){
                    v.destroy();
                    closedCount += 1;
                }
            });
            if(closedCount > 0){
                // 画面全体の Modal 背景装飾要素を取り除く
                $('body').removeClass('modal-open').children('.modal-backdrop').remove();
            }
        },

        /**
         * モーダル View インスタンス取得
         */
        getModalView: function(){
            return $('.cl_modal:last').data('cl_modal');
        }
    });

});
/**
 * モーダルダイアログクラス
 * clutil.view.modal.js に依存。
 */
if(clutil == null || clutil.view == null || clutil.view.modal == null){
    throw 'require clutil.view.modal.js !!!';
}
if(clutil.view.dialog == null){
    clutil.view.dialog = {};
}
$(function(){

    /**
     * メッセージダイアログ
     */
    var MessageDialogView = clutil.view.modal.AbstractModalView.extend({
        className: 'modal fade',
        templates: {
            main: _.template(''
                +'<div class="modal-dialog">'
                +   '<div class="modal-content">'
                +       '<div class="modal-header">'
                +           '<div class="modal-header--title"><%- title %></div>'
                +       '</div>'
                +       '<div class="modal-body"></div>'
                +       '<div class="modal-footer">'
                +           '<div class="row">'
                +               '<% _.each(buttons || [], function(btn, idx, btns){ %>'
                +                   '<div class="col-xs-<%= clutil.view.dialog.colspan(btns.length, idx) %> col-sm-<%= clutil.view.dialog.colspan(btns.length, idx) %> col-md-<%= clutil.view.dialog.colspan(btns.length, idx) %>">'
                +                       '<button class="btn btn-flat-<%= btn.level || "default" %> btn-block" data-optionCode=<%= btn.code || "cancel" %>><%- btn.name %></button>'
                +                   '</div>'
                +               '<% }) %>'
                +           '</div>'
                +       '</div>'
                +   '</div>'
                +'</div>'
            ),
            messageBodyInner: _.template(''
                + '<div class="row">'
                +   '<div class="col-md-12">'
                +       '<p class="modal--diaglog--text text-center">'
                +           '<%= _.map(messages, function(m){ return _.escape(m); }).join("<br />") %>'
                +       '</p>'
                +   '</div>'
                + '</div>')
        },
        events: {
            'click .modal-footer button'    :   '_onClickOptionBtn'
        },
//        model: {
//            title: '',
//            message: '削除してよろしいですか？',
//            buttons: [{ code:'cancel', name: 'キャンセル'}, { code:'del', name: '削除', level:'danger' }]
//        },
        initialize: function(opt){
            var fixOpt = (opt == null || opt.options == null) ? {} : opt.options;
            var o = {
                onClosed:  this._onClosedWrapper,
                appOnClosed: fixOpt.onClosed
            };
            this.options = _.defaults(o, fixOpt);
        },
        render: function(){
            var m = this.model;
            var $main = $(this.templates.main(m)), $modalBody = $main.find('.modal-body');
            if(m.message instanceof $ || _.isElement(m.message)){
                // modal.message が DOM 要素または jQuery と見做して、modal-body へ放り込む
                $modalBody.append(m.message);
            }else{
                var msgs = _.isArray(m.message) ? m.message : [ m.message ];
                var msgBodyHtml = this.templates.messageBodyInner({ messages: msgs });
                $modalBody.html(msgBodyHtml);
            }
            this.$el.append($main);
            this.$el.addClass('cl_modal').data('cl_modal', this);
            return this;
        },
        _onClickOptionBtn: function(ev){
            this.optionCode = ev.currentTarget.getAttribute('data-optionCode');
            this.close();
        },
        _onClosedWrapper: function(ev, me){
            if(_.isFunction(me.options.appOnClosed)){
                me.options.appOnClosed(me.optionCode || 'cancel', me);
            }
        }
    });

    // -------------------------------------------
    // clutil.view.dialog へインポート
    _.extend(clutil.view.dialog, {
        /**
         * ボタン幅割り表
         * @param colIndex 何番目のボタンかのインデックス
         * @param total ボタンの数
         * @return conIndex 番目のボタン幅
         */
        colspan: function(total, colIndex){
            const ColspanMap = [
                [],
                [ 12 ],
                [ 6, 6 ],
                [ 4, 4, 4 ],
                [ 3, 3, 3, 3 ],
                [ 2, 2, 2, 3, 3 ],
                [ 2, 2, 2, 2, 2, 2 ],
                [ 1, 1, 2, 2, 2, 2, 2 ],
                [ 1, 1, 1, 1, 2, 2, 2, 2 ],
                [ 1, 1, 1, 1, 1, 1, 2, 2, 2 ],
                [ 1, 1, 1, 1, 1, 1, 1, 1, 2, 2 ],
                [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 ],
                [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
            ];
            var spanMap = ColspanMap[total] || ColspanMap[0];
            var colspan = spanMap[colIndex] || 1;
            console.log('total[' + total + '] colIndex[' + colIndex + '] colspan[' + colspan + ']');
            return colspan;
        },

        /**
         * メッセージダイアログ
         * @param title タイトル
         * @param message メッセージ
         * @param buttons フッターボタン定義
         * 			[ { code:ボタンコード, name:ボタンラベル, level:ボタンレベル },... ]
         * 			＜レベル定義＞
         * 				primary		青
         *				info		緑
         *				danger		赤
         *				success		青
         *				warning		黄緑
         *				default		グレー
         * @param onClosed コールバック。コールバック第１引数に、どのボタンの応答かをボタンコードで返す。
         * @return Backbne.View インスタンス
         */
        showMessageDialog: function(title, message, buttons, onClosed){
            var opt;
            if(arguments.length === 1 && _.has(arguments[0], 'message')){
                // プロパティ引数と見做す
                opt = arguments[0];
            }else{
                opt = {
                    title: title,
                    message: message,
                    buttons: buttons,
                    onClosed: onClosed
                };
            }
            var v = new MessageDialogView({
                model: {
                    title: opt.title || '',
                    message: opt.message,
                    buttons: opt.buttons
                },
                options: {
                    onClosed: opt.onClosed
                }
            }).render();
            return v.show();
        },

        /**
         * 確認ダイアログ
         * @param message メッセージ
         * @param onClosed コールバック
         * @return Backbne.View インスタンス
         */
        showConfirmDialog: function(message, onClosed){
            var opt;
            if(message && _.has(message, 'message')){
                // プロパティ引数と見做す
                opt = arguments[0];
            }else{
                opt = { message: message, onClosed: onClosed };
            }
            _.defaults(opt, { buttons: [ { name:'キャンセル' }, { code:'yes' , name:'はい', level:'primary'} ] });
            return clutil.view.dialog.showMessageDialog(opt);
        },

        /**
         * エラーダイアログ
         * @param message メッセージ
         * @return Backbne.View インスタンス
         */
        showErrorDialog: function(message){
            var opt;
            if(message && _.has(message, 'message')){
                // プロパティ引数と見做す
                opt = arguments[0];
            }else{
                opt = { message: message };
            }
            _.defaults(opt, { buttons: [ { name:'確認', level:'danger'} ] });
            return clutil.view.dialog.showMessageDialog(opt);
        },

        /**
         * 警告ダイアログ
         * @param message メッセージ
         * @return Backbne.View インスタンス
         */
        showWarnDialog: function(message){
            var opt;
            if(message && _.has(message, 'message')){
                // プロパティ引数と見做す
                opt = arguments[0];
            }else{
                opt = { message: message };
            }
            _.defaults(opt, { buttons: [ { name:'確認', level:'warning'} ] });
            return clutil.view.dialog.showMessageDialog(opt);
        },

        /**
         * 通知ダイアログ
         * @param message メッセージ
         * @return Backbne.View インスタンス
         */
        showInfoDialog: function(message){
            var opt;
            if(message && _.has(message, 'message')){
                // プロパティ引数と見做す
                opt = arguments[0];
            }else{
                opt = { message: message };
            }
            _.defaults(opt, { buttons: [ { name:'確認', level:'info'} ] });
            return clutil.view.dialog.showMessageDialog(opt);
        },

        /**
         * 削除確認ダイアログ: 「削除してもよろしいですか？」
         * @param onClosed コールバック。コールバック第１引数に、どのボタンの応答かを返す。
         * 					'cancel': キャンセルボタンで復帰、'del': 削除ボタンで復帰
         * @return Backbne.View インスタンス
         */
        showDelConfirmDialog: function(onClosed){
            return clutil.view.dialog.showMessageDialog({
                message: '削除してもよろしいですか？',
                buttons: [ { name:'キャンセル' }, { code:'del' , name:'削除', level:'danger'} ],
                onClosed: onClosed
            });
        }
    });

});
/**
 * ページャー
 */
var clutil = _.extend({}, clutil);
$(function(){

    /**
     * ページャ
     * イベント:
     *  ・clutil.view.PagerView:click
     *
     * ＜モデル設計＞
     * ページ応答パケットから拾うプロパティ名
     *  // am_proto_page_rsp ... 応答パケット
     *  // .curr_record     : 開始レコードの全体に対するインデックス
     *  // .total_record    : 総レコード数
     *  // .page_size       : １ページ当たりのレコード件数
     *  // .page_record     : func(curr_record, total_record, page_size) 計算できるので無視
     *  // .page_num        : func(curr_record, total_record, page_size) 計算できるので無視
     *  pageRspPickingProps: [ "curr_record", "total_record", "page_size" ],
     *  // モデルデータ中の静的プロパティ
     *  savingPickingProps: [ "showLineNoRange", "pageSizeCollection" ],
     *  // モデルデータのかたち
     *  model: {
     *      // 静的変数
     *      showLineNoRange: true,
     *      showPageSizeCollection: true,
     *      // 可変な変数
     *      curr_record: 0,
     *      total_record: 0,
     *      page_size: 0,
     *      // 可変（算出）
     *      page_num: 0,
     *      page_record: 0,
     *      // 拡張
     *      st_lineno: 0,   // 開始行番号
     *      ed_lineno: 0,   // 終了行番号
     *      st_page: 1,     // 開始ページボタンのページ番号
     *      curr_page: 1,   // 今何ページ？
     *      ed_page: 1,     // 終了ページボタンのページ番号
     *  },
     */
    var PagerViewClass = clutil.view.AbstractView.extend({
        className: 'pagination col-md-12',

        template: _.template(''
                // 表示件数ラベル
                + '<% if (showLineNoRange && total_record > 0) { %>'
                +   '<p class="page-count"><%= st_lineno %>-<%= ed_lineno %>表示 / <%= total_record %>件</p>'
                + '<% } %>'
                // ページ内レコード数ボタン: [ 10 | 50 | 100 ]
                + '<% if(pageSizeCollection.length > 1) { %>'
                +   '<p class="page-unit">'
                +       '<%= '
                //          ['<a class="active">10</a>','<a>50</a>','<a>100</a>'].join(" | ")
                +           '_.reduce(pageSizeCollection, function(arr,pgsiz){'
                +               'if(pgsiz == page_size){ arr.push("<a class=\\"active\\">" + pgsiz + "</a>"); }'
                +               'else{ arr.push("<a>" + pgsiz + "</a>"); }'
                +               'return arr;'
                +           '},[]).join(" | ")'
                +       ' %>'
                +   '</p>'
                + '<% } %>'
                // ページボタン
                + '<ul>'
                //	[<] prev ボタン
                +   '<% if(page_num >= 2) { %>'
                +       '<% if(curr_page === 1){ %>'
                +           '<li class="previous disabled"><a class="icon-abui-arrow-left cl_prev"></a></li>'
                +       '<% }else{ %>'
                +           '<li class="previous"><a class="icon-abui-arrow-left cl_prev"></a></li>'
                +       '<% } %>'
                +   '<% } %>'
                // 左端ページボタン
                +   '<% if(side_l.num > 0){ %>'
                +       '<% for(var i=0; i<side_l.num; i++){ %>'
                +           '<% if((i+1)===curr_page){ %>'
                +               '<li class="active"><a><%= (i+1) %></a></li>'
                +           '<% }else{ %>'
                +               '<li><a><%= (i+1) %></a></li>'
                +           '<% } %>'
                +       '<% } %>'
                +   '<% } %>'
                +   '<% if(side_l.sep) { %>'
                +       '<li>...</li>'
                +   '<% } %>'
                // 中央ページボタン
                +   '<% for(var i=st_page; i<=ed_page; i++){ %>'
                +       '<% if(i == curr_page){ %>'
                +           '<li class="active"><a><%= i %></a></li>'
                +       '<% }else{ %>'
                +           '<li><a><%= i %></a></li>'
                +       '<% } %>'
                +   '<% } %>'
                // 右端ページボタン
                +   '<% if(side_r.sep){ %>'
                +       '<li>...</li>'
                +   '<% } %>'
                +   '<% if(side_r.num > 0){ %>'
                +       '<% for(var i=side_r.num-1; i>=0; i--){ %>'
                +           '<% if((page_num-i)===curr_page){ %>'
                +               '<li class="active"><a><%= (page_num-i) %></a></li>'
                +           '<% }else{ %>'
                +               '<li><a><%= (page_num-i) %></a></li>'
                +           '<% } %>'
                +       '<% } %>'
                +   '<% } %>'
                //	[>] next ボタン
                +   '<% if(page_num >= 2) { %>'
                +       '<% if(curr_page === page_num){ %>'
                +           '<li class="next disabled"><a class="icon-abui-arrow-right cl_next"></a></li>'
                +       '<% }else{ %>'
                +           '<li class="next"><a class="icon-abui-arrow-right cl_next"></a></li>'
                +       '<% } %>'
                +   '<% } %>'
                + '</ul>'
        ),

        events: {
            'click ul li:not(.disabled,.active) a'  : '_onClickPageBtn',
            'click .page-unit a:not(.active)'       : '_onClickPageSizeBtn'
        },

        // ページ選択のデフォルトセット
        defaultModel: {
            maxPageBtn: 9,                      // ページボタン設置個数最大数 -- 10個
            sidePageNum: 2,                     // 両端のページボタン数
            showLineNoRange: true,              // 行番号表示範囲: true:表示する、false:非表示しない
            pageSizeCollection: [10, 50, 100],  // ページ内レコード数の選択肢
            curr_record: 0,                     // 1行目のレコードの全体に対するインデックス(初期値)
            total_record: 0,                    // 全体レコード数(初期値)
            page_size: 10                       // pageSizeCollection[0] を初期値とする
        },

        // 内部関数: モデルオブジェクト補完
        _fixModel: function(model0){
            var model = _.defaults(model0 || {}, this.myDefault || this.defaultModel);

            // 変数エイリアス
            var maxPgBtn   = model.maxPageBtn;
            var totalNRec  = model.total_record;
            var stRecIndex = model.curr_record;
            var pgSize     = model.page_size;
            // { curr_record, total_record, page_size } から計算できる変数
            var pgNum      = Math.floor((totalNRec      + (pgSize-1)) / pgSize);
            var currPage   = Math.floor(((stRecIndex+1) + (pgSize-1)) / pgSize);    // 今何ページかを算出
            var currNRec = (currPage >= pgNum) ? (totalNRec - ((pgNum - 1) * pgSize)) : pgSize; // 今ページのレコード数を算出
            var stLineNo   = (totalNRec > 0) ? (stRecIndex + 1) : 0;
            var edLineNo   = stRecIndex + currNRec;

            // ページ: 開始ボタン、選択中ページボタン、終了ボタン
            var stPage, edPage;
            if(pgNum <= 0){
                // 総ページ数＝０: 初期化時など
                stPage = edPage = 1;
            }else{
                // currPage が中心になるよう、開始、終了ページ番号を求める。
                var dpg = Math.floor(maxPgBtn / 2);
                stPage = currPage - dpg;
                edPage = stPage + maxPgBtn -1;
                if(stPage < 1 && edPage > pgNum){
                    // 両端がはみ出ている
                    stPage = 1;
                    edPage = pgNum;
                }else if(stPage < 1){
                    // 左側がはみ出ている
                    var dl = 1 - stPage;		// はみ出し量を edPage 側に分配
                    stPage = 1;
                    edPage += dl;
                    if(edPage > pgNum){
                        edPage = pgNum;
                    }
                }else if(edPage > pgNum){
                    // 右側がはみ出ている
                    var dr = edPage - pgNum;	// はみ出し量を stPage 側に分配
                    edPage = pgNum;
                    stPage -= dr;
                    if(stPage < 1){
                        stPage = 1;
                    }
                }else{
                    // 範囲内に収まっている
                    // ページボタン数が偶数個で、currPage 番後が中央値以下 ⇒ 左シフト
                    if(stPage > 1 && maxPgBtn % 2 === 0 && (currPage * 2) < pgNum){
                        stPage += 1;
                        edPage += 1;
                    }
                }
            }

            // 両端ページボタン数とセパレタ有無
            var leftSideCount  	= 0;
            var hasLeftSep     = false;
            var rightSideCount = 0;
            var hasRightSep    = false;
            if(pgNum > maxPgBtn && model.sidePageNum > 0){
                var sn = model.sidePageNum;
                hasLeftSep     = stPage > (sn+1);
                leftSideCount  = (hasLeftSep === true) ? sn : (stPage - 1);
                hasRightSep    = edPage < (pgNum - sn);
                rightSideCount = (hasRightSep === true) ? sn : (pgNum - edPage);
            }

            // 算出変数を補完する
            return _.extend(model, {
                // 応答再計算
                page_num: pgNum,        // 総ページ数
                page_record: currNRec,  // 選択ページ内のレコード数
                // 拡張変数
                st_lineno: stLineNo,    // 開始行番号
                ed_lineno: edLineNo,    // 終了行番号
                st_page: stPage,        // 開始ページボタン番号
                curr_page: currPage,    // 今何ページ？
                ed_page: edPage,        // 終端ページボタン番号
                side_l: {
                    sep: hasLeftSep,    // 左側分割有無
                    num: leftSideCount  // 左端ペースボタン数
                },
                side_r: {
                    sep: hasRightSep,   // 右側分割有無
                    num: rightSideCount // 右端ページボタン数
                }
            });
        },

        /**
         * 初期化
         */
        initialize: function(opt){
            // ページャモデルをセット: デフォルト設定値補完する
            this.model = this._fixModel(opt.model);

            // デフォルトセットを保持
            var defaultKeys = _.keys(this.defaultModel);
            this.myDefault = _.pick(this.model, defaultKeys);
        },

        // 内部のモデルデータに従い、ページャViewを描画する。
        render: function(){
            var html_src = this.template(this.model);
            this.$el.html(html_src).addClass('cl_pager').data('cl_pager', this);
            return this;
        },

        // ページボタン押下
        _onClickPageBtn: function(ev){
            if(!this.isEnabled()){
                return;
            }
            // <a href="#">4</a>
            var $a = $(ev.currentTarget);
            var pg = 0;
            if($a.hasClass('cl_prev')){
                pg = Math.max(this.model.curr_page - 1, 1);
            }else if($a.hasClass('cl_next')){
                pg = Math.min(this.model.curr_page + 1, this.model.page_num);
            }else{
                pg = parseInt($a.text(),10);
            }
            var stIndex = Math.max((pg -1),0) * this.model.page_size;
            if(pg == this.model.curr_page){
                // 現在表示と同一ページの要求につき、スキップ
                return;
            }

            // 部品としては、イベントを発火するだけとする。
            // イベントにページャリクエストを添える。
            var pgReq = {
                start_record: stIndex,
                page_size: this.model.page_size
            };
            this.trigger('clutil.view.PagerView:click', pgReq, this);
        },

        // 表示件数ボタン押下
        _onClickPageSizeBtn: function(ev){
            if(!this.isEnabled()){
                return;
            }
            // <li><a>50</a></li>
            var $a = $(ev.currentTarget);
            var pgSize = parseInt($a.text(),10);

            // 部品としては、イベントを発火するだけとする。
            // イベントにページャリクエストを添える。
            var pgReq = {
                start_record: 0,
                page_size: pgSize
            };
            this.trigger('clutil.view.PagerView:click', pgReq, this);
        },

        /**
         * 現在設定中のページリクエストパケットを返す
         */
        getPageReq: function(){
            var stIndex = Math.max(0, this.model.st_lineno-1);
            var pgSize = this.model.page_size;
            return {
                start_record: stIndex,
                page_size: pgSize
            };
        },

        /**
         * 応答パケットに応じたページャ要素を再構築する
         */
        setPageRsp: function(pgRsp){
            // 応答パケットでモデルデータを更新
            var staticProp = _.pick(this.model || {}, "maxPageBtn", "showLineNoRange", "pageSizeCollection");
            var fixPgRsp0 = _.pick(pgRsp || {},  "curr_record", "total_record", "page_size");
            var fixPgRsp1 = _.extend(staticProp, fixPgRsp0);
            this.model = this._fixModel(fixPgRsp1);

            // ページャViewを再構築する
            return this.render();
        },
        /**
         * 活性/非活性状態を取得する。
         * @return true:活性状態、false:非活性状態
         */
        isEnabled: function(){
            return this.$el.hasClass('disabled') == false;
        },
        /**
         * 活性/非活性状態を設定する。
         */
        setEnable: function(enable){
            if(enable){
                this.$el.removeClass('disabled');
            }else{
                this.$el.addClass('disabled');
            }
        },
        /**
         * 全レコード数を取得する。
         */
        getTotalRecords: function(){
            return this.model.total_record || 0;
        }
    });

    /**
     * ページャの上下セットを構築する
     * $divs: ページャーを仕込む div 要素
     * opt: clutil.view.PagerView のオプション引数
     * callback: ページャクリックのイベントを受け取る関数
     */
    var PagerSynchronizerClass = function($divs, opt, callback){
        var pgViews = [];
        var ndiv = $divs.length;
        $divs.each(function(index, div){
            var cpyOpt = clutil.deepClone(opt || {});
            var pgOpt = _.extend(cpyOpt, {
                el: div,
                className: (index === (ndiv-1)) ? "pagerSec bottom" : "pagerSec"	// XXX .bottom 区別はいらないかも．．．

            });
            var pgv = new clutil.view.PagerView(pgOpt);
            if(_.isFunction(callback)){
                pgv.listenTo(pgv, 'clutil.view.PagerView:click', callback);
            }
            pgViews.push(pgv);
        });
//      if(pgViews.length > 1){
//      _.last(pgViews).model.showLineNoRange = false;
//      }
        this.pgViews = pgViews;
    };
    PagerSynchronizerClass.prototype = {
        /**
         * ページ応答をセットする。
         */
        setPageRsp: function(pgRsp){
            if(this.pgViews == null){
                return;
            }
            for(var i=0; i<this.pgViews.length; i++){
                var pgv = this.pgViews[i];
                pgv.setPageRsp(pgRsp);
            }
        },
        /**
         * 現在設定ページのページ要求を返す。
         */
        getPageReq: function(){
            if(this.pgViews == null){
                return;
            }
            var pgv = this.pgViews[0];
            return pgv.getPageReq();
        },
        /**
         * 描画する
         */
        render: function(){
            if(this.pgViews == null){
                return;
            }
            for(var i=0; i<this.pgViews.length; i++){
                var pgv = this.pgViews[i];
                pgv.render();
            }
            return this;
        },
        /**
         * クリア
         */
        clear: function(){
            this.setPageRsp({});
        },
        /**
         * 活性/非活性状態を設定する。
         * @param enable true:活性化する、false:非活性化にする
         */
        setEnable: function(enable){
            if(this.pgViews == null){
                return;
            }
            for(var i=0; i<this.pgViews.length; i++){
                var pgv = this.pgViews[i];
                pgv.setEnable(enable);
            }
        },
        /**
         * 全レコード数を取得する。
         */
        getTotalRecords: function(){
            if(this.pgViews == null){
                return;
            }
            var pgv = this.pgViews[0];
            return pgv.model.total_record || 0;
        },
        /**
         * 破棄
         */
        destroy: function(){
            if(this.pgViews == null){
                return;
            }
            for(var i=0; i<this.pgViews.length; i++){
                var pgv = this.pgViews[i];
                pgv.stopListening(pgv);
                pgv.remove();
            }
            return this;
        }
    };

    // -------------------------------------------
    // clutil.view へインポート
    if(!clutil.view){
        clutil.view = {};
    }
    _.extend(clutil.view, {
        PagerView: PagerViewClass,
        PagerSynchronizer: PagerSynchronizerClass
    });
});
/**
 * ファイルアップロードモーダル View
 * clutil.view.dropzone
 */
if(window.Dropzone){
    // おまじない やっとかないと already attached error が出る
    Dropzone.autoDiscover = false;
}
var clutil = _.extend({}, clutil);
$(function () {
    if(!clutil.view){
        clutil.view = {};
    }

    if(clutil.view.modal == null){
        throw 'clutil.view.modal.js required!!!';
    }

    /**
     * ファイルタイプ定義
     */
    var FileTypes = {
        image: {
            description:'画像ファイル',
            matcher:[/^image\//]
        },
        excel: {
            description:'エクセルファイル',
            matcher:[
                //'application/vnd.ms-excel',
                //'application/vnd.ms-excel.sheet.macroEnabled.12',
                /^application\/vnd.ms-excel/,
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // *.xlsx
            ],
            extension: [/\.xls$/i, /\.xlsx$/i, /\.xlsm$/i]
        },
        csv: {
            description: 'ＣＳＶファイル',
            // OS 環境設定要因で、{ type: "" } 空文字列で入ることがある。
            matcher: ['', 'text/csv', 'application/vnd.ms-excel'],
            extension: [ /\.csv$/i ]
        },
        xml: {
            description: 'ＸＭＬファイル',
            matcher: ['text/xml']
        },
        text: {
            description: 'テキストファイル',
            matcher: ['text/plain']
        },
        pdf: {
            description: 'ＰＤＦファイル',
            extension: [ /\.pdf$/i ]
        },
        zip: {
            description: 'ＺＩＰファイル',
            extension: [ /\.zip$/i ]
        }
    };

    /**
     * モーダル View
     */
    var DropzoneModalView = clutil.view.modal.AbstractModalView.extend({
        className: 'cl_dropzone-modal-view',

        template: _.template(''
            + '<div class="modal fade">'
            +   '<div class="modal-dialog">'
            +     '<div class="modal-content">'
            +       '<div class="modal-header">'
            +         '<div class="modal-header--title"><%- title %></div>'
            +       '</div>'
            +       '<div class="modal-body">'
            +         '<div class="row">'
            +           '<div class="col-md-12">'
            +             '<form action="/upload-target" class="dropzone cl_dz-progress-hidden" id="ca_fileupload"></form>'
            +           '</div>'
            +         '</div>'
            +       '</div>'
            +       '<div class="modal-footer">'
            +         '<div class="row">'
            +           '<div class="col-xs-6 col-sm-6 col-md-6">'
            +             '<button class="btn btn-flat-default btn-block" id="ca_cancel">キャンセル</button>'
            +           '</div>'
            +           '<div class="col-xs-6 col-sm-6 col-md-6">'
            +             '<button class="btn btn-primary btn-block" id="ca_submit" disabled>アップロード</button>'
            +           '</div>'
            +         '</div>'
            +       '</div>'
            +     '</div>'
            +   '</div>'
            + '</div>'),

        // @Override
        getModalElem: function(){
            return this.$el.children('.modal');
        },
        // @Override
        onClosed: function(ev){
            if(this.options && _.isFunction(this.options.onClosed)){
                var ans = this.answer || 'cancel';
                var selectedItems = (ans == 'cancel') ? null : this.model;
                this.options.onClosed(ans, selectedItems);
            }
            this.destroy();
        },
        // @Override
        onShowing: function(ev){
            // モーダル表示直前のタイミングで呼ばれる。この時点で <body> 下に DOM 実体が置かれる。
        },
        // @Override
        onShown: function(ev){
            // モーダル表示完了のタイミングで呼ばれる。
        },
        events: {
            'click #ca_cancel'  : '_onClickCancelBtn',
            'click #ca_submit'  : '_onClickSubmitBtn'
        },
        initialize: function(opt){
            clutil.bindAll(this);

            this.options = _.defaults(opt.options, { title: 'ファイルのアップロード' });
        },
        render: function(){
            var myContent = this.template(this.options);
            this.$el.html(myContent).addClass('cl_modal').data('cl_modal', this);

            this.initDropzone();

            return this;
        },

        // ファイルアップロード Dropzone初期化
        initDropzone:function() {
            var _this = this;

            var dzopt = {
                url:'dummy',
                init: function() {
                    this.on('addedfile', _this.handleDroppedFile),
                    this.on('removedfile', _this.checkRemove)
                },
                accept: _this.checkAccept, //ドロップファイルタイプのチェック
                autoProcessQueue:false,
                maxFiles:1, // XXX
                maxFilesize: this.options.maxsize / (1024*1024), // 最大ファイルサイズ[MB]
                addRemoveLinks: true,
                dictDefaultMessage: '<span class="icon icon-abui-file"></span>ファイルをここにドラッグアンドドロップ<small>またはクリックで場所を指定</small>',
                dictFallbackMessage: 'ブラウザーがドラッグアンドドロップでのアップロードに対応していません',
                dictFileTooBig: 'サイズが大きすぎます（{{filesize}}MB）。上限は{{maxFilesize}}MBです',
                dictInvalidFileType: 'この形式のファイルはアップロードできません',
                dictResponseError: 'エラーが発生しました。コード： {{statusCode}}',
                dictCancelUpload: 'アップロードをキャンセル',
                dictCancelUploadConfirmation: 'アップロードをキャンセルしてよろしいですか',
                dictRemoveFile: '',
                dictMaxFilesExceeded: 'アップロード可能なファイル数の上限に達しました。',
            };

            // Dropzoneインスタンス生成
            var $form = this.$('#ca_fileupload');
            this.myDropzone =  new Dropzone($form[0], dzopt);
        },

        /**
         * Dropzoneでファイルがドロップされたときのイベントハンドラ
         */
        handleDroppedFile: function(){
            var zone = this.myDropzone;

            //２個以上ドロップされたら前のファイルを削除する
            if (zone.files.length >= 2){
                zone.removeFile(zone.files[0]);
            }
        },

        /**
         * Dropzoneドロップファイルタイプのチェックハンドラ
         */
        checkAccept: function(file, done){
            var zone = this.myDropzone;

            // 1) ファイル種別チェック
            var chkFileType = this.options.filetype;    // chkFileType は上記 FileTypes.* で、呼び出し元が指定するもの。
            if (chkFileType) {
                var isMatch = function(str, matcher){
                    if(_.isRegExp(matcher)){
                        return str.match(matcher);
                    }else{
                        return str == matcher;
                    }
                    return false;
                };

                var isOK = false;
                do{
                    // 1-1) コンテントタイプチェック
                    if (_.isArray(chkFileType.matcher) & !_.isEmpty(chkFileType.matcher)) {
                        for (var i = 0; i < chkFileType.matcher.length; i++) {
                            var matcher = chkFileType.matcher[i];
                            if (isMatch(zone.files[0].type, matcher)) {
                                isOK = true;
                                break;
                            }
                        }
                        if(isOK === false){
                            var reason = chkFileType.description + 'を選択してください。';
                            done(reason);
                            break;
                        }
                    }

                    // 1-2) ファイル拡張子チェック
                    if (_.isArray(chkFileType.extension) && !_.isEmpty(chkFileType.extension)) {
                        var isOK = false;
                        for (var i = 0; i < chkFileType.extension.length; i++) {
                            var ext = chkFileType.extension[i];
                            if (isMatch(zone.files[0].name, ext)) {
                                isOK = true;
                                break;
                            }
                        }
                        if (!isOK) {
                            var reason = chkFileType.description + 'を選択してください。';
                            done(reason);
                            break;
                        }
                    }

                    // 2) ファイルサイズチェック
                    // サイズチェック
                    if((this.options.maxsize||0) > 0 && zone.files[0].size > this.options.maxsize){
                        // メッセージは Dropzone.options.fileupload.dictFileTooBig が表示される。
                        isOK = false;
                    }else if(zone.files[0].size <= 0){
                        isOK = false;
                        done('空ファイルです。');
                    }

                }while(false);

                // UI コントロール
                if(isOK){
                    this.$(".dz-error-message").hide();     //吹き出し非表示
                    this.$("#ca_submit").prop('disabled', false);
                }else{
                    this.$("#ca_submit").prop('disabled', true);
                }
            }
        },

        //ドロップボックス内の「削除」アイコンクリック時の処理
        checkRemove: function(dz){
            var _this = this;
            if(!_this.myDropzone) return;
            var dropzone = _this.myDropzone;
            if(dropzone.files.length==0 ){
                _this.$("#ca_submit").prop('disabled', true);
            }
        },

        // [キャンセル] ボタンクリック
        _onClickCancelBtn: function(ev){
            this.close();
        },
        // [確定] ボタンクリック
        _onClickSubmitBtn: function(ev){
            var _this = this;
            var zone = this.myDropzone;

            // 入力チェックをする。
            if(zone.files.length==0 ){
                // 入力項目が間違っています。
                clutil.view.showAlert({ message: 'ファイルを選択してください。' });
                return;
            }

            var formData = new FormData();
            formData.append('file', zone.files[0]);

            // attributes JSON
            var attrJSON = _.extend({
                filename: zone.files[0].name
            }, this.options.uploadAttributes);
            formData.append('attr', JSON.stringify(attrJSON));

            var url = clcom.config.uploaderURI;

            clutil.blockUI(url);
            $.ajax({
                url     : url,          //実行するサーブレット(PersonalControllerAjax)
                dataType: 'json',       //サーバから返却されるデータの型(xml, html, json, jsonp, text ...
                type    : 'post',       //post, get
                //FormDataを指定してデータ送信する
                data: formData,         // dataに FormDataを指定
                processData: false,     //Ajaxがdataを整形しない指定
                contentType: false      //contentTypeもfalseに指定
            }).always(function(){
                clutil.unblockUI(url);
            }).done(function(resData) { //callback resData:response
                var mData = _.omit(resData);
                mData.name = zone.files[0].name;
                _this.model = mData;
                _this.answer = 'correct';
                _this.close();
            }).fail(function(resData) {
                clutil.view.showAlert({message: resData});
                return;
            });
        }
    });

    // ----------------------------------------------------
    // export clutil.view.*
    _.extend(clutil.view, {
        /**
         * ファイルアップロード
         * @param filetype                  : ファイル種別: 'image', 'excel', 'csv', 'xml', 'text', 'pdf', 'zip' の中から指定すること。
         * @param maxsize                   : ファイルサイズ上限、単位は [bytes] 指定。
         * @param callback(ans, result)     : ダイアログ応答を返す。ans='correct' のとき、result にアップロード結果を通知するコールバック関数。
         * または、
         * @param opt object
         *  .title                          : モーダルダイアログのタイトル（省略可）
         *  .filetype                       : ファイル種別
         *  .maxsize                        : ファイルサイズ上限
         *  .callback(ans, result)          : コールバック関数
         *  .uploadAttributes               : アップロード要求に付加する属性情報オブジェクト（省略可）
         */
        dropzone: function(filetype, maxsize, callback){
            if(Dropzone == null){
                throw 'Requiered dropzone.js !!!!!!!!!!';
            }

            // ----------------------------------------------
            // コンフィグチェック
            if(_.isEmpty(clcom.config.uploaderURI)){
                throw '[DropzoneModalView]: uploader uri not defined, clcom.config.uploaderURI required !!!';
            }

            // ----------------------------------------------
            // 引数チェック
            var opt = null;
            if(arguments.length === 1 && _.isObject(arguments[0])){
                opt = arguments[0];
            }else if(arguments.length >= 3){
                opt = {
                    filetype: filetype,
                    maxsize: maxsize,
                    onClosed: callback
                };
            }else{
                // 引数不正
                throw '[DropzoneModalView]: Invalid arguments.';
            }

            // ファイルタイプ引数補正
            if(_.isString(opt.filetype)){
                opt.filetype = FileTypes[opt.filetype];
            }
            if(!_.contains(FileTypes, opt.filetype)){
                throw '[DropzoneModalView]: Invalid Arguments, bad filetype[' + opt.filetype + ']';
            }
            // ファイルサイズ上限
            if(!_.isFinite(opt.maxsize)){
                throw '[DropzoneModalView]: Invalid Arguments, bad maxsize[' + opt.maxsize + ']';
            }
            opt.maxsize = Number(opt.maxsize);
            if(opt.maxsize <= 0){
                throw '[DropzoneModalView]: Invalid Arguments, maxsize[' + opt.maxsize + '] must be more than 0';
            }

            // ----------------------------------------------
            // モーダル表示
            var v = new DropzoneModalView({
                options: opt
            });
            v.render();
            v.show();
            return v;
        }
    });
});
/**
 * clutil.model.*
 */
var clutil = _.extend({}, clutil);
if(!clutil.model){
    clutil.model = {};
}
$(function(){

    // -------------------------------------------
    // clutil.model.* へインポート
    _.extend(clutil.model, {
        /**
         * ドリルダウンモデル
         *
         * ・change イベントは、drill:down イベント伝播開始の合図に使う。
         * ・以後の値変更の伝播は、dril:lhit イベントを発火して知らせる。
         * ・drillSet(attrName, val) setter メソッドは、change イベント発火しない。
         * ・かわりに drill:down イベントを発火してリレーション伝播をする。
         * ・drill:down を受けた者は、drillSet() して行き、イベント伝播を繋げる。
         *
         * ＜UIオペレーションによるdrilldown開始＞
         * 一般的には change イベントなどで入力値変更を察知する。
         * ドメイン管理下 input の場合は、"cl_edit:stop" イベントで編集中状態から抜けたタイミングがとれる。
         *
         * ＜プログラムによる drilldown 制御＞
         * ・初期化 - イベント伝播オーダー定義マップをつくり、model インスタンスを作成する。
         * ・ドリルダウンイベントの監視 - 本 DrillModel インスタンスの "drill:down" イベントを監視しておく。
         *     myView.listenTo(model, 'drill:down', function(model, txAttrName, rxAttrName, rxValue){
         *         // @param model モデルインスタンス自身
         *         // @param txAttrName ドリルダウン発火元の attribute 名
         *         // @param rxAttrName ドリルダウン伝播先の attribute 名
         *         // @param rxValue ドリルダウン伝播先の現在の設定値
         *
         *         // サーバAPIからネタをとるとか・・・
         *         clutil.postJSON(uri, req, { blockUI: false}).done(function(data){
         *             // 選択肢をセットするとか、現在の設定値を戻すとかの処理
         *             // ...
         *
         *             // 自身の UI 選択値を model にも再設定する
         *             // drillSet メソッドで次の子らへイベント伝播開始する。
         *             model.drillSet(rxAttrName, newId);
         *         }).fail(function(data){
         *             // エラー時においても、次の子らへイベント伝播しておかないといけない！
         *             // たとえば、自身の txAttrName に対しては undefined にするとか。。。
         *             model.drillSet(rxAttrName, undefined);
         *         });
         *     });
         *
         */
        DrillModel: Backbone.Model.extend({
            /**
             * @param opt オプション
             * opt.order	ドリルダウンオーダーを定義したツリー構造なマッピングオブジェクト
             * ------------------------------------------------------------------
             * ＜イベント伝播オーダー例＞
             * [0] srchToDate       ┐
             * ↓[A1] srchCompany   ↓[B1] srchSaleCompany
             * ↓[A2] srchStore     ↓[B2] srchSaleStore
             * ↓[A3] srchDiv       ↓[B3] srchSaleDiv
             * ↓[A4] srchSm        ↓[B4] srchSaleSm       ──┐
             * ↓[A5] srchPlace     ↓[B5] srchSalePlace    ┐  ・[C5] srchRegi
             * ・[A6] srchCtg       ・[B6] srchSaleCtg      ・[D6] srchSalesCd
             *
             * ドリルダウン発火オーダーの定義マップ例
             * key に発火トリガーを弾いたノード、[values] に次ノードへドリルダウン伝播するノードを記述。
             *
             *     // イベント伝播オーダー
             *     var oder = {
             *          srchFromDate: [ 'srchCompany', 'srchSaleCompany' ],
             *
             *          srchCompany: [ 'srchStore' ],
             *          srchStore: [ 'srchDiv' ],
             *          srchDiv: [ 'srchSm' ],
             *          srchSm: [ 'srchPlace' ],
             *          srchPlace: [ 'srchCtg' ],
             *          srchCtg: null, // leafノードは null 留め
             *
             *          srchSaleCompany: [ 'srchSaleStore' ],
             *          srchSaleStore: [ 'srchSaleDiv' ],
             *          srchSaleDiv: [ 'srchSaleSm' ],
             *          srchSaleSm: [ 'srchSalePlace', 'srchRegi' ],
             *          srchSalePlace: [ 'srchSaleCtg', 'srchSalesCd' ],
             *          srchRegi: null, // leafノードは null 留め
             *          srchSaleCtg: null, // leafノードは null 留め
             *          srchSalesCd: null // leafノードは null 留め
             *     },
             * ------------------------------------------------------------------
             * ＜使用例＞
             * ------------------------------------------------------------------
             */
            initialize: function(attr, opt){
            	var o = opt || {};
                this.order = o.order || {};

                // 自身の change イベントを監視しつつ、
                // change イベント察知すると drill:down イベント発火へとイベント伝播を繋げる。
                this.listenTo(this, 'change', this.fireDrilldown)
            },
            /**
             * ドリルダウン発火する。
             * ただし、Backbone.Model.changed に記録済の変更 attribute に対して発火開始する。
             * 発火起点の attrubute を知るには、thisModelInstance.changed プロパティを確認する。
             */
            fireDrilldown: function(){
                var odr = this.order, keys = [], wkMap = {};

                // ツリーウォーク再帰関数
                // @param k {string} プロパティ名
                // @param isChild {boolean} 明示的に子である場合は true 指定する。
                function _walkTree(k, isChild){
                    if(!_.has(odr,k)){
                        // ツリー番外
                        return;
                    }
                    if(_.has(wkMap, k)){
                    	wkMap[k] += 1;
                    }else{
                    	wkMap[k] = (isChild) ? 1 : 0;
                    }
                    var cc = odr[k];
                    if(cc === null){
                        // leaf
                        return;
                    }
                    for(var i=0; i<cc.length; i++){
                    	_walkTree(cc[i], true);
                    }
                }

                // 発火点ルートを探る
                var changedKeys = _.keys(this.changed);
                for(var i=0; i<changedKeys.length; i++){
                    var k = changedKeys[i];
                    _walkTree(k);
                }

                // トップレベルのものだけ抽出、 drilldown イベント発火スタート
                for(var k in wkMap){
                    if(wkMap[k] === 0){
                        var v = this.get(k);
                        this.drillSet(k, v);
                    }
                }
            },
            /**
             * ドリルダウン処理用の setter 関数。change イベントを発火せぬよう SILENT setter である。
             * @param attrName 発火点 attribute 名
             * @param value 発火点の設定値
             * @return Array<string> 伝播した attribute 名を返す。
             */
            drillSet: function(attrName, value){

            	// 発火点の設定値更新 - SILENT
                var kv = {};
                kv[attrName] = value;
                this.set(kv, { silent: true } );

                // 子らへイベント伝播のトリガーを弾く
                var children = this.order[attrName] || [];
                for(var i=0; i<children.length; i++){
                    var rxAttrName = children[i], rxValue = this.get(rxAttrName), nextAttrs = this.order[rxAttrName];
                    this.trigger('drill:down', this, attrName, rxAttrName, rxValue, nextAttrs);
                }
                return children;
            },
            /**
             * 子ノードの attribute 名を返す。
             * @return Array<string> 子ノードの attribute 名
             */
            getChildren: function(attrName){
                return this.order[attrName];
            }
        })
    });

});
/**
 * サジェスト
 * Namespace: clutil.view.suggest.*
 */
var clutil = _.extend({}, clutil);
if(!clutil.view){
    clutil.view = {};
}
if(!clutil.view.suggest){
    clutil.view.suggest = {};
}
$(function(){

    /**
     * マスタアイテム選択用のサジェストAPI
     * View.el は <input type="text"> 要素である。
     *
     * Twitter Typeahead を利用。
     * 参考URL:
     * Typeahead 本体 - https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md
     * Bloodhound 検索エンジン - https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md
     */
    var MstSuggestView = clutil.view.AbstractView.extend({
        tagName: 'input',
        className: 'form-control',
        attributes: {
            type: 'text'
        },

        // typeahead カスタムイベントを取りまわす。
        events: {
            'typeahead:active': '_onActive',
            'typeahead:idle': '_onIdle',
            'typeahead:open': '_onOpen',
            'typeahead:close': '_onClose',
            'typeahead:change': '_onChange',
            'typeahead:render': '_onRender',
            'typeahead:select': '_onSelect',
            'typeahead:autocomplete': '_onAutocomplete',
            'typeahead:cursorchange': '_onCursorchange',
            'typeahead:asyncrequest': '_onAsyncrequest',
            'typeahead:asynccancel': '_onAsynccancel',
            'typeahead:asyncreceive': '_onAsyncreceive'
        },

//        model: {},          // リレーション情報は model に渡す。
//        collection: [],     // カレントの選択肢情報は collection に保持しておく。【XXX 検討中】

        /**
         * デフォルトのオプション集
         */
        defaultOption: {

            // サジェスト全般の設定情報
            settings: {
                // 'id', 'text', 'obj' から指定。
                // [types]
                // ---------------------------------------
                // id       マスタ id によるサジェスト
                // text     文字列入力支援によるサジェスト
                // obj      応答レコードサジェスト
                valueType: 'id',

                // 表示用テキスト表示フォーマッタ
                // typeahead が idle 状態（フォーカスが無いとき）かつ、disabled 時の表示形式。
                // レコードオブジェクトのプロパティ名または、function(rec){ return 表示用テキスト; } 関数が指定可能。
                // ゆえに、_.template('テンプレート') でも可。
                // デフォルトは、clutil.text.labelTextBuilder('code:name') 形式をとるフォーマッタを設定する。
                viewTextFormatter: null,

                // 編集用テキストフォーマッタ
                // typeahead が active 状態（フォーカスがあるとき）かつ、enabled 時の入力待機な状態下での表示形式。
                // レコードオブジェクトのプロパティ名または、function(rec){ return 表示用テキスト; } 関数が指定可能。
                // ゆえに、_.template('テンプレート') でも可。
                // ★ このフォーマッタは、typeahead 初期化引数 dataset.displayName プロパティに設定する。
                editTextFormatter: null,    // ★必須

                // サジェスト候補ドロップダウンの表示フォーマッタ
                // function(rec){ return htmlStr; }、_.template() でも可
                // 未指定デフォルトは、viewTextFormatter と同じであると見做す。
                // ★ このフォーマッタは、typeahead 初期化引数 dataset.templates.suggestion に設定する。
                dropdownItemFormatter: null,

                // テキスト入力中の interval 時間 (miliseconds)
                // ★Bloodhound 初期化引数 remote.rateLimitWait に設定する。
                rateLimitWait: 300,

                // サジェスト表示件数
                // ドロップダウンに表示する件数
                // ★typeahead 初期化引数 dataset.limit に設定する。
                dropdownMaxCount: 10,

                // サジェスト検索発火する文字数
                // ★typeahead 初期化引数 minLength に設定する。
                triggerLength: 1
            },

            // リレーション情報
            // this.model にリレーション情報を Backbone.Model で入れる。
            // モデルデータは、Backbone.Model で扱う。
            // 他リレーションを持つ場合は、１つの Model インスタンスを複数 MtSuggestView で共有する。
            //
            relation: {
                // デフォルト設定値

//                // リレーションデータモデルのイメージ
//                defaults: {
//                    srchiymd: clcom.limits.max_date,  // srchIYmdAttrName
//                    comp_id: 0,
//                    store_id: 0,
//                    div_id: 0,
//                    sm_id: 0,
//                    place_id: 0,
//                    ctg_id: 0       // myAttrName
//                },

                // モデルデータ中のマスタ検索日を指すプロパティ名 - 利用しない場合は undefined 明示すること！
                srchIYmdAttrName: 'srchiymd',

                // モデルのチェンジイベントにおいて、関心を持つキー
                // リレーション階層順に表記すること。
                relAttrNames: [],  // [ 'comp_id', 'store_id', 'div_id', 'sm_id', 'place_id' ]

                // モデル中における自身の id プロパティ名
                myAttrName: 'id',    // ctg_id

                // サジェスト可能なリレーショナルID を備えているかどうかチェックする。
                // デフォルト実装では、modelObj 中の relAttrNames プロパティが存在し、かつ、
                // 値が 0 以上であることで判定する。
                // もし、全 relAttrNames が完全でなくても、途中からサジェスト入力可である場合は、
                // オーバーライド実装すること。
                // @param srchIYmd iymd 形式の8桁日付数値。不正日付の場合は null or undefined を渡す。
                // @param relObj リレーショナルID設定値を格納したオブジェクト
                // @param relAttrNames リレーション階層順キー配列
                // @return true:サジェスト可能、false:サジェスト不可
                isSuggestable: function(srchIYmd, relObj, relAttrNames){
                    if(!_.isFinite(srchIYmd)){
                        return false;
                    }
                    for(var i=0; i<relAttrNames.length; i++){
                        var k = relAttrNames[i];
                        var v = parseInt(modelObj[v],10);
                        if(_.isNaN(v) || v <= 0){
                        	return false;
                        }
                    }
                    return true;
                }
            },

            // サジェスト検索サーバ情報
            remote: {
                // -- request ----------------------------------
                // サーバジョブ名
                resId: null,    // ★必須

                // 検索ワードによるリクエストを作る方法を関数で指定する
                // function(word, relation)
                buildReqByWord: null, // ★必須

                // マスタIDによるリクエストを作る方法を関数で指定する
                // function(mstId, relation)
                buildReqById: null,   // ★settings.valType == 'text' の場合に限り不要

                // -- response ---------------------------------
                // 検索結果コレクションの格納プロパティ名
                resultsPropName: 'list',    // デフォルトは rsp.list プロパティに応答レコード配列を期待。
                // 応答データの id, code, name プロパティ名辞書
                recPropName: {
                    id: 'id',       // ctg_id
                    code: 'code',   // ctg_code
                    name: 'name'    // ctg_name
                }
            },

            // typeahead カスタムイベントハンドラ
            // @see https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#custom-events
            ttEvents: {
//                onActive: function(ev){},
//                onIdle: function(ev){},
//                onOpen: function(ev){},
//                onClose: function(ev){},
//                onChange: function(ev){},
//                onRender: function(ev){},
//                onSelect: function(ev, rec){},
//                onAutocomplete: function(ev, rec){},
//                onAsyncrequest: function(ev){},
//                onAsynccancel: function(ev){},
//                onAsyncreceive: function(ev){}
            }
        },

        /**
         * 値タイプを取得する。
         * @return 値タイプ - 'text' テキストサジェスト、'id' マスタID型、'obj' マスタレコードのオブジェクト
         */
        getValueType: function(){
            return this.options.settings.valueType || 'text';
        },

        /**
         * typeahead.js の初期化引数
         * settings では手が届かないところを補完する typeahead 初期化カスタム用の情報
         */
        defaultCustomOption: {
            // typeahead 拡張引数
            // @see https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#options
            typeahead: {
                hint: true,         // サジェストのヒント
                hilight: true,      // ハイライト
                classNames: {
                    hint: 'tt-hint cl_valid_ignore'
                }
            },
            // typeahead dataset 拡張引数
            // @see https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#datasets
            datasets: {}
        },

        /**
         * @param opt0 backbone 的な引数、基本的な引数
         * @param customOpts typeahead の拡張的なオプション
         */
        initialize: function(opt, customOpts){
            clutil.bindAll(this);

            // ---------------------------------------
            // オプション引数を補完
            var o = opt || {}, dfopt = this.defaultOption;
            o.settings = _.defaults(o.settings || {}, dfopt.settings);
            o.relation = _.defaults(o.relation || {}, dfopt.relation);
            o.remote = _.defaults(o.remote || {}, dfopt.remote);
            o.ttEvents = _.defaults(o.ttEvents || {}, dfopt.ttEvents);

            // 省略パラメタの補完
            if(o.settings.viewTextFormatter == null){
                // 表示用フォーマッタ
                // 省略時は "code: name" 形式で表示する。
                var altPropName = o.remote.recPropName;
                var tmpl = _.bind(_.template('<%- clutil.text.labelTextBuilder("code:name", obj, this) %>'), altPropName);
                o.settings.viewTextFormatter = tmpl;
            }
            if(o.settings.dropdownItemFormatter == null){
                // サジェスト候補ドロップダウンの表示フォーマッタ
                // 省略時は "code: name" 形式で表示する。
                var altPropName = o.remote.recPropName;
                var tmpl = _.bind(_.template('<div><%- clutil.text.labelTextBuilder("code:name", obj, this) %></div>'), altPropName);
                o.settings.dropdownItemFormatter = tmpl;
            }
            if(o.settings.editTextFormatter == null){
                // 編集用フォーマッタ
                // 省略時は、name プロパティで表示する。
                var altPropName = o.remote.recPropName;
                o.settings.editTextFormatter = altPropName.name || altPropName.code;
            }
            // オブジェクトのプロパティ名より、値を区切り文字で連結するテンプレートを生成する関数。
            // 例： code name 値を「: 」で連結するテンプレート
            // var tmpl = tmplBuilder([ "code", "name" ], ": ");
            // tmpl({ code:'01234', name: 'item01234' }) ⇒ "01234: item01234"
            function tmplBuilder(props, delim){
                props = _.isArray(props) ? props : [ props ];
                delim = delim || ': ';
                return _.template('<%= _.compact([' + _.map(props||[],function(p){ return 'obj[\"' + p + '\"]'; }).join(',') + ']).join(\"' + delim + '\") %>');
            }
            // フォーマッタ関連でプロパティ名指定のものは、関数化する。
            function fmtFunctionary(settings, fmtProp){
                var fmtVal = settings[fmtProp];
                if(_.isFunction(fmtVal)){
                    ; // フォーマット関数が設定済と見做す
                }else{
                    var delim = ': ';          // TODO: 区切り文字変更できるようなオプション化を検討しておく。
                    settings[fmtProp] = tmplBuilder(fmtVal, delim);
                }
            }
            fmtFunctionary(o.settings, 'viewTextFormatter');
            fmtFunctionary(o.settings, 'editTextFormatter');
            fmtFunctionary(o.settings, 'dropdownItemFormatter');

            this.options = o;

            // ---------------------------------------
            // オプション.relation に基づくモデルを生成する
            if(this.model instanceof Backbone.Model){
                ; // Backbone.Model ならそのまま使う。
            }else{
                var r = o.relation;
                var modelObj = _.isObject(this.model) || {};
                if(r.srchIYmdAttrName != null && !_.has(modelObj, r.srchIYmdAttrName)){
                    // マスタ検索日のデフォルトは運用日としておく。
                    modelObj[r.srchIYmdAttrName] = clcom.ope_date;
                }
                this.model = new clutil.model.DrillModel(modelObj,{});
            }
            // モデルの change イベントを購読
            this.listenTo(this.model, 'drill:down', this._onDrilldown);

            // ---------------------------------------
            // typeahead カスタムオプション
            var co = customOpts || {}, dfopt = this.defaultCustomOption;
            co.typeahead = _.defaults(co.typeahead || {}, dfopt.typeahead);
            co.datasets = _.defaults(co.datasets || {}, dfopt.datasets);
            this.typeaheadCustomOptions = co;

            // ---------------------------------------
            // Bloodhound 検索エンジンをビルドする。
            this.bloodhound = this.buildBloodhound();
        },
        render: function(){
            this._initTypeahead();

            // リレーション設定状態に応じて、自身の入力可否を設定する。
            this.checkSuggestable();

            // this.model に基づく初期検索キックをする。
            var myAttrName = this.options.relation.myAttrName;
            if(myAttrName){
                var myId = this.model.get(myAttrName);
                if(_.isObject(myId)){
                    this.setItem(myId);
                }else if(myId){
                    this.setById(myId);
                }
            }
            return this;
        },

        /**
         * リレーション設定状態に応じて、自身の入力可否を設定する。
         * @return 入力可否状態、true:入力可、false:入力不可
         */
        checkSuggestable: function(){
            var ro = this.options.relation;
            var relObj = this.model.toJSON();
            var srchIYmd = relObj[ro.srchIYmdAttrName];
            var relAttrNames = ro.relAttrNames;

            var enable = ro.isSuggestable(srchIYmd, relObj, relAttrNames);
            this.setEnable(enable);

            return enable;
        },

        // -------------------------------------------
        // public methods

        /**
         * 参照モード（Readonly）設定
         * @param {boolean} ro true:参照モード（入力不可）、false:リレーション条件を満たす場合は入力化、満たさない場合は入力不可
         */
        setReadonly: function(ro){
            this.readonly = (ro === true);
            if(this.readonly){
                // 参照モード化 - 入力非活性にセットする。
                this.setEnable(false);
            }else{
                // 参照モード解除 - 入力活性にセットする。ただし、リレーション条件が満たしている場合に限る。
                this.checkSuggestable();
            }
        },
        /**
         * 参照モードの設定状態を返す。
         * @return {boolean} true:参照モード設定されている、false:参照モード解除されている
         */
        isReadonly: function(){
            return this.readonly === true;
        },

        /**
         * 入力可否設定
         * リレーション間の活性化条件変化によるアクセスコントロールで利用される。
         * 恒久的な入力可否設定を行いたい場合は、setReadonly() メソッドを利用すること。
         * @param {boolean} enable true:活性化、false:非活性
         */
        setEnable: function(enable){
            // 権限設定下による readonly/disabled への考慮
            if(this.readonly === true){
                console.log('MstSuggestView[readonly]: setEnable(' + enable + '->false)');
                enable = false;
            }
//            if(this.isEnabled() === enable){
//                // 変更ナシ: show/fadeIn/hide などやるので、事前チェックして余計な処理を省略する。
//                return;
//            }

            // [Typeahead 内部構造メモ] this.$el と ttInput.$input は等価である。
            var ronly = !enable, ttInput = this.$el.data('ttTypeahead').input;
            if(ronly){
                if(ttInput && ttInput.$hint){
                    // ヒント表示用の typeahead 内部 input:text 要素
                    ttInput.$hint.hide();
                }
                this.$el.prop({ disabled: ronly }).typeahead('disable');
                this.$pencilIcon.hide();
            }else{
                if(ttInput && ttInput.$hint){
                    // ヒント表示用の typeahead 内部 input:text 要素
                    ttInput.$hint.show();
                }
                this.$el.prop({ disabled: ronly }).typeahead('enable');
                this.$pencilIcon.fadeIn('fast');
            }
        },

        /**
         * 現在、非活性化しているかどうか判定
         * @return true:活性、false:非活性
         */
        isEnabled: function(){
            return this.suggestReady ? this.$el.typeahead('isEnabled') : false;
        },

        /**
         * サジェストが活性化されているかどうか判定。
         * 入力不可 disabled 状態にあれば、非活性と判定する。
         * @return true:活性、false:非活性
         */
        isActive: function(){
            var isEnabled = this.isEnabled();
            return isEnabled ? this.$el.typeahead('isActive') : false;
        },

        /**
         * サジェスト準備ができているかどうか判定
         * @param true:サジェスト初期化済、false:サジェスト破棄して素の input:text になっている。
         */
        isReady: function(){
            return this.suggestReady || false;
        },

        /**
         * サジェスト検索における ajax 呼出
         * @param req 検索条件
         * @return Ajax通信の promise オブジェクト
         */
        doSearch: function(req){
            var url = this.options.remote.resId;
            return clutil.postJSON(url, req, { blockUI: false });
        },

        /**
         * Bloodhound 内部キャッシュをクリアする。
         * 【利用想定】
         * マスタ検索日が変更されたときにキャッシュクリアする。か？
         * @param saveInput true: 入力保持する、 false: クリアする(default)
         * @return 再入力時の promise オブジェクト
         */
        clearRemoteCache: function(){
            var $input = this.$el;
            var savedWord = saveInput ? $.trim($input.val()) : '';
            var bh = this.bloodhound;
            bh.clearRemoteCache();
            return this.setWord($input, savedWord);
        },

        /**
         * 選択クリア
         * @param option
         *   .cacheClear    true: Bloodhound 内部キャッシュもクリアする。
         *   .drillSet      false明示: ドリルダウン発火する、デフォルトでは !active の場合に発火する。
         */
        clear: function(opt){
            this.explicitSelectedItem = null;
            this.$el.typeahead('val', '');
            opt = opt || {};
            if(opt.drillSet === false){
                ; // イベント伝播させないことを明示している。
            }else{
                // 下方レイヤへ変更通知する。
                var myAttrName = this.options.relation.myAttrName;
                this.model.drillSet(myAttrName, undefined);
            }

            if(opt.cacheClear){
                this.clearRemoteCache();
            }
        },

        /**
         * 現在保持中のサジェスト一覧
         */
        getSuggests: function($input){
            var $input = this.$el, minLength = this.options.settings.triggerLength;
            var rawVal = $input.val();
            if(rawVal.length < minLength){
                // 入力文字数が発火点に達していないので未選択と見做す
                return [];
            }

            // キャッシュの内部を探る
            // 最後に発火したときの LRU キャッシュキーで、キャッシュ値を参照してみる。
            var bh = this.getBloodhound, suggests = null;
            if(bh && bh.remote && bh.remote.transport && bh.remote.transport._cache){
                var hash = bh.remote.transport.lastReq;
                suggests = bh.remote.transport._cache.hash[bh.remote.transport.lastReq].val.list;
            }
            return suggests || [];
        },

        /**
         * 値を設定する。
         * @param val 設定値
         */
        setValue: function(val){
            if(val){
                var valType = this.getValueType();
                switch(valType){
                case 'id':
                    this.setById(val);
                    return;    // モデル設定は setById の先で実行。（Ajax通信により非同期）
                case 'obj':
                    this.setItem(val);
                    return;    // モデル設定は setById の先で実行。
                default:
                case 'text':
                    var str = $.trim(val);
                    this.setQueryStr(str);
                    // モデルにセット
                    var myAttrName = this.options.relation.myAttrName;
                    this.model.drillSet(myAttrName, str);
                }
            }else{
                // クリア
                this.clear();
            }
        },

        /**
         * マスタレコードのオブジェクトで値を設定する。
         * @param itemObj マスタアイテムのオブジェクト
         */
        setItem: function(itemObj){
            this.explicitSelectedItem = itemObj;
            var str = this.itemToString(itemObj);
            this.setQueryStr(str);

            // モデルにセット
            var ro = this.options.relation, myAttrName = ro.myAttrName, val = itemObj[myAttrName];
            this.model.drillSet(myAttrName, val);
        },

        /**
         * 設定値を取得する。
         * settings.valType により応答を変える。
         * @return 選択値
         *  id  : マスタアイテムのID値を返す。
         *  obj : マスタアイテム自身を返す。
         *  text: input:text の入力値そのものを返す。
         */
        getValue: function(){
            var so = this.options.settings, ro = this.options.remote;

            // テキストサジェストの場合
            if(so.valueType == 'text'){
                return this.$el.val();
            }

            var selectedItem = this.getSelectedItem();
            switch(so.valueType){
            case 'id':
                var idPropName = ro.recPropName.id;
                return (selectedItem == null) ? 0 : selectedItem[idPropName];
            case 'obj':
                return selectedItem;
            }
        },

        /**
         * 設定値の正当性をチェック
         * テキスト入力が残存している場合、選択アイテムと合致するか、途中入力かを判定する。
         * return true:空（未選択）または入力アリで選択Itemが特定できる状態、false:中途入力状態で選択肢Itemが特定できない状態。
         */
        isValid: function(){
            var rawValue = $.trim(this.$el.val());
            if(_.isEmpty(rawValue)){
                // 未設定である。required なチェックは、ドメインチェックの方にまかせる。
                return true;
            }

            // テキスト入力が残存しているので、選択アイテムチェックする。
            var so = this.options.settings;
            switch(so.valueType){
            case 'text':
            default:
                return true;
            case 'id':
            case 'obj':
                var selectedItem = this.getSelectedItem();
                return (selectedItem != null);
            }
        },

        /**
         * input に検索文字列をセットしてサジェスト検索発火する。
         * @param srchWord 検索ワード
         * @return Deferred の promise オブジェクト。promise.done() により、検索発火したときの完了タイミングをとる。
         */
        setWord: function(srchWord){
            return this.fireInputChanged(srchWord);
        },

        /**
         * クエリ文字列を設定する。
         * 利用想定は、表示上設定するだけで、変更に関してのリレーションイベント伝播はなされない。
         * なお、リレーションイベント伝播を伴う選択値設定する場合は、setValue(), setById(), setWord()
         * メソッドを使用すること。
         *
         * ＜本メソッドの利用想定＞
         * $input.val(str) においては、typeahead 内部保持の query 値に反映されないため、
         * typeahead 内部ハンドルの blur イベントにおいて typahead.Input.resetInput() によってクリアされてしまう。
         * ゆえに、表示上の文字列を設定するだけの場合は、$input.val(str) 直接行使せず、本メソッドを利用する。
         * @param str {string} クエリ文字列
         */
        setQueryStr: function(str){
            var ttInput = this.$el.data('ttTypeahead').input;
            ttInput.setQuery(str, true/*silent*/);
        },

        /**
         * 対象マスタ id によるサジェスト検索発火
         * @param srchWord 検索ワード
         * @return Deferred の promise オブジェクト。promise.done() により、検索発火したときの完了タイミングをとる。
         */
        setById: function(id){
            if(!id){
                this.clear();
                return $.Defer().resolve().done();
            }
            var that = this, so = this.options.settings, relo = this.options.relation, ro = this.options.remote;

        	var srchIYmd = this.model.get(relo.srchIYmdAttrName),
                relObj = _.pick(this.model.toJSON(), relo.relAttrNames),
                req = ro.buildReqById(id, srchIYmd, relObj);

            this.clear({ drillSet: false });
            return this.doSearch(req).done(function(data){
                var list = data[ro.resultsPropName] || [];
                if(list.length === 1){
                    // 選択！
                    var item = list[0], str = that.itemToString(item);
                    that.explicitSelectedItem = item;
                    that.setQueryStr(str);

                    // モデルに id 設定
                    // Active 時は、引き続きセレクト作業中につき、model チェンジは発生させない。
                    // iddle になったら、確定値で model チェンジを発生させる。
                    if(!that.isActive()){
                        that.model.drillSet(relo.myAttrName, id);
                    }
                }else{
                    // 複数ヒット、唯一ヒットでないとセットできない。
                    console.warn('MstSuggestView.setById(' + id +'): uri[' + that.options.remote.resId + ']: id[' + id + '] not uniq or empty? -- list.length=' + list.length);
                    that.clear();
                }
            }).fail(function(data){
                console.warn('MstSuggestView.setById[id=' + id + ']: failed -- ' + clutil.getclmsg(data));
                // エラー: 値クリアしておく
                that.clear();
            });
        },

        /**
         * 選択されたマスタアイテムを取得する。
         * settings.valType により応答を変える。
         * @return 選択値
         */
        getSelectedItem: function(){
            var rawVal = $.trim(this.$el.val());
            if(_.isEmpty(rawVal)){
                return;     // 未選択
            }

            // ひとまず、明示的指定のアイテムを保持。
            var item = this.explicitSelectedItem;
            if(item == null){
                // 無い場合は、キャッシュから推測する。
                // キャッシュが１個まで限定されているなら、選択中アイテムであると推測。
                var items = this.getSuggests();
                if(items.length === 1){
                    item = items[0];
                }
            }
            if(item == null){
                return;     // 未選択
            }

            // 表示上のテキストがマッチングしていれば選択アイテムと判定する。
            var str = this.itemToString(item);
            if(rawVal != str){
                return;     // 未選択
            }

            return item;
        },

        /**
         * 活性/非活性、Active 内部状態に応じたマスタアイテムの表示文字列を返す。
         * @param item マスタオブジェクト
         * @param fmtKind フォーマットタイプ: 表示用:"view"、編集用:"edit" を指定。
         *     未指定の場合は内部の iddle / active 状態に応じた適切な表示フォーマットを選ぶ。
         * @return 表示または編集用の input に設定するための文字列
         */
        itemToString: function(item, fmtKind){
            if(item == null || !_.isObject(item)){
                return '';
            }
            var so = this.options.settings, fmt = null;
            switch(fmtKind){
            case 'view':
                fmt = so.viewTextFormatter;
            	break;
            case 'edit':
                fmt = so.editTextFormatter;
            	break;
            }
            if(fmt == null){
                switch(this.status){
                case 'activating':
                    // 表示 → 編集モード切替中
                    fmt = so.viewTextFormatter;
                    break;
                case 'idling':
                    // 編集 → 表示モード切替中
                    fmt = so.editTextFormatter;
                    break;
                default:
                    fmt = this.isActive() ? so.editTextFormatter : so.viewTextFormatter;
                }
            }
            return fmt(item);
        },

        /**
         * サジェスト検索を発火させる。
         * @param srchWord 検索ワード。省略時は、input:text に現在設定中のテキストで検索発火させる。
         * @return Deferred の promise オブジェクト。promise.done() により、検索発火したときの完了タイミングをとる。
         */
        fireInputChanged: function(srchWord){
            var $input = this.$el;

            // TODO: プログラム起動の場合は、blockUI 有効化する？？？
            var df = $.Deferred();

            if(srchWord == null){
                srchWord = $input.val();
            }
            srchWord = $.trim(srchWord);

            // 応答返却タイミングは、typeahead カスタムイベントで取得する。
            // ・typeahead:asyncreceive
            // ・typeahead:asynccancel
            function doAsyncreceive(){
                $input.off('typeahead:asynccancel', doAsynccancel);
                df.resolve($input, srchWord, 'canceled');
            }
            function doAsynccancel(){
                $input.off('typeahead:asyncreceive', doAsyncreceive);
                df.reject($input, srchWord, 'receiveed');
            }

            // 一旦クリアな設定をする。
            // ∵）同一ワードでは検索発火しない。マスタ検索日のような内部状態値の変更も
            //     考慮するため、強制発火させるために一旦クリア設定する。
            //     Bloodhound では、検索リクエストの JSON をキーとする LRU キャッシュを備えているので、
            //     すでに LRU キャッシュに乗った場合は再検索の通信は発生しない。
            $input.typeahead('val', '');
            if(_.isEmpty(srchWord)){
                // クリア設定の場合
                df.resolve($input, '', 'clear');
            }else{
                // 入力テキスト変更する場合
                $input
                    .one('typeahead:asyncreceive', doAsyncreceive)
                    .one('typeahead:asynccancel', doAsynccancel)
                    .typeahead('val', srchWord);
            }
            return df.promise();
        },

        /**
         * Bloodhound 検索エンジンを作る内部関数
         */
        buildBloodhound: function(){
            var that = this, so = this.options.settings, relo = this.options.relation, ro = this.options.remote;
            var bh = new Bloodhound({
                datumTokenizer: function(){
                    // デバグトレースのため関数 wrap しておく。
                    // ダミーな実装としておく。bloodound.remote 採用下では使われないはず。
                    return Bloodhound.tokenizers.obj.whitespace('__dommy__');
                },
                queryTokenizer: function(str){
                    var token = $.trim(str);
                    return [ token ];
                },
                remote: {
                    url: ro.resId,        // 仮
                    /*
                     * $.ajax() のリクエストパラメタを作る。
                     * ajax 呼出は、remote.transport() 関数内で clutil.postJSON() により実装するため、
                     * 本関数が生成する ajax リクエストパラメタは $.ajax() には利用しない。
                     * ただし、bloodhound 内部の LRU キャッシュにおいて、キャッシュキーの生成に用いられるため、
                     * 適度に整えておく。
                     */
                    prepare: function(inputWord, ajaxArgs){
                        var srchIYmd = that.model.get(relo.srchIYmdAttrName),
                            relObj = _.pick(that.model.toJSON(), relo.relAttrNames),
                            req = ro.buildReqByWord(inputWord, srchIYmd, relObj);

                        // ajax 引数は clutil.postJSON() できちんと整えるので、
                        // ここではキャッシュキーになるための ajaxArgs を適当に組む。
                        _.extend(ajaxArgs, {
                            type: 'POST',
                            contentType: 'application/json',
                            data: req
                        });
                        return ajaxArgs;
                    },
                    /*
                     * $.ajax() 通信を担うところ。
                     */
                    transport: function(ajaxArgs, onSuccess, onError){
                        var req = ajaxArgs.data;
                        that.doSearch(req).done(function(){
                            if(_.isFunction(onSuccess)){
                                onSuccess.apply(this, arguments);
                            }
                        }).fail(function(){
                            if(_.isFunction(onError)){
                                onError.apply(this, arguments);
                            }
                        });
                    },
                    /*
                     * $.ajax() 応答結果の中から、サジェスト候補のデータを引き出すところ。
                     */
                    transform: function(res){
                        // ここで返ってきたデータを整形する。
                        // typeheadに渡すオプションのdisplayに設定したキーを持っているobjectの配列にする。
                        var propName = ro.resultsPropName, list = clutil.getObjProperty(res, propName);
                        return list || [];
                    },
                    // ユーザ操作中であるインターバル時間（ミリ秒）デフォルトは 300 [msec] としている。
                    // 即ち、300 [msec] 期間、ユーザ操作が無い場合、入力ひと段落と見做してサジェスト取得が発火する。
                    rateLimitWait: so.rateLimitWait
                }
            });

            return bh;
        },
        /**
         * typeahead 初期化引数を生成する内部関数
         * @return typeahead 初期化引数
         *  array[0]    : 第１引数
         *  array[1]    : 第２引数（datasets）
         */
        buildTypeaheadInitArgs: function(){
            var so = this.options.settings, ro = this.options.remote;

            // typeahead 初期化オプション
            var ttOpt = {
                minLength: so.triggerLength
            };
            // カスタムオプション拡張
            _.extend(ttOpt, this.typeaheadCustomOptions.typeahead);

            // typeahead dataset 初期化オプション
            var dispFunc = so.editTextFormatter;
            var dropdownItemFormatter = so.dropdownItemFormatter || so.viewTextFormatter;
            var dsOpt = {
                name: ro.resId,
                limit: so.dropdownMaxCount,
                source: this.bloodhound,
                display: function(){
                    // デバグトレースのため関数 wrap しておく。
                    return dispFunc.apply(null, arguments);
                },
                templates: {
                    suggestion: function(){
                        // デバグトレースのため関数 wrap しておく。
                        return dropdownItemFormatter.apply(null, arguments);
                    }
                }
            };
            // カスタムオプション拡張
            _.extend(dsOpt, this.typeaheadCustomOptions.datasets);

            return [ ttOpt, dsOpt ];
        },

        // -------------------------------------------
        // private methods
        _initTypeahead: function(){
            // 鉛筆アイコン描画
            this.$pencilIcon = this._renderPencilIcon();

            var ttArgs = this.buildTypeaheadInitArgs();
            this.$el.typeahead.apply(this.$el, ttArgs);

            this.$el.data('cl_MtSuggestView', this).addClass('cl_MtSuggestView');

            // サジェスト初期化済
            this.suggestReady = true;
        },
        _finiTypeahead: function(){
            // 鉛筆アイコン削除
            this.$pencilIcon.remove();

            var $input = this.$el;
            if(!$input.is('.cl_MtSuggestView')){
                $input.typeahead('destroy').removeData('cl_MtSuggestView').removeClass('cl_MtSuggestView');
            }

            // サジェストエンジンのキャッシュをクリアしておく。
            this.clearRemoteCache();

            // サジェスト閉塞
            this.suggestReady = false;

            return $input;
        },
        _renderPencilIcon: function(){
            var $input = this.$el, $icon = this.$pencilIcon;
            if($icon != null){
                $input.after($icon);
                return $icon;
            }
            $icon = $input.next();
            if($icon.is('.input-icon-typeahead') === false){
                // 鉛筆アイコン無し ⇒ 補完
                $icon = $('<span class="input-icon-typehead icon-abui-pencil"></span>');
                $input.after($icon);
            }
            return $icon;
        },

        // -------------------------------------------
        // イベントハンドラ
        _eventOptBus: function(ev){
            var o = this.options, h = o.handlers || {}, evName = _.last(ev.type.split(':')), fnName = clutil.text.camelCase('on_' + evName), fn = h[fnName];
            if(_.isFunction(fn)){
                fn.apply(this.$el, arguments);
            }
        },
        _onActive: function(ev){
            // IE11 特別対処
            // onIdle 時で変更がある場合、change イベント発火しないので、
            // Active-Iddle 間で入力値変更（テキストとして）有無を記録しておく。
            // 修正経緯については、後述「IE11 特別対処 [†1]」を参照のこと。
            if(clcom.isLegacyIE()){
                var rawval = this.$el.val();
                this.$el.data('cl_suggest_savedval', rawval);
            }

            // 選択確定しているアイテムを取得する。
            // 確定アイテムが存在するならば、テキストフォーマットをサジェスト入力用の形式に差替えておく。
            this.status = 'activating';
            var item = this.getSelectedItem();
            if(item != null){
                var str = this.itemToString(item, 'edit');
                this.setQueryStr(str);
                this.$el.select();
            }

            this._eventOptBus.apply(this, arguments);

            this.status = null; //'activated';

            // suggestドメイン - "cl_edit:start" イベント発火
            this.$el.trigger('cl_edit:start');
        },
        _onIdle: function(ev){
            // IE11 特別対処 [†1]
            // App 実装層で change イベント監視している場合、以下条件で change イベントが発火しない。
            // ・JS プログラム的に入力値を .val(text) で設定している。
            // ・上記設定から UI 入力による変更がなされていない場合。
            // 本 suggest 部品では、Active 状態のテキスト形式と、iddle 状態のテキスト形式の表現に
            // 違いがあることを想定しており、idle 状態遷移時において、正しい入力アイテムが存在する場合は、
            // 入力値を再フォーマットして .val(formattedText) として設定している。
            // このとき、IE では change イベント発火しなくなるため、Active 時の入力textとの比較をして、
            // change イベント発火の補助をするよう修正。
            var ie = {
                isIE: clcom.isLegacyIE(),
                changed: false
            };

            // アイテム選択確定できる場合は、表示用の形式に差替えておく。
            this.status = 'idling';
            var item = this.getSelectedItem({status: 'idling'}), myId = undefined;
            if(item != null){
                var str = this.itemToString(item, 'view');
                this.setQueryStr(str);

                // IE11 特別対処
                if(ie.isIE){
                    var rawval = this.$el.val(), savedVal = this.$el.data('cl_suggest_savedval');
                    if(rawval != savedVal){
                        ie.changed = true;
                    }
                }

                if (this.getValueType() == 'obj') {
                    myId = item[this.options.remote.recPropName.obj];
                } else {
                    myId = item[this.options.remote.recPropName.id];
                }
            }

            // モデルに設定値をセットすることで、チェンジイベント発火させる。
            var myModelAttrName = this.options.relation.myAttrName;
            this.model.set(myModelAttrName, myId);
            // IE11 特別対処
            if(ie.isIE){
                this.$el.removeData('cl_suggest_savedval');
                if(ie.changed){
                    console.info('[IE] suggest: fire changed!!!!!!!!');
                    this.$el.change();
                }
            }

            this._eventOptBus.apply(this, arguments);

            this.status = null; //'idled';

            // suggest ドメイン - "cl_edit:stop" イベント発火
            this.$el.trigger('cl_edit:stop');
        },
        _onOpen: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onClose: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onChange: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onRender: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onSelect: function(ev, rec){
            // 明示的なサジェスト選択
            this.explicitSelectedItem = rec;

            this._eventOptBus.apply(this, arguments);
        },
        _onAutocomplete: function(ev, rec){
            // 明示的なサジェスト選択
            this.explicitSelectedItem = rec;

            this._eventOptBus.apply(this, arguments);
        },
        _onCursorchange: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onAsyncrequest: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onAsynccancel: function(ev){
            this._eventOptBus.apply(this, arguments);
        },
        _onAsyncreceive: function(ev){
            // TODO: 応答を this.collection に保存しておく？    -- 検討中
            this._eventOptBus.apply(this, arguments);
        },

        // ドリルダウン
        _onDrilldown: function(m, txAttrName, rxAttrName, rxValue, nextAttrs){
            var myAttrName = this.options.relation.myAttrName;

            if(rxAttrName == myAttrName){
                // 親、txAttrName が変化されてきている。
                console.log('drill:down[' + txAttrName + ' -> ' + rxAttrName + ']=' + rxValue);

                // リレーション設定状態に応じて、自身の入力可否を設定する。
                var enabled = this.checkSuggestable();
                if(enabled){
                    // 次の階層へイベント伝播 -- これは setValue() の先で発火する
                    this.setValue(rxValue);
                }else{
                	// クリアをセットしてドリルダウン連鎖
                    this.clear();
                }
            }else{
                //
                console.log('drill:down[' + txAttrName + ' -> ' + rxAttrName + '] is not mine(' + myAttrName + '), skip.');
            }
        }
    });

    // -------------------------------------------
    // clutil.view.suggest へインポート
    _.extend(clutil.view.suggest, {
        /**
         * マスタ用のサジェストAPI
         */
        MstSuggestView: MstSuggestView

    });
});
/**
 * 共通 View クラスなど（デモ 用）
 */
var clutil = _.extend({}, clutil);
$(function () {
    if(!clutil.view){
        clutil.view = {};
    }

    /**
     * ボタン列挙な区分値選択の HTML ビルダ関数
     * @param uiType 'radio' or 'checkbox'
     * @param arg
     * @return HTML
     */
    function buildTypeSelectInputGroup(uiType, arg){
        var typelist = clcom.getTypeList({typetype_id: parseInt(arg.typeTypeId, 10)});

        // フィルタ処理
        if(arg.filter){
            var _func, typeFlt;
            if(_.isArray(arg.filter.typeIds)){
                typeFlt = function(t){
                    var index = arg.filter.typeIds.indexOf(t.type_id);
                    return (index >= 0);
                };
            }else if(_.isFunction(arg.filter.typeIds)){
                typeFlt = arg.filter.typeIds;
            }
            if(typeFlt != null){
                _func = (arg.filter.policy == 'exclude') ? _.reject : _.filter;
                typelist = _func(typelist, typeFlt);
            }
        }

        // name2 名称重複する場合、ラベルは name1 + name2 とする。
        var n2grp = _.groupBy(typelist, 'type_name2');
        for(var n2 in n2grp){
            var list = n2grp[n2];
            if(list.length >= 2){
                for(var i=0; i<list.length; i++){
                    var t = list[i];
                    t._label = t.type_name1 + t.type_name2;
                }
            }else{
                var t = list[0];
                t._label = t.type_name2;
            }
        }

        var items = [];
        for(var i=0; i<typelist.length; i++){
            var t = typelist[i];
            items.push({
                id: t.type_id,
                code: t.type_code,
                name: t._label,
                _seqno: t.type_sort
            });
        }
        if(items.length === 0){
            return '';
        }else if(items.length >= 2){
            items = _.sortBy(items, '_seqno');
        }

        switch(uiType){
        case 'radio':
            return clutil.view.buildRadioGroup({
                radio: {
                    name: arg.inputNameAttr || _.uniqueId('cl_radiogrp')
                },
                selection: {
                    items: items,
                    key: arg.key || 'id'
                },
                display: arg.display
            });
        case 'checkbox':
            return clutil.view.buildCheckboxGroup({
                checkbox: {
                    name: arg.inputNameAttr || ''
                },
                selection: {
                    items: items,
                    key: arg.key || 'id'
                },
                display: arg.display
            });
        }
    }

    /**
     * [デモ版] サイドバーメニュー
     */
    var NavSideBarView = clutil.view.AbstractView.extend({
        className: 'sidebar displaynone sidebar-fixed-top-bs',
        templates: {
            category: _.template(''
                + '<div class="ca_category">'
                +	'<% if(hasDivider){ %><div class="divider"></div><% } %>'
                +	'<% if(!_.isEmpty(category_name)){ %><p class="category"><%- category_name %></p><% } %>'
                +	'<ul><%= contentHtml %></ul>'
                + '</div>'
            ),
            node: _.template(''
                + '<li><span class="expander"><span class="caret"></span></span><span><%- name %></span></span>'
                +	'<ul class="nav expander-target displaynone ca_folder" data-app_root="<%= altAppRoot %>">'
                +		'<%= childContentHtml %>'
                +	'</ul>'
                + '</li>'
            ),
            leaf: _.template('<li data-func_code="<%= func_code %>" title="<%= func_code %>"><a href="#fakelink"><%- func_name %></a></li>')
        },

        // [XXX 暫定 XXX] TODO: メニュー情報取得して構築するようになる
        collection: [],

        events: {
            'click .expander'			: '_onClickExpander',
            'click li[data-func_code]'	: '_onClickMenuItem'		// リーフメニューアイテムクリック
        },
        initialize: function(opt){
            clutil.bindAll(this);

            // 外部イベント購読
//              this.listenTo(clutil.mediator, {
//              'subscribe:gsan_cm_menu_get': this._subscribeMenuChanged	// メニュー情報更新
//              });

            // TEST用メニュー
            if(window.TestMenuItems){
                this.collection = this.collection.concat(window.TestMenuItems);
            }
        },
        render: function(){
            // メニューツリー構築用再帰関数
            var that = this;
            function rMenuTreeWalk(item){
                if(item.kind == 'folder'){
                    var childContentHtml = '';
                    var children = item.items;
                    if(!_.isEmpty(children)){
                        for(var i=0; i<children.length; i++){
                            var child = children[i];
                            childContentHtml += rMenuTreeWalk(child);
                        }
                    }
                    var bindObj = {
                        name: item.name || '',
                        altAppRoot: item.altAppRoot,
                        childContentHtml: childContentHtml
                    };
                    return that.templates.node(bindObj);
                }else{
                    return that.templates.leaf(item);
                }
            }

            // 第１ループはカテゴリループ
            var cates = this.collection;
            for(var i=0; i<cates.length; i++){
                // 第２ループ以降、メニューツリーのループ
                var cate = cates[i];
                var contentHtml = '';
                var items = cate.items;
                for(var j=0; j<items.length; j++){
                    contentHtml += rMenuTreeWalk(items[j]);
                }
                var bindObj = {
                    hasDivider: (i > 0),
                    category_name: cate.category_name || '',
                    contentHtml: contentHtml
                };
                var cateContentHtml = this.templates.category(bindObj);
                this.$el.append(cateContentHtml);
            }

            // 現在画面のアイテムをActive表示にする
            this.setActiveMenuItem(clcom.pageId);

            // サイドバーナビ表示状態の初期設定
            if(this.isEnableSidbarToggle()){
                this.hide();
            }

            return this;
        },

        // メニュフォルダ▼クリック: ネストされたsidebar内メニュー項目の開閉処理
        _onClickExpander: function(ev){
            var $expander = $(ev.currentTarget);
            $expander.toggleClass('open');
            $expander.nextAll('.expander-target').slideToggle('fast');
        },
        // メニューアイテムクリック
        _onClickMenuItem: function(ev){
            this.hide();

            var $folder = $(ev.currentTarget).closest('.ca_folder')
            , altAppRoot = $folder.data('app_root');

            var funcCode = ev.currentTarget.getAttribute('data-func_code');
            var url = clcom.buildScreenUrl(funcCode, altAppRoot);
            clcom.pushPage({ url: url, clear: true });
        },

//        // メニュー情報更新イベント購読
//        _subscribeMenuChanged: function(arg){
//            if(arg.status == 'success'){
//                this.model = this._buildMenuRoot();
//                this.render();
//            }
//        },

        /**
         * サイドバートグル [三] が有効かどうか判定。
         * true: [三] ボタン有効、false: 無効（十分な window 幅がある）
         */
        isEnableSidbarToggle: function(){
            // スクリーンモード - 最小:"xs", 小:'s', 中:"m", 大:"xl"
            // xl のときは常に表示サイドバーメニュー
            var scMode = clutil.view.getScreenMode();
            return scMode.name != 'xl';
        },

        /**
         * 表示する
         */
        show: function(){
            if(this.isVisible()){
                return;
            }
            // application.js より移植, $('.sidebar') と this.$el は同じ。
            var $sidebar = this.$el;
            var sidebarWidth = $sidebar.width();
            $sidebar.css("left", "-" + sidebarWidth + "px");
            $sidebar.show();
            $sidebar.animate({
                left: '+=' + sidebarWidth
            }, 300);
        },
        /**
         * 折り畳む
         */
        hide: function(){
            if(!this.isVisible()){
                return;
            }
            if(!this.isEnableSidbarToggle()){
                return;
            }
            // application.js より移植, $('.sidebar') と this.$el は同じ。
            var $sidebar = this.$el;
            var sidebarWidth = $sidebar.width();
            $sidebar.animate({
                left: '-=' + sidebarWidth
            }, 300,function () {
                $sidebar.hide();
                $sidebar.css("left", "0");
            });
        },
        /**
         * 表示しているかどうか判定
         */
        isVisible: function(){
            return !(this.$el.css('display') == 'none');
        },
        /**
         * 表示する/折り畳む
         */
        toggle: function(){
            if(this.isVisible()){
                this.hide();
            }else{
                this.show();
            }
        },
        /**
         * 指定 func_code のメニューアイテムをアクティブ表示に設定
         */
        setActiveMenuItem: function(func_code){
            this.$('li[data-func_code]').removeClass('active');
            var $li = this.$('[data-func_code="' + func_code + '"]').addClass('active');
            $li.parents('.expander-target').each(function(){
                var $this = $(this);
                $this.prevAll('.expander').addClass('open');	// キャレット［▼］展開
                $this.show();
            });
        }
    });

    /**
     * [デモ版] アプリタイトルヘッダ View クラス
     */
    var AppHeaderView = clutil.view.AbstractView.extend({
        tagName: 'nav',
        className: 'navbar navbar-default navbar-lg with-sidebar with-navbar-fixed-top',
        attributes: {
            role: 'navigation'
        },
        id: 'header-content',
        templates: {
            body: _.template(''
                + '<div class="navbar-header">'
                /* controller 付きの場合は［…］アイコンを仕込む */
                + '<% if(hasController){ %>'
                +   '<button type="button" class="navbar-toggle" data-toggle="collapse" id="ca_navbar-toggle" data-target="#ca_navbar-collapse-02">'
                +     '<span class="sr-only">Toggle navigation</span>'
                +   '</button>'
                + '<% } %>'
                +   '<h2 class="navbar-title" id="ca_titles">'
                +     '<%= titles.join("<span class=\\"navbar-title-separator\\">｜</span>") %>'
                +   '</h2>'
                + '</div>'
                /* controller 部描画 */
                + '<% if(hasController){ %>'
                +   '<div class="collapse navbar-collapse" id="ca_navbar-collapse-02"><ul class="nav navbar-nav navbar-right prl"></ul></div>'
                + '<% } %>'
            ),
            titles: _.template('<%= titles.join("<span class=\\"navbar-title-separator\\">｜</span>") %>'),
            tabitem: _.template('<li class="ca_<%= kind %> ca_selectable" id="<%= id %>">'
                +   '<a>'
                +     '<% if(!_.isEmpty(options.iconL)){ %><span class="icon-abui-<%= options.iconL %> icon prx"></span><% } %>'
                +     '<%- name %>'
                +     '<% if(!_.isEmpty(options.iconR)){ %><span class="icon-abui-<%= options.iconR %> icon plx"></span><% } %>'
                +   '</a>'
                + '</li>'),
            item: _.template(''
                + '<li class="ca_<%= kind %> ca_clickable" id="<%= id %>">'
                +   '<a>'
                +     '<% if(!_.isEmpty(options.iconL)){ %><span class="icon-abui-<%= options.iconL %> icon prx"></span><% } %>'
                +     '<%- name %>'
                +     '<% if(!_.isEmpty(options.iconR)){ %><span class="icon-abui-<%= options.iconR %> icon plx"></span><% } %>'
                +   '</a>'
                + '</li>'),
            datepicker: _.template(''
                + '<li class="ca_<%= kind %> navbar-datepicker" id="<%= id %>">'
                +   '<div class="ca_navbar-datepicker-wrapper">'
                +     '<a class="ca_dpprev"><span class="icon-abui-arrow-left"></span></a>'
                +     '<input type="text" placeholder="入力または選択" class="form-control input-datepicker" disabled />'
                +     '<a class="ca_dpnext"><span class="icon-abui-arrow-right"></span></a>'
                +   '</div>'
                + '</li>'),
            'select:ym': _.template(''
                + '<li class="ca_<%= kind.replace(":","-") %>" id="<%= id %>">'
                +   '<div class="ca_navbar-select-wrapper">'
                +     '<a class="ca_ymprev"><span class="icon-abui-arrow-left"></span></a>'
                +     '<select name="input" class="select-block"></select>'
                +     '<a class="ca_ymnext"><span class="icon-abui-arrow-right"></span></a>'
                +   '</div>'
                + '</li>'),
            'button:add': _.template('<li class="ca_<%= kind.replace(":","-") %> ca_clickable" id="<%= id %>"><a><span class="icon-abui-plus icon prx"></span><%- name %></a></li>'),
            'button:reload': _.template('<li class="ca_<%= kind.replace(":","-") %> ca_clickable" id="<%= id %>"><a><span class="icon-abui-reload icon prx"></span><%- name %></a></li>')
        },
        events: {
            'click #ca_navbar-toggle'           : '_onClickNavbarToggle',
            // クリックイベント
            'click .ca_clickable'               : '_onClickItem',
            'click .ca_selectable:not(.active)' : '_onSelectTabItem',
            // datepicker
            'click .ca_dpprev,.ca_dpnext'       : '_onClickDatepickerArrow',    // 年月日: <前へ、次へ> ボタン
            'change input:text.hasDatepicker'   : '_onChangeDatepicker',
            // 年月 selector
            // TODO: 年月 <前へ、次へ> ボタンアクション
            'click .ca_ymprev,.ca_ymnext'       : '_onClickYMArrow',            // 年月:   <前へ、次へ> ボタン
            'change .ca_select-ym select'       : '_onChangeYM'
        },
        initialize: function(opt){
            opt = opt || {};
            this.options = {
                controllers: opt.controllers || []
            };

            // -----------------------------
            // 外部イベント購読
            this.listenTo(clutil.mediator, {
                'screen-mode:change'    : this._subscribeScreenModeChanged,		// ウィンドウリサイズ
                'html:click'            : this._subscribeOutsideClicked			// 大域クリックイベント
            });
        },
        render: function(){
            // 枠組みを構築する
            var cc = this.options.controllers || [];
            var bindObj = {
                hasController: cc.length > 0,
                titles: this.model.titles || []
            };
            this.$el.html(this.templates.body(bindObj));

            // コントローラを構築する
            var $ul = this.$('#ca_navbar-collapse-02 > ul');
            for(var i=0; i < cc.length; i++){
                var it = cc[i];
                var tmpl = this.templates[it.kind];
                if(tmpl == null){
                    throw 'AppHeaderView: unknown controller kind: ' + it.kind;
                }
                var $it = $(tmpl(_.extend({ options:{} }, it))).data('ca_item', it);
                switch(it.kind){
                case 'datepicker':
                    // Datepicker 初期化
                    var $dp = $it.find('input:text.input-datepicker');
                    var dpOpt = _.defaults(it.options||{}, { dateFormat: 'yy/mm/dd（D）' });
                    clutil.view.datepicker($dp, dpOpt);
                    break;
                case 'select:ym':
                    // 年月セレクタ初期化
                    var $select = $it.find('select');
                    if(it.options == null){
                        it.options = {};
                    }
                    var opt = it.options;
                    opt.minYM = (opt.minYM != null) ? parseInt(opt.minYM,10) : Math.floor(clcom.limits.min_date / 100);
                    opt.maxYM = (opt.maxYM != null) ? parseInt(opt.maxYM,10) : Math.floor(clcom.limits.max_date / 100);
                    var ymCollection = function(fromYM, toYM){
                        var xx = [];
                        for(var curYM = fromYM; curYM <= toYM; ){
                            var y = Math.floor(curYM / 100), m = curYM % 100;
                            xx.push({
                                id: curYM,
                                name: y + '/' + m
                            });
                            if(m < 12){
                                curYM += 1;
                            }else {
                                curYM = ((y+1) * 100) + 1;
                            }
                        }
                        return xx;
                    }(opt.minYM, opt.maxYM);
                    clutil.view.initSelectpicker({
                        $el: $select,
                        selection: {
                            items: ymCollection
                        }
                    });
//                    // 初期選択値は運用日年月
//                    var opeYM = Math.floor(clcom.ope_date / 100);
//                    if(opeYM < opt.minYM){
//                        opeYM = opt.minYM;
//                    }else if(opeYM > opt.maxYM){
//                        opeYM = opt.maxYM;
//                    }
//                    $select.selectpicker('val', opeYM);
                }
                $ul.append($it);
            }

            // 初期表示値を設定する。
            var selectedTabId = this.model.selectedTabItem;
            if(selectedTabId){
                this.setSelectedTabItem(selectedTabId);
            }
            var dpItems = _.filter(cc, {kind:'datepicker'});
            if(!_.isEmpty(dpItems)){
                var iniVal = this.model.ymd || clcom.ope_date;
                for(var i=0; i<dpItems.length; i++){
                    var it = dpItems[i];
                    this.setDate(it.id, iniVal);
                }
            }
            var ymItems = _.filter(cc, {kind:'select:ym'});
            if(!_.isEmpty(ymItems)){
                var iniVal = this.model.ym || Math.floor(clcom.ope_date / 100);
                for(var i=0; i<ymItems.length; i++){
                    var it = ymItems[i];
                    this.setYM(it.id, iniVal);
                }
            }

            // Bootstrap collapse 初期化
//            this.$('#ca_navbar-collapse-02').collapse('hide');
//            this.compactNavi();
//            this.$('#ca_navbar-collapse-02').collapse();//.collapse('hide');
//            _.defer(this.compactNavi);

            return this;
        },
        // スクリーンモード "xs" 時の［…］トグル
        _onClickNavbarToggle: function(ev){
            var $btn = $(ev.currentTarget);
            if($btn.is('[data-target]')){
                return;	// Bootstrap で collapse 開閉制御
            }
            var $c = this.$('#ca_navbar-collapse-02');
            $c.collapse('toggle');
        },
        // --- 外部イベントハンドリング -------------------------------
        // ウィンドウサイズ変更監視
        _subscribeScreenModeChanged: function(curMode, prevMode){
            // [xs]時 collapse の dropdown 表示
            if(prevMode.name == 'xs'){
                this.compactNavi();
            }
        },
        // ナビ外部クリック監視
        _subscribeOutsideClicked: function(ev){
            var isMyContent = this.$el.has(ev.target).length > 0;
            if(isMyContent === false){
                this.compactNavi();
            }
        },

        // --- コントローラ部イベントハンドリング ---------------------
        // クリック
        _onClickItem: function(ev){
            var $li = $(ev.currentTarget).closest('li');
            var it = $li.data('ca_item');
            if(it == null){
                return;
            }
            // イベント発火で App の MainView と連携。
            this.trigger(it.kind + ':click', it, ev);
        },
        // タブ選択
        _onSelectTabItem: function(ev){
            var $li = $(ev.currentTarget).closest('li');
            var it = $li.data('ca_item');
            if(it == null){
                return;
            }
            this.setSelectedTabItem(it.id);
            // イベント発火で App の MainView と連携。
            this.trigger(it.kind + ':select', it, ev);
        },
        // 年月日「＜」「＞」ボタンクリック
        _onClickDatepickerArrow: function(ev){
            var $btn = $(ev.currentTarget)
            , $li = $btn.closest('li')
            , it = $li.data('ca_item');
            if(it == null){
                return;
            }

            var $dp = $btn.parent().find('input.hasDatepicker'),
            curDate = $dp.datepicker('getDate'),
            dt = $btn.hasClass('ca_dpprev') ? -1 : 1,
                    date = clutil.date.computeDate(curDate, 0/*-y-*/, 0/*-m-*/, dt/*-d-*/);
            this.setDate(it.id, date, true);
        },
        // 年月日変更
        _onChangeDatepicker: function(ev){
            console.info('_onChangeDatepicker: ' + ev);

            var $dp = $(ev.currentTarget)
            , dt = $dp.datepicker('getDate')
            , $li = $dp.closest('li')
            , it = $li.data('ca_item');
            if(it == null){
                return;
            }

            // <前へ|次へ> UI 調整
            this._updateDatepickerArrowUI($li);

            this.trigger(it.kind + ':change', dt, it);
        },
        // 年月「＜」「＞」ボタンクリック
        _onClickYMArrow: function(ev){
            var $btn = $(ev.currentTarget)
            , $li = $btn.closest('li.ca_select-ym')
            , it = $li.data('ca_item');
            if(it == null){
                return;
            }

            var $select = $li.find('select')
                , curYM = $select.selectpicker('val')
                , curDate = clutil.date.toDateByYM(curYM)
                , dMonth = $btn.hasClass('ca_ymprev') ? -1 : 1
                , date = clutil.date.computeDate(curDate, 0/*-y-*/, dMonth/*-m-*/)
                , ym = date.getFullYear() * 100 + (date.getMonth() + 1);
            this.setYM(it.id, ym, true);
        },
        // 年月変更
        _onChangeYM: function(ev){
            var $select = $(ev.currentTarget)
            , ym = $select.selectpicker('val')
            , $li = $select.closest('li.ca_select-ym')
            , it = $li.data('ca_item');
            if(it == null){
                return;
            }
            // <前へ|次へ> UI 調整
            this._updateYMArrowUI($li);

            this.trigger(it.kind + ':change', ym, it);
        },

        /**
         * コントローラ部のメニューを折り畳む。
         * 視覚的効果は、スクリーンモードが "xs" の場合のみ有効。
         */
        compactNavi: function(){
            // Dropdown メニューが開いている状態 ⇒ hasClass('in') で判断。
            var $c = this.$('#ca_navbar-collapse-02');
            $c.hasClass('in') && $c.collapse('hide');
        },
        /**
         * コントローラ部のメニューを展開する。
         * 視覚的効果は、スクリーンモードが "xs" の場合のみ有効。
         */
        expandNavi: function(){
            var $c = this.$('#ca_navbar-collapse-02');
            $c.collapse('show');
        },

        /**
         * 初期化時に渡した controllers 情報を取得する。
         * @param itemId - controller の id プロパティ値
         * @return controller 情報
         */
        getControllMetaData: function(itemId){
            return _.find(this.options.controllers, {id: itemId});
        },
        /**
         * タブ選択する
         */
        setSelectedTabItem: function(itemId, publish){
            var $li = this.$('#' + itemId);
            var it = $li.data('ca_item');
            if(it == null){
                return;
            }

            // 'active' クラス設定
            this.$('#ca_navbar-collapse-02 > ul > li.ca_tabitem').removeClass('active');
            $li.addClass('active');

            if(publish === true){
                // イベント発火で App の MainView と連携。
                this.trigger(it.kind + ':select', it, ev);
            }
        },
        /**
         * 選択タブに対するメタ情報を取得する。
         */
        getSelectedTabItem: function(){
            var $tabItem = this.$('#ca_navbar-collapse-02 > ul > li.ca_tabitem.active');
            var it = $tabItem.data('ca_item');
            return it;
        },
        /**
         * 指定 itemId に対する日付コントローラに日付を設定する。
         * @param itemId 設定対象日付コントローラの id
         * @param date 日付: Date、yyyyMMdd 数値、文字列など曖昧な指定可
         * @param publish true 明示で変更イベントを発火する。省略可
         */
        setDate: function(itemId, date, publish){
            // itemId リソースチェック
            var $li = this.$('#' + itemId), $dp = $li.find('input.hasDatepicker'), it = $li.data('ca_item');
            if($li.length === 0 || $dp.length === 0 || it == null){
                return;
            }

            // Datepicker セット
            var fixDate = _.isDate(date) ? clutil.date.clearHms(date) : clutil.date.toDate(date);
            var dpMinDate = clutil.date.clearHms($dp.datepicker('option', 'minDate'))
            , dpMaxDate = clutil.date.clearHms($dp.datepicker('option', 'maxDate'));
            if(!(dpMinDate.getTime() <= fixDate.getTime() <= dpMaxDate.getTime())){
                // 最大 / 最小範囲チェック
                if(fixDate.getTime() < dpMinDate.getTime()){
                    fixDate = new Date(dpMinDate);
                }else if(dpMaxDate.getTime() < fixDate.getTime()){
                    fixDate = new Date(dpMaxDate);
                }
                console.warn('select:datepicker#' + it.id +': date'
                    + '['+ clutil.date.dateFormat('yyyy/MM/dd', date) + '] out of range'
                    + '[' + clutil.date.dateFormat('yyyy/MM/dd', dpMinDate) + '-' + clutil.date.dateFormat('yyyy/MM/dd', dpMaxDate) + ']'
                    + ', fixed[' + clutil.date.dateFormat('yyyy/MM/dd', fixDate) + ']');
            }
            $dp.datepicker('setDate', fixDate);

            // 前「＜」、次「＞」ボタンＵＩ調整
//            var $prev = $li.find('.ca_dpprev'), $next = $li.find('.ca_dpnext')
//            , vPrev = 'visible', vNext = 'visible'
//                , minDate = $dp.datepicker('option', 'minDate')
//                , maxDate = $dp.datepicker('option', 'maxDate');
//            if(fixDate <= minDate){
//                vPrev = 'hidden';
//            }else if(fixDate >= maxDate){
//                vNext = 'hidden';
//            }
//            $prev.css('visibility', vPrev);
//            $next.css('visibility', vNext);
            this._updateDatepickerArrowUI($li);

            // イベント通知
            if(publish === true){
                this.trigger(it.kind + ':change', date, it);
            }
        },
        // 年月日: <前へ|次へ> ボタンUI調整
        _updateDatepickerArrowUI: function($li){
            var it = $li.data('ca_item');
            if(it == null || it.kind != 'datepicker'){
                return;
            }
            var $dp = $li.find('input.hasDatepicker'), date = $dp.datepicker('getDate');
            var $prev = $li.find('.ca_dpprev'), $next = $li.find('.ca_dpnext')
            , vPrev = 'visible', vNext = 'visible'
                , minDate = $dp.datepicker('option', 'minDate')
                , maxDate = $dp.datepicker('option', 'maxDate');
            if(date <= minDate){
                vPrev = 'hidden';
            }else if(date >= maxDate){
                vNext = 'hidden';
            }
            $prev.css('visibility', vPrev);
            $next.css('visibility', vNext);
        },
        /**
         * 指定 itemId に対する日付コントローラより、設定値を取得する。
         * @param itemId 初期化時に渡した controllers の id
         * @return 設定値 (Date オブジェクト)
         */
        getDate: function(itemId){
            // itemId リソースチェック
            var $li = this.$('#' + itemId), $dp = $li.find('input.hasDatepicker'), it = $li.data('ca_item');
            if($li.length === 0 || $dp.length === 0 || it == null){
                return;
            }
            return $dp.datepicker('getDate');
        },
        /**
         * 指定 itemId に対する年月選択コントローラに年月を設定する。
         * @param itemId 設定対象年月コントローラの id
         * @param ym 年月 6桁 yyyyMM 数値 or 文字列
         * @param publish true 明示で変更イベントを発火する。省略可
         */
        setYM: function(itemId, ym, publish){
            // itemId リソースチェック
            var $li = this.$('#' + itemId), $select = $li.find('select'), it = $li.data('ca_item');
            if($li.length === 0 || $select.length === 0 || it == null){
                return;
            }

            var fixYM = parseInt(ym, 10);
            if(!(it.options.minYM <= fixYM && fixYM <= it.options.maxYM)){
                if(fixYM < it.options.minYM){
                    fixYM = it.options.minYM;
                }else if(fixYM > it.options.maxYM){
                    fixYM = it.options.maxYM;
                }
                console.warn('select:ym#' + it.id +': ym['+ ym + '] out of range[' + it.options.minYM + '-' + it.options.maxYM + '], fixed[' + fixYM + ']');
            }
            $select.selectpicker('val', fixYM);

            // 前「＜」、次「＞」ボタンＵＩ調整
//            var $prev = $li.find('.ca_ymprev'), $next = $li.find('.ca_ymnext')
//            , vPrev = 'visible', vNext = 'visible';
//            if(fixYM <= it.options.minYM){
//                vPrev = 'hidden';
//            }else if(fixYM >= it.options.maxYM){
//                vNext = 'hidden';
//            }
//            $prev.css('visibility', vPrev);
//            $next.css('visibility', vNext);
            this._updateYMArrowUI($li);

            // イベント通知
            if(publish === true){
                this.trigger(it.kind + ':change', fixYM, it);
            }
        },
        // 年月: <前へ|次へ> ボタンUI調整
        _updateYMArrowUI: function($li){
            var it = $li.data('ca_item');
            if(it == null || it.kind != 'select:ym'){
                return;
            }

            var $select = $li.find('select');
            var ym = parseInt($select.selectpicker('val'),10);

            // 前「＜」、次「＞」ボタンＵＩ調整
            var $prev = $li.find('.ca_ymprev'), $next = $li.find('.ca_ymnext')
            , vPrev = 'visible', vNext = 'visible';
            if(ym <= it.options.minYM){
                vPrev = 'hidden';
            }else if(ym >= it.options.maxYM){
                vNext = 'hidden';
            }
            $prev.css('visibility', vPrev);
            $next.css('visibility', vNext);
        },
        /**
         * 指定 itemId に対する年月選択コントローラより、設定値を取得する。
         * @param itemId 初期化時に渡した controllers の id
         * @return 設定値
         */
        getYM: function(itemId){
            var $li = this.$('#' + itemId), $select = $li.find('select');
            if($select.length === 0){
                return;
            }
            return parseInt($select.selectpicker('val'),10);
        },
        /**
         * メインタイトルを設定する
         * @param title タイトル文字列
         */
        setMainTitle: function(title){
            var titles = this.model.titles;
            titles[0] = title;
            this.setTitles(titles);
        },
        /**
         * サブタイトルを設定する
         * @param stitle サブタイトル文字列 or 配列
         */
        setSubTitle: function(stitle){
            var mtitles = this.model.titles.slice(0,1);
            if(mtitles.length === 0){
                mtitles.push('--');
            }
            var stitles = _.isArray(stitle) ? stitle : [ stitle ];
            var titles = mtitles.concat(stitles);
            this.setTitles(titles);
        },
        /**
         * タイトル、サブタイトル、...を設定する。
         * @param titles タイトル配列
         */
        setTitles: function(titles){
            if(_.isString(titles)){
                titles = [ titles ];
            }
            this.model.titles = titles;
            this.$('#ca_titles').html(this.templates.titles({titles: titles}));
        }
    });

    /**
     * [デモ版] 共通ヘッダ（青ヘッダ）
     *
     * ＜モデルデータ例＞
     *	model: {
     *		navMenuKind: 'sidebar',		// サイドバーナビ:'sidebar'、戻るボタン:'backbtn'
     *		productName: '製品名',
     *		dateLabel: '日付ラベル',
     *		userNameLabel: 'ユーザ名ラベル',
     *		enableGuide: false			// ガイド（？）有無 - true:有、false:無
     *	};
     */
    var BlueHeaderView = clutil.view.AbstractView.extend({
        id: 'header-site',
        className: 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top with-sidebar-below',
        attributes: {
            role: 'navigation'
        },
        template: _.template(''
            // サイドバー部
            + '<div class="navbar-header">'
            +   '<button type="button" class="sidebar-toggle" id="ca_sidebarToggleBtn">'
            +       '<span class="sr-only">Toggle sidebar</span>'
            +   '</button>'
            +   '<% if(navMenuKind == "sidebar"){ %>'
            +       '<button type="button" class="navbar-toggle" data-toggle="collapse" id="ca_navbar-toggle" data-target="#ca_navbar-collapse-01">'
//          +       '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ca_navbar-collapse-01">'
//          +       '<button type="button" class="navbar-toggle" data-toggle="collapse" id="ca_toggle_collapsed">'
            +           '<span class="sr-only">Toggle navigation</span>'
            +           '<span class="icon-bar"></span>'
            +           '<span class="icon-bar"></span>'
            +           '<span class="icon-bar"></span>'
            +       '</button>'
            +   '<% }else{ %>'
            +       '<a class="btn-back" id="ca_backBtn" style="z-index: 1">'
            +           '<span class="icon icon-abui-arrow-left"></span>'
            +       '</a>'
            +   '<% } %>'
            +   '<a id="ca_home"><h1 class="navbar-title"><%- productName %></h1></a>'
            + '</div>'
            // 青ヘッダ部: .cl_fixtop は、上部に貼りつくコンテントであることのマーカークラス -- scrollTop 補正項を求めるのに使う。
            + '<div class="navbar-collapse collapse cl_fixtop" id="ca_navbar-collapse-01">'
//          + '<div class="navbar-collapse collapse" id="ca_navbar-collapse-01">'
            +   '<div class="navbar-right">'
            +       '<p class="navbar-text navbar-logininfo">'
            +           '<span class="datetime" id="ca_hd_datelabel"><%= dateLabel %></span>'
            +           '<span class="username"><%= userNameLabel %></span>'
            +           '<span><a id="ca_logoutBtn">ログアウト</a></span>'
            +       '</p>'
            +   '<% if(enableGuide){ %>'
            +       '<p class="navbar-icon"><a id="ca_guideBtn"><span class="icon-abui-guide"></span></a></p>'
            +   '<% } %>'
            +   '</div>'
            + '</div>'
        ),
        events: {
            'click #ca_sidebarToggleBtn'    : '_onClickSidebarToggleBtn',
            'click #ca_backBtn'             : '_onClickBackBtn',
            'click #ca_home'                : '_onClickHome',
            'click #ca_logoutBtn'           : '_onClickLogoutBtn',
            'click #ca_guideBtn'            : '_onClickGuideBtn',
            'click #ca_navbar-toggle'       : '_onClickNavbarToggle'
        },
        initialize: function(opt){
            // 表示ネタ、表示方法などの情報を渡す
            this.options = opt || {};

            // 運用日変更イベント監視
            this.listenTo(clutil.mediator, 'clcom.ope_date:change', this.setOpeIymd);
        },

        render: function(){
            function buildUserName(u, anonymous){
                var xx = [];
                if(u.name_l){
                    xx.push(u.name_l);
                }
                if(u.name_f){
                    xx.push(u.name_f);
                }
                if(_.isEmpty(xx) && anonymous){
                    xx.push(anonymous);	// 名無し
                }
                return xx.join(' ');
            }
            var defaultModel = {
                navMenuKind: 'sidebar',     // 'sidebar' or 'backbtn'
                productName: clcom.productInfo.serviceName,
                dateLabel: (clcom.ope_date > 0) ? clutil.date.dateFormat('yyyy/MM/dd', clcom.ope_date) : '----/--/--',
                userNameLabel: buildUserName(clcom.userInfo, 'anonymous'),
                enableGuide: false
            };
            var bindObj = _.defaults(this.model || {}, defaultModel);
            var contentHtml = this.template(bindObj);
            this.$el.html(contentHtml);

            // Bootstrap collapse 初期化
//          this.$('#ca_navbar-collapse-01').collapse('hide');
//          this.compactNavi();
//          this.$('#ca_navbar-collapse-01').collapse();//.collapse('hide');
//          _.defer(this.compactNavi);

            return this;
        },
        // サイドバー［三］ボタンクリック
        _onClickSidebarToggleBtn: function(ev){
            this.trigger('sidebar:toggle', {src: this});
        },
        // 戻るボタン
        _onClickBackBtn: function(ev){
            clcom.popPage();
        },
        // ホーム
        _onClickHome: function(ev){
            clcom.gohome();
        },
        // ログアウトボタン
        _onClickLogoutBtn: function(ev){
            clcom.logout();
        },
        // ガイドボタン
        _onClickGuideBtn: function(ev){
            // TODO: ガイド？
            alert('TODO: ガイド？');
        },
        // スクリーンモード "xs" 時の［…］トグル
        _onClickNavbarToggle: function(ev){
            var $btn = $(ev.currentTarget);
            if($btn.is('[data-target]')){
                return;	// Bootstrap で collapse 開閉制御
            }
            var $c = this.$('#ca_navbar-collapse-01');
            $c.collapse('toggle');
        },
        /**
         * 運用日ラベルを設定する
         * @param iymd 運用日(8桁数値 yyyyMMdd 形式)
         */
        setOpeIymd: function(iymd){
            var dateLabel = (iymd > 0) ? clutil.date.dateFormat('yyyy/MM/dd', iymd) : '----/--/--';
            this.$('#ca_hd_datelabel').text(dateLabel);
        },
        /**
         * 「ログアウト」等の青ヘッダメニューを折り畳む。
         * 視覚的効果は、スクリーンモードが "xs" の場合のみ有効。
         */
        compactNavi: function(){
            // Dropdown メニューが開いている状態 ⇒ hasClass('in') で判断。
            var $c = this.$('#ca_navbar-collapse-01');
            $c.hasClass('in') && $c.collapse('hide');
        },
        /**
         * 「ログアウト」等の青ヘッダメニューを展開する。
         * 視覚的効果は、スクリーンモードが "xs" の場合のみ有効。
         */
        expandNavi: function(){
            var $c = this.$('#ca_navbar-collapse-01');
            $c.collapse('show');
        },
        /**
         * サイドバートグル [三] が有効かどうか判定。
         * true: [三] ボタン有効、false: 無効（十分な window 幅がある）
         * undefined: [三]トグルボタンが無い。
         */
        isEnableSidbarToggle: function(){
            return this.$('#ca_sidebarToggleBtn').css('display') !== 'none';
        }
    });

    // --------------------------------------------------------
    // clutil.view.* エクスポート
    _.extend(clutil.view, {

        /**
         * [デモ版] 共通ヘッダ＋サイドバーナビ
         * @param options
         * 	.headerModel: Object	-- 青ヘッダを構築するためのカスタムモデルデータ。
         */
        HeaderView: clutil.view.AbstractView.extend({
            el: '#cl_header',
            initialize: function(opt){
                var fixOpt = opt || {};

                // 青ヘッダ
                var headerModel = fixOpt.headerModel || {};
                this.headerView = new BlueHeaderView({ model: headerModel });

                // サイドバー
                var naviMenuKind = headerModel.naviMenuKind || 'sidebar';
                if(naviMenuKind == 'sidebar'){
                    /*
                     * サイドバーナビ
                     */
                    this.navSideBarView = new NavSideBarView();

                    // 共通ヘッダ部からサイドバーナビ[三]アイコントグルイベントを購読
                    // → サイドバーナビの表示・非表示トグルを行う。
                    this.listenTo(this.headerView, 'sidebar:toggle',this._onToggleSidebar);

                    // -----------------------------
                    // 外部イベント購読
                    this.listenTo(clutil.mediator, {
                        'screen-mode:change'    : this._subscribeScreenModeChanged,	    // ウィンドウリサイズ
                        'html:click'            : this._subscribeOutsideClicked	        // 大域クリックイベント
                    });
                }else{
                    // [サイドバーナビなし]
                    // body 全体で、side-bar 幅調整用スタイルに起因するクラスを取り除く
                    $('.with-sidebar').removeClass('with-sidebar');
                }
            },

            render: function(){
                // サイドバー
                if(this.navSideBarView != null){
                    this.navSideBarView.render();
                    var sc = clutil.view.getScreenMode();
                    if(sc.name == 'xl'){
                        _.defer(this.navSideBarView.show);
                    }
                    this.$el.append(this.navSideBarView.$el);
                }
                // 青ヘッダ
                this.$el.append(this.headerView.render().$el);
                return this;
            },

            // [三]ボタントグル
            _onToggleSidebar: function(arg){
                this.navSideBarView.toggle();
            },
            // ウィンドウサイズ変更監視
            _subscribeScreenModeChanged: function(curMode, prevMode){
                // 青ヘッダの表示
                if(prevMode.name === 'xs'){
                    this.headerView.compactNavi();
                }
                // サイドバー常時表示
                if(curMode.name === 'xl'){
                    this.navSideBarView.$el.css("display","block");
                }else if(prevMode.name === 'xl'){
                    this.navSideBarView.$el.hide().css("left", "0");
                }
            },
            // ナビ外部クリック監視
            _subscribeOutsideClicked: function(ev){
                // サイドバーメニュー開閉
                var isMyContent = this.$el.has(ev.target).length > 0;
                if(isMyContent === false){
                    // 外部のクリックイベント
                    if(this.headerView.isEnableSidbarToggle() === true/* [三]ボタンが有効な場合はメニューを隠す */){
                        this.navSideBarView.hide();
                    }
                }

                // 青ヘッダ[xs]サイズ時の Dropdown メニュー開閉
                var isHdContent = this.headerView.$el.has(ev.target).length > 0;
                if(isHdContent === false){
                    // Dropdown が開いている場合は閉じる。
                    this.headerView.compactNavi();
                }

            },
            __eof:'clutil.view.HeaderView'
        }),

        /**
         * アプリタイトル部の共通ヘッダ View クラス
         */
        AppHeaderView: AppHeaderView,

        /**
         * 区分セレクタ
         * @param arg		: オプション引数。
         * {
         * 	.$el			: <select> 要素
         * 	.typeTypeId		: [必須] 区分種別 ID。区分種別については、tmcm_type.js 参照。
         * 	.key			: [省略可] 'id' or 'code'、デフォルト 'id'。
         * 	.hasNullItem	: [省略可] 未選択要素の有無。省略時は true。オブジェクト { id, name } 指定で未選択アイテムのカスタム可。
         * 	.labelFormat	: [省略可] 選択肢のラベル形式。[ 'code:name', 'name', 'code' ] のいずれか指定。未指定は 'name' を適用する。
         * 	.filter			: [省略可] フィルタオプション。
         * 		.policy		: 'include' or 'exclude'、以下 .typeTypeIds に含む or 除外。デフォルト 'include'。
         * 		.typeIds	: フィルタ対象区分値の配列、またはフィルタ関数。
         * }
         */
        initTypeSelector: function(arg){
//          var sample = {
//          "typetype_id": 30,
//          "typetype_name": "スタッフ業務種別",
//          "type_id": 4,
//          "type_code": "04",
//          "type_name1": "出張面接官",
//          "type_name2": "出張面接官",
//          "type_sort": 4
//          };
            var typelist = clcom.getTypeList({typetype_id: parseInt(arg.typeTypeId, 10)});

            // フィルタ関数整理
            var _func, typeFlt;
            if(arg.filter){
                if(_.isArray(arg.filter.typeIds)){
                    typeFlt = function(t){
                        var index = arg.filter.typeIds.indexOf(t.type_id);
                        return (index >= 0);
                    };
                }else if(_.isFunction(arg.filter.typeIds)){
                    typeFlt = arg.filter.typeIds;
                }
                if(typeFlt != null){
                    _func = (arg.filter.policy == 'exclude') ? _.reject : _.filter;
                }
            }

            // type_name1 でグルーピング
            var groupBy = _.groupBy(typelist, 'type_name1');
            var items = [];
            for(var name1 in groupBy){
                var types = groupBy[name1];
                if(_func){
                    types = _func(types, typeFlt);
                }
                if(types.length === 0){
                    continue;
                }else if(types.length === 1){
                    var c0 = types[0];
                    items.push({
                        id: c0.type_id,
                        code: c0.type_code,
                        name: c0.type_name2,
                        _seqno: c0.type_sort
                    });
                }else/*length > 1*/{
                    var citems = _.chain(types).sortBy('type_sort').reduce(function(cc, t){
                        cc.push({
                            id: t.type_id,
                            code: t.type_code,
                            name: t.type_name2,
                            _seqno: t.type_sort
                        });
                        return cc;
                    }, []).value();
                    var c0 = citems[0];
                    items.push({
                        label: name1,
                        items: citems,
                        _seqno: c0._seqno
                    });
                }
            }
            if(items.length > 1){
                items = _.sortBy(items, '_seqno');
            }
            clutil.view.initSelectpicker({
                $el: arg.$el,
                selection: {
                    items: items,
                    hasNullItem: arg.hasNullItem,
                    labelFormat: arg.labelFormat,
                    key: arg.key || 'id'
                }
            });
        },

        /**
         * 区分選択肢用のチェックボックスグループビルダ
         * @param arg
         * {
         * 	.typeTypeId		: [必須] 区分種別 ID。区分種別については、tmcm_type.js 参照。
         * 	.key			: [省略可] 'id' or 'code'、デフォルト 'id'。
         * 	.filter			: [省略可] フィルタオプション。
         * 		.policy		: 'include' or 'exclude'、以下 .typeTypeIds に含む or 除外。デフォルト 'include'。
         * 		.typeIds	: フィルタ対象区分値の配列、またはフィルタ関数。
         * }
         * @return checkbox チェックボックスグループの html を文字列で返す。
         */
        buildTypeCheckboxGroup: function(arg){
            return buildTypeSelectInputGroup('checkbox', arg);
        },

        /**
         * 区分選択肢用のラジオボタングループビルダ
         * @param arg
         * {
         * 	.typeTypeId		: [必須] 区分種別 ID。区分種別については、tmcm_type.js 参照。
         * 	.key			: [省略可] 'id' or 'code'、デフォルト 'id'。
         * 	.filter			: [省略可] フィルタオプション。
         * 		.policy		: 'include' or 'exclude'、以下 .typeTypeIds に含む or 除外。デフォルト 'include'。
         * 		.typeIds	: フィルタ対象区分値の配列
         * }
         * @return checkbox チェックボックスグループの html を文字列で返す。
         */
        buildTypeRadioGroup: function(arg){
            return buildTypeSelectInputGroup('radio', arg);
        }
    });

}());
(function (Syphon, clutil) {

  ////////////////////////////////////////////////////////////////
  Syphon.ElementExtractor = function ($view) {
    return $view.find('input,textarea,select,span[data-name]');
  };

  ////////////////////////////////////////////////////////////////
  // KeyExtractor
  var supportedTags = ['span'];
  _.each(supportedTags, function (tagName) {
    Syphon.KeyExtractors.register(tagName, function ($el) {
      return $el.attr('data-name');
    });
  });

  ////////////////////////////////////////////////////////////////
  // InputReader
  Syphon.InputReaders.register('text', function ($el) {
    var val = $el.val();

    if ($el.hasClass('cl_date')) {
      val = clutil.dateFormat(val, 'yyyymmdd');
    } else if ($el.hasClass('cl_month')) {
      val = clutil.monthFormat(val, 'yyyymm');
    } else if ($el.hasClass('cl_time')) {
      val = clutil.timeFormat(val, 'hhmm');
    }

    return val;
  });

  Syphon.InputReaders.register('checkbox', function ($el) {
    return $el.prop('checked') ? 1 : 0;
  });

  Syphon.InputReaders.register('span', function ($el) {
    return $el.text();
  });

  ////////////////////////////////////////////////////////////////
  // InputWriters
  Syphon.InputWriters.register('span', function ($el, value) {
    return $el.text(value);
  });

  Syphon.InputWriters.register('text', function ($el, value) {
    if ($el.hasClass('cl_date')) {
      value = clutil.dateFormat(value, 'yyyy/mm/dd');
    } else if ($el.hasClass('cl_month')) {
      value = clutil.monthFormat(value, 'yyyy/mm');
    } else if ($el.hasClass('cl_time')) {
      value = clutil.timeFormat(value, 'hh:mm');
    } else {
      value = clutil.cStr(value);
    }
    $el.val(value);
  });
}(Backbone.Syphon, clutil));
/*
 * jQuery contextmenu plugin
 * Version: 1.1.1
 * Update (d/m/y): 09/03/13
 * Original author: @gokercebeci
 * Licensed under the MIT license
 * Demo: http://gokercebeci.com/dev/contextmenu
 */

(function($) {
    // Methods
    var methods = {
        init: function(element, options) {
            $this = this;
            // Bind options
            var contextmenu = $.extend(element, options);
            contextmenu.init(contextmenu);
            contextmenu.bind({
                'contextmenu': function(e) {
                    if (!e.ctrlKey || !contextmenu.ctrl) {
                        e.preventDefault();
                        $this.start(contextmenu);
                        $('#contextmenu').remove();
                        var c = $('<div id="contextmenu">')
                                .addClass(contextmenu.style)
                        c.css({
                            position: 'absolute',
                            display: 'none',
                            'z-index': '10000'
                        })
                                .appendTo($('body'));
                        for (var i in contextmenu.menu)
                            $('<a value=' + contextmenu.menu[i] + '>')
//                            $('<a>', {
//                                'href': contextmenu.menu[i]
//                            })
                                    .html(i).appendTo(c);
                        /************ 追加shimizu ***************/
                        // id取得 追加Shimizu
                        var tr = $(e.target).closest('tr')
                        var th = $(e.target).closest('th')
                        var id = 0;
                        if (tr != null && contextmenu.idname == 'id') {
                        	id = tr.get(0).id;
                        } else if (th != null) {
                        	// ソート時を想定 thよりKEYを取得する
                        	id = $(th).attr(contextmenu.idname);
                        }
                        c.find('a').click(function(e){
                        	var val = $(e.target).attr('value');
                        	$this.callback(contextmenu, val, id);
                        });
                        /************ 追加shimizu ***************/
                        // Set position
                        var ww = $(document).width();
                        var wh = $(document).height();
                        var w = c.outerWidth();
                        var h = c.outerHeight();
                        var x = e.pageX > (ww - w) ? ww : e.pageX;
                        var y = e.pageY > (wh - h) ? wh : e.pageY;
                        c.css({
                            display: 'block',
                            top: y,
                            left: x
                        });
                    }
                }
            });
            $(document)
                    .click(function() {
                $this.finish(contextmenu);
            })
                    .keydown(function(e) {
                if (e.keyCode == 27) {
                    $this.finish(contextmenu);
                }
            })
                    .scroll(function() {
                $this.finish(contextmenu);
            })
                    .resize(function() {
                $this.finish(contextmenu);
            });
        },
        start: function(contextmenu) {
            contextmenu.start(contextmenu);
            return;
        },
        finish: function(contextmenu) {
            contextmenu.finish(contextmenu);
            $('#contextmenu').remove();
            return;
        },
        error: function(contextmenu) {
            contextmenu.error(contextmenu);
            return;
        },
        callback: function(contextmenu, val, id) {
            contextmenu.callback(val, id);
            return;
        }
    };
    $.fn.contextmenu = function(options) {
        options = $.extend({
            init: function() {
            },
            start: function() {
            },
            finish: function() {
            },
            error: function() {
            },
            callback: function() {
            },
            ctrl: 1,
            style: '',
            menu: [],
            idname : 'id'
        }, options);
        this.each(function() {
            methods.init($(this), options);
        });
    };
})(jQuery);
/**
 * clstart.js (デモ)
 */
jQuery.ajaxSetup({
    timeout: 600000             // 600秒
});

$(function () {

    /*
     *  WebServer がローカルホストの場合、clcom.config をテスト環境仕様にする。
     */
    if(clcom.isLocalHost()){
        clcom.config.disableAbortAction = true;
        clcom.config.preventLoadHtmlCache = true;
        if(location.hostname === '127.0.0.1'){
            clcom.config.enableFixAbsPath = true;
        }
        clcom.config.enableDefaultContextMenu = true;
        console.info('Configure LOCALHOST mode: \n'
            + '  + disableAbortAction = true\n'
            + '  + disableCacheBuster = ' + clcom.config.disableCacheBuster + '\n'
            + '  + preventLoadHtmlCache = true\n'
            + '  + enableDefaultContextMenu = ' + clcom.config.enableDefaultContextMenu);
    }else{
        // サーバ結合環境用
        clcom.config.disableAbortAction = false;
        // AuthToken チェックを有効にする。
        clcom.config.disableAuthTokenCheck = false;
        // 絶対パス補正を有効にする。
        clcom.config.enableFixAbsPath = true;
        // テスト用モック応答を off にする。
        if(window.TestAjaxMock != null){
            TestAjaxMock.enabled = false;
        }
        console.info('Configure: \n'
            + '  + disableAbortAction = false\n'
            + '  + disableAuthTokenCheck = false\n'
            + '  + disableCacheBuster = ' + clcom.config.disableCacheBuster + '\n'
            + '  + preventLoadHtmlCache = ' + clcom.config.preventLoadHtmlCache + '\n'
            + '  + enableFixAbsPath = true\n'
            + '  + enableDefaultContextMenu = ' + clcom.config.enableDefaultContextMenu + '\n'
            + '  + TestAjaxMock.enabled = false');
    }

    /*
     * Web Root パス補正
     */
    if(clcom.config.enableFixAbsPath === true
            && !_.isEmpty(clcom.config.absPathPrefix) && clcom.config.absPathPrefix != '/'){
        // clcom.config.urlRoot
        clcom.config.urlRoot += clcom.config.absPathPrefix;

        // clcom.config.*
        _.each(['apiRoot', 'appRoot', 'templateRoot', 'loginPagePath', 'topPagePath', 'authTokenChkURI', 'loginURI', 'logoutURI', 'uploaderURI'], function(key){
            if(!_.isEmpty(clcom.config[key])){
                clcom.config[key] = clcom.config.absPathPrefix + clcom.config[key];
            }
        });
    }

    /*
     * clcom.config.*Root パスMatcher 生成
     */
    if(true){
        var cnf = clcom.config;
        cnf.apiRootMatcher = new RegExp('^' + cnf.apiRoot + '/');
        cnf.appRootMatcher = new RegExp('^' + cnf.appRoot + '/');
    }

    // -----------------------------------------------------------------------------------------
    // AO-Proj 資産ポーティング

    // テキスト入力フォーカス時、select する
    $(document).on('focus', 'input', function (e) {
        var $input = $(e.currentTarget);
        if(($input.is('input:text') || $input.is('input:password')) && !($input.is('[readonly]') || $input.is(':disabled'))){
            _.defer(function(){ $input.select(); });
        }
    });
    $(document).on('focus', 'textarea', function (e) {
        $(e.currentTarget).select();
    });

    //Enterキーによるフォーカスをする。
    if(clcom.config.enableEenterFocusMode){
        clutil.enterFocusMode();
    }

    // ブラウザデフォルトの右クリックコンテキストメニュー有効/無効設定
    clcom.setDefaultContextMenu(clcom.config.enableDefaultContextMenu);

    // エラーメッセージ等のツールチップ
    $('body').tooltip({
        selector: '[data-cl-errmsg],[data-cl-tooltip]',
        title: function () {
            var $this = $(this);
            if($this.is('[data-cl-errmsg]')){
                return $this.attr('data-cl-errmsg');
            }else{
                return $this.attr('data-cl-tooltip');
            }
        },
        position: {
            my: 'center bottom-10',
            at: 'center top'
        },
        trigger: 'hover',
        jsOnly: true,
        container: 'body'
    });

    // bootstrap-select: ツールチップ対策
    // selectpicker によって、[title],[data-original-title] 属性が勝手に埋め込まれるので刈り取る
    if(true){
        function selectpickerTooltipKiller(ev){
            var $this = $(ev.currentTarget);
            var selectPicker = $this.data('selectpicker');
            if(selectPicker){
                selectPicker.$button.removeAttr('title').removeAttr('data-original-title');
            }
        }
        $(document).on('loaded.bs.select', '.bootstrap-select > select', selectpickerTooltipKiller);
        $(document).on('rendered.bs.select', '.bootstrap-select > select', selectpickerTooltipKiller);
        $(document).on('refreshed.bs.select', '.bootstrap-select > select', selectpickerTooltipKiller);
        $(document).on('changed.bs.select', '.bootstrap-select > select', selectpickerTooltipKiller);
        $(document).on('hidden.bs.select', '.bootstrap-select > select', selectpickerTooltipKiller);
    }
});
