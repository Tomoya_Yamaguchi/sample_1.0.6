//iOSコンポーネント
//カメラ

(function( $ ) {
  var bodyImgSrc = {};
  var defaultCameraUI = {};
  var setImg_header = '';
  var setImg_body = '';
  var setImg_footer = '';
  var $_camera;

  var methods = {
    init : function(options){
      var camera__container =
        '<div class="camera--wrap">\
          <div class="screen">\
            <div class="camera--header">\
              <img src="" alt="">\
            </div>\
            <div class="camera--body">\
              <img src="" alt="" />\
            </div>\
            <div class="camera--footer">\
              <img src="" alt="">\
              <button class="js--shutter"></button>\
              <button class="js--retake--photo"></button>\
              <button class="js--save--photo"></button>\
            </div>\
          </div>\
        </div>'

      $('body').append(camera__container);
      $_camera = $('.camera--wrap');

      if (options.img_sp_portrait && options.img_sp_landscape && options.img_tb_portrait && options.img_tb_landscape) {
        bodyImgSrc = {
            mobile_portrait   : options.img_sp_portrait,
            mobile_landscape  : options.img_sp_landscape,
            tablet_portrait   : options.img_tb_portrait,
            tablet_landscape  : options.img_tb_landscape,
        }
      } else {
        $.error( '引数がセットされていません' );
      }

      defaultCameraUI = {
        mobile_portrait_header    : "images/iOS/iOS_camera_header_portrait.png",
        mobile_portrait_footer    : "images/iOS/iOS_camera_footer_portrait.png",
        mobile_landscape_header   : "images/iOS/iOS_camera_header_landscape.png",
        mobile_landscape_footer   : "images/iOS/iOS_camera_footer_landscape.png",
        tablet_portrait_footer    : "images/iOS/iOS_camera_footer_iPad_portrait.png",
        tablet_landscape_footer   : "images/iOS/iOS_camera_footer_iPad_landscape.png",
      }

      setImg_header = '';
      setImg_body = '';
      setImg_footer = '';

      $_camera.iOSCamera('reset');
    },
    reset : function(){
      if (typeof($_camera) != 'undefined') {
        $_camera.css({
          'top': $(window).outerHeight(),
        });

        //画像パスセット
        if (_ua.Mobile) {
          $_camera.removeClass('tablet').addClass('mobile');
          if (window.orientation == 'portrait') {
            $_camera.removeClass('landscape').addClass('portrait');
            setImg_header = defaultCameraUI.mobile_portrait_header;
            setImg_body = bodyImgSrc.mobile_portrait;
            setImg_footer = defaultCameraUI.mobile_portrait_footer;

          } else if (window.orientation == 'landscape') {
            $_camera.removeClass('portrait').addClass('landscape');

            setImg_header = defaultCameraUI.mobile_landscape_header;
            setImg_body   = bodyImgSrc.mobile_landscape;
            setImg_footer = defaultCameraUI.mobile_landscape_footer;
          }
        } else if (_ua.Tablet) {
          $_camera.removeClass('mobile').addClass('tablet');
          if (window.orientation == 'portrait') {
            this.removeClass('landscape').addClass('portrait');

            setImg_header = '';
            setImg_body   = bodyImgSrc.tablet_portrait;
            setImg_footer = defaultCameraUI.tablet_portrait_footer;
          } else if (window.orientation == 'landscape') {
            $_camera.removeClass('portrait').addClass('landscape');

            setImg_header = '';
            setImg_body   = bodyImgSrc.tablet_landscape;
            setImg_footer = defaultCameraUI.tablet_landscape_footer;
          }
        }

        //画像セット
        $_camera.find($('.camera--header img')).attr('src', setImg_header);
        $_camera.find($('.camera--body img')).attr('src', setImg_body);
        $_camera.find($('.camera--footer img')).attr('src', setImg_footer);

        // var $_camera = this;

        if (_ua.Mobile) {
          var imgLoaded = $('.camera--header img');
        } else if (_ua.Tablet) {
          var imgLoaded = $('.camera--body img');
        }

        //画像ロード完了時の処理
        $_camera.find(imgLoaded).load(function() {
          //スタイル調整
          if (_ua.Mobile) {
            if (window.orientation == 'portrait') {
              var camHeaderH = $('.camera--header').outerHeight();
              var camFooterH = $('.camera--footer').outerHeight();

              $_camera.find($('.camera--body')).css({
                'height': $(window).outerHeight() - (camHeaderH + camFooterH),
                'top': camHeaderH,
                'left': 0,
              });
            } else if (window.orientation == 'landscape') {
              var camHeaderW = $('.camera--header').outerWidth();
              var camFooterW = $('.camera--footer').outerWidth();

              $_camera.find($('.camera--body')).css({
                'width': $(window).outerWidth() - (camHeaderW + camFooterW),
                'top': 0,
                'left': camHeaderW,
              });
            }
          } else if (_ua.Tablet) {
            if (window.orientation == 'portrait') {
              $_camera.find($('.camera--body')).css({
                'width': '100%',
                'top': 0,
                'left': 'auto',
                'right': 0,
                'height': 'inherit',
              });
            } else if (window.orientation == 'landscape') {
              $_camera.find($('.camera--body')).css({
                'width': '100%',
                'top': 0,
                'left': 'auto',
                'right': 0,
                'height': 'inherit',
              });
            }
          }
          $('.camera--body').imgLiquid({fill: true});

        }).each(function() {
          if(this.complete) $(this).load();
        });
      }
    },
    confirm : function(){
      if (typeof($_camera) != 'undefined') {
        var defaultCameraUI = {
          confirm_mobile_portrait_header  : "images/iOS/iOS_camera_header_portrait_confirm.png",
          confirm_mobile_portrait_footer  : "images/iOS/iOS_camera_footer_portrait_confirm.png",
          confirm_mobile_landscape_header : "images/iOS/iOS_camera_header_landscape_confirm.png",
          confirm_mobile_landscape_footer : "images/iOS/iOS_camera_footer_landscape_confirm.png",
        }

        $_camera.addClass('confirm');

        //画像パスセット
        if (_ua.Mobile) {
          $_camera.removeClass('tablet').addClass('mobile');
          if (window.orientation == 'portrait') {
            $_camera.removeClass('landscape').addClass('portrait');
            setImg_header = defaultCameraUI.confirm_mobile_portrait_header;
            setImg_footer = defaultCameraUI.confirm_mobile_portrait_footer;

          } else if (window.orientation == 'landscape') {
            $_camera.removeClass('portrait').addClass('landscape');
            setImg_header = defaultCameraUI.confirm_mobile_landscape_header;
            setImg_footer = defaultCameraUI.confirm_mobile_landscape_footer;
          }
        } else if (_ua.Tablet) {
          var confirmHTML = '<div class="confirm--tablet"><button class="retake">再撮影</button><button class="use">写真を使用</button></div>';

          $_camera.removeClass('mobile').addClass('tablet');
          if (window.orientation == 'portrait') {
            $_camera.removeClass('landscape').addClass('portrait');
            setImg_header = '';
            setImg_footer = '';
          } else if (window.orientation == 'landscape') {
            $_camera.removeClass('portrait').addClass('landscape');
            setImg_header = '';
            setImg_footer = '';
          }

          $('.camera--footer img').hide();
          $_camera.find($('.screen')).append(confirmHTML);
        }

        //画像セット
        $_camera.find($('.camera--header img')).attr('src', setImg_header);
        $_camera.find($('.camera--footer img')).attr('src', setImg_footer);
        $('.js--shutter').hide();
        $('.camera--body').imgLiquid();
      }
    },
    takePhoto: function(){
      $_camera.find($('.screen')).css('opacity', '.1').animate(
        {
          'opacity': '1'
        },
        {
          duration: 1000,
          complete: function(){
            $('.camera--wrap').iOSCamera('confirm');
          },
        });
    },
    hide: function(){
      $_camera.removeClass('show');
      $_camera.iOSCamera('retake');
    },
    retake: function(){
      if ($('.confirm--tablet').length) {
        $('.confirm--tablet').remove();
        $('.camera--footer img').show();
      }

      $('.js--shutter').show();
      $_camera.removeClass('confirm');
      $_camera.iOSCamera('reset');
    }
  }

  $.fn.iOSCamera = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' はiOSCameraに存在しません' );
    }
  }

  //起動
  $('.js--camera').on('click', function(){
    if ($('.camera--wrap').hasClass('mobile') || $('.camera--wrap').hasClass('tablet')){
      $('.camera--wrap').addClass('show');
    }
  });

  //撮影
  $('body').on('click', '.js--shutter', function(){
    $('.camera--wrap').iOSCamera('takePhoto');
  });

  //写真を使用
  $('body').on('click', '.js--save--photo', function(){
    $('.camera--wrap').iOSCamera('hide');
  });

  //再撮影
  $('body').on('click', '.js--retake--photo', function(){
    $('.camera--wrap').iOSCamera('retake');
  });

  $(window).on('resize', function(){
    $('.camera--wrap').iOSCamera('reset');
    if ($('.camera--wrap').hasClass('confirm')) {
      $('.camera--wrap').iOSCamera('confirm');
    }
  });
})( jQuery );
