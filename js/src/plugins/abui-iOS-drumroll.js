(function( $ ) {
  var methods = {
    init : function(options){

      this.each(function(i){
        var drumroll_height;
        //drumrollをbottomへ配置
        $(this).parent().css('position', 'relative');

        //ドラムロール - .outerにmargin: 0 autoを効かせるための処理
        var inner_width = 0;
        var inner_counter = $(this).find($('.inner')).length;

        if (inner_counter == 1) {
          $(this).find($('.js--outer--apply-width')).addClass('one-col');
        }else if(inner_counter == 2) {
          $(this).find($('.js--outer--apply-width')).addClass('two-col');
        }else if(inner_counter == 3) {
          $(this).find($('.js--outer--apply-width')).addClass('three-col');
        }else {
          $.error( '表示可能な列数は1〜3列です。' + $(this).attr('name') + 'のマークアップを確認してください。');
        }

        $(this).find($('.inner')).each(function(){
          inner_width = inner_width + Math.ceil($(this).get(0).getBoundingClientRect().width);
        });

        $(this).find($('.js--outer--apply-width')).css('width', inner_width);
      });

    }
  }

  var concatinated = '';
  var drumroll_caller;
  //drumrollを起動
  $('.js--drumroll').on('click', function(){
    var target = $(this).data('target');

    //一旦全て起動
    $('.drumroll-wrap').addClass('show');
    //targetのドラムロールを表示する
    $('.drumroll-wrap[name="'+target+'"]').css('z-index', '2010');
    $('.drumroll-wrap:not([name="'+target+'"])').css('z-index', '2000');
    drumroll_caller = $(this);
  });
  $('.drumroll-wrap').on('click', function(){
    $('.drumroll-wrap').removeClass('show');
    $(this).find($('.rotate0')).each(function(){
      concatinated += $(this).text();
    });
    $(drumroll_caller).val(concatinated);
    concatinated = '';
  });

  $.fn.iOSDrumroll = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' はiOSDrumrollに存在しません' );
    }
  }

})( jQuery );
