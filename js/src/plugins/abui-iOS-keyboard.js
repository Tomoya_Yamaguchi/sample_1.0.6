//iOS-keyBoard
(function( $ ) {

  var keyBoard;
  var keyBoards = [];

  var keyBoard_src_sp_portrait = [
    'images/iOS/iOS_keyboard_sp_jp_jp_portrait.png',
    'images/iOS/iOS_keyboard_sp_jp_en_portrait.png',
    'images/iOS/iOS_keyboard_sp_jp_num_portrait.png',
    'images/iOS/iOS_keyboard_sp_en_en_portrait.png',
    'images/iOS/iOS_keyboard_sp_en_num_portrait.png',
  ]

  var keyBoard_src_sp_landscape = [
    'images/iOS/iOS_keyboard_sp_jp_jp_landscape.png',
    'images/iOS/iOS_keyboard_sp_jp_en_landscape.png',
    'images/iOS/iOS_keyboard_sp_jp_num_landscape.png',
    'images/iOS/iOS_keyboard_sp_en_en_landscape.png',
    'images/iOS/iOS_keyboard_sp_en_num_landscape.png',
  ]

  var keyBoard_src_tb_portrait = [
    'images/iOS/iOS_keyboard_tb_jp_jp.png',
    'images/iOS/iOS_keyboard_tb_jp_en.png',
    'images/iOS/iOS_keyboard_tb_jp_num.png',
    'images/iOS/iOS_keyboard_tb_en_en.png',
    'images/iOS/iOS_keyboard_tb_en_num.png',
  ]

  var keyBoard_src_tb_landscape = [
    'images/iOS/iOS_keyboard_tb_jp_jp.png',
    'images/iOS/iOS_keyboard_tb_jp_en.png',
    'images/iOS/iOS_keyboard_tb_jp_num.png',
    'images/iOS/iOS_keyboard_tb_en_en.png',
    'images/iOS/iOS_keyboard_tb_en_num.png',
  ]

  var container__mobile =
    '<div class="keyBoard-wrap mobile">\
      <div class="keyBoard--jp">\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
      </div>\
      <div class="keyBoard--en">\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
      </div>\
    </div>'

  var container__tablet =
    '<div class="keyBoard-wrap tablet">\
      <div class="keyBoard--jp">\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch--num"></button>\
          <button class="switch--en"></button>\
          <button class="toggle"></button>\
        </div>\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch--num"></button>\
          <button class="switch--jp"></button>\
          <button class="toggle"></button>\
        </div>\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch--en"></button>\
          <button class="switch--jp"></button>\
          <button class="toggle"></button>\
        </div>\
      </div>\
      <div class="keyBoard--en">\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
        <div class="keyBoard">\
          <img class="" src="">\
          <button class="switch"></button>\
          <button class="toggle"></button>\
        </div>\
      </div>\
    </div>'

  var methods = {
    init : function(options){
      if (_ua.Mobile) {
        $('.keyBoard-wrap.tablet').remove();
        if (!$('.keyBoard-wrap.mobile').length) {
          $('body').append(container__mobile);
        }
      } else if (_ua.Tablet){
        $('.keyBoard-wrap.mobile').remove();
        if (!$('.keyBoard-wrap.tablet').length) {
          $('body').append(container__tablet);
        }
      }

      changeOrientation(window.orientation);

      $('.keyBoard').addClass('displaynone');
    },
    hide : function(options) {
      var keyBoardInView_fl = 0;

      $('.keyBoard').each(function(){
        if (!$(this).hasClass('displaynone')) {
          keyBoardInView_fl = 1;
        }
      });

      if (keyBoardInView_fl) {
        $('.keyBoard-wrap').css('height', 0);
      }
    }
  }

  $.fn.iOSKeyboard = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' はiOSKeyboardに存在しません' );
    }
  }


  //イベントハンドラ
    //文字→英字→数字　切り替え
    $('body').on('click', '.switch', function(e) {
      if (_ua.Mobile) {
        var key = 0;
        for (var i = 0; i < keyBoards.length; i++) {
          if ($(keyBoards[i]).is($(this).parents('.keyBoard'))) {
            key = i;
            break;
          }
        }

        $(keyBoards[key]).addClass('displaynone');

        if (key + 1 >= keyBoards.length) {
          $(keyBoards[0]).removeClass('displaynone');
        } else {
          $(keyBoards[key + 1]).removeClass('displaynone');
        }

        event.stopPropagation();
      }
    });

    //日英　切り替え
    $('body').on('click', '.toggle', function(e) {
      if (_ua.Mobile) {
        //初期化
        $('.keyBoard').addClass('displaynone');
        var targetKeyBoard;

        if ($(this).parents('.keyBoard--jp').length) {
          $($('.keyBoard--en .keyBoard')[0]).removeClass('displaynone');
          targetKeyBoard = 'en';
        } else {
          $($('.keyBoard--jp .keyBoard')[0]).removeClass('displaynone');
          targetKeyBoard = 'jp';
        }

        keyBoard = '';
        keyBoard = $('.keyBoard--' + targetKeyBoard + keyBoard + ' .keyBoard');
        keyBoards = keyBoard;

        event.stopPropagation();
      }
    });


    $('body').on('click', '.switch--num', function(){
      event.stopPropagation();
      switchTarget($(this), 'jp_num');
    });

    $('body').on('click', '.switch--en', function(){
      event.stopPropagation();
      switchTarget($(this), 'jp_en');
    });

    $('body').on('click', '.switch--jp', function(){
      event.stopPropagation();
      switchTarget($(this), 'jp_jp');
    });


    function switchTarget(el, key) {
      $('.keyBoard').addClass('displaynone');

      $(el).parent().siblings('.keyBoard').each(function(){
        if ($(this).find($('img')).attr('src').match(key)) {
          $(this).removeClass('displaynone');
        }
      });
    }

    $('body').on('click', '.switch', function(){
      if (_ua.Tablet){
        event.stopPropagation();

        $('.keyBoard').addClass('displaynone');
        var currentTarget = $(this).parents('.keyBoard');

        $(this).parents('.keyBoard--en').find($('.keyBoard')).each(function(){
          if (!currentTarget.is($(this))) {
            $(this).removeClass('displaynone');
          }
        });
      }
    });

    $('body').on('click', '.toggle', function(e) {
      if (_ua.Tablet){
        event.stopPropagation();

        var currentKeyBoard = $(this).parents('.keyBoard');
        var targetKeyBoard;
        currentKeyBoard.addClass('displaynone');

        if ($(this).parents('.keyBoard--jp').length) {
          $($('.keyBoard--en .keyBoard')[0]).removeClass('displaynone');
          targetKeyBoard = 'en';
        } else {
          $($('.keyBoard--jp .keyBoard')[0]).removeClass('displaynone');
          targetKeyBoard = 'jp';
        }
      }
    });

  //入力せず閉じる
  $(document).click(function(e){
    if (!$(this.activeElement).hasClass('js--keyBoard')) {
      $('.keyBoard-wrap').iOSKeyboard('hide');
    }
  });

  //入力して閉じる
  $('body').on('click', '.keyBoard', function(){
    event.stopPropagation();
    if ($(event.target).is($('img')) && $(event.target).parent().is($('.keyBoard'))) {
      $('.js--keyBoard').val('').val($('.js--keyBoard').data('input'));
      $('.keyBoard-wrap').iOSKeyboard('hide');
    }
  });

  $('body').on('click', '.js--keyBoard', function(){
    if ($('.keyBoard-wrap').height() == 0) {
      //表示
      $('.keyBoard').addClass('displaynone');
      var initView = $(this).data('target');
      var error_fl = 1;

      $('.keyBoard img').each(function(){
        if ($(this).attr('src').match(initView)) {
          //data-targetがimgのsrcにヒットしたら表示
          $(this).parents('.keyBoard').removeClass('displaynone');
          var keyBoard_height = $(this).height();
          $('.keyBoard-wrap').css('height', keyBoard_height);
          error_fl = 0;
        }
      });

      if (error_fl) {
        console.log('data-target指定とimgのファイル名を確認してください');
        return false;
      }

      keyBoard = '';
      keyBoard = $('.keyBoard--' + initView.substr(0,2) + keyBoard + ' .keyBoard');
      keyBoards = keyBoard;

      event.stopPropagation();
    }
  });

  function changeOrientation(orientation) {
    var srcToApply;

    switch (orientation) {
      case 'portrait':
        if (_ua.Mobile) {
          srcToApply = keyBoard_src_sp_portrait;
        } else if (_ua.Tablet) {
          srcToApply = keyBoard_src_tb_portrait;
        }
        break;

      case 'landscape':
        if (_ua.Mobile) {
          srcToApply = keyBoard_src_sp_landscape;
        } else if (_ua.Tablet) {
          srcToApply = keyBoard_src_tb_landscape;
        }
        break;
    }

    var cnt = 0;
    $('.keyBoard').each(function(i){
      $(this).find($('img')).attr('src', srcToApply[i]);
      cnt = i;
    });

    //画像ロード完了時の処理
    $($('.keyBoard').find($('img'))[$('.keyBoard').find($('img')).length - 1]).load(function() {
      if ($('.keyBoard-wrap').height()) {
        $('.keyBoard-wrap').css('height', $('.keyBoard:not(.displaynone)').height());
      }
    }).each(function() {
      if(this.complete) $(this).load();
    });

  }

  $(window).on('resize', function(){
    var _ua_before = _ua;
    var _ua_changed_fl_keyBoard = 0;

    abui.util.getUA();
    if (_ua_before != _ua) {
      _ua_changed_fl_keyBoard = 1;
    }

    setTimeout(function(){
      if (_ua_changed_fl_keyBoard) {
        $('.keyBoard-wrap').iOSKeyboard();
        _ua_changed_fl_keyBoard = 0;
      }
    },200);

    changeOrientation(window.orientation);
  });
})( jQuery );
