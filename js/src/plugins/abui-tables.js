(function( $ ) {
  var table = {};
  var table_settings;

  var TableCollapse = {
    colgroup: {
      init: function(thead_html, colgroup_html, rowgroup, collapsed_fl){
        var style = collapsed_fl ? "display: none" : ""
        // var style = collapsed_fl ? "" : ""

        var template =
          '<td class="colgroup" style="padding: 0px;">\
            <div data-collapse-group="' + rowgroup + '" style="' + style + '">\
              <table class="table table-collapse-childrows">\
                <tbody>' + colgroup_html + '</tbody> \
              </table>\
            </div>\
          </td>';

        return template;
      }
    },
    cell: {
      init: function(value) {
        var template = '<tr class="childrow_cell">' + value + '</tr>'

        return template;
      }
    }
  };

  var methods = {
    init : function(option) {
      var tableid = this.attr('id');
      var $table = $('#' + tableid);

      $(window).on('load', () => {
        //fixedクラス付与
        $table.addClass('table-fixed');

        //行開閉が可能なテーブルの場合は dataTable呼び出し前にテーブル組み替えを行う
        if ($('#' + tableid).hasClass('table-row-collapse')) {
          var getChildRows = function(rowgroup){
            return $('[data-rowgroup="' + rowgroup + '"]').slice(1);
          }

          var groupCellsByColumn = function(childrows){
            var colgroup = []; //convert table to array

            $.each(childrows, function(i, row){
              var cells = $(row).find('td');
              var colindex = i;

              if (colgroup.length < cells.length) {
                for (var k = 0; k < cells.length; k++) {
                  colgroup.push([]);
                }
              }

              $.each(cells, function(i, cell){
                var rowindex = i;
                if ($(cell).html() == "") {
                  $(cell).html("&ensp;")
                }
                colgroup[rowindex][colindex] = $(cell).prop('outerHTML');
              });

            });
            return colgroup;
          }

          var childrows = {};
          var rowgroup = 0;
          $('[data-toggle="table-row"]').each(function(){
            rowgroup = $(this).data('target');
            childrows = getChildRows(rowgroup);

            var colgroup = groupCellsByColumn(childrows);
            var collapsed_fl = $('[data-toggle="table-row"][data-target="' + rowgroup + '"]').data('collapsed');

            var reformulated_html = ""
            $.each(colgroup, function(index_col){
              var colgroup_html = "";
              var thead_html = "";
              for (var index_row = 0; index_row < this.length; index_row++) {
                colgroup_html += TableCollapse.cell.init(this[index_row]);
                thead_html += '<th></th>'
              }

              reformulated_html += TableCollapse.colgroup.init(thead_html, colgroup_html, rowgroup, collapsed_fl);
            });

            $($(childrows)[childrows.length - 1]).after('<tr class="childrows">' + reformulated_html + '</tr>');
            $(childrows).remove();
            // $('.table-row-collapse tbody').append('<tr>' + reformulated_html + '</tr>');
          });
        }

        //initalized fixed table
        if ($('#' + tableid).hasClass('table-row-collapse')) {
          option.tableHeight = ""
        }

        // $(option.width).each(function(){
        //   this.width -= 16
        // });

        table = $('#' + tableid).DataTable( {
          bInfo: false,
          ordering: false,
          searching: false,
          scrollY:        option.tableHeight,
          scrollX:        true,
          scrollCollapse: true,
          paging:         false,
          search: {
            smart: false
          },
          // autoWidth: true,
          fixedColumns: option.fixedColumns,
          columnDefs: option.width,
          initComplete: function(){
            $('.table td, .table th').each(function(){
              //スペーシング調整クラスcell-checkboxの付与
              if ($(this).find('input[type="checkbox"]').length || $(this).find('input[type="radio"]').length) {
                $(this).addClass('cell-checkbox');
              }
            });
          },
        });

        table_settings = option;

        //fixedColHeaderの場合に、abuiに合わせたborderの関係（？）で横位置がズレるので、微調整
        $table.find('.dataTables_wrapper').css('width', window.outerWidth - 64);
        if ($('.dataTables_scroll').width() < $table.width()) {
          $table.find('.DTFC_ScrollWrapper .dataTables_scrollHead .dataTable').css('margin-left', '1px');
        }

        //handler グループのトグル処理
        var $wrapper = $('#' + tableid + '_wrapper');
        var animating_fl = false;


        if ($('#' + tableid).hasClass('table-row-collapse')) {
          if ($wrapper.width() >= $table.width()) {
            $('#' + $table.attr('id') + '_wrapper').find('table, .dataTables_scrollHeadInner').attr('width', $wrapper.width() + 'px');
          }
        }

        // table-hoverクラスがある場合のhoverスタイル付与処理
        if ($wrapper.find('.table-hover').length) {
          if (!$wrapper.find('.table-row-collapse').length) {
            $(document).find('#' + tableid + '_wrapper tbody tr').hover(function(){
              var index = 0
              var tgt = $(this);
              $(this).closest('tbody').find('tr').each(function(i){
                if ($(this).is(tgt)) {
                  index = i;
                }
              });

              $wrapper.find('table tbody').each(function(){
                $($(this).find('tr')[index]).addClass('hover');
              });
            },
            function(){
              $wrapper.find('table tbody').find('tr').removeClass('hover');
            });
          } else {
            $(document).on('mouseover', '#' + tableid + '_wrapper tr', function(e){
              var index = 0
              var tgt = $(this);
              $(this).closest('tbody').children('tr').each(function(i){
                if ($(this).is(tgt)) {
                  index = i;
                }
              });

              if ( $(this).hasClass('childrow_cell') ) {
                e.stopPropagation();

                var tgt_group = $(this).parents('[data-collapse-group]').data('collapse-group');
                $('table tbody').find('tr').removeClass('hover');

                $wrapper.find('.DTFC_LeftWrapper table tbody').find('td.colgroup').each(function(){
                  $($(this).find('[data-collapse-group=' + tgt_group + '] tr.childrow_cell')[index]).addClass('hover');
                });
                $wrapper.find('.dataTables_scroll table tbody').find('td.colgroup').each(function(){
                  $($(this).find('[data-collapse-group=' + tgt_group + '] tr.childrow_cell')[index]).addClass('hover');
                });
                $wrapper.find('.DTFC_RightWrapper table tbody').find('td.colgroup').each(function(){
                  $($(this).find('[data-collapse-group=' + tgt_group + '] tr.childrow_cell')[index]).addClass('hover');
                });
              } else {
                $('table tbody').find('tr').removeClass('hover');

                $($wrapper.find('.DTFC_LeftWrapper table tbody').children('tr')[index]).addClass('hover');
                $($wrapper.find('.dataTables_scroll table tbody').children('tr')[index]).addClass('hover');
                $($wrapper.find('.DTFC_RightWrapper table tbody').children('tr')[index]).addClass('hover');
              }

            }).on('mouseleave', '#' + tableid + '_wrapper tr', function(){
                $('table tbody').find('tr').removeClass('hover');
            });
          }
        }

        // table-checkablerowクラスがある場合の行選択処理
        if ($wrapper.find('.table-checkablerow').length) {
          $('.table-checkablerow').addClass('datatable-checkablerow').removeClass('table-checkablerow')
          $(document).find('#' + tableid + '_wrapper .datatable-checkablerow tbody').on('click', 'tr', function(e){
            var index = 0
            var tgt = $(this);
            $(this).closest('tbody').find('tr').each(function(i){
              if ($(this).is(tgt)) {
                index = i;
              }
            });

            // if (!$(event.target).parents('.checkbox').length) {
            if ( !$($('.DTFC_LeftBodyLiner').find('tbody tr')[index]).find('label').hasClass('checked') ) {
              $($('.DTFC_LeftBodyLiner').find('tbody tr')[index]).find('label').addClass('checked');
              $($('.DTFC_LeftBodyLiner').find('tbody tr')[index]).find('input').prop('checked', true);

              $($('.dataTables_scroll').find('tbody tr')[index]).find('label').addClass('checked');
              $($('.dataTables_scroll').find('tbody tr')[index]).find('input').prop('checked', true);
            } else {
              $($('.DTFC_LeftBodyLiner').find('tbody tr')[index]).find('label').removeClass('checked');
              $($('.DTFC_LeftBodyLiner').find('tbody tr')[index]).find('input').prop('checked', false);

              $($('.dataTables_scroll').find('tbody tr')[index]).find('label').removeClass('checked');
              $($('.dataTables_scroll').find('tbody tr')[index]).find('input').prop('checked', false);
            }

            // }

            if ($('.dataTables_scroll').find('tbody tr').find('label.checked').length == $('.dataTables_scroll').find('tbody tr input[type="checkbox"]').length) {
              $('.DTFC_LeftHeadWrapper').find('thead tr').find('label.checkbox').addClass('checked');
              $('.DTFC_LeftHeadWrapper').find('thead tr').find('input[type="checkbox"]').prop('checked', true);
            } else {
              $('.DTFC_LeftHeadWrapper').find('thead tr').find('label.checkbox').removeClass('checked');
              $('.DTFC_LeftHeadWrapper').find('thead tr').find('input[type="checkbox"]').prop('checked', false);
            }
          });

          // 全選択トグル
          $(document).find('#' + tableid + '_wrapper .datatable-checkablerow thead').on('click', 'tr', function(e){
            var index = 0
            var tgt = $(this);
            $(this).closest('tbody').find('tr').each(function(i){
              if ($(this).is(tgt)) {
                index = i;
              }
            });

            if (!$(event.target).parents('.checkbox').length) {
              if ($($('.dataTables_scroll').find('thead tr')[index]).find('label').hasClass('checked')) {
                $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('label').removeClass('checked');
                $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('input').prop('checked', false);

                $($('.dataTables_scroll').find('thead tr')[index]).find('label').removeClass('checked');
                $($('.dataTables_scroll').find('thead tr')[index]).find('input').prop('checked', false);

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('label.checkbox').removeClass('checked');
                $('.dataTables_scroll').find('tbody tr').find('label.checkbox').removeClass('checked');

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('input[type="checkbox"]').prop('checked', false);
                $('.dataTables_scroll').find('tbody tr').find('input[type="checkbox"]').prop('checked',false);
              } else {
                $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('label').addClass('checked');
                $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('input').prop('checked', true);

                $($('.dataTables_scroll').find('thead tr')[index]).find('label').addClass('checked');
                $($('.dataTables_scroll').find('thead tr')[index]).find('input').prop('checked', true);

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('label.checkbox').addClass('checked');
                $('.dataTables_scroll').find('tbody tr').find('label.checkbox').addClass('checked');

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('input[type="checkbox"]').prop('checked', true);
                $('.dataTables_scroll').find('tbody tr').find('input[type="checkbox"]').prop('checked',true);
              }

            } else {

              if ($(event.target).parents('.checkbox').hasClass('checked')) {
                if ($(event.target).parents('.DTFC_LeftHeadWrapper').length) {
                  $($('.dataTables_scroll').find('thead tr')[index]).find('label').removeClass('checked');
                  $($('.dataTables_scroll').find('thead tr')[index]).find('input').prop('checked', false);
                } else {
                  $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('label').removeClass('checked');
                  $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('input').prop('checked', false);
                }

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('label.checkbox').removeClass('checked');
                $('.dataTables_scroll').find('tbody tr').find('label.checkbox').removeClass('checked');

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('input[type="checkbox"]').prop('checked', false);
                $('.dataTables_scroll').find('tbody tr').find('input[type="checkbox"]').prop('checked',false);
              } else {
                if ($(event.target).parents('.DTFC_LeftHeadWrapper').length) {
                  $($('.dataTables_scroll').find('thead tr')[index]).find('label').addClass('checked');
                  $($('.dataTables_scroll').find('thead tr')[index]).find('input').prop('checked', true);
                } else {
                  $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('label').addClass('checked');
                  $($('.DTFC_LeftHeadWrapper').find('thead tr')[index]).find('input').prop('checked', true);
                }

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('label.checkbox').addClass('checked');
                $('.dataTables_scroll').find('tbody tr').find('label.checkbox').addClass('checked');

                $('.DTFC_LeftBodyLiner').find('tbody tr').find('input[type="checkbox"]').prop('checked', true);
                $('.dataTables_scroll').find('tbody tr').find('input[type="checkbox"]').prop('checked',true);
              }

            }

          });
        }

        // table.columns.adjust();
        //セル幅調整処理（datatableの計算だと合わないので。。。）
        if (option.fixedColumns.leftColumns > 0 || option.fixedColumns.rightColumns > 0) {
          var total_width_head_left = 0;
          var total_width_head_right = 0;
          var total_width_head_scroller = 0;
          var width_cell = 0;

          var $leftWrapper = $wrapper.find('.DTFC_LeftWrapper');
          var $leftHead = $wrapper.find('.DTFC_LeftHeadWrapper');
          var $leftBody = $wrapper.find('.DTFC_LeftBodyWrapper');

          var $rightWrapper = $wrapper.find('.DTFC_RightWrapper');
          var $rightHead = $wrapper.find('.DTFC_RightHeadWrapper');
          var $rightBody = $wrapper.find('.DTFC_RightBodyWrapper');

          var $scrollWrapper = $wrapper.find('.dataTables_scroll');
          var $scrollHead = $wrapper.find('.dataTables_scrollHead');
          var $scrollBody = $wrapper.find('.dataTables_scrollBody');

          $leftHead.find('.dataTable').css('width', 'auto')
          $rightHead.find('.dataTable').css('width', 'auto')
          $scrollHead.find('.dataTable').css('width', 'auto')

          // $wrapper.find('.DTFC_LeftWrapper').css('width', 'auto')
          for (var i = 0; i < option.fixedColumns.leftColumns; i ++) {
            width_cell = 0;
            var settings_lefthead_cell = _.find(option.width, function (o) {
              return o.targets === i;
            });
            if (typeof settings_lefthead_cell == 'undefined') {
              width_cell = $($leftHead.find('th')[i]).width();
            } else {
              width_cell = settings_lefthead_cell.width;
            }

            $($leftHead.find('th')[i]).css('width', width_cell + 'px');
            $($leftBody.find('th')[i]).css('width', width_cell + 'px');
            // total_width_head_left += width_cell
          }

          total_width_head_left = $wrapper.find('.DTFC_LeftHeadWrapper table').width();
          $leftHead.css('width', total_width_head_left + 'px')
          $leftBody.find('.DTFC_LeftBodyLiner').css('width', total_width_head_left + 20 + 'px')
          $leftWrapper.css('width', total_width_head_left + 'px')

          for (var i = 0; i < option.fixedColumns.rightColumns; i ++) {
            width_cell = 0;
            // var index_rightHead = $wrapper.find('.dataTables_scrollHeadInner').find('th').length - i;
            var settings_righthead_cell = _.find(option.width, function (o) {
              return o.targets === $wrapper.find('.dataTables_scrollHeadInner').find('th').length - option.fixedColumns.rightColumns + i;
            });
            if (typeof settings_righthead_cell == 'undefined') {
              width_cell = $($rightHead.find('th')[i]).width();
            } else {
              width_cell = settings_righthead_cell.width;
            }

            $($rightHead.find('th')[i]).css('width', width_cell + 'px');
            $($rightBody.find('th')[i]).css('width', width_cell + 'px');
            // total_width_head_right += width_cell
          }

          total_width_head_right = $wrapper.find('.DTFC_RightHeadWrapper table').width();
          $rightHead.css('width', total_width_head_right)
          $rightBody.find('.DTFC_RightBodyLiner').css('width', total_width_head_right + 20 + 'px')
          $rightWrapper.css('width', total_width_head_right + 'px')

          for (var i = 0; i < $wrapper.find('.dataTables_scrollHeadInner').find('th').length; i++) {
            width_cell = 0;

            var settings_scrollhead_cell = _.find(option.width, function (o) {
              return o.targets === i;
            });
            if (typeof settings_scrollhead_cell == 'undefined') {
              width_cell = $($scrollHead.find('th')[i]).width();
            } else {
              width_cell = settings_scrollhead_cell.width;
            }

            $($scrollHead.find('th')[i]).css('width', width_cell + 'px');
            $($scrollBody.find('th')[i]).css('width', width_cell + 'px');
          }

          total_width_head_scroller = $wrapper.find('.dataTables_scrollHeadInner table').width();
          // $scrollHead.css('width', total_width_head_scroller)
          $scrollBody.find('table:not(.table-collapse-childrows)').css('width', total_width_head_scroller + 'px')
          // $scrollWrapper.css('width', total_width_head_scroller)
        }

        //data-collapse = trueだった場合の初期処理
        var height_colgroup = '0px';

        if ($('#' + tableid).hasClass('table-row-collapse')) {
          //toggleの高さを取得
            var height_colgroup_work = 0;
            var height_childrowcell_work = 0;

            $('[data-collapse-group]').css('display', 'block');
            if ($('.dataTables_scrollBody [data-collapse-group]').parents('.colgroup').outerHeight(true) > height_colgroup_work) {
              height_colgroup = $('.dataTables_scrollBody [data-collapse-group]').parents('.colgroup').outerHeight(true) + 'px';
              height_colgroup_work = $('.dataTables_scrollBody [data-collapse-group]').parents('.colgroup').outerHeight(true);
            }

            var $cell_controls = $('[data-collapse-group]').find('.cell-control');
            var height_cell = 0;
            $cell_controls.each(function(){
              height_cell = height_cell < $(this).outerHeight(true) ? $(this).outerHeight(true) : height_cell;
            });
            if ($cell_controls.length) {
              $('[data-collapse-group]').find('td:not(.cell-control)').parents('.childrow_cell').css('height', height_cell);
            }
            $('[data-rowgroup]').css('height', $cell_controls.outerHeight(true));

            $('[data-collapse-group]').css('display', 'none');


          $('[data-collapsed="true"]').each(function(){
            var toggletarget = $(this).attr('data-target');

            //左固定、中央スクロール領域のchildrowsを親trのdata-childhtmlに格納
            $(this).parents('tr[role="row"]').attr('data-childhtml', $(this).parents('tbody').find('[data-collapse-group="' + toggletarget + '"]').parents('tr[role="row"]').prop('outerHTML'));
            $(this).parents('tbody').find('[data-collapse-group="' + toggletarget + '"]').parents('tr[role="row"]').remove();

            //右固定領域childrowsを親trのdata-childhtmlに格納
            $(this).parents('.DTFC_ScrollWrapper').find('.DTFC_RightBodyLiner > table > tbody > tr[data-rowgroup=' + toggletarget + ']').attr('data-childhtml', $(this).parents('.DTFC_ScrollWrapper').find('.DTFC_RightBodyLiner > table > tbody [data-collapse-group=' + toggletarget + ']').parents('tr[role="row"]').prop('outerHTML'));
            $(this).parents('.DTFC_ScrollWrapper').find('.DTFC_RightBodyLiner > table > tbody [data-collapse-group=' + toggletarget + ']').parents('tr[role="row"]').remove();

            $(this).find('.icon').removeClass('icon-abui-arrow-down').removeClass('icon-abui-arrow-right').addClass('icon-abui-arrow-right');
          });

        }

        $wrapper.on('click', '[data-toggle="table-row"]', function() {
          if (animating_fl) {
            return false;
          }

          animating_fl = true;

          var toggletarget = $(this).attr('data-target');
          var leftHead_w = 0;
          if ($('[data-collapse-group="' + toggletarget + '"]').css('display') == 'block') {
            //datatable高さリセット
            $wrapper.find($('.DTFC_ScrollWrapper, .DTFC_RightWrapper, .DTFC_RightBodyWrapper, .DTFC_RighttBodyLiner, .DTFC_RightHeadWrapper, .DTFC_LeftWrapper, .DTFC_LeftBodyLiner, .DTFC_LeftBodyWrapper, .DTFC_LeftHeadWrapper')).css('height', 'auto');
            $wrapper.find('tbody tr').removeAttr('style');

            height_colgroup = $('.dataTables_scrollBody [data-collapse-group="' + toggletarget + '"]').parents('.colgroup').outerHeight(true) + 'px';

            var $cell_controls = $('[data-collapse-group="' + toggletarget + '"]').find('.cell-control');
            if ($cell_controls.length) {
              $('[data-collapse-group]').find('td:not(.cell-control)').parents('.childrow_cell').css('height', $cell_controls.outerHeight(true));
              $('[data-rowgroup]').css('height', $cell_controls.outerHeight(true));
            }

            $(this).find('.icon').removeClass('icon-abui-arrow-down').addClass('icon-abui-arrow-right');

            $wrapper.find($('.DTFC_ScrollWrapper, .DTFC_RightWrapper, .DTFC_RightBodyWrapper, .DTFC_RightBodyLiner, .DTFC_RightHeadWrapper, .DTFC_LeftWrapper, .DTFC_LeftBodyLiner, .DTFC_LeftBodyWrapper, .DTFC_LeftHeadWrapper')).css('height', 'auto').css('max-height', 'initial');

            //幅リセット
            leftHead_w = 1;
            $wrapper.find('.DTFC_LeftHeadWrapper thead th').each(function(){
              leftHead_w += parseInt($(this).css('width'));
            });

            $wrapper.find($('.DTFC_LeftWrapper, .DTFC_LeftBodyLiner, .DTFC_LeftBodyWrapper, .DTFC_LeftHeadWrapper')).css('width', leftHead_w + 'px');

            //幅リセット
            rightHead_w = 1;
            $wrapper.find('.DTFC_RightHeadWrapper thead th').each(function(){
              rightHead_w += parseInt($(this).css('width'));
            });

            $wrapper.find($('.DTFC_RightWrapper, .DTFC_RightBodyLiner, .DTFC_RightBodyWrapper, .DTFC_RightHeadWrapper')).css('width', rightHead_w + 'px');

            $('[data-collapse-group="' + toggletarget + '"]').animate({height: "0px"}, () => {
              // $('[data-target="' + toggletarget + '"]').each(function(){
                $wrapper.find('.dataTables_scroll .dataTables_scrollBody > table > tbody tr[data-rowgroup="' + toggletarget + '"]').attr('data-childhtml', $wrapper.find('.dataTables_scroll [data-collapse-group="' + toggletarget + '"]').parents('tr[role="row"]').prop('outerHTML'));
                $wrapper.find('.DTFC_LeftWrapper .DTFC_LeftBodyLiner > table > tbody tr[data-rowgroup="' + toggletarget + '"]').attr('data-childhtml', $wrapper.find('.DTFC_LeftWrapper .DTFC_LeftBodyLiner > table > tbody [data-collapse-group="' + toggletarget + '"]').parents('tr[role="row"]').prop('outerHTML'));
                $wrapper.find('.DTFC_RightWrapper .DTFC_RightBodyLiner > table > tbody tr[data-rowgroup="' + toggletarget + '"]').attr('data-childhtml', $wrapper.find('.DTFC_RightWrapper .DTFC_RightBodyLiner > table > tbody [data-collapse-group="' + toggletarget + '"]').parents('tr[role="row"]').prop('outerHTML'));
                $wrapper.find('.DTFC_LeftWrapper, .DTFC_RightWrapper, .dataTables_scroll').find('[data-collapse-group="' + toggletarget + '"]').parents('tr[role="row"]').remove();
              // });
              animating_fl = false;
            });
          } else {
            //datatable高さリセット
            $wrapper.find($('.DTFC_ScrollWrapper, .DTFC_RightWrapper, .DTFC_RightBodyWrapper, .DTFC_RightBodyLiner, .DTFC_RightHeadWrapper, .DTFC_LeftWrapper, .DTFC_LeftBodyLiner, .DTFC_LeftBodyWrapper, .DTFC_LeftHeadWrapper')).removeAttr('style');

            //trのchildhtmlを、trの直後に挿入する
            $wrapper.find('.dataTables_scroll .dataTables_scrollBody > table > tbody tr[data-rowgroup="' + toggletarget + '"]').after($wrapper.find('.dataTables_scroll .dataTables_scrollBody > table > tbody tr[data-rowgroup="' + toggletarget + '"]').data('childhtml')).removeAttr('data-childhtml');
            $wrapper.find('.DTFC_LeftWrapper .DTFC_LeftBodyLiner > table > tbody tr[data-rowgroup="' + toggletarget + '"]').after($wrapper.find('.DTFC_LeftWrapper .DTFC_LeftBodyLiner > table > tbody tr[data-rowgroup="' + toggletarget + '"]').data('childhtml')).removeAttr('data-childhtml');
            $wrapper.find('.DTFC_RightWrapper .DTFC_RightBodyLiner > table > tbody tr[data-rowgroup="' + toggletarget + '"]').after($wrapper.find('.DTFC_RightWrapper .DTFC_RightBodyLiner > table > tbody tr[data-rowgroup="' + toggletarget + '"]').data('childhtml')).removeAttr('data-childhtml');

            // table.columns.adjust();
            $wrapper.find($('.DTFC_ScrollWrapper, .DTFC_RightWrapper, .DTFC_RightBodyWrapper, .DTFC_RightBodyLiner, .DTFC_RightHeadWrapper, .DTFC_LeftWrapper, .DTFC_LeftBodyLiner, .DTFC_LeftBodyWrapper, .DTFC_LeftHeadWrapper')).css('height', 'auto').css('max-height', 'initial');

            //幅リセット
            leftHead_w = 1;
            $wrapper.find('.DTFC_LeftHeadWrapper thead th').each(function(){
              leftHead_w += parseInt($(this).css('width'));
            });

            $wrapper.find($('.DTFC_LeftWrapper, .DTFC_LeftBodyLiner, .DTFC_LeftBodyWrapper, .DTFC_LeftHeadWrapper')).css('width', leftHead_w + 'px');

            //幅リセット
            rightHead_w = 1;
            $wrapper.find('.DTFC_RightHeadWrapper thead th').each(function(){
              rightHead_w += parseInt($(this).css('width'));
            });

            $wrapper.find($('.DTFC_RightWrapper, .DTFC_RightBodyLiner, .DTFC_RightBodyWrapper, .DTFC_RightHeadWrapper')).css('width', rightHead_w + 'px');

            $(this).find('.icon').removeClass('icon-abui-arrow-right').addClass('icon-abui-arrow-down');
            $('[data-collapse-group="' + toggletarget + '"]').removeAttr('style').css('height','0px');
            $('[data-collapse-group="' + toggletarget + '"]').animate({height: height_colgroup}, () => {
              animating_fl = false;
            });
          }
        });

      });

      $(window).on('load resize', (e) => {
        var $wrapper = $('#' + $table.attr('id') + '_wrapper');
        var $scrollWrapper = $wrapper.find('.dataTables_scroll');
        var window_w = window.outerWidth;
        var table_w = $scrollWrapper.find('.dataTables_scrollHead .dataTable').width();

        if (window_w > table_w + parseInt($wrapper.parents('.section').css('padding-left')) + parseInt($wrapper.parents('.section').css('padding-right')) + parseInt($('#header-content').css('margin-left'))) {
          $wrapper.css('width', table_w);
        } else {
          $wrapper.css('width', 'auto');
        }
      });
    },
    adjust: function() {
      table.columns.adjust();
    },
    adjustColumn: function() {
      table.responsive.recalc();
    },
    destroy: function(){
      $('#' + $(this).attr('id')).dataTable2({
        bInfo: false,
        ordering: false,
        searching: false,
        filter: false,
        destroy: true,
        fixedColumns: {
          leftColumns: 3,
          rightColumns: 1
        },
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        search: {
          smart: false
        },
        autoWidth: true,
      });

      table.columns.adjust();
    },
    getTable: function() {
      return table;
    }
  }

  $.fn.initTable = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' is not a function' );
    }
  }

})( jQuery );
