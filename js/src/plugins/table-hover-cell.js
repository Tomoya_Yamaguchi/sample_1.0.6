/****************************************************************************
 テーブル
****************************************************************************/
// Highlight Cells
var mdss_flg = 0;

var overcells = $('.table-hover-cell, .table-hover-cell--pointer').find('td'),
    hoverClass = "hover",
    current_r,
    current_c;
overcells.hover(
  function(){
    if(mdss_flg != 1){
      var $this = $(this);
      (current_r = $this.parent().children("table td")).addClass(hoverClass);
      (current_c = overcells.filter(":nth-child("+ (current_r.index($this)+1) +")")).addClass(hoverClass);
    }
  },
  function(){
    if(mdss_flg != 1){
      current_r.removeClass(hoverClass);
      current_c.removeClass(hoverClass);
    }
  }
);
