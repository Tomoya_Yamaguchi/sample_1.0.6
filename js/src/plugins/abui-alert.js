(function($) {
  var display_timer = 3000;
  var alH = 56;
  var nbH = 48;
  var nlH = 64;
  $.showAlert = function(triggerEl) {
      var deferred = $.Deferred();
      var an = '.' + $(triggerEl).attr('data-tgt');
      //animation中の場合data-status = '1'
      if($(an).attr('data-status') == '0') {
        $(an).addClass('show-alert');
        if($(an).hasClass('alert-center')) {
          lockScreen('lock');
          $('.wrapper-page').addClass('showing-alert').addClass('disable');
          var w = $(an).outerWidth();
          $(an).css('margin-left', -w / 2);
        } else if($(an).hasClass('alert-top-fixed')) {
          var bg = '<div class="alert-top-fixed--bg"></div>'
          $('body').append(bg);
          $('.sidebar-fixed-top').css('top', alH);
          $('.sidebar-fixed-top-bs').css('top', alH + nbH);
          $('.sidebar-fixed-top-bslg').css('top', alH + nbH + nlH);
          $('.wrapper-page').css('padding-top', alH);
          $('.navbar-fixed-top').css('top', alH);
        }
        $(an).attr('data-status', 1);
        setTimeout(function() {
          if(!$(an).hasClass('alert-dismissible') && !$(an).hasClass('alert-top-fixed')) {
            if($(an).hasClass('alert-center')) {
              $('.wrapper-page').removeClass('disable');
            }
            $(an).addClass('hiding');
            $(an).bind('transitionend', function() {
              $(an).addClass('displaynone').removeClass('hiding');
              if($(an).hasClass('alert-center')) {
                $('.wrapper-page').removeClass('showing-alert');
              }
              $(an).removeClass('show-alert');
              setTimeout(function() {
                $(an).removeClass('displaynone');
                unLockScreen('lock');
                $('body').removeClass('disable-operation');
                $(an).unbind('transitionend');
                $(an).attr('data-status', 0);
                deferred.resolve();
              }, 300);
            });
          }
        }, display_timer);
        // return deferred.promise();
      } else {
        return false;
      }
      return deferred.promise();
    }
    // Notification
  $('.alert').attr('data-status', 0);
  $('.alert a:not([data-hide="alert"])').click(function() {
    if(!$(this).parents('.alert').hasClass('alert-top-fixed')) {
      $(this).parents('.alert').fadeOut('fast');
    };
  });
  $("[data-hide]").on("click", function() {
    var an = $(this).closest("." + $(this).attr("data-hide"));
    if($(an).hasClass('alert-center')) {
      $('.wrapper-page').removeClass('disable');
      $(an).bind('transitionend', function() {
        $('.wrapper-page').removeClass('showing-alert');
        $(an).unbind('transitionend');
      });
      $(an).addClass('displaynone').removeClass('show-alert');
    } else if($(an).hasClass('alert-top-fixed')) {
      $(an).css('z-index', '1020');
      $('.alert-top-fixed--bg').css('z-index', '1010');
      $('.sidebar-fixed-top').animate({
        top: "0px"
      }, 300, "swing");
      $('.sidebar-fixed-top-bs').animate({
        top: nbH
      }, 300, "swing");
      $('.sidebar-fixed-top-bslg').animate({
        top: nbH + nlH
      }, 300, "swing");
      $('.wrapper-page').animate({
        paddingTop: "0px"
      }, 300, "swing");
      $('.navbar-fixed-top').animate({
        top: "0px"
      }, 300, "swing");
    } else {
      $(an).addClass('displaynone').removeClass('show-alert');
    }
    setTimeout(function() {
      $(an).removeClass('displaynone');
      unLockScreen('lock');
      $('body').removeClass('disable-operation');
      $(an).unbind('transitionend');
      $(an).attr('data-status', 0);
      $('.alert-top-fixed--bg').remove();
      //リンククリックでaタグがdisplay: noneになる現象を回避（原因不明）
      $('.alert-message').show();
    }, 300);
  });
  //画面をロックする
  function lockScreen(id) {
    var divTag = $('<div />').attr("id", id);
    divTag.css("z-index", "9998").css("position", "fixed").css("top", "0px").css("left", "0px").css("right", "0px").css("bottom", "0px").css("overflow", "hidden");
    $('body').append(divTag).addClass('disable-operation');
  }
  function unLockScreen(id) {
    $("#" + id).remove();
  }
})(jQuery);
