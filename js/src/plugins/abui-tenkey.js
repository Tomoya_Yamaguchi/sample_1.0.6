$(function(){
  var overlayTenkey = '<div class="col-md-6">\
	<button class="btn btn-block brtl btn-stepper"><span class="icon-abui-minus"></span></button>\
	</div>\
	<div class="col-md-6">\
	  <button class="btn btn-block brtr btn-stepper"><span class="icon-abui-plus"></span></button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">7</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">8</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">9</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">4</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">5</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">6</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">1</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">2</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block">3</button>\
	</div>\
	<div class="col-md-8">\
	  <button class="btn btn-block brbl">0</button>\
	</div>\
	<div class="col-md-4">\
	  <button class="btn btn-block brbr">C</button>\
	</div>';
	$(":not(.js-call-tenkey)").on("click focus", function(e){
    if (!($(e.currentTarget).siblings('.control-tenkey').length) && !($(e.target).parents('.control-tenkey').length)) {
      $('.overlay-tenkey').remove();
    }
  });
  $('.js-call-tenkey').on("click focus", function(){
    if (!($(this).parent().find($('.control-tenkey')).length)) {
      var source = '<div class="row control-tenkey overlay-tenkey tenkey--bottom displaynone">' + overlayTenkey + '</div>';
      $(this).before(source);
      $('.overlay-tenkey').fadeIn('fast');
    }
    return false;
  });
});
