/*
for Dashboard
----------------------------------------------------------------
プロジェクト固有の処理記述用
htmlの</body>手前で読み込んでください
*/
// Madonry Initialize
if ($('.dsbd-wdgt--wrap').length) {
  $('.dsbd-wdgt--wrap').masonry({
    itemSelector: '.dsbd-wdgt',
    columnWidth: '.dsbd-wdgt-sizer',
    isFitWidth: true //親要素の幅に合わせてカラム数を自動調整
  });

  // 表示切替
  $('.js--wdgt--switcher .btn').click(function(event) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    var parentPath = $(this).parent().parent().parent().parent().parent();
    $(parentPath).find('.js--wdgt--switcher__tgt').hide();
    console.log(parentPath);
    var tgt = '.' + $(this).attr('id');
    $(tgt).fadeIn('fast');
  });
  $('.js--wdgt--switcher .btn').click(function(event) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    $(this).parent().parent().parent().parent().find('.js--wdgt--switcher__tgt2').hide();
    var tgt = '.' + $(this).attr('id');
    $(tgt).fadeIn('fast');
  });  
}
