//iOSコンポーネント
//カメラロール

var cameraRollSp = $('.cameraRoll.mobile').prop('outerHTML');
var cameraRollTab = $('.cameraRoll.tablet').prop('outerHTML');

(function( $ ) {
  var loaded_fl = 0;

  var methods = {
    init : function(options){
      if (_ua.Mobile) {
        $('.cameraRoll.tablet').remove();
      } else if (_ua.Tablet){
        $('.cameraRoll.mobile').remove();
      }
      adjustCameraRoll();

      loaded_fl = 1;
    }
  }

  $.fn.iOSCameraRoll = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' はiOSCameraRollに存在しません' );
    }
  }

  //表示
  $('.js--cameraRoll').on('click', function(){
    initCameraRoll();
    $('.cameraRoll').addClass('show');
  });

  //[<]クリック
  $('body').on('click', '.js--close--cameraRoll', function(){
    $('.cameraRoll').removeClass('show');
  });

  //[選択]クリック
  $('body').on('click', '.js--select-photo', function(){
    enablePhotoSelect();
  });


  //[キャンセル]クリック
  $('body').on('click', '.js--select-photo-cancel', function(){
    initCameraRoll($(this));
    adjustCameraRoll();
    $('.js--select-photo-cancel').addClass('displaynone');
  });

  $(window).on('orientationchange', function(e){
    var windowH = $(window).height();
    var windowW = $(window).width();
    if (windowH > windowW) {
      $('.cameraRoll--header .header-default.landscape').hide();
      $('.cameraRoll--footer .footer-default.landscape').hide();

      if ($('.cameraRoll--body').hasClass('unselectable')) {
        $('.cameraRoll--header .header-default.portrait').show();
        $('.cameraRoll--footer .footer-default.portrait').show();
      }
    } else if (windowH < windowW) {
      $('.cameraRoll--header .header-default.portrait').hide();
      $('.cameraRoll--footer .footer-default.portrait').hide();

      if ($('.cameraRoll--body').hasClass('unselectable')) {
        $('.cameraRoll--header .header-default.landscape').show();
        $('.cameraRoll--footer .footer-default.landscape').show();
      }
    }
    setTimeout(function(){
      $('.selectedIcon').each(function(){
        $(this).replaceWith('<div class="' + $(this).attr('class') + '" src="' + $(this).attr('src') + '" style="' + $(this).attr('style') +'"></div>');
      });

      adjustCameraRoll();

      $('.selectedIcon').each(function(){
        $(this).replaceWith('<img class="' + $(this).attr('class') + '" src="' + $(this).attr('src') + '" style="' + $(this).attr('style') +'"></div>');
      });

      $('.selectedIcon').each(function(){
        $(this).css('display', 'block');
      });
    },0);


  });

  $(window).on('resize', function(){
    var _ua_before = _ua;
    var _ua_changed_fl_cameraRoll = 0;
    abui.util.getUA();
    if (_ua_before != _ua) {
      _ua_changed_fl_cameraRoll = 1;
    }

    setTimeout(function(){
      if (_ua_changed_fl_cameraRoll) {
        if (_ua.Mobile) {
          $('.cameraRoll.tablet').remove();
          if (!($('.cameraRoll.mobile').length)) {
            $('body').append(cameraRollSp);
          }
        }else if (_ua.Tablet) {
          $('.cameraRoll.mobile').remove();
          if (!($('.cameraRoll.tablet').length)) {
            $('body').append(cameraRollTab);
          }
        }
        $('.cameraRoll').iOSCameraRoll();
        _ua_changed_fl_cameraRoll = 0;
      }
    },200);

  });

  //グループ内、全選択
  $('body').on('click', '.js--selectAll-cameraRoll', function(){
    if (!$(this).parents('cameraRoll--body').hasClass('unselectable')) {

      if ($(this).text() == '選択解除'){
        $(this).parents('.photo-group').find($('.photo')).removeClass('selected');
        $(this).parents('.photo-group').find($('.selectedIcon')).remove();
        $(this).text('選択');

        if (_ua.Tablet) {
          $(this).siblings('.date').css('margin-right', '0px');
        }

        selectCount();
      } else if ($(this).text() == '選択') {
        $(this).parents('.photo-group').find($('.photo')).each(function(){
          if (!$(this).hasClass('selected')) {
            $(this).addClass('selected');
            $(this).append('<img class="selectedIcon" src="images/iOS/iOS_camera_cameraRoll_selected_icon.png" alt="" />')

            if (_ua.Tablet) {
              $(this).getBackgroundSize(function(arg){
                if (arg.width > arg.height) {
                  $(this).find($('.selectedIcon')).css('bottom', '22px')
                } else if (arg.width < arg.height) {
                  $(this).find($('.selectedIcon')).css('right', '20px')
                }
              });
            }
          }
        });

        $(this).text('選択解除');
        if (_ua.Tablet) {
          $(this).siblings('.date').css('margin-right', '32px');
        }

        selectCount();
      }

    }
  });


  $('body').on('click', '.cameraRoll .photo', function(){
    if (!$(this).parents('.cameraRoll--body').hasClass('unselectable')) {

      if (!$(this).hasClass('selected')) {
        $(this).addClass('selected');
        $(this).prepend('<img class="selectedIcon" src="images/iOS/iOS_camera_cameraRoll_selected_icon.png" alt="" />');

        if (_ua.Tablet) {
          $(this).getBackgroundSize(function(arg){
            if (arg.width > arg.height) {
              $(this).find($('.selectedIcon')).css('bottom', '22px')
            } else if (arg.width < arg.height) {
              $(this).find($('.selectedIcon')).css('right', '20px')
            }
          });
        }
      } else {
        $(this).removeClass('selected');
        $(this).find($('.selectedIcon')).remove();
      }

      selectCount();
    }
  });

  function adjustCameraRoll() {
    var windowH = $(window).height();
    var windowW = $(window).width();

    $('.cameraRoll').css({
      'height': windowH,
      'width': windowW,
      'top': windowH,
    });

    $('.cameraRoll--title').css({
      'width': windowW,
    });

    $('.photo-group').css({
      'width': windowW,
    });

    $('.cameraRoll--header').css({
      width: windowW,
    });

    $('.footer-select-button').css({
      width: windowW,
    });

    var headerH = 0;
    var footerH = 0;

    //画像ロード完了時の処理
    $('.cameraRoll--header').find($('img')).load(function() {
      if (windowH > windowW) {
        headerH = $('.header-default.portrait').outerHeight();
        footerH = $('.footer-default.portrait').outerHeight();
      } else if (windowH < windowW) {
        headerH = $('.header-default.landscape').outerHeight();
        footerH = $('.footer-default.landscape').outerHeight();
      }

      $('.cameraRoll--title').css({
        'height': headerH,
      });

      $('.cameraRoll--header').css({
        height: headerH,
      });

      $('.cameraRoll--body').css({
        'height': (windowH - (headerH + footerH)),
        'margin-top': $('.cameraRoll--header').outerHeight(),
        'margin-bottom': $('.cameraRoll--footer').outerHeight(),
      });
    }).each(function() {
      if(this.complete) $(this).load();
    });

    if (_ua.Mobile) {
      $('.cameraRoll--body .photo').css({
        'width': ((windowW -3) / 4), //3pxはmargin分
        'height': ((windowW -3) / 4),
      });
    } else {
      var photoSize = 0;
      if (windowH > windowW) {
        photoSize = ((windowW / 5) - 24); //32pxはmargin分
      } else if (windowH < windowW) {
        photoSize = ((windowW / 7) - 24);
      }

      $('.cameraRoll--body .photo').css({
          'width': photoSize, //32pxはmargin分
          'height': photoSize,
      });
    }

    if (_ua.Mobile) {
      $('.cameraRoll--body .photo').imgLiquid();
    } else if (_ua.Tablet) {
      $('.cameraRoll--body .photo').imgLiquid({ fill: false });
    }
  }

  function initCameraRoll(clickedEl) {
    var windowH = $(window).height();
    var windowW = $(window).width();
    //header
    $('.cameraRoll--header .header-select').hide();
    if (_ua.Mobile) {
      // $('.cameraRoll--header .header-default').show();
      if (windowH > windowW) {
        $('.cameraRoll--header .header-default.portrait').show();
      } else if (windowH < windowW) {
        $('.cameraRoll--header .header-default.landscape').show();
      }
    } else if (_ua.Tablet){
      if (windowH > windowW) {
        $('.cameraRoll--header .header-default.portrait').show();
      } else if (windowH < windowW) {
        $('.cameraRoll--header .header-default.landscape').show();
      }

    }
    $('.cameraRoll--body').addClass('unselectable');
    $('.cameraRoll--title').html('モーメント');
    $('.js--close--cameraRoll').removeClass('displaynone');
    $('.cameraRoll--header .toggle-disable').addClass('disabled');

    //footer
    $('.cameraRoll--footer .footer-select').hide().addClass('disabled');;
    if (_ua.Mobile) {
      // $('.cameraRoll--footer .footer-default').show();
      if (windowH > windowW) {
        $('.cameraRoll--footer .footer-default.portrait').show();
      } else if (windowH < windowW) {
        $('.cameraRoll--footer .footer-default.landscape').show();
      }
    } else if (_ua.Tablet){
      if (windowH > windowW) {
        $('.cameraRoll--footer .footer-default.portrait').show();
      } else if (windowH < windowW) {
        $('.cameraRoll--footer .footer-default.landscape').show();
      }
    }

    $(clickedEl).addClass('displaynone');
    $('.js--select-photo').removeClass('displaynone');
    $(clickedEl).parents('.cameraRoll').find($('.photo')).removeClass('selected');
    $(clickedEl).parents('.cameraRoll').find($('.selectedIcon')).remove();

    //body
    $('.group-title .select').hide().html('共有').fadeIn('fast');
  }

  function enablePhotoSelect() {
    //header
    $('.cameraRoll--header .header-default').hide();
    $('.cameraRoll--header .header-select').show();
    $('.cameraRoll--body').removeClass('unselectable');
    $('.cameraRoll--title').html('項目を選択');
    $('.js--close--cameraRoll').addClass('displaynone');
    $('.cameraRoll--header .toggle-disable').addClass('disabled');

    //footer
    $('.cameraRoll--footer .footer-default').hide();
    $('.cameraRoll--footer .footer-select').show();

    $('.js--select-photo').addClass('displaynone');
    $('.js--select-photo-cancel').removeClass('displaynone');

    //body
    $('.group-title .select').hide().html('選択').fadeIn('fast');

    adjustCameraRoll();
  }

  function selectCount() {
    var photoSelected_fl = 0;
    var photoSelected_cnt = 0;
    var photoSelected_cnt_Total = 0;

    $('.cameraRoll--body .photo-group').each(function(){
      $(this).find($('.photo')).each(function(){
        if ($(this).hasClass('selected')) {
          photoSelected_fl = 1;
          photoSelected_cnt++;
          photoSelected_cnt_Total++;
        }
      });

      //グループ内全て選択されている場合
      if (photoSelected_cnt == $(this).find($('.photo')).length) {
        $(this).find($('.js--selectAll-cameraRoll')).text('選択解除');
        if (_ua.Tablet) {
          $(this).find($('.date')).css('margin-right', '32px');
        }
      } else {
        $(this).find($('.js--selectAll-cameraRoll')).text('選択');
        if (_ua.Tablet) {
          $(this).find($('.date')).css('margin-right', '0px');
        }
      }

      photoSelected_cnt = 0;
    });

    if (photoSelected_fl) {
      $('.cameraRoll--footer .footer-select').removeClass('disabled');
      $('.cameraRoll--header .toggle-disable').removeClass('disabled');
      $('.cameraRoll--title').html('選択中の写真： ' + photoSelected_cnt_Total + '枚');
    } else {
      $('.cameraRoll--footer .footer-select').addClass('disabled');
      $('.cameraRoll--header .toggle-disable').addClass('disabled');
      $('.cameraRoll--title').html('項目を選択');
    }
  }
})( jQuery );
