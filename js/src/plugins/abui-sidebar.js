(function () {
  //sidebarイベントハンドラ
  /****************************************************************************
    sidebar制御
    ****************************************************************************/
  // Sidebarの表示・非表示制御
  $(document).on('click', '.sidebar li a, .sidebar-toggle', function() {
    sidebarOperation();
  });

  // タッチデバイスでサイドバー領域外をタップした時
  $(document).on('touchend', function(e){
    if ($(window).width() > 767) {
      var sidebarDisplay = 192;
    } else {
      var sidebarDisplay = 272;
    }
    if($(e.target).closest('.wrapper').length) {
      $(".sidebar").animate({left:0-sidebarDisplay},300,function(){$(".sidebar").attr("style","")});
    }
  });

  function sidebarOperation() {
    var sidebarWidth = $('.sidebar').width();
    if ($(window).width() > 767) {
      var sidebarDisplay = 192;
    } else {
      var sidebarDisplay = 272;
    }

    if($(window).width() < 1025) {
      // タッチデバイス用コード
      if(parseInt($('.sidebar').css('left')) == (0 - sidebarDisplay)){
        $(".sidebar").animate({
          left: 0
        }, 300);
      } else {
        $(".sidebar").animate({
          left: (0 - sidebarDisplay)
        }, 300,function () {
          $(".sidebar").attr('style', '');
        });
      }
    } else {
      // PC用コード
      if ($('.container').length) {
        var container = $('.container');
      } else {
        var container = $('.container-fluid');
      }
      if(parseInt($('.sidebar').css('left')) == 0 && parseInt(container.css('margin-left')) != sidebarDisplay){
        $(".sidebar").animate({
          left: (0 - sidebarDisplay)
        }, 300,function () {
          $(".sidebar").attr('style', '');
        });
      } else {
        $(".sidebar").animate({
          left: 0
        }, 300);
      }
    }
  }

  $(document).on('click', '.wrapper', function() {
    var sidebarWidth = $('.sidebar').width();
    var windowWidth  = $(window).width();

    if ($(window).width() > 767) {
      var sidebarDisplay = 192;
    } else {
      var sidebarDisplay = 272;
    }

    if ($('.container').length) {
      var container = $('.container');
    } else {
      var container = $('.container-fluid');
    }
    if(parseInt($('.sidebar').css('left')) == 0 && parseInt(container.css('margin-left')) != sidebarDisplay){
      $(".sidebar").animate({
        left: (0 - sidebarDisplay)
      }, 300,function () {
        $(".sidebar").attr('style', '');
      });
    }
  });


  //ネストされたsidebar内メニュー項目の開閉処理
  $(document).on('click', '.expander', function(){
    $(this).toggleClass('open');
    $(this).nextAll('.expander-target').slideToggle('fast');
    if($(this).hasClass('open') == false) {
      var subSection = $(this).nextAll('.expander-target').find('.expander');
      subSection.removeClass('open');
      subSection.nextAll('.expander-target').slideUp('fast');
    }
  });
})();
