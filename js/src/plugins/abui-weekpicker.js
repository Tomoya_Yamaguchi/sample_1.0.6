(function( $ ) {
  var startDate, endDate, today, selectedDate, dateFormat, res, inputVal;

  var options = {
    showOn: "focus",
    showAnim: "fadeIn",
    showOptions: {},
    defaultDate: null,
    appendText: "",
    buttonText: "...",
    buttonImage: "",
    buttonImageOnly: !1,
    hideIfNoPrevNext: !1,
    navigationAsDateFormat: !1,
    gotoCurrent: !1,
    changeMonth: !1,
    changeYear: !1,
    yearRange: "c-10:c+10",
    showOtherMonths: true,
    selectOtherMonths: true,
    showWeek: false,
    calculateWeek: getFiscalWeek,
    shortYearCutoff: "+10",
    minDate: null,
    maxDate: null,
    duration: "fast",
    onChangeMonthYear: null,
    numberOfMonths: 1,
    showCurrentAtPos: 0,
    stepMonths: 1,
    stepBigMonths: 12,
    altField: "",
    altFormat: "",
    constrainInput: !0,
    showButtonPanel: !1,
    autoSize: !1,
    disabled: !1,
    firstDay: 0,
    dateFormat: "yy/mm/dd",
  }

  var getFiscalWeek = function(d){
    function getFC(val) {
      var result = "日付文字列が不正です。";  //日付不正時のメッセージ
      try {
        var y = val.getFullYear();
        var m = val.getMonth();
        var d = val.getDate();
        var dt = new Date(y, m, d);
        if(m < 3){  //4月はじまり
          result = y-1;
        } else {
          result = y;
        }
        return result;

      } catch(ex){
        return result;
      }
    }

    var date = new Date(d.getTime());
    var firstday = new Date(getFC(date), 3, 1);
    var fulldays = Math.floor((date.getTime() - firstday.getTime()) / (1000 * 86400));

    return Math.floor((fulldays - date.getDay() + 12 ) / 7);
  };

  var methods = {
    init : function(args){
      var input = $(this);
      var widget = $(this).datepicker('widget');

      if (args) {
        options.showOn = typeof(args.showOn) != 'undefined' && args.showOn ? args.showOn : "focus";
        options.showAnim = typeof(args.showAnim) != 'undefined' && args.showAnim ? args.showAnim : "fadeIn";
        options.showOptions = typeof(args.showOptions) != 'undefined' && args.showOptions ? args.showOptions : {};
        options.defaultDate = typeof(args.defaultDate) != 'undefined' && args.defaultDate ? args.defaultDate : null;
        options.appendText = typeof(args.appendText) != 'undefined' && args.appendText ? args.appendText : "";
        options.buttonText = typeof(args.buttonText) != 'undefined' && args.buttonText ? args.buttonText : "...";
        options.buttonImage = typeof(args.buttonImage) != 'undefined' && args.buttonImage ? args.buttonImage : "";
        options.buttonImageOnly = typeof(args.buttonImageOnly) != 'undefined' && args.buttonImageOnly ? args.buttonImageOnly : !1;
        options.hideIfNoPrevNext = typeof(args.hideIfNoPrevNext) != 'undefined' && args.hideIfNoPrevNext ? args.hideIfNoPrevNext : !1;
        options.navigationAsDateFormat = typeof(args.navigationAsDateFormat) != 'undefined' && args.navigationAsDateFormat ? args.navigationAsDateFormat : !1;
        options.gotoCurrent = typeof(args.gotoCurrent) != 'undefined' && args.gotoCurrent ? args.gotoCurrent : !1;
        options.changeMonth = typeof(args.changeMonth) != 'undefined' && args.changeMonth ? args.changeMonth : !1;
        options.changeYear = typeof(args.changeYear) != 'undefined' && args.changeYear ? args.changeYear : !1;
        options.yearRange = typeof(args.yearRange) != 'undefined' && args.yearRange ? args.yearRange : "c-10:c+10";
        options.showOtherMonths = true;
        options.selectOtherMonths = true;
        options.showWeek = typeof(args.showWeek) != 'undefined' && args.showWeek ? true : false;
        options.calculateWeek = getFiscalWeek;
        options.shortYearCutoff = typeof(args.shortYearCutoff) != 'undefined' && args.shortYearCutoff ? args.shortYearCutoff : "+10";
        options.minDate = typeof(args.minDate) != 'undefined' && args.minDate ? args.minDate : null;
        options.maxDate = typeof(args.maxDate) != 'undefined' && args.maxDate ? args.maxDate : null;
        options.duration = typeof(args.duration) != 'undefined' && args.duration ? args.duration : "fast";
        options.onChangeMonthYear = typeof(args.showOn) != 'undefined' && args.onChangeMonthYear ? args.onChangeMonthYear : null;
        options.numberOfMonths = typeof(args.numberOfMonths) != 'undefined' && args.numberOfMonths ? args.numberOfMonths : 1;
        options.showCurrentAtPos = typeof(args.showCurrentAtPos) != 'undefined' && args.showCurrentAtPos ? args.showCurrentAtPos : 0;
        options.stepMonths = typeof(args.stepMonths) != 'undefined' && args.stepMonths ? args.stepMonths : 1;
        options.stepBigMonths = typeof(args.stepBigMonths) != 'undefined' && args.stepBigMonths ? args.stepBigMonths : 12;
        options.altField = typeof(args.altField) != 'undefined' && args.altField ? args.altField : "";
        options.altFormat = typeof(args.altFormat) != 'undefined' && args.altFormat ? args.altFormat : "";
        options.constrainInput = typeof(args.constrainInput) != 'undefined' && args.constrainInput ? args.constrainInput : !0;
        options.showButtonPanel = typeof(args.showButtonPanel) != 'undefined' && args.showButtonPanel ? args.showButtonPanel : !1;
        options.autoSize = typeof(args.autoSize) != 'undefined' && args.autoSize ? args.autoSize : !1;
        options.disabled = typeof(args.disabled) != 'undefined' && args.disabled ? args.disabled : !1;
        options.firstDay = typeof(args.firstDay) != 'undefined' && args.firstDay ? args.firstDay : 0;
        options.dateFormat = typeof(args.dateFormat) != 'undefined' && args.dateFormat ? args.dateFormat : "yy/mm/dd";
      }

      options.onClose = function(){
        if (typeof(startDate) != 'undefined') {
          $(this).datepicker('setDate', startDate);
          $(this).val(inputVal);
        }

        widget.removeClass('ui-weekpicker-calendar').removeClass('withWeekNum');
        widget.hide().addClass('hideWeekpicker');
      }

      options.onSelect = function(dateText, inst) {
        $(this).blur();
        var date = $(this).datepicker('getDate');
        if (date.getDay() == 0 &&  inst.settings.firstDay == 1) {
          startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - (7 - inst.settings.firstDay));
          endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        } else {
          startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + inst.settings.firstDay);
          endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6 + inst.settings.firstDay);
        }
        today = new Date();
        dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
        inputVal = $.datepicker.formatDate( dateFormat, startDate, inst.settings ) + '〜' + $.datepicker.formatDate( dateFormat, endDate, inst.settings );
        selectedDate = startDate;

        widget.hide().addClass('hideWeekpicker');
      };

      options.beforeShow = function(input, inst) {
        widget.addClass('ui-weekpicker-calendar');

        if (inst.settings.showWeek) {
          widget.addClass('withWeekNum');
        }

        setTimeout(function(){
          widget.css('opacity', '0').removeClass('hideWeekpicker').addClass('fadeInWeekpicker')
        },0)
        inputVal = $(this).val();

        $(this).datepicker('setDate', startDate);
      },

      options.beforeShowDay = function(date) {
        var cssClass = '';
        if (typeof(startDate) != 'undefined' && typeof(endDate) != 'undefined') {
          if (date >= startDate && date <= endDate) {
            cssClass = 'ui-datepicker-current-day';
          } else if (today < startDate || today > endDate) {
            cssClass = 'reset-ui-active';
          } else {
            cssClass = '';
          }
        }

        var e = event;
        setTimeout(function(){
          input.datepicker('widget').find('.ui-datepicker-current-day:not(.reset-ui-active)').parents('tr').addClass('ui-datepicker-current-week');
          input.val(inputVal);
        });

        return [true, cssClass];
      },

      this.attr('component-type', 'weekpicker');

      this.datepicker(options);

      if (this.val()) {
        var date = new Date(this.val().split('〜')[0]);
        if (date.getDay() == 0 &&  options.firstDay == 1) {
          startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - (7 - options.firstDay));
          endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        } else {
          startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + options.firstDay);
          endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6 + options.firstDay);
        }
        today = new Date();

        this.datepicker('setDate', startDate);
        dateFormat = options.dateFormat || $.datepicker._defaults.dateFormat;
        res = $.datepicker.formatDate( dateFormat, startDate, options ) + '〜' + $.datepicker.formatDate( dateFormat, endDate, options );

        inputVal = res;
      }
    },
    setWeek: function(date) {
      date = new Date(date);
      if (date.getDay() == 0 &&  options.firstDay == 1) {
        startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - (7 - options.firstDay));
        endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      } else {
        startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + options.firstDay);
        endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6 + options.firstDay);
      }
      today = new Date();

      this.datepicker('setDate', startDate);
      dateFormat = options.dateFormat || $.datepicker._defaults.dateFormat;
      res = $.datepicker.formatDate( dateFormat, startDate, options ) + '〜' + $.datepicker.formatDate( dateFormat, endDate, options );

      inputVal = res;
    },
    getWeek: function() {
      return {startDate: $(this).val().split('〜')[0],endDate: $(this).val().split('〜')[1]}
    }
  }

  $.fn.weekpicker = function(method) {
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' is not a function' );
    }
  }

})( jQuery );
