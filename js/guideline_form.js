/*
for Guideline Document Only
----------------------------------------------------------------
*/


$(function() {
  // 表示制御
  function tgtDisable(tgt) {
    $(tgt).find('select, input').attr('disabled', 'disabled');
    $(tgt).find('.select-block, .btn').addClass('disabled');
  }

  function tgtAble(tgt) {
    $(tgt).find('select, input').removeAttr('disabled');
    $(tgt).find('.select-block, .btn').removeClass('disabled');
  }
  // チェックボックスで
  function checkView(id) {
    var tgt = $('.js-check_item' + id + '__tgt');
    if($(".js-check_item#" + id).hasClass('checked')) {
      $('.js-check_item' + id + '__tgt').hide();
      tgtDisable(tgt);
    } else {
      $('.js-check_item' + id + '__tgt').fadeIn('fast');
      tgtAble(tgt);
    }
  }
  $(".js-check_item").click(function() {
    var id = $(this).attr("id");
    checkView(id);
  });
  // スイッチで
  function switchView(id) {
    var tgt = $('.js-switch_item' + id + '__tgt');
    if($(".js-switch_item#" + id + " .switch-animate").hasClass('switch-off')) {
      $('.js-switch_item' + id + '__tgt').hide();
      tgtDisable(tgt);
    } else {
      $('.js-switch_item' + id + '__tgt').fadeIn('fast');
      tgtAble(tgt);
    }
  }
  $('.js-switch_item [data-toggle="switch"]').on('change', function() {
    var switchItem = $(this).parents('.js-switch_item');
    var id = switchItem.attr("id");
    switchView(id);
  });
  // ラジオで
  var selectedRadio = '';
  $(".js-radio_item input:radio").change(function(event) {
    var name = $(this).attr('name');
    var tgt = $('.js-radio_item' + name + '__tgt');
    if($(this).prop('checked')) {
      $(tgt).hide();
      tgtDisable(tgt);
      if($(this).prop('checked') && selectedRadio != this.value) {
        $('.js-radio_item' + name + '__tgt#' + name + '_' + this.value).fadeIn('fast');
        tgtAble(tgt);
        selectedRadio = this.value;
      }
    }
  });
  // ドロップダウンで
  function selectView(id, rel) {
    var tgt = $('.js-select_item-' + id + '__tgt');
    $(tgt).hide();
    tgtDisable(tgt);
    var tgt2 = $('.js-select_item-' + id + '__tgt' + '#' + id + '_' + rel);
    $(tgt2).fadeIn('fast');
    tgtAble(tgt2);
  }
  $(document).on('click', '.js-select_item .dropdown-menu li', function() {
    var id = $(this).parent().parent().parent().find('select').attr("id");
    var rel = parseInt($(this).attr('data-original-index')) + 1;
    selectView(id, rel);
  });
  // 数値入力
  // スライダー
  function addPlus(value) {
    if(value == 0) {
      value = '±' + value;
      return value;
    } else if(value > 0) {
      value = '+' + value;
      return value;
    } else if(value < 0) {
      value = value;
      return value;
    }
  }
  // $( "#slider--numinput" ).slider({
  //   value:0.0,
  //   min: -3.0,
  //   max: 3.0,
  //   step: 0.5,
  //   slide: function( event, ui ) {
  //     var amount = addPlus(ui.value);
  //     $( "#slider--numinput__amount" ).val( amount );
  //   }
  // });
  // $( "#slider--numinput__amount" ).val( $(
  //   "#slider--numinput" ).slider( "value" ) );

  // 入力の追加
  // 入力グループの追加
  $('.js--addcontrol1').click(function() {
    var len_list = $('.js--addcontrol1--wrap > div').length;
    var newitem = $('.js--addcontrol1--1').html();
    var num = len_list;
    newitem = '<div class="row js--addcontrol1--' + num + '">' + newitem + '</div>';
    $($('.js--addcontrol1').parents('.row--addcontrol')).before(newitem);
    $('.js--addcontrol1--' + num + ' input').val('');
    $('.js--addcontrol1--' + num + ' .label--code').html('<span class="label-text">コード' + num + '</span>');
    $('.js--addcontrol1--' + num + ' .label--name').html('<span class="label-text">商品名' + num + '</span>');
    $('.js--addcontrol1--' + num + ' .label--price').html('<span class="label-text">単価' + num + '</span>');
    // 項目が5個になったら、追加ボタンを消去
    if(len_list == 5) $(this).parent().hide();
  });
  // 表組入力行の追加
  $('.js--addcontrol--item').click(function() {
    var id = $(this).attr('id');
    var old_num = $('.' + id + '--table tbody').children().length;
    var new_num = old_num + 1;
    var table = $('.' + id + '--table');
    var new_item = $($('.' + id + '--table tbody tr')[0]).html();
    var last_row_class_name = $($('.' + id + '--table tbody tr')[old_num - 1]).context.className;
    var new_row_class_num = Number(last_row_class_name.split("row")[1]) + 1;
    new_item = '<tr class="' + id + '--row' + new_row_class_num + '">' + new_item + '</tr>';
    $('.' + id + '--table tbody').append(new_item);
    // 値のリセット
    $('.' + id + '--row' + new_row_class_num + ' input').val('');
    $('.' + id + '--row' + new_row_class_num + ' select option').removeAttr('selected');
    // ナンバリング
    $('.' + id + '--row' + new_row_class_num + ' .' + id + '--row--num').html(new_num);
    // ドロップダウン
    $('.' + id + '--row' + new_row_class_num + ' td').each(function() {
      $($(this).find('.dropdown-toggle')[0]).unwrap().remove();
      $($(this).find('.dropdown-menu')[0]).remove();
      $(this).find($("select[name='input']")).selectpicker({
        style: 'btn-input'
      }).on('show.bs.select', function(e) {
        $(this).parents('.bootstrap-select').find($('.dropdown-menu')).addClass('open');
      }).on('hidden.bs.select', function(e) {
        $(this).parents('.bootstrap-select').find($('.dropdown-menu')).removeClass('open');
      });
    });
    // 行数が上限になったら、追加ボタンを消去
    if(new_row_class_num == 10) $(this).parent().hide();
  });
  // 表組からモーダルで入力して行追加
  $('#modal--addcontrol--item-2').on('click', '.btn--addcontrol--item--done', function(event) {
    var modal = '#modal--addcontrol--item-2';
    var id = $('.js--addcontrol--modal').attr('id');
    // 入力した値
    var new_name = $(modal + ' .js--addcontrol--item--name').val();
    var new_org = $(modal + ' .js--addcontrol--item--org1 .dropdown-menu li.selected .text').html();
    var rel = parseInt($(modal + ' .js--addcontrol--item--org1 .dropdown-menu li.selected').attr('data-original-index')) + 1;
    if(new_org) {
      new_suborg = $(modal + ' #org_' + rel + ' .js--addcontrol--item--org2 .dropdown-menu li.selected .text').html();
      if(new_suborg) {
        new_org = new_org + '&ensp;&gt;&ensp;' + new_suborg;
      };
    } else {
      new_org = '';
    };
    var old_num = $('.' + id + '--table tbody').children().length;
    var new_num = old_num + 1;
    var table = $('.' + id + '--table');
    var new_item = $($('.' + id + '--table tbody tr')[0]).html();
    var last_row_class_name = $($('.' + id + '--table tbody tr')[old_num - 1]).context.className;
    var new_row_class_num = Number(last_row_class_name.split("row")[1]) + 1;
    new_item = '<tr class="' + id + '--row' + new_row_class_num + ' feedback-changed">' + new_item + '</tr>';
    $('.' + id + '--table tbody').append(new_item);
    // 値挿入
    $('.' + id + '--row' + new_row_class_num + ' .' + id + '--row--name').html(new_name);
    $('.' + id + '--row' + new_row_class_num + ' .' + id + '--row--org').html(new_org);
    // ナンバリング
    $('.' + id + '--row' + new_row_class_num + ' .' + id + '--row--num').html(new_num);
    // 行数が上限になったら、追加ボタンを消去
    if(new_row_class_num == 10) {
      $(table).find('#js--addcontrol--item-2').hide();
    }
    setTimeout(function() {
      $('.feedback-changed').removeClass('feedback-changed');
    }, 2000);
  });
  // モーダルを閉じたらデータを初期化
  $('#modal--addcontrol--item-2').on('hidden.bs.modal', function() {
    $('.js--addcontrol--item--name', this).val('');
    $('.select-block', this).selectpicker('deselectAll');
    $('.js-select_item-org__tgt', this).each(function(e) {
      if($(this).css('display') == 'block') {
        $(this).css('display', 'none');
      }
    });
  });

  // リストメーカー
  $('.js-listmaker').find('td').click(function(event) {
    if($(this).parent().hasClass('selected')) {
      $(this).parent().removeClass('selected');
      $('.js-listmaker__btn .btn').attr('disabled', 'disabled');
    } else {
      $('.js-listmaker tr').removeClass('selected');
      $(this).parent().addClass('selected');
      $('.js-listmaker__btn .btn').removeAttr('disabled');
    };
  });
  $('.js-listmaker__btn .btn').click(function(event) {
    var parentPath = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
    if($(this).hasClass('btn-justadd')) {
      var listmakerCat = $(parentPath).find('.dropdown-toggle').attr('title');
      console.log(listmakerCat);
      var str = listmakerCat + '： ' + $('.js-listmaker tr.selected td').html();
    } else {
      var str = $('.js-listmaker tr.selected td').html();
      str = str + $(this).html();
    };
    var newitem = '<tr><td class="cell-control sort-control"><a href="#"><span class="icon icon-abui-arrow-up"></span></a><a href="#"><span class="icon icon-abui-arrow-down"></span></a></td><td class="listmaker--trigger-select">' + str + '</td><td class="cell-control text-right"><a class="btn btn-flat-danger btn-sm btn-delete" href="#"><span class="icon-abui-cross"></span></a></td></tr>';
    $('.js-listmaker__tgt').append(newitem);
  });
  $(document).on('click', '.js-listmaker__tgt .sort-control a', function() {
    if($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $('.js-listmaker__tgt .sort-control a').removeClass('active');
      $(this).addClass('active');
    };
  });
  $(document).on('click', '.js-listmaker__tgt .btn-delete', function() {
    var parentPath = $(this).parent().parent();
    $(parentPath).remove();
  });
  $(document).on('click', '.listmaker__tgt--control .btn-delall', function() {
    var parentPath = $(this).parent().parent();
    $('.js-listmaker__tgt').empty();
  });
  $(document).on('click', '.js-listmaker__tgt tr .listmaker--trigger-select', function() {
    if($(this).parent().hasClass('selected')) {
      $(this).parent().removeClass('selected');
    } else {
      $('.js-listmaker__tgt tr').removeClass('selected');
      $(this).parent().addClass('selected');
    };
  });

  // 選択したものを追加
  // (AB_guideline_pattern_form.html#abui-various-input-12)
  // リストに追加
  var onTgtTr = $('.js-add-selected-table').find('.js-btn--add-selected-list-item').closest('tr');
  $('.js-btn--add-selected-list-item').on('click', function(event) {
    var str1 = $(this).closest('tr').find('td').eq(0).html();
    var str2 = $(this).closest('tr').find('td').eq(1).html();
    var newitem = '<li>' + str1 + '：' + str2 + '<span class="icon icon-abui-cross"></span></li>';
    $('.add-selected-list').append(newitem);
  });
  // リストから削除
  $(document).on('click', '.add-selected-list li', function() {
    $(this).remove();
  });
  // リストからすべて削除
  $(document).on('click', '.js-btn--del-selected-list-item', function() {
    var path = $(this).closest('.add-selected__wrap');
    $(path).find('.add-selected-list li').remove();
  });

  // ドリルダウン
  // ダミー動作
  $('.table-drilldown-wrap:nth-last-child(n+2) .table-drilldown tr').click(function(event) {
    var path = $(this).parent().parent().parent().parent().parent();
    $(path).next('.table-drilldown-wrap').fadeIn('fast');
    $(this).parent().find('tr').removeClass('selected');
    $(this).addClass('selected');
  });
  // drilldown リストに追加
  $('.table-drilldown .js-btn--adddrilldownitem').click(function(event) {
    var str = $(this).parent().prev().html();
    var newitem = '<li class="">' + str + '<span class="icon icon-abui-cross"></span></li>';
    var path = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent();
    console.log(path);
    $(path).find('.drilldown-list').append(newitem);
  });
  // drilldown リストから削除
  $(document).on('click', '.drilldown-list li', function() {
    $(this).remove();
  });
  // drilldown リストからすべて削除
  $(document).on('click', '.js-btn--deldriildownlistitem', function() {
    var path = $(this).parent().parent();
    console.log(path);
    $(path).find('.drilldown-list li').remove();
  });

  // 場所
  // メニュークリック時の処理
  $('.js-acordion2').on('click', '.js-acordionList-btn-root, .js-acordionList-btn, .js--addrow', function(e) {
    // バブリング防止
    e.stopPropagation();
    if($(this).children('ul').length) {
      $(this).children('.anchor').find('.icon').toggleClass('icon-abui-arrow-right');
      $(this).children('.anchor').find('.icon').toggleClass('icon-abui-arrow-down');
    }
    //選択時のスタイル
    $('.table-tree').find($('.anchor')).removeClass('selected');
    if(!$(this).children('.anchor').hasClass('addrow') && !$(this).children('.addrow').length) {
      $(this).children('.anchor').addClass('selected');
    } else {
      if(!$($(this).parents('ul')[0]).find($('.js-acordionList-btn-root')).length) {
        var rows = $($(this).parents('ul')[0]).children('.js-acordionList-btn');
        if(rows.length) { //一件以上ある場合
          var index_lastRow = $(rows).length - 1;
          $($(rows)[index_lastRow]).after($('.js--addrow--template').html());
          $(this).parents('ul').find($('.js-acordionList-btn:last input')).focus();
        } else { //0件
          $($(this).parents('ul')[0]).prepend($('.js--addrow--template').html());
        }
      } else { //ルートディレクトリの場合
        if($($(this).parents('ul')[0]).children('.js-acordionList-btn').length) { //一件以上ある場合
          $($(this).parents('ul')[0]).children('.js-acordionList-btn:last').after($('.js--addrow--template').html())
        } else { //0件
          $($(this).parents('ul')[0]).find($('.js-acordionList-btn-root:last')).after($('.js--addrow--template').html());
        }
      }
      applyStyleToModal();
      $($(this).parents('ul')[0]).find('.js-acordionList-btn:last').removeClass('displaynone');
      $($(this).parents('ul')[0]).find('.js-acordionList-btn:last input').focus();
      $($(this).parents('ul')[0]).find('.js-acordionList-btn:last .anchor').addClass('selected');
    }
    // メニュー表示/非表示
    $(this).children('.anchor').next('ul').slideToggle('fast');
  });
  applyStyleToModal();

  function applyStyleToModal(option) {
    "use strict";
    //初期化用
    var anchor = $('.table .anchor');
    var paddingReset = {
      'padding-left': '0px',
    }
    var padding = {}
    for(var i = 0; i < anchor.length; i++) {
      //奇数行の指定
      if(i % 2 == 0) {
        if(i == 0) {
          $(anchor[i]).parent().css(paddingReset).css(padding).removeClass('bg-even').addClass('bg-odd'); //先頭行
        } else {
          var num_nest = $(anchor[i]).parents('ul').length - 1;
          // padding-left: ネストされている回数 × 32px
          padding['padding-left'] = (32 * num_nest) + 'px';
          if($(anchor[i]).hasClass('addrow')) { //行追加ボタン
            $(anchor[i]).parent().css(paddingReset).css(padding).removeClass('bg-even').addClass('bg-odd');
          } else {
            $(anchor[i]).css(paddingReset).css(padding).removeClass('bg-even').addClass('bg-odd');
          }
        }
      } else {
        var num_nest = $(anchor[i]).parents('ul').length - 1;
        if(num_nest >= 1) {
          if($(anchor[i]).hasClass('addrow')) { //行追加ボタン
            $(anchor[i]).parent().css(paddingReset).css('padding-left', (32 * num_nest) + 'px').removeClass('bg-odd').addClass('bg-even');
          } else {
            $(anchor[i]).css(paddingReset).css('padding-left', (32 * num_nest) + 'px').removeClass('bg-odd').addClass('bg-even');
          }
        }
      }
    }
  }
  // Sortable
  if($('.table--sortable').length) {
    $('.table--sortable tbody').sortable({
      items: 'tr:has(td)',
      connectWith: 'tbody',
    });
  }
  $(".list--sortable").sortable();
  $(".list--sortable").disableSelection();
  // Switcher
  $('.js-switcher .btn').click(function(event) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    $('.js-switcher__tgt').hide();
    var tgt = '.' + $(this).attr('id');
    $(tgt).fadeIn('fast');
  });
  // Comment
  $('.communication-comennt textarea').click(function(event) {
    $(this).attr('rows', '4');
    var parentPath = $(this).parent().parent().parent().parent();
    $(parentPath).find('.comment--expand').fadeIn('fast');
  });
  $('.js-btn--comment--post').click(function(event) {
    var parentPath = $(this).parents('.communication-comennt');
    console.log(parentPath);
    $(parentPath).find('textarea').attr('rows', '1');
    $(parentPath).find('textarea').val('');
    $(parentPath).find('.comment--expand').hide();
    $(parentPath).parent().find('.comment--tgt').fadeIn('fast');
  });


  /**
   * サジェストで絞込&選択したものを追加
   * (AB_guideline_pattern_form.html#abui-various-input-13)
   */
  'use strict';
  /**
   * 初期状態 定数
   * @type {String}
   */
  const li = '<li class="nospecify">おまかせ</li>';
  if (!$('#add-list--destination .add-list').find('li').length) {
    $('#add-list--destination .add-list').append(li);
  }

  /**
   * すべて削除 処理
   */
  $(document).on('click', '.js-btn--deladdlistitem', function() {
    $(this).parents('.add-list--wrap').find('.add-list').empty();
    $(this).parents('.add-list--wrap').find('.add-list').append(li);
  });

  /**
   * 追加された項目の削除 処理
   */
  $(document).on('click', '.add-list li .icon', function() {
    $(this).parent().fadeOut('fast');
    if (!$(this).parent('li').siblings().length) {
      $(this).parents('.add-list').append(li);
    }
    $(this).parent().remove();
  });

  /**
   * リスト内の項目の判定 関数
   * @param  {Object} list 追加された項目リスト
   * @param  {String} item 追加する項目
   * @return {Boolean}      [description]
   */
  let check_if_item_in_list = function(list, item){
    let item_found = true;
    //convert to string
    item = String(item);
    list.find('.add-list li').each(function() {
      if ($(this).text() === item) {
        item_found = false;
      }
    });
    return item_found;
  }

  /**
   * サジェストの項目が選択された時 処理
   * @param  {Object} event     typeahead:selected
   * @param  {String} selection 選択した項目
   */
  $('#typeahead--destination').on('typeahead:selected', function(event, selection) {
    let item = $(this).val();
    let list = $('#add-list--destination');

    if (check_if_item_in_list(list, item)) {
      let str = '<li>' + item + '<span class="icon icon-abui-cross"></span></li>';
      list.find('.add-list').append(str);
      $('#add-list--destination li.nospecify').remove();
    }
  });

  // Typeahead Desitination
  $('#typeahead--destination').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    limit: 5,
  },
  {
    name: 'destination',
    source: abui.util.substringMatcher([
      "北海道全域",
      "北海道 - 札幌・定山渓",
      "北海道 - 小樽・積丹・キロロ",
      "北海道 - 千歳・支笏・苫小牧・夕張・岩見沢",
      "北海道 - 函館・大沼・江差・松前",
      "北海道 - 旭川・層雲峡・大雪山",
      "北海道 - 帯広・十勝・然別湖・日高・えりも",
      "北海道 - 釧路・阿寒・摩周・根室",
      "北海道 - 知床・網走・北見・紋別",
      "北海道 - 稚内・利尻・礼文・留萌",
      "北海道 - ニセコ・ルスツ",
      "北海道 - 洞爺・登別・室蘭",
      "北海道 - 富良野・美瑛・トマム",
      "青森県全域",
      "青森県 - 青森・東津軽",
      "青森県 - 奥入瀬・十和田湖・八甲田",
      "青森県 - 八戸・三沢",
      "青森県 - 五所川原・北津軽・西津軽",
      "青森県 - 弘前・黒石・白神山地",
      "青森県 - 下北半島",
      "東京全域",
      "東京 - 東京駅・銀座・お台場",
      "東京 - 新宿・渋谷・赤坂・六本木",
      "東京 - 上野・浅草・葛飾・葛西",
      "東京 - 池袋・飯田橋・巣鴨・練馬",
      "東京 - 品川・羽田・蒲田・自由が丘",
      "東京 - 下北沢・中野・高円寺・吉祥寺",
      "東京 - 立川・調布",
      "東京 - 町田・八王子・高尾",
      "東京 - 奥多摩",
      "東京 - 伊豆七島・小笠原",
      "京都全域",
      "京都 - 京都駅・四条河原町・京都御所",
      "京都 - 清水・祇園・銀閣寺",
      "京都 - 下鴨・上賀茂・大原・鞍馬",
      "京都 - 金閣寺・嵐山・高雄",
      "京都 - 伏見・醍醐",
      "京都 - 宇治・南山城",
      "京都 - 亀岡・京丹波・福知山",
      "京都 - 天橋立・丹後半島"
    ])
  }).on('keydown', this, function (event) {
    /**
     * エンターを押した時
     * @param  {Number} event.keyCode キーコード
     * @return {Boolean}
     */
    if (event.keyCode == 13) {
      let item = $(event.currentTarget).val();
      let list = $('#add-list--destination');

      if (check_if_item_in_list(list, item)) {
        let str = '<li>' + item + '<span class="icon icon-abui-cross"></span></li>';
        list.find('.add-list').append(str);
        $('#add-list--destination li.nospecify').remove();
      }
      return false;
    }
  });

});