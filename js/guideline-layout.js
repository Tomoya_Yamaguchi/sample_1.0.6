//guideline-layout
(function() {
  //options
  var currentLayout,
    sidebar = $('#sidebar-01'),
    navbar = $('#header-site'),
    navbarLg = $('#header-content'),
    wrapper = $('#js-wrapper');
  //レイアウト形成function ズ
  var layoutPatterns = {
    layout01: function() {
      console.log('------- layout01 サイトnavbar＋コンテンツnavbar-lg  ------');
      //サイトnavbar＋コンテンツnavbar-lg
      sidebar[0].className = 'sidebar sidebar-fixed-top';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar';
      wrapper[0].className = 'wrapper';
      navbarLg[0].className = 'navbar navbar-default navbar-lg with-sidebar';
    },
    layout02: function() {
      console.log('------- layout02 navbar固定 .navbar-fixed-top ------');
      //navbar固定 .navbar-fixed-top
      sidebar[0].className = 'sidebar sidebar-fixed-top';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top';
      wrapper[0].className = 'wrapper';
      navbarLg[0].className = 'navbar navbar-default navbar-lg with-sidebar with-navbar-fixed-top';
    },
    layout03: function() {
      console.log('------- layout03 サイトnavbar（固定）＋コンテンツnavbar-lg（固定）------');
      //サイトnavbar（固定）＋コンテンツnavbar-lg（固定）
      sidebar[0].className = 'sidebar sidebar-fixed-top';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top';
      wrapper[0].className = 'wrapper with-navbar-fixed-top-bslg';
      navbarLg[0].className = 'navbar navbar-default navbar-lg with-sidebar with-navbar-fixed-top navbar-fixed-top';
    },
    layout04: function() {
      console.log('------- layout04 サイトnavbar のみ ------');
      //サイトnavbar のみ
      sidebar[0].className = 'sidebar sidebar-fixed-top';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar';
      wrapper[0].className = 'wrapper';
      navbarLg[0].className = 'displaynone';
    },
    layout05: function() {
      console.log('------- layout05 サイトnavbar（固定）------');
      //サイトnavbar（固定）
      sidebar[0].className = 'sidebar sidebar-fixed-top';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top';
      wrapper[0].className = 'wrapper with-navbar-fixed-top';
      navbarLg[0].className = 'displaynone';
    },
    layout06: function() {
      console.log('------- layout06 サイトnavbar（固定）＋コンテンツnavbar-lg ------');
      //サイトnavbar（固定）＋コンテンツnavbar-lg
      sidebar[0].className = 'sidebar sidebar-fixed-top-bs';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top with-sidebar-below';
      wrapper[0].className = 'wrapper';
      navbarLg[0].className = 'navbar navbar-default navbar-lg with-sidebar with-navbar-fixed-top';
    },
    layout07: function() {
      console.log('------- layout07 サイトnavbar（固定）＋コンテンツnavbar-lg（固定） ------');
      //サイトnavbar（固定）＋コンテンツnavbar-lg（固定）
      sidebar[0].className = 'sidebar sidebar-fixed-top-bs';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top with-sidebar-below';
      wrapper[0].className = 'wrapper with-navbar-fixed-top-bslg';
      navbarLg[0].className = 'navbar navbar-default navbar-lg with-sidebar with-navbar-fixed-top navbar-fixed-top';
    },
    layout08: function() {
      console.log('------- layout08 サイトnavbar（固定）------');
      sidebar[0].className = 'sidebar sidebar-fixed-top-bs';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top with-sidebar-below';
      wrapper[0].className = 'wrapper with-navbar-fixed-top';
      navbarLg[0].className = 'displaynone';
    },
    layout09: function() {
      console.log('------- layout09 サイトnavbar（固定）＋コンテンツnavbar-lg ------');
      sidebar[0].className = 'sidebar sidebar-fixed-top-bslg';
      navbar[0].className = 'navbar navbar-inverse navbar-brandlogo with-sidebar navbar-fixed-top with-sidebar-below';
      wrapper[0].className = 'wrapper with-navbar-fixed-top-bslg';
      navbarLg[0].className = 'navbar navbar-default navbar-lg with-sidebar with-sidebar-below with-navbar-fixed-top navbar-fixed-top';
    }
  }
  $(function() {
    //初期レイアウトの指定
    var initLayout = 'layout07';
    $('#js-' + initLayout).addClass('active');
    layoutPatterns[initLayout]();
    $('.layout-pattern').on('click', function() {
      //.active をセット
      $('.layout-pattern').removeClass('active');
      $(this).addClass('active');
      //レイアウトパターンを取得
      currentLayout = this.id.replace('js-', '');
      //該当レイアウトクラスを呼び出す
      layoutPatterns[currentLayout]();
    });
  });
  // リキッドレイアウト
  $('.js--btn--layout-fluid').click(function(event) {
    $('.js--btn--layout-fluid').toggle();
    $('#layout-container').toggleClass('container');
    $('#layout-container').toggleClass('container-fluid');
  });
})();
