/*
for Guideline Document Only
----------------------------------------------------------------
*/


$(function() {
  /**************************************************
    ドロップダウン
  **************************************************/
  // Custom Selects : abui-guideline用
  // if($($('select').attr('name')).length){
    $("select[name='huge']").selectpicker({style: 'btn-hg btn-primary', menuStyle: 'dropdown-inverse'});
    $("select[name='herolist']").selectpicker({style: 'btn-primary', menuStyle: 'dropdown-inverse'});
    $("select[name='info']").selectpicker({style: 'btn-info'});
    $("select[name='input']").selectpicker({style: 'btn-input'});
  // }
});

/**************************************************
  ツールチップ
**************************************************/
$(function() {
  // components.html の　Tooltips
  if($('[data-toggle="tooltip"]').length){
    //実行する内容
    $('.example[data-toggle="tooltip"]').tooltip({html: true});
    $('.example[data-toggle="tooltip"]').tooltip("show");
  }
});

/**************************************************
  tagsinput
**************************************************/
$(function() {
  if($('.tagsinput').length){
    $('.tagsinput').tagsInput();
  }
});

/**************************************************
  Context Menu
**************************************************/
$(function() {
  if ($('testButton').length) {
    var menu = [{
      name: 'Button',
      title: 'Button',
      fun: function () {
          alert('i am button')
      }
    }, {
      name: 'Link1',
      title: 'Link1',
      fun: function () {
          alert('i am link')
      }
    }, {
      name: 'Link2',
      title: 'Link2',
      fun: function () {
          window.open('#', '_blank');
      }
    }];
    //Calling context menu
    $('.testButton').contextMenu(menu,{
      mouseClick: 'right'
    });
  }
});


/**************************************************
 datepicker
**************************************************/
$(function() {
  if($('[class^=exampleDatepicker]').length || $('[id^=exampleDatepicker]').length) {
    $('#exampleDatepicker0, #exampleDatepicker5, #exampleDatepicker6, #exampleDatepicker7').datepicker();
    $('#exampleDatepicker1').datepicker({
      changeMonth: true,
      changeYear: true
    });
    $('#exampleDatepicker2').datepicker({
      dateFormat: 'yy/mm/dd（D）'
    });
    $('#exampleDatepicker3').datepicker({
      showOtherMonths: true,
      selectOtherMonths: true,
    });
    $('#exampleDatepicker4').datepicker({
      showOn: 'button'
    });
    $('.exampleDatepicker').datepicker();
    $('#exampleDatepicker8').datepicker({
      numberOfMonths: 3
    });
    $('#exampleDatepicker-from').datepicker({
      // オプション指定
      onSelect: function (selectDate) {   // DatePickerが選択された際に実行
        var option = 'minDate';
        $('#exampleDatepicker-to').datepicker('option', option, selectDate);
      }
    });
    $('#exampleDatepicker-to').datepicker({
      // オプション指定
      onSelect: function (selectDate) {   // DatePickerが選択された際に実行
        var option = 'maxDate';
        $('#exampleDatepicker-from').datepicker('option', option, selectDate);
      }
    });
  }
});


/**************************************************
 slimScroll
**************************************************/
$(function() {
  if ($('#innerScroll').length){
    var before_px = 0;
    $('#innerScroll').slimScroll({
      height: '360px',
      color: '#ccc'
    }).bind('slimscroll', function(e, pos) {
      if(pos == "bottom"){
        $(".fadeout").fadeOut();
      }
    }).bind('slimscrolling', function(e, pos) {
      // console.log(pos+":::"+before_px);
      if($(".fadeout").css("display") == "none" && before_px > pos){
        $(".fadeout").css("display","block");
      }
      before_px = pos;
    });
  }
});

/**************************************************
 photoswipte
**************************************************/
$(function() {
  if ($('.my-gallery').length) {
    initPhotoSwipeFromDOM( '.my-gallery' ) ;
  }

  //photoswipeのイメージクリック時の処理
  $('.my-gallery').on('click', 'a', function(){
    $('<div class="modal-backdrop op8"></div>').appendTo(document.body).hide().fadeIn();
    $('body').addClass('modal-open');
  });

});

$.fn.addSliderSegments = function (amount, orientation) {
  return this.each(function () {
    if (orientation == "vertical") {
      var output = ''
        , i;
      for (i = 1; i <= amount - 2; i++) {
        output += '<div class="ui-slider-segment" style="top:' + 100 / (amount - 1) * i + '%;"></div>';
      };
      $(this).prepend(output);
    } else {
      var segmentGap = 100 / (amount - 1) + "%"
        , segment = '<div class="ui-slider-segment" style="margin-left: ' + segmentGap + ';"></div>';
      $(this).prepend(segment.repeat(amount - 2));
    }
  });
};

/****************************************************************************
 検索&一覧 #s02-02-03 パターン
  チェックボックスの処理
****************************************************************************/
// 行クリックでcheckbox checked
$('.table-checkablerow').addClass('js-is-checked');
$('.table-checkablerow.js-is-checked').on('change', function(e) {
  var targetElm = $(e.target);
  checkableRow(targetElm);
});
$('.table-checkablerow.js-is-checked tbody td:not(.js-check-exclude)').on('click', function(e) {
  var targetElm = $(this).closest('tr').find('input');
  if($(targetElm).prop('checked')) {
    $(targetElm).prop('checked', false).parent().removeClass('checked');
  } else {
    $(targetElm).prop('checked', true).parent().addClass('checked');
  };
  checkableRow(targetElm);
});

//  対象が1行または複数行でアクションが複数の場合
$('#table-s02-02-04').addClass('js-is-checked');
$('#table-s02-02-04').on('change', function(e) {
  var targetElm = $(e.target);
  checkableRow(targetElm);
});
//  各行のボタンでアクションを明示
$('#table-s02-02-04--b').addClass('js-is-checked');
$('#table-s02-02-04--b').on('change', function(e) {
  var targetElm = $(e.target);
  checkableRow(targetElm);
});
//  さらにアクションが混在する例
$('#table-s02-02-04--c').addClass('js-is-checked');
$('#table-s02-02-04--c').on('change', function(e) {
  var targetElm = $(e.target);
  checkableRow(targetElm);
});

$('#table-s02-02-04--d').addClass('js-is-checked');
$('#table-s02-02-04--d').on('change', function(e) {
  var targetElm = $(e.target);
  checkableRow(targetElm);
});

function checkableRow(targetElm) {
  "use strict";
  event.stopPropagation();

  var btn = {
    del:  targetElm.parents('.js-is-checked').next('.btn-row').find('.js--btn-del--sample'),
    copy: targetElm.parents('.js-is-checked').next('.btn-row').find('.js--btn-copy--sample'),
    edit: targetElm.parents('.js-is-checked').next('.btn-row').find('.js--btn-edit--sample')
  }

  //全選択時
  if(targetElm.closest('th').length) {
    if(targetElm.prop('checked')) { //チェックつける時
      targetElm.closest('thead').next().find('label.checkbox').addClass('checked').find('input').prop('checked', true);
      // 削除、編集ボタンdisabled解除、複製ボタンdisabled
      btn.del.prop('disabled', false);
      btn.copy.prop('disabled', true);
      btn.edit.prop('disabled', false);
    } else {
      targetElm.closest('thead').next().find('label.checkbox').removeClass('checked').find('input').prop('checked', false);
      // 全てのボタンdisabled
      $.each(btn, function(){
        $(this).prop('disabled', true);
      });
    }
  } else {
    var checkablerow_closest = targetElm.closest('.js-is-checked');
    // td内チェックボックスの数
    var checkbox_num = checkablerow_closest.find('td .checkbox').length;
    // チェックされてる数
    var checked_num = checkablerow_closest.find('td .checkbox.checked').length;
    // 全ての行にチェックを入れた場合、全選択にもチェックを入れる処理
    var checkablerow_parents = targetElm.parents('.js-is-checked');
    if(targetElm.prop('checked')) { //チェックつける時
      if(checkbox_num == checked_num) {
        checkablerow_parents.find('label.checkbox').addClass('checked').find('input').prop('checked', true);
      }
    } else {
      checkablerow_parents.find('th label.checkbox').removeClass('checked').find('input').prop('checked', false);
    }
    if(checked_num == 0) {
      // 全てのボタンを非表示にする
      $.each(btn, function(){
        $(this).prop('disabled', true);
      });
    } else if(checked_num >= 1) {
      btn.copy.prop('disabled', true);
      btn.edit.prop('disabled', false);
      if(checked_num == 1) {
        btn.copy.prop('disabled', false);
        btn.edit.prop('disabled', false);
      }
      btn.del.prop('disabled', false);
    }
  }
}


/****************************************************************************
 Bootstrap Pretty Print
****************************************************************************/
//起動
window.prettyPrint && prettyPrint();


// Some general UI pack related JS
// Extend JS String with repeat method
String.prototype.repeat = function(num) {
  return new Array(num + 1).join(this);
};



/**************************************************
  現在スクロール位置に合わせてサイドバーをactiveにする処理
  **************************************************/
$(function() {
  /**
   * ナビゲーションのリンクを指定
   * @type {Object}
   */
  let navLink = $('#sidebar-01').find('li > a');

  /**
   * 各コンテンツのページ上部からの開始位置と終了位置を配列に格納しておく
   * @type {Array}
   */
  let contentsArr = new Array();

  /**
   * contentsArr配列用
   * @type {Number}
   */
  let num = 0;

  for (let i = 0; i < navLink.length; i++) {
    /**
     * コンテンツのIDを取得
     * @type {String}
     */
    let targetContents = navLink.eq(i).attr('href');

    /**
     * #ハッシュタグを取得
     * @type {Object}
     */
    let target = targetContents.split('#');

    /**
     * URL取得
     * @type {String}
     */
    let location = window.location.href;

    /**
     * layoutによるoffsetを計算する
     * @type {Number}
     * @param currentcheck 現在位置
     */
    let offset = calculateOffset('currentcheck');

    /**
     * ページ内リンクでないナビゲーション(-1)が含まれている場合は除外する
     */
    if(location.indexOf(target[target.length - 2]) != -1) {
      /**
       * ハッシュタグ取得
       * @type {String}
       */
      let id = '#' + target[target.length - 1];

      /**
       * コンテンツの開始位置
       * @type {Number}
       */
      let targetContentsTop = 0;

      /**
       * コンテンツの終了位置
       * @type {Number}
       */
      let targetContentsBottom = 0;

      if ($(id).length) {
        /**
         * ページ上部からコンテンツの開始位置までの距離を取得
         * @type {Number}
         */
        targetContentsTop = $(id).offset().top;

        /**
         * ページ上部からコンテンツの終了位置までの距離を取得
         * @type {Number}
         */
        targetContentsBottom = targetContentsTop + $(id).outerHeight(true) - 1;

        /**
         * @fileoverview ABUIドキュメントでのみ使用する。プロジェクトでは削除
         */
        if (location.indexOf('AB_guideline_document_layout.html') != -1){
          offset = 336;
        }
      }

      /**
       * 配列に格納
       */
      contentsArr[num] = [targetContentsTop, targetContentsBottom, i, offset];
      num++;
    }
  };


  /**
   * 現在地をチェックする
   * @type function
   * @return {undefined}
   */
  function currentCheck() {
    'use strict';
    /**
     * 現在のスクロール位置を取得
     * @type {Number}
     */
    let windowScrolltop = $(window).scrollTop();

    for (let i = 0; i < contentsArr.length; i++) {
      /**
       * 現在のスクロール位置が、配列に格納した開始位置と終了位置の間にあるものを調べる
       */
      if(contentsArr[i][0] - contentsArr[i][3] <= windowScrolltop && contentsArr[i][1] - contentsArr[i][3] >= windowScrolltop) {
        /**
         * 開始位置と終了位置の間にある場合、ナビゲーションにclass="active"をつける処理
         */
        navLink.parent().removeClass('active');
        navLink.parents('.expander-target').hide().parents('li').children('.expander').removeClass('open');
        navLink.eq(contentsArr[i][2]).parent().addClass('active');
        navLink.eq(contentsArr[i][2]).parents('.expander-target').show().parents('li').children('.expander').addClass('open');
        // i == contentsArr.length;
      }
    };
  }


  /**
   * #ハッシュタグの位置にscroll位置を移動 / navbarにて現在地をactiveにするための、scroll位置補正関数
   * @param  {String} CalledBy [description]
   * @return {Number}          scroll位置
   */
  function calculateOffset(CalledBy) {
    'use strict';
    /**
     * スクロール位置
     * @type {Number}
     */
    let offset = 0;

    /**
     * -48はnavbar一段表示時のnavbar高さ
     */
    if(!$('.navbar-inverse').hasClass('navbar-fixed-top')) {
      offset = -48;
    }
    /**
     * navbar-fixed時の2段目の高さ（マージン含む）
     */
    if($('.navbar-default').hasClass('navbar-fixed-top')) {
      offset = $('#header-content').outerHeight(true);
      // offset = 56;
    }
    if($('.wrapper').hasClass('with-navbar-fixed-top')) {
      offset = 0;
    }

    /**
     * navbarの現在地active処理からcallされた場合マージンを追加する
     */
    if (CalledBy == 'currentcheck') {
      offset = offset + 64;
    }
    return offset;
  }

  /**
   * ハッシュタグの位置にscroll位置を移動する処理
   * @param  {String}    url 画面のURL
   * @return {undefined}
   */
  function JumpPage(url) {
    'use strict';
    if(url.indexOf("#") != -1){
      /**
       * id取得
       * @type {Object}
       */
      let id     = url.split("#");

      /**
       * オフセットの計算
       * @type {Number}
       */
      let offset = calculateOffset('jumppage');

      if ($('#' + id[1]).length) {
        /**
         * 画面のスクロール位置計算
         * @type {Number}
         */
        let pos = $('#' + id[1]).offset().top - offset;
        $('html, body').animate({scrollTop:pos}, 500);
      }
    }
  }


  /**
   * ページ読み込み時とスクロール時に、現在地をチェックする
   */
  $(window).on('load scroll', function() {
    currentCheck();
  });

  /**
   * Jumping scroll-pos from one place to another on page-load
   */
  $(window).on('load', function() {
    setTimeout(function(){
      JumpPage(window.location.href);
    }, 500);
  });

  /**
   * Jumping scroll-pos from one place to another by clicking links on sidebar.
   */
  $('.sidebar').on('click', 'a', function() {
    JumpPage($(this).attr('href'));
  });
});
