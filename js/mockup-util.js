//全画面共通で利用する処理を定義
var _ua;
var orientation;

var abui = {
  init: function() {
    //sidebarの描画
    $('#sidebar-01').prepend(abui.template.sidebar.default);
    //ユーザーエージェント取得
    _ua = abui.util.getUA();
    //画面orientaiton取得
    orientation = abui.util.getOrientation(window.outerWidth, window.outerHeight);
    //現在参照中の画面をactiveにする
    // abui.util.sidebar_ActivatePageShowingSidebar();


    /****************************************************************************
      特定のテキストフィールドへのアイコン自動付与
    ****************************************************************************/
    function addIcon(type) {
      var type = '.' + type;
      $('input.form-control' + type).wrap('<div class="input-with-icon" />');
      var icn = '';
      if ($(type).parent('.input-with-icon').length) {
        if (type == '.input-timepicker') {
          icn = 'time';
          $(type).parent('.input-with-icon').append('<span class="input-icon icon-abui-' + icn + '"></span>');
        } else if (type == '.input-datepicker'){
          icn = 'calendar-solid'
          $(type).parent('.input-with-icon').append('<span class="input-icon icon-abui-' + icn + '"></span>');
        } else if (type == '.input-typeahead'){
          icn = 'pencil'
          $(type).parent('.input-with-icon').append('<span class="input-icon-typeahead icon-abui-' + icn + '"></span>');
        };
      };
    }
    if ($('.input-timepicker').length) {
      addIcon('input-timepicker');
    };
    if ($('.input-datepicker').length) {
      addIcon('input-datepicker');
    };
    if ($('.input-typeahead').length) {
      addIcon('input-typeahead');
    };


    /****************************************************************************
      検索用テキストボックスクリック時へのfocusクラス追加削除制御(UI調整用)
    ****************************************************************************/
    $('.input-group').on('focus', '.form-control', function () {
      $(this).closest('.input-group, .form-group').addClass('focus');
    }).on('blur', '.form-control', function () {
      $(this).closest('.input-group, .form-group').removeClass('focus');
    });

    /****************************************************************************
      .input-group-btnを後ろに持つ.form-controlのpaddingを制御するためのclass付与
    ****************************************************************************/
    $('.input-group .input-group-btn').prev('.form-control').addClass('has-btn-after');

    /****************************************************************************
      パジネーション : fakelink制御
    ****************************************************************************/
    // Disable link clicks to prevent page scrolling
    $(document).on('click', 'a[href="#fakelink"]', function (e) {
      e.preventDefault();
    });

    /****************************************************************************
     ボタングループへのactiveクラス付与制御
    ****************************************************************************/
    $(".btn-group").on('click', "a", function() {
      $(this).siblings().removeClass("active").end().addClass("active");
    });


    // コントロール内テキスト無しチェックボックスの横幅 自動付与
    $('.table').each(function() {
      $(this).find('.checkbox.no-label input[type="checkbox"]').closest('th').css('width','28px');
    });

    //cell-control及び、inputのサイズ調整クラスが自動でつくように
    $(window).on('load', function(){
      $('.table td, .table th').each(function(){
        if (!$(this).hasClass('cell-control') && $(this).find('.dropdown-toggle, input[type="text"], .btn').length) {
          $(this).addClass('cell-control');
        }

        if ($(this).find('input[type="text"]').length && !$(this).find('input[type="text"]').is('.input-sm, .input-lg, .input-hg')) {
          $(this).find('input[type="text"]').addClass('input-sm');
        }

        if ($(this).find('.dropdown-toggle, input[type="text"], .btn').length && !$(this).find('.dropdown-toggle select').is('.select-xs, .select-sm, .select-lg, .select-hg')) {
          $(this).find('select').addClass('select-sm');
        }

        if ($(this).find('.btn').length && !$(this).find('.btn').is('.btn-xs, .btn-sm, .btn-lg, .btn-hg')) {
          $(this).find('.btn').addClass('btn-sm');
        }
      });
    });

    // サジェスト付きフィールド
    // フォーカス時ペンシルアイコンをprimaryカラーにする
    if ($('.form-group-typeahead').length) {
      $(document).on('focus', 'input.input-typeahead', function(){
          $(this).closest('.input-with-icon').children('.input-icon-typeahead.icon-abui-pencil').css('color','#029df4');
      }).on('blur', 'input.input-typeahead', function(){
          $(this).closest('.input-with-icon').children('.input-icon-typeahead.icon-abui-pencil').css('color','');
      });
    }

    //入力業追加コンポーネントがあった場合に、delrowhoverでrowをdangerでハイライト
    if ($('.table .delrow').length) {
      $(document).on('mouseover', '.table .delrow', function(e){
        e.stopPropagation();
        var rowCell_maxNum = null;
        var this_td = $(this).parent('td')[0];
        var items_reverse = $(this).parents('tr').find('td').get().reverse();
        var index_hoveredCell_reverse = null;

        var this_tr = $(this).parents('tr')[0];
        var index_hoveredRow = null;
        var table_tr = $(this).parents('tbody').find('tr');
        $.each(table_tr, function(i) {
          if ($(this_tr).is(this)) {
            index_hoveredRow = i;
          }

          rowCell_maxNum = rowCell_maxNum < $(this).find('td').length - 1 ? $(this).find('td').length - 1 || rowCell_maxNum == null : rowCell_maxNum;
        });

        var index_hoveredCell = null;
        $.each($(this).parents('tr').find('td').get(), function(i){
          if ($(this_td).is(this)) {
            index_hoveredCell = i;
          }
        });

        index_hoveredCell_reverse = rowCell_maxNum != null ? rowCell_maxNum - index_hoveredCell : null;

        switch(index_hoveredCell_reverse) {
          case null:
            //do nothing
            break;

          case 0:
            var rowspan = $(this).parent('td').attr('rowspan') ? $(this).parent('td').attr('rowspan') : 1;
            for (var i = 0; i < rowspan; i++) {
              $($(this).parents('tbody').find('tr')[i + index_hoveredRow]).addClass('tgt--delrow');
            }
            break;

          //1以上
          default:
            var rowspan = $(this).parent('td').attr('rowspan') ? $(this).parent('td').attr('rowspan') : 1;
            for (var i = 0; i < rowspan; i++) {
              for (var j = index_hoveredCell_reverse; j <= index_hoveredCell; j++) {
                $($($(this).parents('tbody').find('tr')[i + index_hoveredRow]).find('td')[j]).addClass('tgt--delrow');
              }
            }
            break;
        }
      }).on('mouseout', function(e){
        e.stopPropagation();
        $('.tgt--delrow').removeClass('tgt--delrow');
      });
    }

    /****************************************************************************
     パジネーション
    ****************************************************************************/
    // Make pagination demo work
    $(".pagination").on('click', "a", function(e) {
      if (!$(this).parents('li').hasClass('disabled')){
        $(this).parent().siblings("li").removeClass("active").end().addClass("active");
        e.preventDefault();
      }
    });

    /****************************************************************************
     検索条件の開閉
    ***************************************************************************/
    $(function(){
      $('.filter-wrap').each(function(){
        var height = 0;
        var $filter_wrap = $(this);
        var $filter = $filter_wrap.find('.filter');
        var $section_submit = $filter_wrap.find('.section-submit');
        var $btn_filter_again = $filter_wrap.find('.btn-filter-again');
        var $filter_result = $filter_wrap.find('.filter-result');
        var $section_filter = $filter_wrap.find('div.section-filter');

        if ($(this).parents('.modal').length) {
          $(this).parents('.modal').on('shown.bs.modal', function(){
            height = $section_filter.outerHeight(true);
          });
        }

        $filter.css('height', 'auto');
        $(window).on('load resize', function(){
          height = $section_filter.outerHeight(true);
        });
        $('.js-btn-filter').on('click', function(){
          $filter.css('overflow', 'hidden');
          $section_submit.hide();
          $filter.animate({
            height: "0px",
            }, 200, function () {
                $section_filter.css('visibility', 'hidden');
              }
            );
           $btn_filter_again.fadeIn();
          $filter_result.show();
        });
        $btn_filter_again.on('click', function(){
           $btn_filter_again.hide();
          $section_filter.hide();
          $filter.animate({
            height: height
          }, 200, function () {
            $section_filter.css('visibility', 'visible');
            $section_filter.fadeIn();
            $filter.css('height', 'auto');
            $filter.css('overflow', 'visible');
          });
          $section_submit.fadeIn();
        });
      });
    });

    /****************************************************************************
     DropZoneのイニシャライズ
    ***************************************************************************/
    if ($('.dropzone').length) {
      Dropzone.options.fileupload = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 5, // MB
        addRemoveLinks: 'true',
        dictDefaultMessage: '<span class="icon icon-abui-file"></span>ファイルをここにドラッグアンドドロップ<small>またはクリックで場所を指定</small>',
        dictFallbackMessage: "ブラウザーがドラッグアンドドロップでのアップロードに対応していません",
        dictFileTooBig: "サイズが大きすぎます（{{filesize}}MB）。上限は{{maxFilesize}}MBです",
        dictInvalidFileType: "この形式のファイルはアップロードできません",
        dictResponseError: "エラーが発生しました。コード： {{statusCode}}",
        dictCancelUpload: "アップロードをキャンセル",
        dictCancelUploadConfirmation: "アップロードをキャンセルしてよろしいですか",
        dictRemoveFile: "",
        dictMaxFilesExceeded: "アップロード可能なファイル数の上限に達しました。"
      };
    }

    /****************************************************************************
     フットブロック時パディング自動付与制御
    ****************************************************************************/
    if($('.navbar-footblock').length) {
      $('.wrapper').addClass('with-navbar-fixed-bottom'); // padding-bottom 48px
    }

    /****************************************************************************
     iPadのフルスクリーンモード実行時にaタグクリックでsafariの呼び出しを禁止する
    ***************************************************************************/
    $(document).on('click', 'a', function(e) {
      var $a = $(e.target);
      if (!typeof($a.attr('href') == 'undefined') && !$a.attr('href').match(/^#/)) {
        e.preventDefault();
        window.location = $a.attr('href');
      }
    });

  },
  util: {
    //ユーザーエージェントを返却する
    //return: boolean
    getUA: function() {
        var u = window.navigator.userAgent.toLowerCase();
        return {
          Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
            || u.indexOf("ipad") != -1
            || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
            || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
            || u.indexOf("kindle") != -1
            || u.indexOf("silk") != -1
            || u.indexOf("playbook") != -1,
          Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
            || u.indexOf("iphone") != -1
            || u.indexOf("ipod") != -1
            || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
            || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
            || u.indexOf("blackberry") != -1
        }
    },
    //対象要素の幅、高さをベースにデバイス向きを判定する
    //第一引数：width
    //第二引数：height
    //return： 'portrait', 'landscape', 'square'
    getOrientation: function(w, h) {
      var res = '';
      if (h > w) {
        res = 'portrait';
      } else if (h < w) {
        res = 'landscape';
      } else {
        res = 'square';
      }
      return res;
    },
    //GETパラメーター取得
    GetQueryString: function(){
      var result = {};
      if( 1 < window.location.search.length ) {
        // 最初の1文字 (?記号) を除いた文字列を取得する
        var query = window.location.search.substring( 1 );

        // クエリの区切り記号 (&) で文字列を配列に分割する
        var parameters = query.split( '&' );

        for( var i = 0; i < parameters.length; i++ ) {
          var element = parameters[ i ].split( '=' );

          var paramName = decodeURIComponent( element[ 0 ] );
          var paramValue = decodeURIComponent( element[ 1 ] );

          result[ paramName ] = paramValue;
        }
      }
      return result;
    },
    //ファイル名返却
    GetFileName: function() {
      return window.location.pathname.split('/')[window.location.pathname.split('/').length - 1];
    },
    //slimScrollの高さを自動計算して返却する int
    getSlimScroll_h: function(el) {
      var headerContent_h = $('#header-content').css('display') != 'none' ? $('#header-content').height() : 0;
      var headerSite_h = $('#header-site').css('display') != 'none' ? $('#header-site').height() : 0;
      var $this_section = $(el.parents('.section')[0]);
      var other_section_h = 0;

      var this_section_pt = parseInt($this_section.css('padding-top'));
      var this_section_pb = parseInt($this_section.css('padding-bottom'));

      $.each($('.wrapper .section'), function(){
        if (!($this_section.is(this))) {
          other_section_h += $(this) .outerHeight(true);
        }
      });

      return $(window).height() - headerContent_h - headerSite_h - other_section_h - this_section_pt - this_section_pb;
    },
    //うるう年かどうかを返却する boolean
    isLeapYear: function(y) {
      return !(y % 4) && (y % 100) || !(y % 400) ? true : false;
    },
    lhref: function(url){
      var query = abui.util.GetQueryString();
      location.href = url + '?user=' + query.user;
    },
    //twitter typeaheadのdataに指定する配列を引数に、tokenとvalueのマッチングを行う
    substringMatcher: function(strs) {
      return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    }
  },
  template: {
    sidebar: {
      default: '<ul class="nav">\
        <li><a href="sample_01.html">社員検索</a></li>\
      </ul>',
    }
  }
}

abui.init();

//共通のリサイズイベントを定義
$(window).on('resize', function(){
  abui.util.orientation = abui.util.getOrientation(this.outerWidth, this.outerHeight);
});


//プラグイン的に利用
//背景画像をベースに、デバイス向きを判定する
(function (factory) {
  if (typeof module === "object" && typeof module.exports === "object") {
    module.exports = factory(require("jquery"), window, document);
  }
  else {
    factory(jQuery, window, document);
  }
}(function ($, window, document, undefined) {
  // CSSの各幅表記に対する正規表現
  var urlRegex = /url\(['"]*(.*?)['"]*\)/g;

  /**
   * コンストラクタ
   * @param element
   * @param optionsArg
   * @constructor
   */
  var GetBackgroundSize = function(element, callback){
    this.$element = $(element);
    this.backgroundSize = this.$element.css('background-size').split(' ');
    this.img = new Image();
    this.callback = callback.bind(element);
  };

  /**
   * サイズ最適化
   * @param origin
   * @param current
   * @param target
   * @returns {number}
   */
  GetBackgroundSize.prototype.size_optimization = function(origin, current, target){
    return target * (current/origin);
  };

  /**
   * 要素幅取得
   * @returns {boolean}
   */
  GetBackgroundSize.prototype.getBackgroundWidth = function() {
    var pxRegex = /px/, percentRegex = /%/;

    if (pxRegex.test(this.backgroundSize[0])) {
      this.width = parseInt(this.backgroundSize[0])
    }
    if (percentRegex.test(this.backgroundSize[0])) {
      this.width = this.$element.width() * (parseInt(this.backgroundSize[0]) / 100);
    }
    return (typeof this.width != 'undefined');
  };

  /**
   * 要素高さ取得
   * @returns {boolean}
   */
  GetBackgroundSize.prototype.getBackgroundHeight = function() {
    var pxRegex = /px/, percentRegex = /%/;

    // 背景高さ取得
    if (pxRegex.test(this.backgroundSize[1])) {
      this.height = parseInt(this.backgroundSize[1]);
    }
    if (percentRegex.test(this.backgroundSize[1])) {
      this.height = this.$element.height() * (parseInt(this.backgroundSize[1]) / 100);
    }
    return (typeof this.height != 'undefined');
  };

  /**
   * 生画像の情報を入手
   */
  GetBackgroundSize.prototype.getNaturalImageProperties = function(){
    var _this = this;
    this.img.onload = function () {

      if (typeof _this.width == 'undefined') {
        if (typeof _this.height != 'undefined') {
          _this.width = _this.size_optimization(this.naturalHeight, _this.height, this.naturalWidth);
        }else{
          _this.width = _this.$element.width();
        }
      }

      if (typeof _this.height == 'undefined') {
        if (typeof _this.width != 'undefined') {
          _this.height = _this.size_optimization(this.naturalWidth, _this.width, this.naturalHeight);
        }else {
          _this.height = _this.$element.height();
        }
      }

      if(_this.backgroundSize[0] == "cover") {
        /* 縦横比比較 */
        if(_this.$element.width()/_this.$element.height() > _this.width/_this.height){
          _this.width = _this.$element.width();
          _this.height = _this.size_optimization(this.naturalWidth, _this.width, this.naturalHeight);
        }else{
          _this.height = _this.$element.height();
          _this.width = _this.size_optimization(this.naturalHeight, _this.height, this.naturalWidth);
        }
      }
      _this.callback({width: _this.width, height: _this.height});
    };

    /* Call img.onload to refer natural size. */
    this.img.src = this.$element.css('background-image').replace(urlRegex, '$1');
  };

  /**
   * 初期化処理
   * @returns {{width: *, height: *}}
   */
  GetBackgroundSize.prototype.init = function(){

    // 高さおよび幅が両方与えられていたらそのままReturn
    var rst = [this.getBackgroundHeight(), this.getBackgroundWidth()];

    if(rst[0] && rst[1]) {
      this.callback({width: this.width, height: this.height});
      return;
    }
    return this.getNaturalImageProperties();
  };

  $.fn.getBackgroundSize = function (callback){
    return this.each(function(){
      new GetBackgroundSize(this, callback).init();
    });
  }
}));

//cssアニメーションの実行順制御
(function($){
  $.fn.animateCallback = function(callback){
    var alias = "animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd"
      return this.each(function() {
        $(this).bind(alias, function(){
        $(this).unbind(alias);
        return callback.call(this)
      });
    });
  };
})(jQuery);

$(function() {
  // CSS Cache Busting
  $('link[rel="stylesheet"]').each(function(){
    // ファイルの末尾にクエリー文字列を追加
    var txt          = $(this).attr('href');
    var queryString  = txt.replace(/(\.css)/g, '$1?');
    var cacheBusting = queryString + timer();
    $(this).attr('href', cacheBusting);
  });
  // JS Cache Busting
  $('script[src]').each(function(){
    // ファイルの末尾にクエリー文字列を追加
    var txt          = $(this).attr('src');
    var queryString  = txt.replace(/(\.js)/g, '$1?');
    var cacheBusting = queryString + timer();
    $(this).attr('src', cacheBusting);
  });

  function timer () {
    'use strict';
    //現在時刻を取得し文字列に変換
    var now  = new Date();
    var y    = now.getFullYear().toString();
    var m    = (now.getMonth() + 1).toString();
    var d    = now.getDate().toString();
    var h    = now.getHours().toString();
    var mi   = now.getMinutes().toString();
    var time = y + m + d + h + mi;
    return time;
  };
});


$(function() {
  // 桁数付きテキストフィールド
  if($('.textfield-digit').length){
    $('.textfield-digit').each(function(){
      var input = $(this).find('.form-control');
      var length = parseInt(input.attr('maxlength'));
      var str = '';
      for (var i = 0; ++i <= length;) {
        str = str + '&ndash;';
      }
      var size = '';
      var paddingW = parseInt(input.css('padding-left'));
      var fWidth = parseInt(input.css('font-size'))*0.73;

      /* 文字幅を取得する方法　希望の値が得られず　要再考 */
      // var fSize = parseInt(input.css('font-size'));
      // input.before('<span id="elm">0</span>');
      // $('#elm').css({
      //   'position': 'absolute',
      //   'z-index': '-1',
      //   'opacity': '0',
      //   'font-family': '"Courier New", monospace',
      //   'font-size': fSize
      // });
      // var fWidth = $('#elm').get(0).offsetWidth;
      // $('#elm').remove();

      // console.log(fWidth);
      if (input.hasClass('input-sm')) {
        size = 'input-sm';
      } else if (input.hasClass('input-lg')) {
        size = 'input-lg';
      } else if (input.hasClass('input-hg')) {
        size = 'input-hg';
      } else {
        size = '';
      };
      var w = fWidth*length + fWidth*(length - 1)/2 + paddingW*2;
      input.css('width', w + 'px');
      var underline = '<input type="text" class="' + size + ' form-control digit-underline" style="padding-left:' + paddingW + 'px; width: ' + w + 'px;" placeholder="" value="' + str + '" disabled>';
      input.before(underline);
    });
  }
});

/**************************************************
 ファイルアップロード
**************************************************/
$(function(){
  if ($('.dropzone').length) {
    Dropzone.options.fileupload = {
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 5, // MB
      addRemoveLinks: 'true',
      dictDefaultMessage: '<span class="icon icon-abui-file"></span>ファイルをここにドラッグアンドドロップ<small>またはクリックで場所を指定</small>',
      dictFallbackMessage: "ブラウザーがドラッグアンドドロップでのアップロードに対応していません",
      dictFileTooBig: "サイズが大きすぎます（{{filesize}}MB）。上限は{{maxFilesize}}MBです",
      dictInvalidFileType: "この形式のファイルはアップロードできません",
      dictResponseError: "エラーが発生しました。コード： {{statusCode}}",
      dictCancelUpload: "アップロードをキャンセル",
      dictCancelUploadConfirmation: "アップロードをキャンセルしてよろしいですか",
      dictRemoveFile: "",
      dictMaxFilesExceeded: "アップロード可能なファイル数の上限に達しました。"
    };
  }
});

$('.input-timepicker').on('click', function(e){
  if ($(this).hasClass('hasWickedpicker')) {
    // setTimeout(function(){
      $('.input-timepicker').attr('aria-showingpicker', false)
      $(this).attr('aria-showingpicker', true)
    // }.bind(this));
  }
});
