### Summary ###
アストロラボでつくるブラウザベースのシステム製品のユーザーインターフェースの標準化および開発の効率化のために用意されたデザインライブラリです。

### Version ###
1.0.5

### Documents URL ###
http://astrolab.co.jp/dev/abui/AB_guideline_document_objective.html

### Dev env ###
Please install Docker
https://store.docker.com/editions/community/docker-ce-desktop-mac

```bash
docker-compose build
docker-compose up
```
