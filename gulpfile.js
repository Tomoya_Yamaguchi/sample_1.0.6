const gulp       = require('gulp');
const sass       = require('gulp-sass');
const compass    = require('gulp-compass');
const uglify     = require('gulp-uglify-es').default;
const concat     = require("gulp-concat");
const plumber    = require("gulp-plumber");
const rename     = require('gulp-rename');
const webserver  = require('gulp-webserver');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('webserver', () => {
  gulp.src('.')
    .pipe(webserver({
      directoryListing: true,
      open: true,
      port: 8080,
      host: "0.0.0.0",
    }));
});

gulp.task('sass', () => {
  return gulp.src("scss/**/*.scss")
  .pipe(plumber({
    errorHandler: function (error) {
      this.emit('end');
  }}))
  .pipe(compass({
      config_file: 'config.rb',
      comments: false,
      css: 'css/',
      sass: 'scss/'
  }))
  .pipe(gulp.dest('css/'));
});

gulp.task('js.concat', () => {
  return gulp.src(
    [
      'js/src/jquery-2.2.4.min.js',
      'js/src/jquery-ui-1.12.1.min.js',
      'js/src/bootstrap-3.3.7.min.js',
      'js/src/plugins/*.js',
    ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
      .pipe(concat('abui.js'))
      .pipe(sourcemaps.write())
    .pipe(gulp.dest('js'));
});

gulp.task('watch', () => {
  gulp.watch('scss/**/*.scss', gulp.task('sass'));
  gulp.watch(['js/src/*.js', 'js/src/plugins/*.js'], gulp.task('js.concat'));
});

gulp.task('default', gulp.series( gulp.parallel('sass', 'js.concat', 'webserver', 'watch'), () => {
}));


gulp.task('minify', () => {
  return gulp.src("js/abui.js")
    .pipe(rename("abui.min.js"))
    .pipe(uglify(/* options */))
    .pipe(gulp.dest("js/"));
});
