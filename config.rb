http_path       = "/"
css_dir         = "css"
sass_dir        = "scss"
images_dir      = "/"
font_dir        = "fonts"
javascripts_dir = "js"
# output_style    = (environment == :production) ? :compressed : :expanded
# environment     = :development
output_style    = :compressed
# line_comments   = false
sourcemap       = true
relative_assets = true
# sass_options  = { :debug_info => false }
